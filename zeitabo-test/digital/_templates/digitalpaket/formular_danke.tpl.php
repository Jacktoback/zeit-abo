<div class="container_2">
	<div class="grid_1 gradient_h436">
        <div class="box">
            <h2>Vielen Dank für Ihre Bestellung.</h2>
            
            <p class="bodytext">
                <br>Sie erhalten umgehend eine E-Mail an die von Ihnen angegebene E-Mail-Adresse. Bitte klicken Sie auf den enthaltenen Bestätigungslink.
                
                <br><br>Sie können sich nun jederzeit unter <a href="http://www.zeit.de/digitalpaket" target="_blank">www.zeit.de/digitalpaket</a> im rechten Bereich mit Ihren Zugangsdaten einloggen, um Ihre abonnierten E-Paper, Audios oder E-Reader-Formate herunterzuladen.
                
                <br><br>Für die Freischaltung der App-Ausgaben, geben Sie nach dem Download innerhalb der jeweiligen App Ihre Benutzerdaten für das Digital-Paket ein.
                
                <br><br>Antworten auf weitere Fragen und Kontakt zum Kundensupport finden Sie hier <a href="https://premium.zeit.de/hilfe" target="_blank">https://premium.zeit.de/hilfe</a><br><br>Wir wünschen Ihnen viel Vergnügen mit unseren digitalen Angeboten!
            </p>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.grid_1 -->
    
    
    <div class="grid_1 gradient_h436">
        <a id="teaser_danke" href="http://shop.zeit.de/category/2084-E-Books?et=l6VVNm&et_cid=11&et_lid=20&et_sub=ZOL-Abo" target="_blank"><img src="<?= $this->directory ?>img/digitalpaket/danke_ebooks.png?<?= $this->version ?>" width="437" height="476" border="0" alt=""></a>
    </div>
    <!-- /.grid_1 -->
    
    
</div>
<!-- /.container_2 -->