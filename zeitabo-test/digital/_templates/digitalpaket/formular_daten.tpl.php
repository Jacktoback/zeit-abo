<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>
<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 1))) ?>">
	<input type="hidden" name="bestellen" value="1" /> 

    <div class="container_2">
        <div class="color grid_2">
            <div class="box">
                <h2>ZEIT Digital-Paket</h2>
                <div class="countries country_de">
                    <p class="bodytext">Abonnieren Sie das Digital-Paket der ZEIT für nur <strong>4,10&nbsp;€</strong><sup>1</sup> pro Ausgabe. Abonnenten der gedruckten ZEIT erhalten das Digital-Paket zum Vorteilspreis für nur <strong>0,60&nbsp;€</strong><sup>2</sup> pro Ausgabe.<br /><br />

Das Digital-Paket ist jederzeit mit einer Frist von zwei Wochen kündbar.</p>
                    <p class="bodytext" style="margin-top: 30px;"><sup>1</sup> Preis inklusive 3,50&nbsp;€ für das ZEIT E-Paper<br />
<sup>2</sup> Preis inklusive 0,50&nbsp;€ für das ZEIT E-Paper</p>
                </div>
               
                
                <div class="countries country_ch">
                    <p class="bodytext">Abonnieren Sie das Digital-Paket der ZEIT für nur <strong>SFr.&nbsp;4,50</strong><sup>1</sup> pro Ausgabe. Abonnenten der gedruckten ZEIT erhalten das Digital-Paket zum Vorteilspreis für nur <strong>SFr.&nbsp;0,70</strong><sup>2</sup> pro Ausgabe.

Das Digital-Paket ist jederzeit mit einer Frist von zwei Wochen kündbar.</p>
                    <p class="bodytext" style="margin-top: 30px;"><sup>1</sup> Preis inklusive SFr.&nbsp;4,00 für das ZEIT E-Paper<br />
<sup>2</sup> Preis inklusive SFr.&nbsp;0,52 für das ZEIT E-Paper</p>
                </div>
                <div class="countries country_ch_student"></div>
            </div>
        </div>
    </div>
      	
	<? if($error_text): ?>
    	<div class="container_2" id="errorMessages">
            <div class="color redbox grid_2">
                <div class="box" id="errorBox"><?= $error_text ?></div>
            </div>
        </div>
    <? endif ?>
      
    <div class="container_2">
        <div class="grid_1 gradient_h436">
            <div class="box">
                <h2>Kundendaten</h2>
                
                <fieldset class="<?= isset($errors['anrede']) ? 'error' : '' ?>">
                    <label>Anrede<span class="mandatory">*</span></label>
                    <input type="radio" name="anrede" value="Herr" <?= $form->anrede == 'Herr' ? 'checked="checked"' :'' ?> /> Herr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="anrede" value="Frau" <?= $form->anrede == 'Frau' ? 'checked="checked"' :'' ?> /> Frau&nbsp;&nbsp;&nbsp;
                </fieldset>
    
                <fieldset class="<?= (isset($errors['vorname']) || isset($errors['name'])) ? 'error' : '' ?>">
                    <label>Vor- und Nachname<span class="mandatory">*</span></label>
                    <input type="text" class="text half" name="vorname" value="<?= $this->escape($form->vorname) ?>" />
					<input type="text" class="text half" name="name" value="<?= $this->escape($form->name) ?>" />
                </fieldset>
    
                <fieldset class="<?= isset($errors['strasse']) ? 'error' : '' ?>">
                    <label>Straße und Hausnr.<span class="mandatory">*</span></label>
                    <input type="text" class="text full" name="strasse" value="<?= $this->escape($form->strasse) ?>" />
                </fieldset>
    
                <fieldset class="<?= isset($errors['adresszusatz']) ? 'error' : '' ?>">
                    <label>Adresszusatz</label>
                    <input type="text" class="text full" name="adresszusatz" value="<?= $this->escape($form->adresszusatz) ?>" />
                </fieldset>
    
                <fieldset class="<?= (isset($errors['plz']) || isset($errors['ort'])) ? 'error' : '' ?>">
                    <label>PLZ und Ort<span class="mandatory">*</span></label>
                    <input type="text" class="text small" name="plz" value="<?= $this->escape($form->plz) ?>" />
                    <input type="text" class="text medium" name="ort"  value="<?= $this->escape($form->ort) ?>" />
                </fieldset>
    
                <fieldset class="<?= isset($errors['land']) ? 'error' : '' ?>">
                    <label>Land</label>
                    <select class="full" id="country" name="land">
                        <optgroup label="Bitte treffen Sie eine Auswahl">
                            <option  id="country_de" value="DE" <?= !$form->land || ($form->land == 'DE') ? 'selected="selected"' :'' ?>>Deutschland</option>
                            <option  id="country_at" value="AT" <?= $form->land == 'AT' ? 'selected="selected"' :'' ?>>Österreich</option>
                            <option  id="country_ch" value="CH" <?= $form->land == 'CH' ? 'selected="selected"' :'' ?>>Schweiz</option>
                        </optgroup>
                        <optgroup label="-----------------">
                        	<? foreach($this->countries as $k => $v): ?>
                            	<option value="<?= $k ?>" <?= $form->land == $k ? 'selected="selected"' :'' ?> ><?= $v ?></option>
                            <? endforeach ?>
                        </optgroup>
                    </select>
                </fieldset>
    
    			
    
                <fieldset class="<?= isset($errors['telefon']) ? 'error' : '' ?>">
                    <label>Telefon</label>
                    <input type="text" class="text full" name="telefon" value="<?= $this->escape($form->telefon) ?>" />
                    <span class="hint_telefon">Notwendig zur Nutzung des ZEIT Audio<br/>Mobilservice über das Mobil-Telefon</span>
                </fieldset>
    
                <fieldset class="<?= isset($errors['abo_type']) ? 'error' : '' ?>">
                    <label>Sind Sie bereits <br />Abonnent der ZEIT<span class="mandatory">*</span></label>
                    <div id="newclient">
						<label><input type="radio" name="abo_type" value="upgrade" <?= $form->abo_type == 'upgrade' ? 'checked="checked"' :'' ?> /> <strong>Ja</strong>, ich bin bereits Abonnent.</label>
						<label><input type="radio" name="abo_type" value="digital" <?= $form->abo_type == 'digital' ? 'checked="checked"' :'' ?> /> <strong>Nein</strong>, ich bin Neukunde.</label>
                         <div style="clear:both;"></div>
                    </div>              
                </fieldset>
                
                <div style="clear:both;"></div>
                <p class="mandatory"><span class="mandatory">*</span> Pflichtfelder</p>
			</div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
        
        <div class="grid_1 gradient_h436">
            <div class="box">
                <h2>ZEIT Digital-Paket</h2>
                <img src="<?= $this->directory ?>img/digitalpaket/landingpage.png?<?= $this->version ?>" width="466" height="578" border="0" alt="" />
            </div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
        
    </div>
    <!-- /.container_2 -->


	<div class="container_2">
        <div class="grid_2 gradient_h75">
            <a class="btn_zurueck" href="<?= $this->uri ?>">Zurück</a>
            <input class="btn_weiter btn2" type="submit" value="Weiter" />
        </div>
    </div>
    <!-- /.container_2 -->
  
</form>


        
<script type="text/javascript">
	$(document).ready( function() {
		$('#country').change(toggleCountries).trigger('change').blur(toggleCountries);
		  
		return;
		$('input[type="radio"]').change(function(){
			var obj = $(this);
			validateInputRadio(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
		
		
		$('input[type="text"]').change(function(){
			var obj = $(this);
			validateInputText(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
		
		
		$('select').change(function(){
			var obj = $(this);
			validateInputSelect(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
	});
	
</script>