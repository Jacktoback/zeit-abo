<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>
<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 2))) ?>">
	<input type="hidden" name="bestellen" value="2" /> 

	<? if($error_text): ?>
    	<div class="container_2" id="errorMessages">
            <div class="color redbox grid_2">
                <div class="box" id="errorBox"><?= $error_text ?></div>
            </div>
        </div>
    <? endif ?>

    <div class="container_2">
        <div class="grid_1 gradient_h436">
            <div class="box">
				<h2>Zahlungs-Informationen</h2>
				
				<fieldset class="<?= isset($errors['zahlart']) && ($form->zahlart == 'bankeinzug') ? 'error' : '' ?>">
					<label><input type="radio" name="zahlart" class="zahlart" value="bankeinzug" id="bankeinzug" <?= (!$form->zahlart ||($form->zahlart == 'bankeinzug')) ? 'checked="checked"' :'' ?>> <span class="bigradiotitle">Ich bezahle per Bankeinzug</span></label>
				</fieldset>
                <div style="clear:both;"></div>
                 
				<div id="bankEinzugBox">
					<fieldset class="<?= isset($errors['kto_inh']) ? 'error' : '' ?>">
						<label>Kontoinhaber<span class="mandatory">*</span></label>
                    	<input type="text" class="text full" name="kto_inh" value="<?= $this->escape($form->kto_inh) ?>" />
					</fieldset>
            
					<fieldset class="<?= isset($errors['kto_nr']) ? 'error' : '' ?>">
						<label>Kontonummer<span class="mandatory">*</span></label>
						<input type="text" class="text full" name="kto_nr" value="<?= $this->escape($form->kto_nr) ?>" />
					</fieldset>
            
					<fieldset class="<?= isset($errors['kto_blz']) ? 'error' : '' ?>">
						<label>BLZ<span class="mandatory">*</span></label>
						<input type="text" class="text full" name="kto_blz" value="<?= $this->escape($form->kto_blz) ?>" id="kontoBLZ" />
					</fieldset>
            
					<fieldset class="<?= isset($errors['kto_bank']) ? 'error' : '' ?>">
						<label>Kreditinstitut</label>
						<input type="text" class="text full" name="kto_bank" value="<?= $this->escape($form->kto_bank) ?>" id="kontoInstitut">
					</fieldset>
                    
                    <div style="clear:both;"></div>
				</div>
                <!-- /#bankEinzugBox -->
                
				<fieldset class="<?= isset($errors['zahlart']) && ($form->zahlart == 'kreditkarte') ? 'error' : '' ?>">
                    <label><input type="radio" name="zahlart" class="zahlart" value="kreditkarte" id="kreditkarte" <?= ($form->zahlart == 'kreditkarte') ? 'checked="checked"' :'' ?>> <span class="bigradiotitle">Ich bezahle per Kreditkarte</span></label>
				</fieldset>
                <div style="clear:both;"></div>
                  
				<div id="kreditKartenBox">
					<fieldset class="<?= isset($errors['cc_type']) ? 'error' : '' ?>">
						<label>Kreditkarten-Typ<span class="mandatory">*</span></label>
						<select class="text half" name="cc_type">
                            <option <?= !$form->cc_type ? 'selected="selected"' :'' ?> value="">Bitte wählen</option>
                            <option <?= ($form->cc_type == 'MasterCard') ? 'selected="selected"' :'' ?> value="MasterCard">Euro-/Mastercard</option>
							<option <?= ($form->cc_type == 'Visa') ? 'selected="selected"' :'' ?> value="Visa">VISA</option>
							<option <?= ($form->cc_type == 'DinersClub') ? 'selected="selected"' :'' ?> value="DinersClub">Diners-Club</option>
						</select>
					</fieldset>
                      
					<fieldset class="<?= isset($errors['cc_inh']) ? 'error' : '' ?>">
						<label>Kreditkarten-Inhaber<span class="mandatory">*</span></label>
						<input type="text" class="text full" name="cc_inh" value="<?= $this->escape($form->cc_inh) ?>" />
					</fieldset>
                
					<fieldset class="<?= isset($errors['cc_nr']) ? 'error' : '' ?>">
                        <label>Kreditkartennummer<span class="mandatory">*</span></label>
                        <input type="text" class="text full" name="cc_nr" value="<?= $this->escape($form->cc_nr) ?>" />
					</fieldset>
                      
					<fieldset class="<?= (isset($errors['cc_monat']) || isset($errors['cc_jahr'])) ? 'error' : '' ?>">
                        <label>Gültig bis<span class="mandatory">*</span></label>
						<select class="text half" name="cc_monat">
                        	<option <?= !$form->cc_monat ? 'selected="selected"' :'' ?> value="">Monat</option>
                            <option <?= ($form->cc_monat == '01') ? 'selected="selected"' :'' ?> value="01">01</option>
                            <option <?= ($form->cc_monat == '02') ? 'selected="selected"' :'' ?> value="02">02</option>
                            <option <?= ($form->cc_monat == '03') ? 'selected="selected"' :'' ?> value="03">03</option>
                            <option <?= ($form->cc_monat == '04') ? 'selected="selected"' :'' ?> value="04">04</option>
                            <option <?= ($form->cc_monat == '05') ? 'selected="selected"' :'' ?> value="05">05</option>
                            <option <?= ($form->cc_monat == '06') ? 'selected="selected"' :'' ?> value="06">06</option>
                            <option <?= ($form->cc_monat == '07') ? 'selected="selected"' :'' ?> value="07">07</option>
                            <option <?= ($form->cc_monat == '08') ? 'selected="selected"' :'' ?> value="08">08</option>
                            <option <?= ($form->cc_monat == '09') ? 'selected="selected"' :'' ?> value="09">09</option>
                            <option <?= ($form->cc_monat == '10') ? 'selected="selected"' :'' ?> value="10">10</option>
                            <option <?= ($form->cc_monat == '11') ? 'selected="selected"' :'' ?> value="11">11</option>
                            <option <?= ($form->cc_monat == '12') ? 'selected="selected"' :'' ?> value="12">12</option>
                        </select>
					
                        <select class="text half" name="cc_jahr">
                            <option value="" <?= !$form->cc_jahr ? 'selected="selected"' :'' ?>>Jahr</option>
                            <? $startdate = date('Y'); ?>
                            <? $enddate = $startdate+10 ?>
                            
							<? for($i = $startdate; $i <= $enddate; $i++): ?>
                                <option <?= ($form->cc_jahr == $i) ? 'selected="selected"' : '' ?> value="<?= $i ?>"><?= $i ?></option>
							<? endfor ?>
							</select>
					</fieldset>
                
					<fieldset class="<?= isset($errors['cc_check']) ? 'error' : '' ?>">
						<label>Kartenprüfziffer<span class="mandatory">*</span></label>
						<input type="text" class="text full" name="cc_check" value="<?= $this->escape($form->cc_check) ?>" id="kreditkartenPruefziffer" />
                        <span class="hint_pruefziffer">Zumeist dreistellige Nummer auf der <br>Rückseite der Kreditkarte</span>
                        
					</fieldset>
				
                	<div style="clear:both;"></div>
				
                </div>
                <!-- /#kreditKartenBox -->
                
                
			</div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
        
		<div class="grid_1 gradient_h436">
			<div class="box">
        	
            	<? if($form->abo_type == 'upgrade'): ?>
                    <div id="zeit_abonnent">    
                        <h3>Ihre ZEIT Abonnentennummer</h3>
                        <fieldset class="<?= isset($errors['abo_plz']) ? 'error' : '' ?>">
                            <label>Postleitzahl<span class="mandatory">*</span></label>
                            <input type="text" class="text full" name="abo_plz" value="<?= $this->escape($form->abo_plz) ?>" />
                        </fieldset>
                    
                        <fieldset class="<?= isset($errors['abo_nr']) ? 'error' : '' ?>">
                            <label>Abonummer<span class="mandatory">*</span></label>
                            <input type="text" class="text full" name="abo_nr" value="<?= $this->escape($form->abo_nr) ?>" />
                        </fieldset>
                        
                        <div style="clear:both;"></div>
                    </div>
                    <!-- /#zeit_abonnent -->
             	<? endif ?>
                
           
				<div id="newclient">
            		<h3>Zugangsdaten für das Digital-Abo</h3>
                	
					<fieldset class="<?= isset($errors['zd_username']) ? 'error' : '' ?>">
						<label>Gewünschter <br />Benutzername<span class="mandatory">*</span></label>
						<input type="text" class="text full" name="zd_username" value="<?= $this->escape($form->zd_username) ?>" />
					</fieldset>
                
					<fieldset class="<?= isset($errors['zd_email']) ? 'error' : '' ?>">
						<label>E-Mail Adresse<span class="mandatory">*</span></label>
						<input type="text" class="text full" name="zd_email" value="<?= $this->escape($form->zd_email) ?>" />
					</fieldset>
                
					<fieldset class="<?= isset($errors['zd_email2']) ? 'error' : '' ?>">
						<label>Bestätigen Sie Ihre <br>E-Mail Adresse<span class="mandatory">*</span></label>
                    	<input type="text" class="text full" name="zd_email2" value="<?= $this->escape($form->zd_email2) ?>" />
					</fieldset>
                    
                    <div style="clear:both;"></div>
				</div>
          		<!-- /#newclient -->
                
        		<div class="form_legals">
                    <fieldset class="<?= isset($errors['optin_agb']) ? 'error' : '' ?>"> 
                        <label>
                        	<input type="checkbox" name="optin_agb" value="ja" <?= $form->optin_agb == 'ja' ? 'checked="checked"' :'' ?> />
                        	<strong>Ja</strong>, ich stimme den <a href="http://www.zeit.de/administratives/agb-kommentare-artikel" target="_blank" onclick="return popup(this.href, 850, 600)">AGB</a> zu.<span class="mandatory">*</span>
                   		</label>
                    </fieldset>
                </div>
                
                <div style="clear:both;"></div>
                
                <div class="form_legals">
                    <fieldset class="<?= isset($errors['optin_marketing']) ? 'error' : '' ?>">
                        <input type="checkbox" name="optin_marketing" value="ja" id="callback" <?= $form->optin_marketing == 'ja' ? 'checked="checked"' :'' ?> />
                        <strong>Ja</strong>, ich möchte von weiteren Vorteilen profitieren. Ich bin daher einverstanden, dass mich DIE ZEIT per Post, Telefon oder E-Mail über interessante Medien-Angebote und kostenlose Veranstaltungen informiert.
                    </fieldset>
               	</div>
                
          		
                <div style="clear:both;"></div>
          		<p class="mandatory hintmandatory"><span class="mandatory hintmandatory">*</span> Pflichtfelder</p>

            </div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
	</div>
    <!-- /.container_2 -->


	<div class="container_2">
        <div class="grid_2 gradient_h75">
            <a class="btn_zurueck" href="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 1))) ?>">Zurück</a>
            <input class="btn_bestellen btn2" type="submit" value="Jetzt Kaufen" />
        </div>
    </div>
    <!-- /.container_2 -->
  
</form>


<script type="text/javascript">
	$(document).ready( function() {
		$('.zahlart').change(function(){
			var obj = $('.zahlart:checked');
			if(obj.attr('id') == 'bankeinzug'){
				$('#bankEinzugBox').removeClass('fade')
				$('#kreditKartenBox').addClass('fade')
			} else {
				$('#bankEinzugBox').addClass('fade')
				$('#kreditKartenBox').removeClass('fade')
			}
		}).change();
		
		return;
		
		$('input[type="radio"]').change(function(){
			var obj = $(this);
			if(obj.hasClass('zahlart')){
				return;	
			}
			validateInputRadio(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
		
		$('input[type="checkbox"]').change(function(){
			var obj = $(this);
			validateInputRadio(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
		
		
		$('input[type="text"]').change(function(){
			var obj = $(this);
			validateInputText(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
		
		
		$('select').change(function(){
			var obj = $(this);
			validateInputSelect(obj);
		}).each(function(){
			var obj = $(this);
			if(!obj.closest('fieldset').hasClass('error')){
				obj.change();
			}
		});
		
	});
	
</script>