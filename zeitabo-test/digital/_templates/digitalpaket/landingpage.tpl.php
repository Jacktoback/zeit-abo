<div class="container_4">
    <div class="grid_2 gradient_h578">
        <div class="box">
            <h2>Das Digital-Paket der ZEIT</h2>
            <h4></h4>
            <img src="<?= $this->directory ?>img/digitalpaket/landingpage.png?<?= $this->version ?>" width="466" height="578" border="0" alt="" />
        </div>
    </div>

    
    <div class="grid_2 gradient_h578">
        <div class="box cf">
            <h3>Entdecken Sie DIE ZEIT neu:<br />Auf Ihrem Tablet, Smartphone oder E-Reader!</h3>
            <p class="bodytext">
            <br />Lesen Sie DIE ZEIT im Digital-Paket, und sichern Sie sich viele&nbsp;Vorteile:</p>
            
            <ul>
            	<li>Optimal aufbereitet für Ihren digitalen Lesegenuss.</li>
				<li>Bereits am Mittwochabend verfügbar – einen Tag früher.</li>
				<li>Immer und überall dabei.</li>
				<li>Mit Audio-Beiträgen, Videos und Bilderstrecken.</li>
				<li>Zusätzlich als E-Paper im praktischen PDF-Format.</li>
			</ul>
            
            <p class="bodytext">&nbsp;</p>
            
    		<p class="bodytext">Jetzt bestellen und sofort genießen.</p>
            
    		<div class="btn_section" style="margin-top: 20px;"><a class="btn_weiter left" href="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 1))) ?>" id="selected_btn">weiter</a></div>
        </div>
    </div>
    
</div>