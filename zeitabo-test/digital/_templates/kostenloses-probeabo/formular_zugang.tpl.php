<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>

<h2 class="balken">Für Tablet, Smartphone und E-Reader!</h2>

<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 2))) ?>#angaben">
	<input type="hidden" name="abo_type" value="digital" />
    <input type="hidden" name="bestellen" value="2" /> 

	<div id="teaser">
        <h2>Ja, ich möchte die digitale ZEIT 4 Wochen gratis testen.</h2>
        <div class="countries country_de">
            <p>Ja, ich möchte die digitale ZEIT inkl. ZEITmagazin auf meinem Tablet, Smartphone oder E-Reader 4&nbsp;Wochen lang gratis testen. Wenn ich danach weiterlese, erhalte ich die digitale ZEIT für zzT. nur 4,10&nbsp;€ pro Ausgabe (inkl. 19%&nbsp;MwSt. und 3,50&nbsp;€ für das ZEIT E-Paper) statt 3,99&nbsp;€ im Einzelverkauf und kann jederzeit kündigen.</p>
        </div>
        
        <div class="countries country_ch" style="display:none;">
            <p>Ja, ich möchte die digitale ZEIT inkl. ZEITmagazin auf meinem Tablet, Smartphone oder E-Reader 4&nbsp;Wochen lang gratis testen. Wenn ich danach weiterlese, erhalte ich die digitale ZEIT für zzT. nur SFr.&nbsp;4,50 Ausgabe (inkl. SFr.&nbsp;4,– für das ZEIT E-Paper) und kann jederzeit kündigen.</p>
        </div>
    </div>
    
    <h2 class="gap">Ihre Vorteile:</h2>
    <ul>
    	<li>Testen Sie 4x die <strong>digitale ZEIT gratis</strong></li>
		<li>In Ihrem <strong>Lieblingsformat</strong> - als E-Paper, App, Audio und für E-Reader</li>
		<li>Mit Videos, Hörbeiträgen und exklusiven Bilderstrecken</li>
	</ul>
    

	<h2 class="gap" id="angaben">Meine persönlichen Angaben:</h2>
    
    
    <? if($error_text): ?>
    	<p class="error"><?= $error_text ?></p>
    <? endif ?>
    
    <fieldset class="<?= isset($errors['anrede']) ? 'error' : '' ?>">
        <label>Anrede</label>
    	<label class="radio"><input type="radio" name="anrede" value="Frau" <?= $form->anrede == 'Frau' ? 'checked="checked"' :'' ?> /> Frau</label>
		<label class="radio"><input type="radio" name="anrede" value="Herr" <?= $form->anrede == 'Herr' ? 'checked="checked"' :'' ?> /> Herr</label>
    </fieldset>
    
    <fieldset class="<?= (isset($errors['vorname']) || isset($errors['name'])) ? 'error' : '' ?>">
        <label>Vor-/Nachname</label>
        <input type="text" class="text half" name="vorname" value="<?= $this->escape($form->vorname) ?>" />
        <input type="text" class="text half" name="name" value="<?= $this->escape($form->name) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['strasse']) ? 'error' : '' ?>">
        <label>Straße/Nr.</label>
        <input type="text" class="text full" name="strasse" value="<?= $this->escape($form->strasse) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['adresszusatz']) ? 'error' : '' ?>">
        <label>Adresszusatz</label>
        <input type="text" class="text full" name="adresszusatz" value="<?= $this->escape($form->adresszusatz) ?>" />
    </fieldset>
    
    <fieldset class="<?= (isset($errors['plz']) || isset($errors['ort'])) ? 'error' : '' ?>">
        <label>PLZ/Ort</label>
        <input type="text" class="text small" name="plz" value="<?= $this->escape($form->plz) ?>" />
        <input type="text" class="text medium" name="ort"  value="<?= $this->escape($form->ort) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['land']) ? 'error' : '' ?>">
        <label>Land</label>
        <select class="full" id="country" name="land">
            <optgroup label="Bitte treffen Sie eine Auswahl">
                <option  id="country_de" value="DE" <?= !$form->land || ($form->land == 'DE') ? 'selected="selected"' :'' ?>>Deutschland</option>
                <option  id="country_at" value="AT" <?= $form->land == 'AT' ? 'selected="selected"' :'' ?>>Österreich</option>
                <option  id="country_ch" value="CH" <?= $form->land == 'CH' ? 'selected="selected"' :'' ?>>Schweiz</option>
            </optgroup>
            <optgroup label="-----------------">
                <? foreach($this->countries as $k => $v): ?>
                    <option value="<?= $k ?>" <?= $form->land == $k ? 'selected="selected"' :'' ?> ><?= $v ?></option>
                <? endforeach ?>
            </optgroup>
        </select>
    </fieldset>
    
    <? if(isset($errors['zd_username']) || ($form->zd_username != $form->zd_email)):?>
        <fieldset class="<?= isset($errors['zd_username']) ? 'error' : '' ?>">
            <label>Gewünschter <br />Benutzername</label>
            <input type="text" class="text full" name="zd_username" value="<?= $this->escape($form->zd_username) ?>" />
        </fieldset>
 	<? else: ?>
    	 <input type="hidden" name="zd_username" value="<?= $this->escape($form->zd_username) ?>" />
    <? endif ?>
    
    <fieldset class="<?= isset($errors['zd_email']) ? 'error' : '' ?>">
        <label>E-Mail Adresse</label>
        <input type="text" class="text full" name="zd_email" value="<?= $this->escape($form->zd_email) ?>" />
        
        <? if(!$form->zd_username): ?>
			<small>(Dient als Benutzername)</small>
    	<? endif ?>
    </fieldset>
    
    
    
	<div style="clear:both;"></div>

    <p class="gap">Wenn ich danach weiterlesen möchte, bevorzuge ich folgenden Zahlungsweg:<br /><br /></p>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == 'rechnung') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="rechnung" id="rechnung" <?= (!$form->zahlart ||($form->zahlart == 'rechnung')) ? 'checked="checked"' :'' ?>> <span><strong>Ich bezahle per Rechnung</strong></span></label>
    </fieldset>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == 'bankeinzug') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="bankeinzug" id="bankeinzug" <?= ($form->zahlart == 'bankeinzug') ? 'checked="checked"' :'' ?>> <span><strong>Ich bezahle per Bankeinzug</strong></span></label>
    </fieldset>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == 'kreditkarte') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="kreditkarte" id="kreditkarte" <?= ($form->zahlart == 'kreditkarte') ? 'checked="checked"' :'' ?>> <span><strong>Ich bezahle per Kreditkarte</strong></span></label>
	</fieldset>
    <div style="clear:both;"></div>
     
    <div class="bankeinzug gap">
        <fieldset class="<?= isset($errors['kto_inh']) ? 'error' : '' ?>">
            <label>Kontoinhaber</label>
            <input type="text" class="text full" name="kto_inh" value="<?= $this->escape($form->kto_inh) ?>" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['kto_nr']) ? 'error' : '' ?>">
            <label>Kontonummer <br /><small>oder</small> IBAN</label>
            <input type="text" class="text full" name="kto_nr" value="<?= $this->escape($form->kto_nr) ?>" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['kto_blz']) ? 'error' : '' ?>">
            <label>Bankleitzahl <br /><small>oder</small> BIC</label>
            <input type="text" class="text full" name="kto_blz" value="<?= $this->escape($form->kto_blz) ?>" id="kontoBLZ" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['kto_bank']) ? 'error' : '' ?>">
            <label>Kreditinstitut</label>
            <input type="text" class="text full" name="kto_bank" value="<?= $this->escape($form->kto_bank) ?>" id="kontoInstitut">
        </fieldset>
        <div style="clear:both;"></div>
	</div> 
   
   
	  
	<div class="kreditkarte gap">
        <fieldset class="<?= isset($errors['cc_type']) ? 'error' : '' ?>">
            <label>Kreditkarten-Typ</label>
            <select class="text half" name="cc_type">
                <option <?= !$form->cc_type ? 'selected="selected"' :'' ?> value="">Bitte wählen</option>
                <option <?= ($form->cc_type == 'MasterCard') ? 'selected="selected"' :'' ?> value="MasterCard">Euro-/Mastercard</option>
                <option <?= ($form->cc_type == 'Visa') ? 'selected="selected"' :'' ?> value="Visa">VISA</option>
                <option <?= ($form->cc_type == 'DinersClub') ? 'selected="selected"' :'' ?> value="DinersClub">Diners-Club</option>
            </select>
        </fieldset>
          
        <fieldset class="<?= isset($errors['cc_inh']) ? 'error' : '' ?>">
            <label>Kreditkarten-Inhaber</label>
            <input type="text" class="text full" name="cc_inh" value="<?= $this->escape($form->cc_inh) ?>" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['cc_nr']) ? 'error' : '' ?>">
            <label>Kreditkartennummer</label>
            <input type="text" class="text full" name="cc_nr" value="<?= $this->escape($form->cc_nr) ?>" />
        </fieldset>
          
        <fieldset class="<?= (isset($errors['cc_monat']) || isset($errors['cc_jahr'])) ? 'error' : '' ?>">
            <label>Gültig bis</label>
            <select class="text half" name="cc_monat">
                <option <?= !$form->cc_monat ? 'selected="selected"' :'' ?> value="">Monat</option>
                <option <?= ($form->cc_monat == '01') ? 'selected="selected"' :'' ?> value="01">01</option>
                <option <?= ($form->cc_monat == '02') ? 'selected="selected"' :'' ?> value="02">02</option>
                <option <?= ($form->cc_monat == '03') ? 'selected="selected"' :'' ?> value="03">03</option>
                <option <?= ($form->cc_monat == '04') ? 'selected="selected"' :'' ?> value="04">04</option>
                <option <?= ($form->cc_monat == '05') ? 'selected="selected"' :'' ?> value="05">05</option>
                <option <?= ($form->cc_monat == '06') ? 'selected="selected"' :'' ?> value="06">06</option>
                <option <?= ($form->cc_monat == '07') ? 'selected="selected"' :'' ?> value="07">07</option>
                <option <?= ($form->cc_monat == '08') ? 'selected="selected"' :'' ?> value="08">08</option>
                <option <?= ($form->cc_monat == '09') ? 'selected="selected"' :'' ?> value="09">09</option>
                <option <?= ($form->cc_monat == '10') ? 'selected="selected"' :'' ?> value="10">10</option>
                <option <?= ($form->cc_monat == '11') ? 'selected="selected"' :'' ?> value="11">11</option>
                <option <?= ($form->cc_monat == '12') ? 'selected="selected"' :'' ?> value="12">12</option>
            </select>
        
            <select class="text half" name="cc_jahr">
                <option value="" <?= !$form->cc_jahr ? 'selected="selected"' :'' ?>>Jahr</option>
                <? $startdate = date('Y'); ?>
                <? $enddate = $startdate+10 ?>
                
                <? for($i = $startdate; $i <= $enddate; $i++): ?>
                    <option <?= ($form->cc_jahr == $i) ? 'selected="selected"' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                <? endfor ?>
                </select>
        </fieldset>
    
        <fieldset class="<?= isset($errors['cc_check']) ? 'error' : '' ?>">
            <label>Kartenprüfziffer</label>
            <input type="text" class="text full" name="cc_check" value="<?= $this->escape($form->cc_check) ?>" id="kreditkartenPruefziffer" />
            <small>Zumeist dreistellige Nummer auf der <br>Rückseite der Kreditkarte</small>
            
        </fieldset>
        <div style="clear:both;"></div>
    </div>
    
    
        
    <fieldset class="gap <?= isset($errors['optin_agb']) ? 'error' : '' ?>"> 
      <label class="checkbox">
          <input type="checkbox" name="optin_agb" value="ja" <?= $form->optin_agb == 'ja' ? 'checked="checked"' :'' ?> />
          <span><strong>Ja</strong>, ich stimme den <a href="http://www.zeit.de/administratives/agb-kommentare-artikel" target="_blank" class="popup">AGB</a> zu.</span>
      </label>
    </fieldset>
    
	<fieldset class="<?= isset($errors['optin_marketing']) ? 'error' : '' ?>">
		<label class="checkbox">
        	<input type="checkbox" name="optin_marketing" value="ja" <?= $form->optin_marketing == 'ja' ? 'checked="checked"' :'' ?> />
			<span><strong>Ja</strong>, ich möchte von weiteren Vorteilen profitieren. Ich bin daher einverstanden, dass mich DIE ZEIT per Post, Telefon oder E-Mail über interessante Medien-Angebote und kostenlose Veranstaltungen informiert.</span>
		</label>
	</fieldset>
          		
	<div style="clear:both;"></div>

	
	<input class="button gap" type="image" src="<?= $this->directory ?>img/<?= $this->page_id ?>/abschicken.png?<?= $this->version ?>" alt="Abschicken" />
  	<div class="clear"></div>
    
    <p id="hinweis"><a href="http://www.zeitabo.de/die-zeit-im-abo/digital-abo/kombi-abo.html">Sie sind bereits Abonnent der ZEIT?</a></p>

  
</form>