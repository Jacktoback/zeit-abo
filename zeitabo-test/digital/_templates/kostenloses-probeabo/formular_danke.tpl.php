<h2 class="balken"></h2>

<p>Ihr Passwort wurde generiert und Ihnen per E-Mail zugestellt. Bitte prüfen Sie Ihr Postfach und schließen Sie Ihre Bestellung durch eine Anmeldung auf <a href="http://www.zeit.de/digitalezeit">www.zeit.de/digitalezeit</a> ab. Bei Ihrer Anmeldung geben Sie bitte <strong><?= $this->username ? $this->username : 'Ihre E-Mail-Adresse' ?></strong> als Benutzernamen an.</p>

<a class="button" href="http://premium.zeit.de"><img src="<?= $this->directory ?>img/<?= $this->page_id ?>/anmelden.png?<?= $this->version ?>" alt="Hier anmelden!" /></a>
<div class="clear"></div>