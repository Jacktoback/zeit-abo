<?='<?xml version="1.0" encoding="utf-8"?>' ?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="de" lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/reset.css?<?= $this->version ?>" media="all" />
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/aboshop/screen.css?<?= $this->version ?>" media="all" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/aboshop/ie6.css?<?= $this->version ?>" media="all" /><![endif]-->
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/aboshop/jquery-ui-1.8.11.custom.css?<?= $this->version ?>" media="all" />
    
    <script src="<?= $this->directory ?>js/aboshop/jquery-1.4.4.min.js?<?= $this->version ?>" type="text/javascript"></script>
    <script src="<?= $this->directory ?>js/aboshop/jquery-ui-1.8.11.custom.min.js?<?= $this->version ?>" type="text/javascript"></script>
    
    <title>DIE ZEIT - Abonnement - <?= $this->escape($this->title) ?></title>
    <link rel="icon" href="<?= $this->directory ?>img/favicon.ico" type="image/ico" />
    <meta property="og:title" content="<?= $this->escape($this->title) ?>"/>
    <meta property="og:image" content="<?= $this->directory ?>img/facebook.jpg"/>
    <meta property="og:description" content=""/>
    
    
    <? if($this->nofollow): ?>
		<meta name="robots" content="noindex, nofollow">
    <? endif ?>
    
    <? if($this->meta_canonical): ?>
		<link rel="canonical" href="<?= $this->meta_canonical ?>" />
    <? endif ?>
    
    <script type="text/javascript">
		function popup (url, w, h) {
			fenster = window.open(url, "fenster1", "width="+w+",height="+h+",status=no,scrollbars=yes,resizable=no");
			fenster.focus();
			return false;
		}
		
		function validateInputRadio(obj){
			if(!$('input[name="'+obj.attr('name')+'"]:checked').length){
				obj.closest('fieldset').removeClass('checked');
				return;
			} 
			
			obj.closest('fieldset').removeClass('error').addClass('checked');	
		}
		
		function validateInputText(obj){
			if(obj.val().length < 2){
				obj.closest('fieldset').removeClass('checked');
				return;
			}
			
			var has_error = false;
			obj.siblings('input').each(function(){
				if($(this).val().length < 2){
					has_error = true;
					return;
				}
			});
			
			if(has_error){
				obj.closest('fieldset').removeClass('checked');
				return;
			}
			
			obj.closest('fieldset').removeClass('error').addClass('checked');	
		}
		
		function validateInputSelect(obj){
			if(obj.find(':selected').val() == ''){
				obj.closest('fieldset').removeClass('checked');
				return;
			}
			
			var has_error = false;
			obj.siblings('select').each(function(){
				if($(this).find(':selected').val() == ''){
					has_error = true;
					return;
				}
			});
			
			if(has_error){
				obj.closest('fieldset').removeClass('checked');
				return;
			}
			
			obj.closest('fieldset').removeClass('error').addClass('checked');	
		}
		
		function toggleCountries(){
			var country = $('#country option:selected').attr('id');
			if(country != 'country_ch'){
				country = 'country_de';
			}
			if(country){
				$('.countries').hide();
				$('.'+country).show();
			}
		}
		
		function toggleAddons(){
			var zugabe = $('#zugabe option:selected').val();
			if(zugabe){
				$('.zugabe').hide();
				$('.zugabe-'+zugabe).show();
			}
		}
	</script>

</head>

<body id="<?= $this->page_id.'_'.$this->content_id ?>">
<?= $this->render('pixel_tagmanager.tpl.php') ?>


<div id="header" class="container_4 append">
	<div class="grid_3 first">
		<div id="logo"><a href="/">ZEIT ABO-SHOP</a></div>
	</div>
</div>


<div class="container_4" id="menu">
	<? if($this->show_menu): ?>
    	<div id="main_menu" class="grid_4 first">
        	<ul>
            	<li class="active"><a href="/die-zeit-im-abo.html">DIE ZEIT im Abo</a></li>
                <li><a href="/magazin-angebote.html">Magazin-Angebote</a></li>
                <li><a href="/zeit-leser-service.html">ZEIT Leser-Service</a></li>
			</ul>
		</div>
        <!--/#main_menu -->
    
    	<div id="sub_menu" class="grid_4 first">
        	<ul>
                <li><a href="/die-zeit-im-abo/vorteils-abo.html">Vorteils-Abo</a></li>
                <li><a href="/die-zeit-im-abo/probe-abo.html">Probe-Abo</a></li>
                <li><a href="/die-zeit-im-abo/studenten-abo.html">Studenten-Abo</a></li>
                <li><a href="/die-zeit-im-abo/praemien-abo.html">Prämien-Abo</a></li>
                <li><a href="/die-zeit-im-abo/geschenk-abo.html">Geschenk-Abo</a></li>
                <li class="active"><a href="/die-zeit-im-abo/digital-abo.html">Digital-Abo</a></li>
                <li><a href="/die-zeit-im-abo/auslands-abo.html">Auslands-Abo</a></li>
            </ul>
		</div>
    
    	
        <div id="tabs" class="container_4">
        	<div class="grid_4">
            	<ul>
                	<? if(in_array($this->page_id, array('digitalpaket'))): ?>
                    	<li class="active"><a href="digital-paket.html<?= $this->req_params ? '?'.http_build_query($this->req_params) : '' ?>"><span>Digital-Paket</span></a></li>
                    <? else: ?>
                		<li><a href="/die-zeit-im-abo/digital-abo/digital-paket.html"><span>Digital-Paket</span></a></li>
                    <? endif ?>
                    
                    
                    
                    <li><a href="/die-zeit-im-abo/digital-abo/ipad-e-reader-paket.html"><span>iPad-/E-Reader-Paket</span></a></li>
                    
                    <? if(in_array($this->page_id, array('probeabo'))): ?>
						<li class="active"><a href="probe-abo.html<?= $this->req_params ? '?'.http_build_query($this->req_params) : '' ?>"><span>Probe-Abo</span></a></li>
                    <? else: ?>
                    	<li><a href="/die-zeit-im-abo/digital-abo/probe-abo.html"><span>Probe-Abo</span></a></li>
                    <? endif ?>
                    
                    <li><a href="/die-zeit-im-abo/digital-abo/kombi-abo.html"><span>Kombi-Abo</span></a></li>
                    <li><a href="/die-zeit-im-abo/digital-abo/studenten-abo.html"><span>Studenten-Abo</span></a></li>
                    
                    <li><a href="/die-zeit-im-abo/digital-abo/e-paper.html"><span>E-Paper</span></a></li>
                    <li><a href="/die-zeit-im-abo/digital-abo/ueber-die-digitale-zeit.html"><span>Über die digitale ZEIT</span></a></li>
				</ul>
			</div>
		</div>
        <!-- /#tabs -->
		
        
	<? else: ?>
    	<div class="grid_4 first" id="order_status"><span><?= $this->escape($this->abo_title) ?> bestellen</span></div>
    <? endif ?>
</div>
<!-- /#menu -->




<div id="mainContentWrap">
	<? if($this->cookie_error): ?>
		<div class="container_2" id="errorMessages">
			<div class="color redbox grid_2">
				<div class="box" id="errorBox">Bitte aktivieren Sie Cookies in Ihrem Browser!</div>
			</div>
		</div>
	<? endif ?>
	<?= $this->render($this->page_id.'/'.$this->content_id.'.tpl.php') ?>
</div>
<!-- /#mainContentWrap -->


<div id="footer">
	<div class="container_4">
		<div class="grid_4">
        	<img src="<?= $this->directory ?>img/footer_logo.png?v2" width="288" height="22" border="0" id="footer_logo" alt="" />
            
            <ul id="footer_contact">
            	<li class="last">ZEIT Aboservice: <strong>040 / 42 23 70 70</strong></li>
			</ul>
			
            <ul id="footer_legals">
            	<li><a href="/agb.html" target="_blank" onclick="return popup(this.href, 850, 600)" >AGB</a></li>
                <li><a href="/impressum.html" target="_blank" onclick="return popup(this.href, 850, 600)">Impressum</a></li>
				<lI><a href="https://leserservice.zeit.de/static/datenschutz.html" target="_blank" onclick="return popup(this.href, 850, 600)">Datenschutz</a></li>
                <li class="last"><a href="/zeit-leser-service/kontakte.html">Kontakt</a></li>
			</ul>
		</div>
	</div>
</div>
<!-- /#footer -->


<?= $this->render('pixel_webtrekk.tpl.php') ?>
<?= $this->render('pixel_ivw.tpl.php') ?>
<?= $this->render('pixel_analytics.tpl.php') ?>


<? if($this->is_dev_server): ?>
<hr />
<strong>DEBUG FOR DIR: <?= $this->directory ?></strong>
<pre>
	Aktionsnummer: <?= var_dump($this->aktionsnummer); ?>
	<hr />
	<? var_dump($_SERVER) ?>
    <? var_dump($_REQUEST) ?>
    <hr />
    FORM ERRROR:
    <? var_dump($this->errors) ?>
</pre>

<? else: ?>

<!-- <?= var_dump($this->aktionsnummer); ?> -->

<? endif ?>

</body>
</html>