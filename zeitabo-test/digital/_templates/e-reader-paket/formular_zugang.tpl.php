<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>

<h2 class="balken">Die digitale ZEIT + gratis E-Reader!</h2>

<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 2))) ?>#angaben">
	<input type="hidden" name="abo_type" value="digital" />
    <input type="hidden" name="bestellen" value="2" /> 
    <input type="hidden" id="zugabe" name="zugabe" value="1129904" />
    
    <div id="zugaben">
        <h2 class="large">Wählen Sie Ihr Wunschgerät:</h2>
        
        <fieldset class="<?= isset($errors['zugabe']) ? 'error' : '' ?>">
            <div class="addon addon_checked" id="1129904">
            	<img src="<?= $this->directory ?>img/<?= $this->page_id ?>/addon_kindle.jpg" alt="Kindle" />
                <h2>Kindle</h2>
                <p>Geringes Gewicht, Touch-Display sowie ein schneller 1-GHz-Prozessor.<br />Zusätzlich: integriertes WLAN und 4 GB-Speicher für Tausende Bücher</p>
			</div>
            
            <div class="addon addon_last" id="1125731">
				<img src="<?= $this->directory ?>img/<?= $this->page_id ?>/addon_tolino.jpg" alt="Tolino shine" />
				<h2>Tolino Shine</h2>
                <p>Integrierte Beleuchtung, Touch-Display, offenes System und vorinstallierte Gratis-E-Books. Zusätzlich: integriertes WLAN und 2 GB-Speicher</p>
            </div>
        </fieldset>
        
       
   </div> 
   
	<script type="text/javascript">
		$(document).ready(function(){
			$('.addon').click(function(){
				var $el = $(this);
				$('.addon').removeClass('addon_checked');
				$el.addClass('addon_checked');
				$('#zugabe').val($el.attr('id'));
			});
			
			<? if($form->zugabe): ?>
				$('#<?= $this->escape($form->zugabe) ?>').click();
			<? endif ?>
		});
	</script>
   
	<!-- /#zugaben -->
    
    

	<div id="teaser">
        <? /*<h2>Ja, ich möchte die digitale ZEIT 4 Wochen gratis testen.</h2>*/?>
        <div class="countries country_de">
            <p>Ja, ich möchte ein Jahr die digitale ZEIT für 4,10 € pro Ausgabe (213,20 € pro Jahr inkl. 19% MwSt.) statt 3,99 € im Einzelverkauf lesen und kann nach einem Jahr jederzeit kündigen. Gratis zu meinem Abonnement erhalte ich den E-Reader meiner Wahl für 0,– € Zuzahlung. Versand direkt nach Zahlungseingang.</p>
        </div>
        
        <div class="countries country_ch" style="display:none;">
            <p>Ja, ich möchte ein Jahr die digitale ZEIT für SFr. 4,50 pro Ausgabe (SFr. 234,– pro Jahr inkl. MwSt.) statt SFr. 4,80 im Einzelverkauf lesen und kann nach einem Jahr jederzeit kündigen. Gratis zu meinem Abonnement erhalte ich den E-Reader meiner Wahl für SFr. 0,– Zuzahlung. Versand direkt nach Zahlungseingang.</p>
        </div>
        
        
        
    </div>


	<h2 class="gap" id="angaben">Meine persönlichen Angaben:</h2>
    
    
    <? if($error_text): ?>
    	<p class="error"><?= $error_text ?></p>
    <? endif ?>
    
    <fieldset class="<?= isset($errors['anrede']) ? 'error' : '' ?>">
        <label>Anrede</label>
    	<label class="radio"><input type="radio" name="anrede" value="Frau" <?= $form->anrede == 'Frau' ? 'checked="checked"' :'' ?> /> Frau</label>
		<label class="radio"><input type="radio" name="anrede" value="Herr" <?= $form->anrede == 'Herr' ? 'checked="checked"' :'' ?> /> Herr</label>
    </fieldset>
    
    <fieldset class="<?= (isset($errors['vorname']) || isset($errors['name'])) ? 'error' : '' ?>">
        <label>Vor-/Nachname</label>
        <input type="text" class="text half" name="vorname" value="<?= $this->escape($form->vorname) ?>" />
        <input type="text" class="text half" name="name" value="<?= $this->escape($form->name) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['strasse']) ? 'error' : '' ?>">
        <label>Straße/Nr.</label>
        <input type="text" class="text full" name="strasse" value="<?= $this->escape($form->strasse) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['adresszusatz']) ? 'error' : '' ?>">
        <label>Adresszusatz</label>
        <input type="text" class="text full" name="adresszusatz" value="<?= $this->escape($form->adresszusatz) ?>" />
    </fieldset>
    
    <fieldset class="<?= (isset($errors['plz']) || isset($errors['ort'])) ? 'error' : '' ?>">
        <label>PLZ/Ort</label>
        <input type="text" class="text small" name="plz" value="<?= $this->escape($form->plz) ?>" />
        <input type="text" class="text medium" name="ort"  value="<?= $this->escape($form->ort) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['land']) ? 'error' : '' ?>">
        <label>Land</label>
        <select class="full" id="country" name="land">
            <optgroup label="Bitte treffen Sie eine Auswahl">
                <option  id="country_de" value="DE" <?= !$form->land || ($form->land == 'DE') ? 'selected="selected"' :'' ?>>Deutschland</option>
                <option  id="country_at" value="AT" <?= $form->land == 'AT' ? 'selected="selected"' :'' ?>>Österreich</option>
                <option  id="country_ch" value="CH" <?= $form->land == 'CH' ? 'selected="selected"' :'' ?>>Schweiz</option>
            </optgroup>
        </select>
    </fieldset>
    
    <? if(isset($errors['zd_username']) || ($form->zd_username != $form->zd_email)):?>
        <fieldset class="<?= isset($errors['zd_username']) ? 'error' : '' ?>">
            <label>Gewünschter <br />Benutzername</label>
            <input type="text" class="text full" name="zd_username" value="<?= $this->escape($form->zd_username) ?>" />
        </fieldset>
 	<? else: ?>
    	 <input type="hidden" name="zd_username" value="<?= $this->escape($form->zd_username) ?>" />
    <? endif ?>
    
    <fieldset class="<?= isset($errors['zd_email']) ? 'error' : '' ?>">
        <label>E-Mail Adresse</label>
        <input type="text" class="text full" name="zd_email" value="<?= $this->escape($form->zd_email) ?>" />
        
        <? if(!$form->zd_username): ?>
			<small>(Dient als Benutzername)</small>
    	<? endif ?>
    </fieldset>
    
    
    
	<div style="clear:both;"></div>

    <p class="gap">Mein bevorzugter Zahlungsweg:<br /><br /></p>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == 'rechnung') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="rechnung" id="rechnung" <?= (!$form->zahlart ||($form->zahlart == 'rechnung')) ? 'checked="checked"' :'' ?>> <span><strong>Ich bezahle per Rechnung</strong></span></label>
    </fieldset>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == 'bankeinzug') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="bankeinzug" id="bankeinzug" <?= ($form->zahlart == 'bankeinzug') ? 'checked="checked"' :'' ?>> <span><strong>Ich bezahle per Bankeinzug</strong></span></label>
    </fieldset>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == 'kreditkarte') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="kreditkarte" id="kreditkarte" <?= ($form->zahlart == 'kreditkarte') ? 'checked="checked"' :'' ?>> <span><strong>Ich bezahle per Kreditkarte</strong></span></label>
	</fieldset>
    <div style="clear:both;"></div>
     
    <div class="bankeinzug gap">
        <fieldset class="<?= isset($errors['kto_inh']) ? 'error' : '' ?>">
            <label>Kontoinhaber</label>
            <input type="text" class="text full" name="kto_inh" value="<?= $this->escape($form->kto_inh) ?>" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['kto_nr']) ? 'error' : '' ?>">
            <label>Kontonummer <br /><small>oder</small> IBAN</label>
            <input type="text" class="text full" name="kto_nr" value="<?= $this->escape($form->kto_nr) ?>" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['kto_blz']) ? 'error' : '' ?>">
            <label>Bankleitzahl <br /><small>oder</small> BIC</label>
            <input type="text" class="text full" name="kto_blz" value="<?= $this->escape($form->kto_blz) ?>" id="kontoBLZ" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['kto_bank']) ? 'error' : '' ?>">
            <label>Kreditinstitut</label>
            <input type="text" class="text full" name="kto_bank" value="<?= $this->escape($form->kto_bank) ?>" id="kontoInstitut">
        </fieldset>
        <div style="clear:both;"></div>
	</div> 
   
   
	  
	<div class="kreditkarte gap">
        <fieldset class="<?= isset($errors['cc_type']) ? 'error' : '' ?>">
            <label>Kreditkarten-Typ</label>
            <select class="text half" name="cc_type">
                <option <?= !$form->cc_type ? 'selected="selected"' :'' ?> value="">Bitte wählen</option>
                <option <?= ($form->cc_type == 'MasterCard') ? 'selected="selected"' :'' ?> value="MasterCard">Euro-/Mastercard</option>
                <option <?= ($form->cc_type == 'Visa') ? 'selected="selected"' :'' ?> value="Visa">VISA</option>
                <option <?= ($form->cc_type == 'DinersClub') ? 'selected="selected"' :'' ?> value="DinersClub">Diners-Club</option>
            </select>
        </fieldset>
          
        <fieldset class="<?= isset($errors['cc_inh']) ? 'error' : '' ?>">
            <label>Kreditkarten-Inhaber</label>
            <input type="text" class="text full" name="cc_inh" value="<?= $this->escape($form->cc_inh) ?>" />
        </fieldset>
    
        <fieldset class="<?= isset($errors['cc_nr']) ? 'error' : '' ?>">
            <label>Kreditkartennummer</label>
            <input type="text" class="text full" name="cc_nr" value="<?= $this->escape($form->cc_nr) ?>" />
        </fieldset>
          
        <fieldset class="<?= (isset($errors['cc_monat']) || isset($errors['cc_jahr'])) ? 'error' : '' ?>">
            <label>Gültig bis</label>
            <select class="text half" name="cc_monat">
                <option <?= !$form->cc_monat ? 'selected="selected"' :'' ?> value="">Monat</option>
                <option <?= ($form->cc_monat == '01') ? 'selected="selected"' :'' ?> value="01">01</option>
                <option <?= ($form->cc_monat == '02') ? 'selected="selected"' :'' ?> value="02">02</option>
                <option <?= ($form->cc_monat == '03') ? 'selected="selected"' :'' ?> value="03">03</option>
                <option <?= ($form->cc_monat == '04') ? 'selected="selected"' :'' ?> value="04">04</option>
                <option <?= ($form->cc_monat == '05') ? 'selected="selected"' :'' ?> value="05">05</option>
                <option <?= ($form->cc_monat == '06') ? 'selected="selected"' :'' ?> value="06">06</option>
                <option <?= ($form->cc_monat == '07') ? 'selected="selected"' :'' ?> value="07">07</option>
                <option <?= ($form->cc_monat == '08') ? 'selected="selected"' :'' ?> value="08">08</option>
                <option <?= ($form->cc_monat == '09') ? 'selected="selected"' :'' ?> value="09">09</option>
                <option <?= ($form->cc_monat == '10') ? 'selected="selected"' :'' ?> value="10">10</option>
                <option <?= ($form->cc_monat == '11') ? 'selected="selected"' :'' ?> value="11">11</option>
                <option <?= ($form->cc_monat == '12') ? 'selected="selected"' :'' ?> value="12">12</option>
            </select>
        
            <select class="text half" name="cc_jahr">
                <option value="" <?= !$form->cc_jahr ? 'selected="selected"' :'' ?>>Jahr</option>
                <? $startdate = date('Y'); ?>
                <? $enddate = $startdate+10 ?>
                
                <? for($i = $startdate; $i <= $enddate; $i++): ?>
                    <option <?= ($form->cc_jahr == $i) ? 'selected="selected"' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                <? endfor ?>
                </select>
        </fieldset>
    
        <fieldset class="<?= isset($errors['cc_check']) ? 'error' : '' ?>">
            <label>Kartenprüfziffer</label>
            <input type="text" class="text full" name="cc_check" value="<?= $this->escape($form->cc_check) ?>" id="kreditkartenPruefziffer" />
            <small>Zumeist dreistellige Nummer auf der <br>Rückseite der Kreditkarte</small>
            
        </fieldset>
        <div style="clear:both;"></div>
    </div>

   
    
    
        
    <fieldset class="gap <?= isset($errors['optin_agb']) ? 'error' : '' ?>"> 
      <label class="checkbox">
          <input type="checkbox" name="optin_agb" value="ja" <?= $form->optin_agb == 'ja' ? 'checked="checked"' :'' ?> />
          <span><strong>Ja</strong>, ich stimme den <a href="http://www.zeit.de/administratives/agb-kommentare-artikel" target="_blank" class="popup">AGB</a> zu.</span>
      </label>
    </fieldset>
    
	<fieldset class="<?= isset($errors['optin_marketing']) ? 'error' : '' ?>">
		<label class="checkbox">
        	<input type="checkbox" name="optin_marketing" value="ja" <?= $form->optin_marketing == 'ja' ? 'checked="checked"' :'' ?> />
			<span><strong>Ja</strong>, ich möchte von weiteren Vorteilen profitieren. Ich bin daher einverstanden, dass mich DIE ZEIT per Post, Telefon oder E-Mail über interessante Medien-Angebote und kostenlose Veranstaltungen informiert.</span>
		</label>
	</fieldset>
          		
	<div style="clear:both;"></div>

	
	<input class="button gap" type="image" src="<?= $this->directory ?>img/<?= $this->page_id ?>/abschicken.png?<?= $this->version ?>" alt="Abschicken" />
  	<div class="clear"></div>
    
    <div id="hinweis"><ul>
		<li>Sie sind bereits Abonnent der gedruckten ZEIT und möchten die digitale ZEIT dazubuchen?<br />
		<a href="https://premium.zeit.de/backbone/subscribe.html?wt_zmc=fix.int.zonme.diezeit.kombi.upsell.ereader.link.link&utm_medium=fix&utm_source=diezeit_zonme_int&utm_campaign=kombi&utm_content=upsell_ereader_link_link#order/800146">Hier bestellen »</a></li>

		<li style="margin-left: 10px;">Sie besitzen bereits ein Tablet/E-Reader und wollen 4&nbsp;Wochen die digitale ZEIT lesen?<br />
		<a href="https://premium.zeit.de/backbone/subscribe.html?wt_zmc=fix.int.zonme.diezeit.kombi.probe.ereader.link.link&utm_medium=fix&utm_source=diezeit_zonme_int&utm_campaign=kombi&utm_content=probe_ereader_link_link#order/1203546">Hier testen »</a></li>
	</ul></div>

	<div style="clear:both;"></div>

  
</form>