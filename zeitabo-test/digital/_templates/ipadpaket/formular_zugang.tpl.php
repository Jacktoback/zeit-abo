<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>
<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 2))) ?>">
	<input type="hidden" name="bestellen" value="2" /> 
	<input type="hidden" name="zahlart" value="rechnung" />
    
    <? if($form->optin_marketing == 'ja'): ?>
	    <input type="hidden" name="optin_marketing" value="ja" />
	<? endif ?>
    
    <? if($form->optin_agb == 'ja'): ?>
	    <input type="hidden" name="optin_agb" value="ja" />
	<? endif ?>
    
	<? if($error_text): ?>
    	<div class="container_2" id="errorMessages">
            <div class="color redbox grid_2">
                <div class="box" id="errorBox"><?= $error_text ?></div>
            </div>
        </div>
    <? endif ?>

    <div class="container_2">
        <div class="grid_1 gradient_h436">
            <div class="box">
				<!--<h2>Vielen Dank für Ihre Bestellung.</h2>-->
                <p><strong>Jetzt müssen Sie nur noch einen Benutzernamen für die digitale ZEIT festlegen!</strong></p>
				
                <div id="newclient">
            		
                	
					<fieldset class="<?= isset($errors['zd_username']) ? 'error' : '' ?>">
						<label>Gewünschter <br />Benutzername<span class="mandatory">*</span></label>
						<div class="form_bg">
	                        <input type="text" class="text full" name="zd_username" value="<?= $this->escape($form->zd_username) ?>" />
						</div>
    				</fieldset>
                
					<fieldset class="<?= isset($errors['zd_email']) ? 'error' : '' ?>">
						<label>E-Mail Adresse<span class="mandatory">*</span></label>
						<div class="form_bg">
	                        <input type="text" class="text full" name="zd_email" value="<?= $this->escape($form->zd_email) ?>" />
						</div>
    				</fieldset>
                
					<fieldset class="<?= isset($errors['zd_email2']) ? 'error' : '' ?>">
						<label>Bestätigen Sie Ihre <br>E-Mail Adresse<span class="mandatory">*</span></label>
                    	<div class="form_bg">
	                        <input type="text" class="text full" name="zd_email2" value="<?= $this->escape($form->zd_email2) ?>" />
						</div>
    				</fieldset>
                    
                    <div style="clear:both;"></div>
                   
               	</div>
          		<!-- /#newclient -->
                
                <p>Ihr neues iPad erhalten Sie sofort nach Zahlungseingang. Ihre Rechnung wird Ihnen bequem per E-Mail zugestellt.</p>
                <p>Die digitalen Formate ZEIT E-Paper und ZEIT Audio können Sie umgehend nach der Bestellung nutzen. Ihren Zugang erhalten
Sie ebenfalls per E-Mail.</p>
                    
				<input class="btn_bestellen" type="submit" value="Jetzt Kaufen" />
                
                 <p class="mandatory hintmandatory"><span class="mandatory hintmandatory">*</span> Pflichtfelder</p>
                
			</div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
        
		<div class="grid_1 gradient_h436">
			<div class="box">
        		 <img src="<?= $this->directory ?>img/ipadpaket/landingpage.jpg?<?= $this->version ?>" width="466" height="481" border="0" alt="" />
			</div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
	</div>
    <!-- /.container_2 -->


	
  
</form>