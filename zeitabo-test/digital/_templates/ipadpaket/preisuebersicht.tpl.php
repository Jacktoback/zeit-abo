<h2>Preiszusammensetzung</h2>

<? if($this->land !='ch'): ?>

    <p class="bodytext">Das ZEIT Digital-Paket (App, E-Paper, Audio und E-Reader-Formate inklusive) erhalten Sie <strong>für 12 Monate für 3,80 €*</strong> pro Ausgabe zzgl. einer <strong>Zuzahlung</strong> von <strong>214,99 €</strong> für das iPad mini, 16 GB, W-LAN, bzw. <strong>299,99 €</strong> für das iPad2, 16 GB, W-LAN. Die Versandkosten übernehmen wir für Sie. Dieses Angebot gilt nur in Kombination mit einem Jahresabonnement des ZEIT Digital-Pakets. Nach Ablauf der 12 Monate Lesegenuss gehört das iPad Ihnen, Sie zahlen den regulären Preis pro digitaler ZEIT-Ausgabe von 3,80 €* und können jederzeit kündigen.</p>
    
    <p class="bodytext">Ihr neues Lesegerät erhalten Sie <strong>sofort nach Eingang der Zahlung</strong> Ihres ZEIT-Jahresabonnements und Ihres neuen iPads. Ihre Rechnung senden wir Ihnen direkt an Ihre angegebene E-Mail-Adresse.</p>
    
    <p class="bodytext">Die Zugangsdaten für das ZEIT Digital-Paket erhalten Sie ebenfalls an die von Ihnen angegebene E-Mail-Adresse.</p>
    
    <p class="bodytext"><strong>* Preis inklusive 3,20 € für das ZEIT E-Paper</strong></p>
    
<? else: ?>

	 <p class="bodytext">Das ZEIT Digital-Paket (App, E-Paper, Audio und E-Reader-Formate inklusive) erhalten Sie <strong>für 12 Monate für 4,20 SFr*</strong> pro Ausgabe zzgl. einer <strong>Zuzahlung</strong> von <strong>274,99 SFr</strong> für das iPad mini, 16 GB, W-LAN, bzw. <strong>384,99 SFr</strong> für das iPad2, 16 GB, W-LAN. Die Versandkosten übernehmen wir für Sie. Dieses Angebot gilt nur in Kombination mit einem Jahresabonnement des ZEIT Digital-Pakets. Nach Ablauf der 12 Monate Lesegenuss gehört das iPad Ihnen, Sie zahlen den regulären Preis pro digitaler ZEIT-Ausgabe von 4,20 SFr* und können jederzeit kündigen.</p>
    
    <p class="bodytext">Ihr neues Lesegerät erhalten Sie <strong>sofort nach Eingang der Zahlung</strong> Ihres ZEIT-Jahresabonnements und Ihres neuen iPads. Ihre Rechnung senden wir Ihnen direkt an Ihre angegebene E-Mail-Adresse.</p>
    
    <p class="bodytext">Die Zugangsdaten für das ZEIT Digital-Paket erhalten Sie ebenfalls an die von Ihnen angegebene E-Mail-Adresse.</p>
    
    <p class="bodytext"><strong>* Preis inklusive 3,70 SFr für das ZEIT E-Paper</strong></p>

<? endif ?>