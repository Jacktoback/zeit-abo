<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>
<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 1))) ?>">
	<input type="hidden" name="bestellen" value="1" /> 
    <input type="hidden" name="abo_type" value="digital" />

    <? if($error_text): ?>
    	<div class="container_2" id="errorMessages">
            <div class="color redbox grid_2">
                <div class="box" id="errorBox"><?= $error_text ?></div>
            </div>
        </div>
    <? endif ?>
      
    <div class="container_2">
    	
    	<div class="grid_1 gradient_h436">
            <div class="box">
                <h2>Die digitale ZEIT<br /> abonnieren − <br />jetzt inklusive iPad!</h2>
               	<p>Entdecken Sie DIE ZEIT neu: angereichert mit exklusiven Audiobeiträgen, Videoinhalten
und Bilderstrecken – digital auf Ihrem iPad!</p>
                <img src="<?= $this->directory ?>img/ipadpaket/landingpage.jpg?<?= $this->version ?>" width="466" height="481" border="0" alt="" />
                <div class="stoerer">
                	<div class="countries country_de">
		                
    		            <img class="zugabe zugabe-1123793 zugabe-1118179" src="<?= $this->directory ?>img/ipadpaket/stoerer-ipad-mini.png?<?= $this->version ?>" width="139" height="138" border="0" alt="" />
                        
                        <img style="display:none"  class="zugabe zugabe-1106804 zugabe-1107401" src="<?= $this->directory ?>img/ipadpaket/stoerer-ipad2.png?<?= $this->version ?>" width="139" height="138" border="0" alt="" />
                        
                	</div>
                    
                    <div class="countries country_ch" style="display:none;">
		                
    		            <img class="zugabe zugabe-1123793 zugabe-1118179" src="<?= $this->directory ?>img/ipadpaket/stoerer-ipad-mini-ch.png?<?= $this->version ?>" width="139" height="138" border="0" alt="" />
                        
                        <img style="display:none"  class="zugabe zugabe-1106804 zugabe-1107401" src="<?= $this->directory ?>img/ipadpaket/stoerer-ipad2-ch.png?<?= $this->version ?>" width="139" height="138" border="0" alt="" />
                        
                	</div>
        		</div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
    	
    
        <div class="grid_1 gradient_h436">
            <div class="box">
            	<div class="countries country_de">
                    <p class="zugabe zugabe-1123793 zugabe-1118179">Ja, ich möchte die <strong>digitale ZEIT im Abo</strong> zum vergünstigten Preis von nur <strong>3,80&nbsp;€</strong> pro Woche lesen und zahle für das <strong>iPad&nbsp;mini</strong> einmalig nur 214,99&nbsp;€. Ich spare so über 100&nbsp;€ gegenüber dem Ladenpreis und dem Einzelverkauf.<br />Hier finden Sie unsere <a href="<?= $this->secure_uri ?>?preisuebersicht" target="_blank" onclick="return popup(this.href, 640, 480)" class="internal-link">Preisübersicht</a>.</p>
                    
                    <p style="display:none" class="zugabe zugabe-1106804 zugabe-1107401">Ja, ich möchte die <strong>digitale ZEIT im Abo</strong> zum vergünstigten Preis von nur <strong>3,80&nbsp;€</strong> pro Woche lesen und zahle für das <strong>iPad&nbsp;2</strong> einmalig nur 299,99&nbsp;€.Ich spare so über 100&nbsp;€ gegenüber dem Ladenpreis und dem Einzelverkauf.<br />Hier finden Sie unsere <a href="<?= $this->secure_uri ?>?preisuebersicht" target="_blank" onclick="return popup(this.href, 640, 480)" class="internal-link">Preisübersicht</a>.</p>
				</div>
                
                <div class="countries country_ch"  style="display:none;">
                    <p class="zugabe zugabe-1123793 zugabe-1118179">Ja, ich möchte die <strong>digitale ZEIT im Abo</strong> zum vergünstigten Preis von nur <strong>4,20&nbsp;SFr</strong> pro Woche lesen und zahle für das <strong>iPad&nbsp;mini</strong> einmalig nur 274,99&nbsp;SFr. Ich spare so über 120&nbsp;SFr gegenüber dem Ladenpreis und dem Einzelverkauf.<br />Hier finden Sie unsere <a href="<?= $this->secure_uri ?>?preisuebersicht=ch" target="_blank" onclick="return popup(this.href, 640, 480)" class="internal-link">Preisübersicht</a>.</p>
                    
                    <p style="display:none" class="zugabe zugabe-1106804 zugabe-1107401">Ja, ich möchte die <strong>digitale ZEIT im Abo</strong> zum vergünstigten Preis von nur <strong>4,20&nbsp;SFr</strong> pro Woche lesen und zahle für das <strong>iPad&nbsp;2</strong> einmalig nur 384,99&nbsp;SFr. Ich spare so über 120&nbsp;SFr gegenüber dem Ladenpreis und dem Einzelverkauf.<br />Hier finden Sie unsere <a href="<?= $this->secure_uri ?>?preisuebersicht=ch" target="_blank" onclick="return popup(this.href, 640, 480)" class="internal-link">Preisübersicht</a>.</p>
				</div>
                
                <fieldset class="zugaben <?= isset($errors['zugabe']) ? 'error' : '' ?>">
                    <label>
                    	<img style="display:none" class="zugabe zugabe-1106804" src="<?= $this->directory ?>img/ipadpaket/ipad2-schwarz.png?<?= $this->version ?>" alt="iPad 2 (schwarz)" width="91" height="74" />
                        <img style="display:none" class="zugabe zugabe-1107401" src="<?= $this->directory ?>img/ipadpaket/ipad2-weiss.png?<?= $this->version ?>" alt="iPad 2 (weiss)" width="91" height="74" />
                        <img class="zugabe zugabe-1123793" src="<?= $this->directory ?>img/ipadpaket/ipad-mini-schwarz.png?<?= $this->version ?>" alt="iPad Mini (spacegrau)" width="91" height="74" />
                        <img style="display:none" class="zugabe zugabe-1118179" src="<?= $this->directory ?>img/ipadpaket/ipad-mini-weiss.png?<?= $this->version ?>" alt="iPad Mini (weiss)" width="91" height="74" />
                    </label>
                    <div class="form_bg">
                        <select class="full" id="zugabe" name="zugabe">
                        	<option  value="1123793" <?= !$form->zugabe || ($form->zugabe == '1123793') ? 'selected="selected"' :'' ?>>iPad Mini (spacegrau)</option>
                            <option  value="1118179" <?= ($form->zugabe == '1118179') ? 'selected="selected"' :'' ?>>iPad Mini (weiss)</option>
                            <option  value="1106804" <?= ($form->zugabe == '1106804') ? 'selected="selected"' :'' ?>>iPad 2 (schwarz)</option>
                            <option  value="1107401" <?= ($form->zugabe == '1107401') ? 'selected="selected"' :'' ?>>iPad 2 (weiss)</option>
                        </select>
                  	</div>
                </fieldset>
                
                
                <p>Bitte liefern Sie mein iPad an folgende Adresse:</p>
                
                <fieldset class="<?= isset($errors['anrede']) ? 'error' : '' ?>">
                    <label>Anrede<span class="mandatory">*</span></label>
                    <input type="radio" name="anrede" value="Herr" <?= $form->anrede == 'Herr' ? 'checked="checked"' :'' ?> /> Herr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="anrede" value="Frau" <?= $form->anrede == 'Frau' ? 'checked="checked"' :'' ?> /> Frau&nbsp;&nbsp;&nbsp;
                </fieldset>
    
                <fieldset class="<?= isset($errors['vorname']) ? 'error' : '' ?>">
                    <label>Vorname<span class="mandatory">*</span></label>
                    <div class="form_bg">
	                    <input type="text" class="text full" name="vorname" value="<?= $this->escape($form->vorname) ?>" />
    				</div>
                </fieldset>
                
                <fieldset class="<?= isset($errors['name']) ? 'error' : '' ?>">
                    <label>Nachname<span class="mandatory">*</span></label>
					<div class="form_bg">
	                    <input type="text" class="text full" name="name" value="<?= $this->escape($form->name) ?>" />
    				</div>
                </fieldset>
    
                <fieldset class="<?= isset($errors['strasse']) ? 'error' : '' ?>">
                    <label>Straße/Nr.<span class="mandatory">*</span></label>
                    <div class="form_bg">
	                    <input type="text" class="text full" name="strasse" value="<?= $this->escape($form->strasse) ?>" />
    				</div>
                </fieldset>
    
                <fieldset class="<?= isset($errors['adresszusatz']) ? 'error' : '' ?>">
                    <label>Adresszusatz</label>
                    <div class="form_bg">
	                    <input type="text" class="text full" name="adresszusatz" value="<?= $this->escape($form->adresszusatz) ?>" />
    				</div>
                </fieldset>
    
                <fieldset class="<?= (isset($errors['plz']) || isset($errors['ort'])) ? 'error' : '' ?>">
                    <label>PLZ/Ort<span class="mandatory">*</span></label>
                    <div class="form_bg">
	                    <input type="text" class="text small" name="plz" value="<?= $this->escape($form->plz) ?>" />
    				</div>
                    <div class="form_bg">
                    	<input type="text" class="text medium" name="ort"  value="<?= $this->escape($form->ort) ?>" />
                	</div>
                </fieldset>
    
                <fieldset class="<?= isset($errors['land']) ? 'error' : '' ?>">
                    <label>Land</label>
                    <div class="form_bg">
                        <select class="full" id="country" name="land">
                            <option  id="country_de" value="DE" <?= !$form->land || ($form->land == 'DE') ? 'selected="selected"' :'' ?>>Deutschland</option>
                            <option  id="country_at" value="AT" <?= $form->land == 'AT' ? 'selected="selected"' :'' ?>>Österreich</option>
                            <option  id="country_ch" value="CH" <?= $form->land == 'CH' ? 'selected="selected"' :'' ?>>Schweiz</option>
                        </select>
					</div>
                </fieldset>
    
    			<fieldset class="<?= isset($errors['telefon']) ? 'error' : '' ?>">
                    <label>Telefon</label>
                    <div class="form_bg">
	                    <input type="text" class="text full" name="telefon" value="<?= $this->escape($form->telefon) ?>" />
    				</div>
                    <span class="hint_telefon">Notwendig zur Nutzung des ZEIT Audio<br/>Mobilservice über das Mobil-Telefon</span>
                </fieldset>
    
                
                <div style="clear:both;"></div>
                
                <div class="form_legals">
                    <fieldset class="<?= isset($errors['optin_agb']) ? 'error' : '' ?>"> 
                        <label>
                        	<input type="checkbox" name="optin_agb" value="ja" <?= $form->optin_agb == 'ja' ? 'checked="checked"' :'' ?> />
                        	<strong>Ja</strong>, ich stimme den <a href="http://www.zeit.de/administratives/agb-kommentare-artikel" target="_blank" onclick="return popup(this.href, 850, 600)">AGB</a> und der <a href="https://premium.zeit.de/datenschutz" target="_blank" onclick="return popup(this.href, 850, 600)">Datenschutzbestimmung</a> zu und bin über <a href="https://premium.zeit.de/node/12212" target="_blank" onclick="return popup(this.href, 850, 600)">Widerrufsmöglichkeiten</a> informiert.<span class="mandatory">*</span>
                   		</label>
                    </fieldset>
                </div>
                
                <div style="clear:both;"></div>
                
                <div class="form_legals">
                    <fieldset class="<?= isset($errors['optin_marketing']) ? 'error' : '' ?>">
                        <label>
                            <input type="checkbox" name="optin_marketing" value="ja" id="callback" <?= $form->optin_marketing == 'ja' ? 'checked="checked"' :'' ?> />
                            <strong>Ja</strong>, ich möchte von weiteren Vorteilen profitieren. Ich bin daher einverstanden, dass mich DIE ZEIT per Post, Telefon oder E-Mail über interessante Medien-Angebote und kostenlose Veranstaltungen informiert.
                        </label>
                    </fieldset>
               	</div>
                
                
                <div style="clear:both;"></div>
                <p class="mandatory"><span class="mandatory">*</span> Pflichtfelder</p>
                
                <input class="btn_abschicken btn" type="submit" value="Abschicken" />
			</div>
            <!-- /.box -->
        </div>
        <!-- /.grid_1 -->
        
        
        
        
        
    </div>
    <!-- /.container_2 -->


  
</form>


        
<script type="text/javascript">
	$(document).ready( function() {
		$('#country').change(toggleCountries).trigger('change').blur(toggleCountries);
		$('#zugabe').change(toggleAddons).trigger('change').blur(toggleAddons);
	});
</script>