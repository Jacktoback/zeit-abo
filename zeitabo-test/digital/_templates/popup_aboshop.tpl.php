<?='<?xml version="1.0" encoding="utf-8"?>' ?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="de" lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/reset.css?<?= $this->version ?>" media="all" />
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/aboshop/screen.css?<?= $this->version ?>" media="all" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/aboshop/ie6.css?<?= $this->version ?>" media="all" /><![endif]-->
     
    <title>DIE ZEIT - Abonnement - <?= $this->escape($this->title) ?></title>
   	<meta name="robots" content="noindex, nofollow">
    
</head>

<body id="<?= $this->page_id.'_'.$this->content_id ?>">
<?= $this->render('pixel_tagmanager.tpl.php') ?>



	<div id="popupwrapper">
		<div id="popupcontent">
        	<div class="content-elements">
				<?= $this->render($this->page_id.'/'.$this->content_id.'.tpl.php') ?>
			</div>
    	</div>
	</div>




<?= $this->render('pixel_webtrekk.tpl.php') ?>
<?= $this->render('pixel_analytics.tpl.php') ?>


</body>
</html>