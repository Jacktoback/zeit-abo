<? 
$form = $this->session->form;
$errors = $this->errors;
if((!$error_text = $this->error_text) && $errors){
	$error_text = 'Bitte füllen Sie das Formular vollständig und korrekt aus.';
}
?>

<form method="post" action="<?= $this->secure_uri.'?'.http_build_query(array_merge($this->req_params, array('bestellen' => 'geschenk'))) ?>#teaser">
	<input type="hidden" name="abo_type" value="digital" />
    <input type="hidden" name="bestellen" value="geschenk" /> 
    
    
    

	<div id="teaser">
        <h2>Ja, ich möchte DIE ZEIT im Digital-Paket verschenken! </h2>
        <div class="countries country_de">
            <p>Das Abo beginnt im Januar mit Ausgabe 2/2016.
Ich verschenke die digitale ZEIT an den genannten Empfänger und bezahle den Vorzugspreis von zzt. nur 4,10&nbsp;€ pro Ausgabe. </p>
        </div>
        
        <div class="countries country_ch" style="display:none;">
            <p>Das Abo beginnt im Januar mit Ausgabe 2/2016.
Ich verschenke die digitale ZEIT an den genannten Empfänger und bezahle den Vorzugspreis von zzt. nur sFr.&nbsp;4,50 pro Ausgabe. </p>
        </div>
        
        
    </div>


	


	<h2 class="gap">Gewünschtes Geschenkabo:</h2>

	
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == '3monate') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="3monate" id="3monate" <?= (!$form->zahlart ||($form->zahlart == '3monate')) ? 'checked="checked"' :'' ?>> <span><strong>3 Monate</strong><br /> 13 Ausgaben für <span class="countries country_de">53,30 €</span><span class="countries country_ch" style="display:none;">SFr. 58,50</span></span></label>
    </fieldset>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == '6monate') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="6monate" id="6monate" <?= ($form->zahlart == '6monate') ? 'checked="checked"' :'' ?>> <span><strong>6 Monate</strong><br /> 26 Ausgaben für <span class="countries country_de">106,60 €</span><span class="countries country_ch" style="display:none;">SFr. 117,–</span></span></span></label>
    </fieldset>
    
    <fieldset class="zahlart_auswahl <?= isset($errors['zahlart']) && ($form->zahlart == '12monate') ? 'error' : '' ?>">
		<label class="radio"><input type="radio" name="zahlart" class="zahlart" value="12monate" id="12monate" <?= ($form->zahlart == '12monate') ? 'checked="checked"' :'' ?>> <span><strong>12 Monate</strong><br /> 52 Ausgaben für <span class="countries country_de">213,20 €</span><span class="countries country_ch" style="display:none;">SFr. 234,–</span></span></span></label>
	</fieldset>
    <div style="clear:both;"></div>
    
    
	

	<h2 class="gap">Meine persönlichen Angaben (Rechnungsadresse):</h2>
    
    <? if($error_text): ?>
    	<p class="error"><?= $error_text ?></p>
    <? endif ?>
    
    
    <fieldset class="<?= isset($errors['anrede']) ? 'error' : '' ?>">
        <label>Anrede</label>
    	<label class="radio"><input type="radio" name="anrede" value="Frau" <?= $form->anrede == 'Frau' ? 'checked="checked"' :'' ?> /> Frau</label>
		<label class="radio"><input type="radio" name="anrede" value="Herr" <?= $form->anrede == 'Herr' ? 'checked="checked"' :'' ?> /> Herr</label>
    </fieldset>
    
    <fieldset class="<?= (isset($errors['vorname']) || isset($errors['name'])) ? 'error' : '' ?>">
        <label>Vor-/Nachname</label>
        <input type="text" class="text half" name="vorname" value="<?= $this->escape($form->vorname) ?>" />
        <input type="text" class="text half" name="name" value="<?= $this->escape($form->name) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['strasse']) ? 'error' : '' ?>">
        <label>Straße/Nr.</label>
        <input type="text" class="text full" name="strasse" value="<?= $this->escape($form->strasse) ?>" />
    </fieldset>
    
    <fieldset class="<?= (isset($errors['plz']) || isset($errors['ort'])) ? 'error' : '' ?>">
        <label>PLZ/Ort</label>
        <input type="text" class="text small" name="plz" value="<?= $this->escape($form->plz) ?>" />
        <input type="text" class="text medium" name="ort"  value="<?= $this->escape($form->ort) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['land']) ? 'error' : '' ?>">
        <label>Land</label>
        <select class="full" id="country" name="land">
            <optgroup label="Bitte treffen Sie eine Auswahl">
                <option  id="country_de" value="DE" <?= !$form->land || ($form->land == 'DE') ? 'selected="selected"' :'' ?>>Deutschland</option>
                <option  id="country_at" value="AT" <?= $form->land == 'AT' ? 'selected="selected"' :'' ?>>Österreich</option>
                <option  id="country_ch" value="CH" <?= $form->land == 'CH' ? 'selected="selected"' :'' ?>>Schweiz</option>
            </optgroup>
        </select>
    </fieldset>
    
	<fieldset class="<?= isset($errors['zahler_email']) ? 'error' : '' ?>">
        <label>E-Mail Adresse</label>
        <input type="text" class="text full" name="zahler_email" value="<?= $this->escape($form->zahler_email) ?>" />
    </fieldset>
    
    
	<div style="clear:both;"></div>

    
    
    <h2 class="gap">Angaben zum Empfänger (Lieferadresse):</h2>
    
    
    <fieldset class="<?= isset($errors['empfaenger_anrede']) ? 'error' : '' ?>">
        <label>Anrede</label>
    	<label class="radio"><input type="radio" name="empfaenger_anrede" value="Frau" <?= $form->empfaenger_anrede == 'Frau' ? 'checked="checked"' :'' ?> /> Frau</label>
		<label class="radio"><input type="radio" name="empfaenger_anrede" value="Herr" <?= $form->empfaenger_anrede == 'Herr' ? 'checked="checked"' :'' ?> /> Herr</label>
    </fieldset>
    
    <fieldset class="<?= (isset($errors['empfaenger_vorname']) || isset($errors['empfaenger_name'])) ? 'error' : '' ?>">
        <label>Vor-/Nachname</label>
        <input type="text" class="text half" name="empfaenger_vorname" value="<?= $this->escape($form->empfaenger_vorname) ?>" />
        <input type="text" class="text half" name="empfaenger_name" value="<?= $this->escape($form->empfaenger_name) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['empfaenger_strasse']) ? 'error' : '' ?>">
        <label>Straße/Nr.</label>
        <input type="text" class="text full" name="empfaenger_strasse" value="<?= $this->escape($form->empfaenger_strasse) ?>" />
    </fieldset>
    
    <fieldset class="<?= (isset($errors['empfaenger_plz']) || isset($errors['empfaenger_ort'])) ? 'error' : '' ?>">
        <label>PLZ/Ort</label>
        <input type="text" class="text small" name="empfaenger_plz" value="<?= $this->escape($form->empfaenger_plz) ?>" />
        <input type="text" class="text medium" name="empfaenger_ort"  value="<?= $this->escape($form->empfaenger_ort) ?>" />
    </fieldset>
    
    <fieldset class="<?= isset($errors['empfaenger_land']) ? 'error' : '' ?>">
        <label>Land</label>
        <select class="full" name="empfaenger_land">
            <optgroup label="Bitte treffen Sie eine Auswahl">
                <option value="DE" <?= !$form->empfaenger_land || ($form->empfaenger_land == 'DE') ? 'selected="selected"' :'' ?>>Deutschland</option>
                <option value="AT" <?= $form->empfaenger_land == 'AT' ? 'selected="selected"' :'' ?>>Österreich</option>
                <option value="CH" <?= $form->empfaenger_land == 'CH' ? 'selected="selected"' :'' ?>>Schweiz</option>
            </optgroup>
        </select>
    </fieldset>
    
    <fieldset class="<?= isset($errors['zd_email']) ? 'error' : '' ?>">
        <label>E-Mail Adresse</label>
        <input type="text" class="text full" name="zd_email" value="<?= $this->escape($form->zd_email) ?>" />
        
        <small>Die E-Mail-Adresse dient als Benutzername. An diese E-Mail-Adresse werden die Zugangsdaten verschickt.</small>
    </fieldset>
    
    
	<div style="clear:both;"></div>
    
    
    
        
    <fieldset class="gap <?= isset($errors['optin_agb']) ? 'error' : '' ?>"> 
      <label class="checkbox">
          <input type="checkbox" name="optin_agb" value="ja" <?= $form->optin_agb == 'ja' ? 'checked="checked"' :'' ?> />
          <span><strong>Ja</strong>, ich stimme den <a href="http://www.zeit.de/administratives/agb-kommentare-artikel" target="_blank" class="popup">AGB</a> zu.</span>
      </label>
    </fieldset>
    
	<fieldset class="<?= isset($errors['optin_marketing']) ? 'error' : '' ?>">
		<label class="checkbox">
        	<input type="checkbox" name="optin_marketing" value="ja" <?= $form->optin_marketing == 'ja' ? 'checked="checked"' :'' ?> />
			<span><strong>Ja</strong>, ich möchte von weiteren Vorteilen profitieren. Ich bin daher einverstanden, dass mich DIE ZEIT per Post, Telefon oder E-Mail über interessante Medien-Angebote und kostenlose Veranstaltungen informiert.</span>
		</label>
	</fieldset>
          		
	<div style="clear:both;"></div>

	
	<input class="button gap" type="image" src="<?= $this->directory ?>img/<?= $this->page_id ?>/kaufen.png?<?= $this->version ?>" alt="Jetzt Kaufen" />
  	<div class="clear"></div>
    
    
    <div id="hinweis"><ul>
		<li>Sie möchten selber die digitale ZEIT lesen?<br>
		<a href="http://www.zeitabo.de/die-zeit-im-abo/digital-abo/probe-abo.html">Hier Probeabo bestellen »</a></li>

		
	</ul></div>
    <div class="clear"></div>

  
</form>