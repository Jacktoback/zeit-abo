<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7304872/690942/css/fonts.css" />

    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/reset.css?<?= $this->version ?>" media="all" />
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/landingpage/screen.css?<?= $this->version ?>" media="all" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/landingpage/ie6.css?<?= $this->version ?>" media="all" /><![endif]-->
    
    <link rel="stylesheet" type="text/css" href="<?= $this->directory ?>css/<?= $this->page_id ?>.css?<?= $this->version ?>" media="all" />
    
    <script src="<?= $this->directory ?>js/landingpage/jquery.js?<?= $this->version ?>" type="text/javascript"></script>
    
    <title>DIE ZEIT - Abonnement - <?= $this->escape($this->title) ?></title>
    <link rel="icon" href="<?= $this->directory ?>img/favicon.ico" type="image/ico" />
    <meta property="og:title" content="<?= $this->escape($this->title) ?>"/>
    <meta property="og:image" content="<?= $this->directory ?>img/facebook.jpg"/>
    
    
    
    <? if($keywords = $this->keywords): ?>
        <meta name="keywords" content="<?= $keywords ?>">
    <? endif ?>
    
    <? if($description = $this->description): ?>
        <meta name="description" content="<?= $description ?>">
        <meta property="og:description" content="<?= $description ?>"/>
  	<? else: ?>
    	<meta property="og:description" content=""/>
    <? endif ?>
    
    <? if($this->nofollow): ?>
		<meta name="robots" content="noindex, nofollow">
    <? endif ?>
    
    <? if($this->meta_canonical): ?>
		<link rel="canonical" href="<?= $this->meta_canonical ?>" />
    <? endif ?>
    
    <script type="text/javascript">
		function popup (url, w, h) {
			fenster = window.open(url, "fenster1", "width="+w+",height="+h+",status=no,scrollbars=yes,resizable=no");
			fenster.focus();
			return false;
		}
		
		function toggleCountries(){
			var country = $('#country option:selected').attr('id');
			if(country != 'country_ch'){
				country = 'country_de';
			}
			if(country){
				$('.countries').hide();
				$('.'+country).show();
			}
		}
		
		$(document).ready(function(){
			$('#country').change(toggleCountries).trigger('change').blur(toggleCountries);
			
			$('.zahlart').change(function(){
				var obj = $('.zahlart:checked');
				$('.kreditkarte').addClass('fade');
				$('.bankeinzug').addClass('fade');
				$('.'+obj.attr('id')).removeClass('fade');
			}).change();
			
			$('a.popup').click(function(){
				var url = $(this).attr('href');
				return popup(url, 795, 550);
			});

		});
    </script>

</head>

<body id="<?= $this->page_id.'_'.$this->content_id ?>">
<?= $this->render('pixel_tagmanager.tpl.php') ?>


<div id="main">
	<div id="header">

		<? $header_start =  $this->header_start ? $this->header_start : 'header.jpg'; ?>

		<? if($this->content_id == 'formular_daten'): ?>
            <img src="<?= $this->directory ?>img/<?= $this->page_id ?>/<?= $header_start ?>?<?= $this->version ?>" alt="" />
			<h1><?= $this->headline_daten ?></h1>
		<? elseif($this->content_id == 'formular_zugang'): ?>
        	<img src="<?= $this->directory ?>img/<?= $this->page_id ?>/<?= $header_start ?>?<?= $this->version ?>" alt="" />
            <h1><?= $this->headline_zugang ?></h1>
		<? else: ?>
            <img src="<?= $this->directory ?>img/<?= $this->page_id ?>/header_danke.jpg?<?= $this->version ?>" alt="" />
            <h1><?= $this->headline_danke ?></h1>
        <? endif ?>
    </div>
    <!-- /#header -->
	
	
	<div id="content">
	
		<? if($this->cookie_error): ?>
        	<p><br /><br />Bitte aktivieren Sie Cookies in Ihrem Internet-Browser,<br /> oder benutzen Sie einen Internet-Browser, der Cookies annimmt.</p>
		<? else: ?>
			<?= $this->render($this->page_id.'/'.$this->content_id.'.tpl.php') ?>
		<? endif ?>
	</div>
	<!-- /#content -->
	
	<div id="footer">
		<a href="http://www.zeitabo.de/impressum.html" class="popup" target="_blank">Impressum</a> | <a href="http://www.zeitabo.de/agb.html" target="_blank" class="popup">AGB</a>
	</div>
	<!-- /#footer -->
	
</div>
<!-- /#main -->

	

<?= $this->render('pixel_webtrekk.tpl.php') ?>
<?= $this->render('pixel_ivw.tpl.php') ?>
<?= $this->render('pixel_analytics.tpl.php') ?>


<? if(false && $this->is_dev_server): ?>
<hr />
<strong>DEBUG FOR DIR: <?= $this->directory ?></strong>
<pre>
	Aktionsnummer: <?= var_dump($this->aktionsnummer); ?>
	<hr />
	<? var_dump($_SERVER) ?>
    <? var_dump($_REQUEST) ?>
    <hr />
    FORM ERRROR:
    <? var_dump($this->errors) ?>
</pre>

<? else: ?>

<!-- <?= var_dump($this->aktionsnummer); ?> -->

<? endif ?>

</body>
</html>