<?php

require_once "AboshopTable.class.php";

class Landingpage 
{
	private $form;
	
	public function __construct($form){
		$this->form = $form;
	}
	
	public static function getForm($req, $session){
		$form = self::getFormFields();
	
		$session_form = $session->form ? $session->form : new stdClass();
		
		
		foreach($form as $k => $v){
			if(isset($session_form->$k)){
				$form[$k] = $session_form->$k;
			}
			
			if($req->has($k)){
				$form[$k] = trim($req->get($k));
			}
		}
		
		$form = (object) $form;
		
		//overwrite session data for checkboxes (otherwise they cannot been unset)
		if($req->isPost()){
			$form->optin_agb = $req->get('optin_agb');
			$form->optin_marketing = $req->get('optin_marketing');
		}
		
		//prefil if possible
		//var_dump($form);
		if(!$form->kto_inh && !$req->has('kto_inh') && ($form->vorname || $form->name)){
			$form->kto_inh = $form->vorname.' '.$form->name;
		}
		
		if(!$form->cc_inh && !$req->has('cc_inh')  && ($form->vorname || $form->name)){
			$form->cc_inh = $form->vorname.' '.$form->name;
		}
		
		if(!$form->abo_plz && !$req->has('abo_plz') && $form->plz){
			$form->abo_plz = $form->plz;
		}
		
		
		$session->form = $form;	
		
		return $form;
	}
	
	public static function saveForm($form){
		$result = false;
		
		try {
			$table = new AboshopTable('ID', 'zeit_lp_abos');
			$row = $table->createRow();
			
			$row->Datum = date('Y-m-d H:i:s');
			$row->PAGE_ID = isset($form->page_id) ? $form->page_id : '';
			$row->ORDER_ID = isset($form->order_id) ? $form->order_id : '';
			$row->Referrer = isset($form->ref) ? $form->ref : '';
			$row->Aktionsnummer = isset($form->aktionsnummer) ? $form->aktionsnummer : '';
			$row->Zugabe = isset($form->zugabe) ? $form->zugabe : '';
			$row->Abotyp = isset($form->abo_type) ? $form->abo_type : '';
			
			$row->OptIn_AGB = isset($form->optin_agb) && ($form->optin_agb == 'ja') ? 'Y' : 'N';
			$row->OptIn_Marketing = isset($form->optin_marketing) && ($form->optin_marketing == 'ja') ? 'Y' : 'N';
			
			$row->Anrede = isset($form->anrede) ? $form->anrede : '';
			$row->Vorname = isset($form->vorname) ? $form->vorname : '';
			$row->Nachname = isset($form->name) ? $form->name : '';
			$row->Adresse = isset($form->strasse) ? $form->strasse : '';
			$row->Adresszusatz = isset($form->adresszusatz) ? $form->adresszusatz : '';
			$row->PLZ = isset($form->plz) ? $form->plz : '';
			$row->Ort = isset($form->ort) ? $form->ort : '';
			$row->Land = isset($form->land) ? $form->land : '';
			$row->Telefon = isset($form->telefon) ? $form->telefon : '';
			
			$row->Empfaenger_Anrede = isset($form->empfaenger_anrede) ? $form->empfaenger_anrede : '';
			$row->Empfaenger_Vorname = isset($form->empfaenger_vorname) ? $form->empfaenger_vorname : '';
			$row->Empfaenger_Nachname = isset($form->empfaenger_name) ? $form->empfaenger_name : '';
			$row->Empfaenger_Adresse = isset($form->empfaenger_strasse) ? $form->empfaenger_strasse : '';
			$row->Empfaenger_PLZ = isset($form->empfaenger_plz) ? $form->empfaenger_plz : '';
			$row->Empfaenger_Ort = isset($form->empfaenger_ort) ? $form->empfaenger_ort : '';
			$row->Empfaenger_Land = isset($form->empfaenger_land) ? $form->empfaenger_land : '';
			$row->Zahler_Email = isset($form->zahler_email) ? $form->zahler_email : '';
			
			$row->Zahlart = isset($form->zahlart) ? $form->zahlart : '';
			$row->Kontoinhaber = isset($form->kto_inh) ? $form->kto_inh : '';
			$row->Kontonummer = isset($form->kto_nr) ? $form->kto_nr : '';
			$row->Bankleitzahl = isset($form->kto_blz) ? $form->kto_blz : '';
			$row->Bank = isset($form->kto_bank) ? $form->kto_bank : '';

			$row->Abo_PLZ = isset($form->abo_plz) && ($form->abo_type == 'upgrade') ? $form->abo_plz : '';
			$row->Abo_NR = isset($form->abo_nr) && ($form->abo_type == 'upgrade') ? $form->abo_nr : '';
			$row->Username = isset($form->zd_username) ? $form->zd_username : '';
			$row->EMail = isset($form->zd_email) ? $form->zd_email : '';
			
			if(isset($_REQUEST['r'])){
				$row->ReqParam_R = trim($_REQUEST['r']);
			}
			
			if(isset($_REQUEST['pid'])){
				$row->ReqParam_PID = trim($_REQUEST['pid']);
			}
			
			$result = $row->save();
			
		} catch(Exception $e) {
			// do nothing		
		}
		
		return $result;
	}
	
	public static function getFormFields(){
		$form = array(
			'page_id' => '',
			'aktionsnummer' => '',
			'ref' => '',
			'order_id' => '',
			'abo_type' => '',
			'zugabe' => '',
			
			'anrede' => '',
			'vorname' => '',
			'name' => '',
			'strasse' => '',
			'adresszusatz' => '',
			'plz' => '',
			'ort' => '',
			'land' => '',
			'telefon' => '',
			
			'empfaenger_anrede' => '',
			'empfaenger_vorname' => '',
			'empfaenger_name' => '',
			'empfaenger_strasse' => '',
			'empfaenger_plz' => '',
			'empfaenger_ort' => '',
			'empfaenger_land' => '',
			'zahler_email' => '',
			
			'zahlart' => '',
			'kto_inh' => '',
			'kto_nr' => '',
			'kto_blz' => '',
			'kto_bank' => '',
			'cc_type' => '',
			'cc_inh' => '',
			'cc_nr' => '',
			'cc_monat' => '',
			'cc_jahr' => '',
			'cc_check' => '',
			
			'abo_plz' => '',
			'abo_nr' => '',
			'zd_username' => '',
			'zd_email' => '',
			'zd_email2' => '',
			
			'optin_agb' => '',
			'optin_marketing' => ''
		);
		
		return $form;	
	}
}

