<?php
// INIT ZEND
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));
require_once "Zend/Session.php";
require_once "Zend/Controller/Request/Http.php";
require_once "Zend/View.php";
require_once "AboshopTable.class.php";


// Init View Objects and set Parameters
$view = new Zend_View();
$view->setScriptPath('_templates');

// Request Object
$view->req = $req = new Zend_Controller_Request_Http();
if(!$page_id = $req->get('page_id')){
	$page_id = 'digitalpaket';	
}
$view->page_id = $page_id;

// Session Object
Zend_Session::start();
$session = new Zend_Session_Namespace( 'zeitabo_landingpages_'.$page_id );
$session->setExpirationSeconds(3600);
$view->session = $session;

// uri and params
$server = $req->getServer('SERVER_NAME');
$uri = parse_url($req->getServer('REQUEST_URI'), PHP_URL_PATH);
$view->wt_content_id = $uri;


//Enforce Cookies
if(!isset($_COOKIE['cookie_check'])){
	setcookie("cookie_check", true, time()+60*60*24*365, '/');
	
	$params = $req->getQuery();
	unset($params['page_id']);
	
	$proto = $req->isSecure() ? 'https://' : 'http://';
	
	if(!$req->has('cc')){	
		$params['cc'] = '1';
		header('Location: '.$proto.$server.$uri.'?'.http_build_query($params));
		exit();
	} //second try; if site is linked with param cc=1
	else if($req->get('cc') == '1'){
		$params['cc'] = '2';
		header('Location: '.$proto.$server.$uri.'?'.http_build_query($params));
		exit();
	}
	
	$view->cookie_error = true;
}


// SETTINGS DEV SERVER
if($is_dev_server = ($server == 'zeitabo-dev.zeit.de')){
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}

$view->is_dev_server = $is_dev_server;
define('IS_DEV_SERVER', $is_dev_server);

//params array which are concatenated to url
$req_params = array();
$valid_params = array('r','pid', 'fb');
foreach($valid_params as $p){
	if($req->has($p)){
		$req_params[$p] = $req->get($p);
	}
}
$view->req_params = $req_params;

$aktionsnummer = $session->aktionsnummer;
$ref = $req->get('r');

if(!$aktionsnummer || $ref){
	
	$table = new AboshopTable('ID', 'zeit_lp_ref');
	
	$select = $table->select();
	$select->where('page_id = ?', $page_id);
	$select->where('ref = ?', 'standard');
	$row_standard = $table->fetchRow($select);
	
	if($ref){
		$select = $table->select();
		$select->where('page_id = ?', $page_id);
		$select->where('ref = ?', $ref);
		$row = $table->fetchRow($select);
	} else {
		$row = $row_standard;	
	}
	
	if(!$row && !$row_standard){
		echo 'AKTION '.$page_id.': Aktionsnummern nicht gefunden.';
		exit();	
	}
	
	$aktionsnummer = array(
		'ref' => $row && $row->ref ? $row->ref : $row_standard->ref,
		'nr_digital' => $row && $row->nr_digital ? $row->nr_digital : $row_standard->nr_digital,
		'nr_upgrade' => $row && $row->nr_upgrade ? $row->nr_upgrade : $row_standard->nr_upgrade
	);
	
	if(!$aktionsnummer['nr_digital']  || !$aktionsnummer['nr_digital']){
		echo 'AKTION '.$page_id.': Standard Aktionsnummern nicht gefunden.';
		exit();	
	}
	
	$session->aktionsnummer = $aktionsnummer;
}

$view->aktionsnummer = $aktionsnummer;

 
$view->uri = 'http://'.$server.$uri;
$view->secure_uri = ($is_dev_server ? 'http://' : 'https://').$server.$uri;


$view->version = $is_dev_server ? time() : date('Ymdhi');

// BASE DIRECTORIES
$base_href = ($req->isSecure() ? 'https://' : 'http://').$server;
$view->directory = $base_href.dirname($req->getServer('PHP_SELF')).'/';
$view->base_href = $base_href.'/';

require_once('_includes/form.inc.php');

$view->countries = getCountries();
$form = initForm($req, $session);
