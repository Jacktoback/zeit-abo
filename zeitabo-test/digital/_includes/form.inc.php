<?php
require_once "Landingpage.class.php";

function is_dev_server(){
	return IS_DEV_SERVER;
}

function getFormFields(){
	return Landingpage::getFormFields();
}

function saveFormToDB($form){
	return Landingpage::saveForm($form);
}


function initForm($req, $session){
	return Landingpage::getForm($req, $session);
}

//$empfaenger = true für geschenkabo
function validateUserData($form, $empfaenger = false){
	//return array();
	
	require_once('Zend/Validate/EmailAddress.php');
	require_once('Zend/Validate/StringLength.php');
	require_once('Zend/Validate/InArray.php');	
	
	$errors = array();
	
	$validator = new Zend_Validate_InArray(array('Herr', 'Frau'));
	if (!$validator->isValid($form->anrede)) {
		$errors['anrede'] = true;
	}
	
	if($empfaenger){
		if (!$validator->isValid($form->empfaenger_anrede)) {
			$errors['empfaenger_anrede'] = true;
		}	
	}
	
	$validator = new Zend_Validate_StringLength();
	$validator->setMin(2);
	$validator->setMax(512);
	
	if (!$validator->isValid($form->vorname)) {
		$errors['vorname'] = true;
	}
	
	if (!$validator->isValid($form->name)) {
		$errors['name'] = true;
	}
	
	if (!$validator->isValid($form->strasse)) {
		$errors['strasse'] = true;
	}
	
	if($form->adresszusatz){
		if(!$validator->isValid($form->adresszusatz)) {
			$errors['adresszusatz'] = true;	
		}
	}
	
	if (!$validator->isValid($form->ort)) {
		$errors['ort'] = true;
	}
	
	if($form->telefon){
		if (!$validator->isValid($form->telefon)) {
			$errors['telefon'] = true;
		}
	
	}
	
	if($empfaenger){
		if (!$validator->isValid($form->empfaenger_vorname)) {
			$errors['empfaenger_vorname'] = true;
		}
		
		if (!$validator->isValid($form->empfaenger_name)) {
			$errors['empfaenger_name'] = true;
		}
		
		if (!$validator->isValid($form->empfaenger_strasse)) {
			$errors['empfaenger_strasse'] = true;
		}
		
		if (!$validator->isValid($form->empfaenger_ort)) {
			$errors['empfaenger_ort'] = true;
		}
		
		
	}
	
	$validator = new Zend_Validate_StringLength();
	switch($form->land){
		case 'DE':
			$validator->setMin(5);
			$validator->setMax(5);
			if(!$validator->isValid($form->plz)) {
				$errors['plz'] = true;	
			}
			break;
		case 'AT':
			$validator->setMin(4);
			$validator->setMax(4);
			if(!$validator->isValid($form->plz)) {
				$errors['plz'] = true;	
			}
			break;
		case 'CH':
			$validator->setMin(4);
			$validator->setMax(4);
			if(!$validator->isValid($form->plz)) {
				$errors['plz'] = true;	
			}
			break;
		default:
			$validator->setMin(1);
			$validator->setMax(10);
			if(!$validator->isValid($form->plz)) {
				$errors['plz'] = true;	
			}
			
			
			$countries = getCountries();
			if(!isset($countries[$form->land])){
				$errors['land'] = true;
			}
	}
	
	if($empfaenger){
		$validator = new Zend_Validate_StringLength();
		switch($form->empfaenger_land){
			case 'DE':
				$validator->setMin(5);
				$validator->setMax(5);
				if(!$validator->isValid($form->empfaenger_plz)) {
					$errors['empfaenger_plz'] = true;	
				}
				break;
			case 'AT':
				$validator->setMin(4);
				$validator->setMax(4);
				if(!$validator->isValid($form->empfaenger_plz)) {
					$errors['empfaenger_plz'] = true;	
				}
				break;
			case 'CH':
				$validator->setMin(4);
				$validator->setMax(4);
				if(!$validator->isValid($form->empfaenger_plz)) {
					$errors['empfaenger_plz'] = true;	
				}
				break;
			default:
				$validator->setMin(1);
				$validator->setMax(10);
				if(!$validator->isValid($form->empfaenger_plz)) {
					$errors['empfaenger_plz'] = true;	
				}
				
				
				$countries = getCountries();
				if(!isset($countries[$form->empfaenger_land])){
					$errors['empfaenger_land'] = true;
				}
		} // endswitch
		
		$validator = new Zend_Validate_EmailAddress();
		if (!$validator->isValid($form->zd_email)) {
			$errors['zd_email'] = true;	
		}
		if (!$validator->isValid($form->zahler_email)) {
			$errors['zahler_email'] = true;	
		}
		
	} //end if emfpänger
	
	/*
	$validator = new Zend_Validate_EmailAddress();
	if (!$validator->isValid($form->email)) {
		$errors['email'] = true;	
	}
	*/
	
	$validator = new Zend_Validate_InArray(array('digital', 'upgrade'));
	if (!$validator->isValid($form->abo_type)) {
		$errors['abo_type'] = true;	
	}
	
	return $errors;
}

function validateAccountData($form){
	//return array();
	
	require_once('Zend/Validate/EmailAddress.php');
	require_once('Zend/Validate/StringLength.php');
	require_once('Zend/Validate/InArray.php');	
	
	$errors = array();
	
	
	if($form->optin_agb != 'ja'){
		$errors['optin_agb'] = true;
	}
	
	$validator = new Zend_Validate_EmailAddress();
	if (!$validator->isValid($form->zd_email)) {
		$errors['zd_email'] = true;
		$errors['zd_email2'] = true;	
	} else if($form->zd_email != $form->zd_email2){
		$errors['zd_email2'] = true;
	}
	
	$validator = new Zend_Validate_StringLength();
	$validator->setMin(2);
	$validator->setMax(512);
	
	if($form->abo_type == 'upgrade'){
		if(!$validator->isValid($form->abo_plz)){
			$errors['abo_plz'] = true;	
		}
		
		if(!$validator->isValid($form->abo_nr)){
			$errors['abo_nr'] = true;	
		}		
	}
	
	if(!$validator->isValid($form->zd_username)){
		$errors['zd_username'] = true;	
	}
	
	switch($form->zahlart){
		
		case 'bankeinzug';
			
			if (!$validator->isValid($form->kto_inh)) {
				$errors['kto_inh'] = true;
			}
			
			if (!$validator->isValid($form->kto_nr)) {
				$errors['kto_nr'] = true;
			}
			
			if (!$validator->isValid($form->kto_blz)) {
				$errors['kto_blz'] = true;
			}
			
			if($form->kto_bank){
				if(!$validator->isValid($form->kto_bank)) {
					$errors['kto_bank'] = true;	
				}
			}
			
			break;
			
			
		case 'kreditkarte':
		
			if (!$validator->isValid($form->cc_inh)) {
				$errors['cc_inh'] = true;
			}
			
			if (!$validator->isValid($form->cc_nr)) {
				$errors['cc_nr'] = true;
			}
			
			if (!$validator->isValid($form->cc_check)) {
				$errors['cc_check'] = true;
			}
			
			$validator = new Zend_Validate_InArray(array('MasterCard', 'Visa', 'DinersClub'));
			if (!$validator->isValid($form->cc_type)) {
				$errors['cc_type'] = true;
			}
			
			$validator = new Zend_Validate_InArray(array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'));
			if (!$validator->isValid($form->cc_monat)) {
				$errors['cc_monat'] = true;
			}
			
			$startdate = date('Y');
			$enddate = $startdate + 10;
			$date = intval($form->cc_jahr);
			if (($date < $startdate) || ($date > $enddate)) {
				$errors['cc_jahr'] = true;
			}
			
			break;
			
		case 'rechnung':
			break;
			
		default:
			$errors['zahlart'] = true;
			break;
	}
	
	
	return $errors;
}

function getCountries(){
	$countries = array();
	$countries_eu = array();
	
	$countries_eu['AL'] = 'Albanien';
	$countries_eu['AD'] = 'Andorra';
	$countries_eu['AM'] = 'Armenien';
	$countries_eu['AZ'] = 'Aserbaidschan';
	$countries['AU'] = 'Australien';
	$countries_eu['BY'] = 'Belarus (Weißrussland)';
	$countries_eu['BE'] = 'Belgien';
	$countries_eu['BA'] = 'Bosnien Herzegowina';
	$countries_eu['BG'] = 'Bulgarien';
	$countries['BR'] = 'Brasilien';
	$countries['CN'] = 'China';
	$countries_eu['DK'] = 'Dänemark';
	$countries_eu['EE'] = 'Estland';
	$countries_eu['FI'] = 'Finnland';	
	$countries_eu['FR'] = 'Frankreich';
	$countries_eu['GE'] = 'Georgien';
	$countries_eu['GI'] = 'Gibraltar';
	$countries_eu['GR'] = 'Griechenland';
	$countries_eu['UK'] = 'Großbritannien';
	$countries['HK'] = 'Hongkong';
	$countries['IN'] = 'Indien';
	$countries_eu['IE'] = 'Irland';
	$countries_eu['IS'] = 'Island';
	$countries_eu['IT'] = 'Italien';
	$countries['JP'] = 'Japan';
	$countries['CA'] = 'Kanada';
	$countries_eu['KO'] = 'Kosovo';
	$countries_eu['HR'] = 'Kroatien';
	$countries_eu['LV'] = 'Lettland';
	$countries['LI'] = 'Liechtenstein';
	$countries_eu['LT'] = 'Litauen';
	$countries_eu['LU'] = 'Luxemburg';
	$countries_eu['MT'] = 'Malta';
	$countries_eu['MK'] = 'Mazedonien';
	$countries_eu['MD'] = 'Moldawien';
	$countries_eu['MC'] = 'Monaco';
	$countries_eu['NL'] = 'Niederlande';
	$countries_eu['NO'] = 'Norwegen';
	$countries_eu['PL'] = 'Polen';
	$countries_eu['PT'] = 'Portugal';
	$countries_eu['RO'] = 'Rumänien';
	$countries_eu['RU'] = 'Russland';
	$countries_eu['SM'] = 'San Marino';
	$countries['SA'] = 'Saudi-Arabien';
	$countries_eu['SE'] = 'Schweden';
	$countries_eu['YU'] = 'Serbien u. Montenegro';
	$countries_eu['SK'] = 'Slowakische Republik';
	$countries_eu['SI'] = 'Slowenien';
	$countries_eu['ES'] = 'Spanien';
	$countries['ZA'] = 'Südafrika';
	$countries_eu['CZ'] = 'Tschechische Republik';
	$countries_eu['TR'] = 'Türkei';
	$countries_eu['UA'] = 'Ukraine';
	$countries_eu['HU'] = 'Ungarn';
	$countries['US'] = 'USA';
	$countries['AE'] = 'VAE';
	$countries_eu['VA'] = 'Vatikanstadt';
	$countries_eu['CY'] = 'Zypern';
	
	$countries = array_merge($countries, $countries_eu);
	asort($countries);
	return $countries;	
}

function submitFormToApi($form){
	
	$dom = new DOMDocument("1.0", "utf-8");
	$order = $dom->createElement("order");
	$dom->appendChild($order);
	
	$customer = $dom->createAttribute("customer");
	$order->appendChild($customer);
		$customer->value = 'new';
	
	$type = $dom->createAttribute("type");
	$order->appendChild($type);
		$type->value = $form->abo_type;
		
	$productId = $dom->createElement("productId");
	$order->appendChild($productId);
		$productIdValue = $dom->createTextNode($form->aktionsnummer);
		$productId->appendChild($productIdValue);
	
	
	if($form->abo_type == 'upgrade'){
		$verification = $dom->createElement("verification");
		$order->appendChild($verification);
			$subscriptionId = $dom->createElement("subscriptionId");
			$verification->appendChild($subscriptionId);
				$subscriptionIdValue = $dom->createTextNode($form->abo_nr);
				$subscriptionId->appendChild($subscriptionIdValue);
			
			$postalCode = $dom->createElement("postalCode");
			$verification->appendChild($postalCode);
				$postalCodeValue = $dom->createTextNode($form->abo_plz);
				$postalCode->appendChild($postalCodeValue);
	}
	
	
	
	$account = $dom->createElement("account");
	$order->appendChild($account);
	
		$username = $dom->createElement("username");
		$account->appendChild($username);
			$usernameValue = $dom->createTextNode($form->zd_username);
			$username->appendChild($usernameValue);
		
		
		$email = $dom->createElement("email");
		$account->appendChild($email);
			$emailValue = $dom->createTextNode($form->zd_email);
			$email->appendChild($emailValue);
		
	$billing = $dom->createElement("billing");
	$order->appendChild($billing);
	
		$salutation = $dom->createElement("salutation");
		$billing->appendChild($salutation);
			$salutationValue = $dom->createTextNode($form->anrede);
			$salutation->appendChild($salutationValue);
		
		$givenName = $dom->createElement("givenName");
		$billing->appendChild($givenName);
			$givenNameValue = $dom->createTextNode($form->vorname);
			$givenName->appendChild($givenNameValue);
		
		$surname = $dom->createElement("surname");
		$billing->appendChild($surname);
			$surnameValue = $dom->createTextNode($form->name);
			$surname->appendChild($surnameValue);
		
		$streetAddress = $dom->createElement("streetAddress");
		$billing->appendChild($streetAddress);
			$streetAddressValue = $dom->createTextNode($form->strasse);
			$streetAddress->appendChild($streetAddressValue);
		
		$streetAddressExtra = $dom->createElement("streetAddressExtra");
		$billing->appendChild($streetAddressExtra);
			$streetAddressExtraValue = $dom->createTextNode($form->adresszusatz);
			$streetAddressExtra->appendChild($streetAddressExtraValue);
		
		$postalCode = $dom->createElement("postalCode");
		$billing->appendChild($postalCode);
			$postalCodeValue = $dom->createTextNode($form->plz);
			$postalCode->appendChild($postalCodeValue);
		
		$locality = $dom->createElement("locality");
		$billing->appendChild($locality);
			$localityValue = $dom->createTextNode($form->ort);
			$locality->appendChild($localityValue);
		
		$country = $dom->createElement("country");
		$billing->appendChild($country);
			$countryValue = $dom->createTextNode($form->land);
			$country->appendChild($countryValue);
		
		$phone = $dom->createElement("phone");
		$billing->appendChild($phone);
			$phoneValue = $dom->createTextNode($form->telefon);
			$phone->appendChild($phoneValue);
	
	$payment = $dom->createElement("payment");
	$order->appendChild($payment);
		$paymentInterval = $dom->createElement("paymentInterval");
		$payment->appendChild($paymentInterval);
			$paymentIntervalValue = $dom->createTextNode('12');
			$paymentInterval->appendChild($paymentIntervalValue);
	
	switch($form->zahlart){
		case 'rechnung':
			$paymenttype = $dom->createAttribute("type");
			$payment->appendChild($paymenttype);
				$paymenttype->value = 'invoice';
			
			break;
		
		case 'bankeinzug':
			$paymenttype = $dom->createAttribute("type");
			$payment->appendChild($paymenttype);
				$paymenttype->value = 'directDebit';
				
			$accountOwner = $dom->createElement("accountOwner");
			$payment->appendChild($accountOwner);
				$accountOwnerValue = $dom->createTextNode($form->kto_inh);
				$accountOwner->appendChild($accountOwnerValue);
			
			$accountNumber = $dom->createElement("accountNumber");
			$payment->appendChild($accountNumber);
				$accountNumberValue = $dom->createTextNode($form->kto_nr);
				$accountNumber->appendChild($accountNumberValue);
			
			$bankId = $dom->createElement("bankId");
			$payment->appendChild($bankId);
				$bankIdValue = $dom->createTextNode($form->kto_blz);
				$bankId->appendChild($bankIdValue);
				
			break;
		
		case 'kreditkarte':
			$paymenttype = $dom->createAttribute("type");
			$payment->appendChild($paymenttype);
				$paymenttype->value = 'creditCard';
				
			$cardType = $dom->createElement("cardType");
			$payment->appendChild($cardType);
				$cardTypeValue = $dom->createTextNode($form->cc_type);
				$cardType->appendChild($cardTypeValue);
			
			$cardOwner = $dom->createElement("cardOwner");
			$payment->appendChild($cardOwner);
				$cardOwnerValue = $dom->createTextNode($form->cc_inh);
				$cardOwner->appendChild($cardOwnerValue);
			
			$cardNumber = $dom->createElement("cardNumber");
			$payment->appendChild($cardNumber);
				$cardNumberValue = $dom->createTextNode($form->cc_nr);
				$cardNumber->appendChild($cardNumberValue);
			
			$expirationMonth = $dom->createElement("expirationMonth");
			$payment->appendChild($expirationMonth);
				$expirationMonthValue = $dom->createTextNode($form->cc_monat);
				$expirationMonth->appendChild($expirationMonthValue);
			
			$expirationYear = $dom->createElement("expirationYear");
			$payment->appendChild($expirationYear);
				$expirationYearValue = $dom->createTextNode($form->cc_jahr);
				$expirationYear->appendChild($expirationYearValue);
			
			$verificationNumber = $dom->createElement("verificationNumber");
			$payment->appendChild($verificationNumber);
				$verificationNumberValue = $dom->createTextNode($form->cc_check);
				$verificationNumber->appendChild($verificationNumberValue);
				
			break;	
	}
		
	$tacAccepted = $dom->createElement("tacAccepted");
	$order->appendChild($tacAccepted);
		$tacAcceptedValue = $dom->createTextNode(($form->optin_agb == 'ja') ? 'yes' : 'no');
		$tacAccepted->appendChild($tacAcceptedValue);
	
	$moreOffers = $dom->createElement("moreOffers");
	$order->appendChild($moreOffers);
		$moreOffersValue = $dom->createTextNode(($form->optin_marketing == 'ja') ? 'yes' : 'no');
		$moreOffers->appendChild($moreOffersValue);
	
	if($zugabe = $form->zugabe){
		$extra = $dom->createElement("extra");
		$order->appendChild($extra);
			$extraValue = $dom->createTextNode($zugabe);
			$extra->appendChild($extraValue);
	}
	
	$xml = $dom->saveXML();
	
	$result = new stdClass();
	
	$url = !IS_DEV_SERVER ? 'https://premium.zeit.de/api/0/orders' : 'http://master.premium.drupalowski.zeit.de/api/0/orders';
	
	//comment out to force live server
	$url = 'https://premium.zeit.de/api/0/orders';
	
	
	$handle = curl_init();
	curl_setopt_array($handle, array(
		CURLOPT_HEADER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $xml,
		CURLOPT_HTTPHEADER => array(
			'Accept: text/xml',
			'Content-Type: text/xml',
			'charset=utf-8'
		),
		CURLOPT_URL => $url
		//CURLOPT_VERBOSE => true
	));
	
	$response = curl_exec($handle);
	libxml_use_internal_errors(true);
	$response_xml = simplexml_load_string($response);
	
	if(IS_DEV_SERVER){
		echo '<!-- API URL: '.$url.' -->';
		
		echo '<!-- REQUEST XQL';
		echo $xml;
		echo '-->';
		
		echo '<!-- RESPONSE XQL';
		if($response_xml){
			echo $response_xml->asXML();
		}
		echo '-->';
	}
	
	$result->data = is_object($response_xml) ? $response_xml : false;
	$result->code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
	
	return $result;
}