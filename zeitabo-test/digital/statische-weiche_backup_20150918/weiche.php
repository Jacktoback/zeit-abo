<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7304872/690942/css/fonts.css" />

    <link rel="stylesheet" type="text/css" href="../css/reset.css" media="all" />
    <link rel="stylesheet" type="text/css" href="../css/landingpage/screen.css" media="all" />
    <!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="../css/landingpage/ie6.css" media="all" /><![endif]-->
    
    <link rel="stylesheet" type="text/css" href="../statische-weiche/css/style.css" media="all" />
    
    <script src="../js/landingpage/jquery.js" type="text/javascript"></script>
    
    <title>DIE ZEIT - Abonnement - Wochenzeitung für Politik, Wirtschaft, Wissen und Kultur</title>
    <link rel="icon" href="../img/favicon.ico" type="image/ico" />
    <meta property="og:title" content="DIE ZEIT - Abonnement - Wochenzeitung für Politik, Wirtschaft, Wissen und Kultur"/>
    <meta property="og:image" content="../img/facebook.jpg"/>
    
    <meta name="keywords" content="<? /*= ???? $keywords */ ?>">
    <meta name="description" content="<? /*= ???? $description */ ?>">
    <meta property="og:description" content="<? /*= ???? $description */ ?>"/>
	<meta name="robots" content="noindex, nofollow">

    <script type="text/javascript">
		function popup (url, w, h) {
			fenster = window.open(url, "fenster1", "width="+w+",height="+h+",status=no,scrollbars=yes,resizable=no");
			fenster.focus();
			return false;
		}
		
		$(document).ready(function(){

			$('a.popup').click(function(){
				var url = $(this).attr('href');
				return popup(url, 795, 550);
			});

		});
    </script>

</head>

<body id="print-digital-weiche">

<div id="main">
	<div id="header"></div>
    <!-- /#header -->


	<div id="content">
		<? /* How to query for cookie-error-ness?
		if($this->cookie_error): ?>
        	<p><br /><br />Bitte aktivieren Sie Cookies in Ihrem Internet-Browser,<br /> oder benutzen Sie einen Internet-Browser, der Cookies annimmt.</p>
		<? else: ?>
			<?= $this->render($this->page_id.'/'.$this->content_id.'.tpl.php') ?>
		<? endif */ ?>
		

		<a id="button_print"   href="http://leserservice.zeit.de/angebote/4-wochen-die-zeit-kostenlos/index2.php?r=gr" title="DIE ZEIT gratis testen">Print</a>
		<a id="button_digital" href="https://www.zeitabo.de/digital/die-zeit-im-abo/digital-abo/kostenloses-probe-abo.html?r=grbei" title="Die digitale ZEIT gratis testen">Digital</a>

		<div class="clear"></div>
	</div>
	<!-- /#content -->



	<div id="footer">
		<a href="http://www.zeitabo.de/impressum.html" class="popup" target="_blank">Impressum</a> | <a href="http://www.zeitabo.de/agb.html" target="_blank" class="popup">AGB</a>
	</div>
	<!-- /#footer -->

</div>
<!-- /#main -->


<? /*= $this->render('pixel_webtrekk.tpl.php') */ ?>
<? /*= $this->render('pixel_ivw.tpl.php') */ ?>
<? /*= $this->render('pixel_analytics.tpl.php') */ ?>


</body>
</html>