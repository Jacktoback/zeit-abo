<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>DIE ZEIT Testwochen - Heute kostenlos sichern</title>
	<meta name="description" content="Heute kostenlos: Testen Sie jetzt 4 Wochen lang gratis DIE ZEIT – Deutschlands führende Wochenzeitung mit dem Wichtigsten aus Politik, Wirtschaft, Wissen und Kultur – inklusive ZEITmagazin!">

	<meta name="keywords" content="DIE ZEIT, Das Original, Heute kostenlos, gratis testen, Testwochen, 4 Wochen, 4 Ausgaben, gratis lesen">


<link rel="SHORTCUT ICON" href="//leserservice.zeit.de/static/img/favicon.ico" />
<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone/css/reset.css" />
<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone/css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone/css/jquery-mobile.css" media="all">

<meta name="viewport" media="@media screen and (orientation: portrait)"  content="initial-scale=1, user-scalable=no">

<!---[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone/css/ie6.css" />
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone/css/ie7.css" />
<![endif]-->

<script type="text/javascript" src="//leserservice.zeit.de/angebote/smartphone/js/jquery.js"></script>
<script type="text/javascript" src="//leserservice.zeit.de/angebote/smartphone/js/trivials.js"></script>
<script type="text/javascript" src="//leserservice.zeit.de/angebote/smartphone/js/jquery-mobile-simpler.js"></script>
<script type="text/javascript">
	function popup (url) {
		fenster = window.open(url, "fenster1", "width=610,height=550,status=no,scrollbars=yes,resizable=no");
		fenster.focus();
	}
	
	function toggleCountries(){
		var country = $('#country option:selected').attr('id');
		$('.country').hide();
		
		$('#zipcode').attr('maxlength', 10);
		
		if(country){
			$('.'+country).show();
			
			if(country == 'country_de'){
				$('#zipcode').attr('maxlength', 5);
			} else if((country == 'country_at') || (country == 'country_ch')){
				$('#zipcode').attr('maxlength', 4);
			}

		} else {
			$('.country_eu').show();
		}

	}
	
	$(document).ready(function(){
		$('a.popup').click(function(){
			var url = $(this).attr('href');
			fenster = window.open(url, "fenster1", "width=610,height=550,status=no,scrollbars=yes,resizable=no");
			fenster.focus();
			return false;
		});
		
		$('#country').change(toggleCountries).trigger('change').blur(toggleCountries);	


		 // damit funktionieren die AGB/LB-Links sowohl bei aktiviertem wie auch bei deaktiviertem JavaScript.
		$('a.no_js_fix').each(function(){
			var agb_or_lb = 
			  ( $(this).hasClass('agb') )
			    ? 'AGB'
			    : 'LB';

			$(this)
			  .attr('href', '#'+ agb_or_lb)
			  .attr('target', null)
			  .click(function(){
				show_legal_info('#secret_'+ agb_or_lb +'_space');
			  });
		});

	});
</script>

</head>

<body id="abo" >
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-8FR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
{'gtm.start': new Date().getTime(),event:'gtm.js'}
);var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-8FR8');</script>
<!-- End Google Tag Manager -->


<img id="background_magic" src="//leserservice.zeit.de/angebote/smartphone/img/hintergrund.jpg" />

<div id="main">
	<div id="header">
    </div>
    <!-- /#header -->
	
	
	<div id="content">
	
					<form id="formular" action="//leserservice.zeit.de/angebote/smartphone/index.php#formular" method="get" style="width: 280px; padding-top: 25px;">
                    <input type="hidden" name="abo" value="2" />
        <? if(isset($_REQUEST['r'])): ?>
        	<input type="hidden" name="r" value="<?= $_REQUEST['r'] ?>" />
        <? endif ?>
		<input type="hidden" name="action" value="save_abo" />
		<input type="hidden" name="abotype" value="n4" />
        <input type="hidden" name="addon" value="" />

		<h2 style="font-size: 18px;">Ja, ich möchte DIE ZEIT vier Wochen kostenlos zur Probe.</h2>


      
      <fieldset>
          <label class="">Anrede<span class="pink">*</span></label>
          <select class="text span4" name="salutation" id="salutation">
              <optgroup label="Bitte wählen">
                  <option selected="selected" value="Frau">Frau</option>
                  <option  value="Herr">Herr</option>
              </optgroup>              
          </select>
      </fieldset>
              
      <fieldset>
          <label class="">Titel</label>
          <select class="text span4" name="title">
              
                  <option selected="selected" value=""></option>
                  <option  value="Dr.">Dr.</option>
                  <option  value="Prof.">Prof.</option>
              
              
          </select>
      </fieldset>


      <fieldset>
          <label class="text ">Vorname<span class="pink">*</span></label>
          <input class="text span4" maxlength="256" type="text" name="forename" value="" />
      </fieldset>

      <fieldset>
          <label class="text ">Nachname<span class="pink">*</span></label>
          <input class="text span4" maxlength="256" type="text" name="lastname" value="" />
      </fieldset>

      <fieldset>
          <label class="text ">Stra&szlig;e/Nr.<span class="pink">*</span></label>
          <input class="text span4" maxlength="256" type="text" name="address" value="" />
      </fieldset>
      
      <fieldset>
          <label class="text ">Adresszusatz</label>
          <input class="text span4" maxlength="256" type="text" name="address2" value="" />
      </fieldset>
      
      <fieldset>
          <label class="text ">PLZ/Ort<span class="pink">*</span></label>
          <input id="zipcode" class="text span1" maxlength="256" type="tel" name="zipcode" value="" />
          <input class="text span3" maxlength="256" type="text" name="city" value="" />
      </fieldset>
      
      <fieldset>
          <label class="">Land<span class="pink">*</span></label>
          <select class="text span4" name="country" id="country">
              <optgroup label="Bitte treffen Sie eine Auswahl">
                  <option selected="selected" id="country_de" value="D">Deutschland</option>
                  <option  id="country_at" value="AT">Österreich</option>
                  <option  id="country_ch" value="CH">Schweiz</option>
              </optgroup>
              
          </select>
      </fieldset>
      
      <fieldset>
          <label class="text ">E-Mail-Adresse<span class="pink">*</span></label>
          <input class="text span4" maxlength="256" type="email" name="email" value="" />
      </fieldset>
      
      
         <div id="optin_abo">
			<p class="country country_de">Ich genieße 4&nbsp;Wochen lang DIE&nbsp;ZEIT gratis. Wenn ich mich nach der 3.&nbsp;Ausgabe nicht melde, erhalte ich DIE&nbsp;ZEIT für zzt. 4,20&nbsp;&euro; pro Ausgabe statt 4,50&nbsp;&euro; im Einzelkauf und kann jederzeit kündigen.</p>

			<p class="country country_at" style="display:none;">Ich genieße 4&nbsp;Wochen lang DIE&nbsp;ZEIT gratis. Wenn ich mich nach der 3.&nbsp;Ausgabe nicht melde, erhalte ich DIE&nbsp;ZEIT für zzt. 4,25&nbsp;&euro; pro Ausgabe statt 4,60&nbsp;&euro; im Einzelkauf und kann jederzeit kündigen.</p>

			<p class="country country_ch" style="display:none;">Ich genieße 4&nbsp;Wochen lang DIE&nbsp;ZEIT gratis. Wenn ich mich nach der 3.&nbsp;Ausgabe nicht melde, erhalte ich DIE&nbsp;ZEIT für zzt. sFr&nbsp;6.20 pro Ausgabe statt sFr&nbsp;7.30 im Einzelkauf und kann jederzeit kündigen.</p>
		</div>
        <!-- /#optin_abo -->
      
      
      
      
      <fieldset class="gap">
          <label class="checkbox ">
              <input type="checkbox" name="student" value="ja"   />
              <span>Ich bin <strong>Student.</strong></span>
          </label>
      </fieldset>
      
      
      
      
      <fieldset style="width: 250px;">
          <label class="checkbox ">
              <input type="checkbox" name="optin_legals" value="ja"   />
			  <span class="country country_de">Ja, über die <a href="//leserservice.zeit.de/static/agb.html" class="agb no_js_fix" target="_blank">AGB</a> und <a href="//leserservice.zeit.de/static/lieferbedingungen_n4.html" class="lb no_js_fix" target="_blank">Lieferbedingungen</a> habe ich mich informiert und bin damit einverstanden.<span class="pink">*</span></span>

			  <span class="country country_at" style="display:none;">Ja, über die <a href="//leserservice.zeit.de/static/agb.html" class="agb no_js_fix" target="_blank">AGB</a> und <a href="//leserservice.zeit.de/static/lieferbedingungen_n4_at.html" class="lb no_js_fix" target="_blank">Lieferbedingungen</a> habe ich mich informiert und bin damit einverstanden.<span class="pink">*</span></span>

			  <span class="country country_ch" style="display:none;">Ja, über die <a href="//leserservice.zeit.de/static/agb.html" class="agb no_js_fix" target="_blank">AGB</a> und <a href="//leserservice.zeit.de/static/lieferbedingungen_n4_ch.html" class="lb no_js_fix" target="_blank">Lieferbedingungen</a> habe ich mich informiert und bin damit einverstanden.<span class="pink">*</span></span>

              
          </label>
      </fieldset>
      
      <input class="button btn_bestellen" type="image" src="//leserservice.zeit.de/angebote/smartphone/img/btn_bestellen.png" alt="" style="margin-left: -10px;" />
      <p id="hinweis"><span class="pink">*</span> <small>Pflichtfelder</small></p>

		<div class="clear"></div>
        
        <div id="secret_AGB_space" class="secret_spaces" style="display: none;">
        	<a name="AGB">
			<h2>Allgemeine Geschäftsbedingungen</h2>
            </a>

			<p>Allgemeine Geschäftsbedingungen für Abonnement-Verträge zwischen Abonnement-Besteller und der Zeitverlag Gerd Bucerius GmbH & Co. KG</p><br />

			<p><strong>§ 1 Geltung der AGB</strong></p>
                  <p>Die Bestellung und Durchführung sämtlicher Abonnement-Verträge erfolgt ausschließlich gemäß den nachfolgenden Allgemeinen Geschäftsbedingungen der Zeitverlag Gerd Bucerius GmbH & Co. KG sowie den besonderen vertraglichen Bestimmungen zu den jeweiligen Abonnement-Angeboten, über die der Kunde gesondert informiert wird. Mündliche Nebenabreden wurden nicht getroffen. Die AGB und Ihr elektronisches Bestellformular können von Ihnen auf Ihrem Rechner abgespeichert und/oder ausgedruckt werden.</p><br />
                  <p><strong>§ 2 Vertragsschluss</strong></p>
                  <p>Der Vertragspartner des Bestellers ist:</p>
                  <p><strong>Zeitverlag Gerd Bucerius GmbH & Co.&nbsp;KG</strong><br />
                  Geschäftsführer Dr. Rainer Esser <br />
                  Pressehaus,  Buceriusstraße, Eingang Speersort 1, <br />
                  20095 Hamburg<br />
                 <p>Telefon: 040 / 42 23 70 70, Fax:  040 / 42 23 70 90, E-Mail: <a href="mailto:diezeit@zeit.de">diezeit@zeit.de</a>, eingetragen beim Amtsgericht Hamburg unter HRA 91123, Ust-IdNr. DE189342458.</p>
                  <p>Die Angaben in unseren Angeboten sind freibleibend. Der Vertrag über das von Ihnen ausgewählte Abonnement wird geschlossen, wenn Ihnen eine schriftliche Bestätigung Ihrer Abonnement-Bestellung zugeht.</p><br />
                  
                  <p><strong>§ 3 Widerrufsbelehrung</strong></p>
                  
                  <p><strong>a) Widerrufsrecht:</strong> Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen. Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die erste Ware in Besitz genommen haben bzw. hat. Der Widerruf ist zu richten an: DPV Deutscher Pressevertrieb GmbH, Düsternstr. 1-3, 20355 Hamburg, Tel.: 040 - 42 23 70 70, mittels einer eindeutigen Erklärung (z. B. ein mit der Post versandter Brief, Telefax oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen. Sie können dafür das nachfolgende Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist. Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p>
                  
                  <p><strong>b) Folgen des Widerrufs:</strong> Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Die Rücksendung der erhaltenen Ware ist nicht erforderlich. Sollten Sie die Ware dennoch an uns zurücksenden, so tragen Sie die unmittelbaren Kosten der Rücksendung.</p>


				<p><strong>c) Widerrufsformular</strong></p>
                
                <p>Wenn Sie den Vertrag widerrufen wollen, können Sie das <a href="//leserservice.zeit.de/static/ZEIT_Widerrufsformular.pdf" target="_blank">hier abrufbare</a> Formular nutzen und zurücksenden an:<br />DPV Deutscher Pressevertrieb GmbH, Düsternstr. 1-3, 20355 Hamburg</p><br />
                  
                  
                  <p><strong>§ 4 Lieferung</strong></p>
                  <p>Sofern nicht anders vereinbart, erfolgt die Lieferung ab Lager an die vom Besteller angegebene Lieferadresse.</p><br />
                  <p><strong>§ 5 Zahlungsbedingungen</strong></p>
                  <p>Mit Zugang der Rechnung wird der Abonnementspreis fällig. Der Kaufpreis ist auf das auf der Rechnung genannte Konto zu überweisen.</p><br />
                  <p><strong>§ 6 Datenschutz</strong></p>
                  <p>Alle personenbezogenen Daten werden grundsätzlich vertraulich behandelt und Ihre schutzwürdigen Belange entsprechend den gesetzlichen Vorgaben streng berücksichtigt.
Die für die Geschäftsabwicklung notwendigen Daten werden gespeichert und im Rahmen der Bestellabwicklung ggf. an uns verbundene Unternehmen oder unsere Dienstleistungspartner weitergegeben.</p>
                  <p>Personenbezogene und sonstige Daten werden von uns nach den Vorschriften des Bundesdatenschutzgesetzes (BDSG) gespeichert und verarbeitet. Sofern wir im Einzelfall im Rahmen Ihres Vertragsverhältnisses auch Mediendienste anbieten sollten, gelten ergänzend die Vorschriften des Telemediengesetzes (TMG).</p>
                  <p>Sie haben jederzeit ein Recht auf kostenlose Auskunft, Berichtigung, Sperrung und Löschung Ihrer gespeicherten Daten. Bitte senden Sie uns Ihr Verlangen per Post an Zeitverlag Gerd Bucerius GmbH & Co. KG, Postfach, 20079 Hamburg oder per E-Mail an <a href="mailto:adress-service@zeit.de">adress-service@zeit.de</a>, wenn Sie von uns keine weiteren Informationen zugesandt bekommen möchten.</p><br />
                  <p><strong>§ 7 Verschiedenes</strong></p>
                  <p>Wenn die Besteller Kaufleute, juristische Personen des öffentlichen Rechts oder öffentlich-rechtliche Sondervermögen sind, gilt als Gerichtsstand Hamburg.</p>
                  <p>Ein Recht zur Aufrechnung steht dem Besteller nur zu, wenn die Gegenansprüche rechtskräftig festgestellt oder unbestritten sind. Ein Zurückbehaltungsrecht steht dem Besteller nur insoweit zu, als der Gegenanspruch auf demselben Vertragsverhältnis beruht.</p>
                  <p>Sind oder werden einzelne Bestimmungen dieser Allgemeinen Geschäftsbedingungen und/oder des durch sie ergänzten Vertrages unwirksam, so wird dadurch die Wirksamkeit der anderen Bestimmungen nicht berührt, und der Vertrag und diese Allgemeinen Geschäftsbedingungen bleiben im Übrigen für beide Teile wirksam.</p>

      <input class="button btn_bestellen" type="image" src="//leserservice.zeit.de/angebote/smartphone/img/btn_bestellen.png" alt="" />
        </div>
        <!-- /#secret_AGB_space -->



      <div id="secret_LB_space" class="secret_spaces" style="display: none;">
        <a name="LB">&nbsp;</a>

        <div class="country country_de" style="display: none;">
			<h2 class="liefer_header">Lieferbedingungen</h2>

			  <p>Ich erhalte DIE&nbsp;ZEIT von der nächsterreichbaren Ausgabe an 4&nbsp;Wochen lang gratis frei Haus sowie kostenlosen Zugang zum Komplett-Archiv der ZEIT und den Newsletter ZEIT-Brief. Wenn mir DIE&nbsp;ZEIT gefällt, brauche ich nichts weiter zu tun. Ich erhalte dann DIE&nbsp;ZEIT inkl. ZEITmagazin 52x im Jahr für zzt. nur 4,20&nbsp;&euro; pro Ausgabe frei Haus statt 4,50&nbsp;&euro; im Einzelverkauf und spare rund 7&nbsp;%. Ansonsten genügt eine kurze Mitteilung nach Erhalt der 3.&nbsp;Ausgabe an den Leser-Service. Mein Abonnement ist jederzeit kündbar. (Angebot nur im Inland gültig. Auslandspreise auf Anfrage.)<br /><br />
 
Wenn ich Student bin erhalte ich DIE&nbsp;ZEIT nach dem Test 52x im Jahr für zzt. 2,65&nbsp;&euro; pro Ausgabe inkl. 6x im Jahr ZEIT&nbsp;CAMPUS. Ich spare &uuml;ber 41&nbsp;%. Meine Immatrikulationsbescheinigung sende ich per Post an DIE&nbsp;ZEIT, Leserservice, 20080&nbsp;Hamburg, Deutschland.<br /><br />

				Diese Bestellung kann selbstverständlich binnen 14&nbsp;Tagen ab Erhalt der 1.&nbsp;Ausgabe ohne Angabe von Gründen formlos widerrufen werden. Ihr Abonnement ist natürlich auch danach jederzeit kündbar. Ausführliche Informationen zum Widerrufsrecht unter <a href="http://www.zeit.de/wr" target="_blank">www.zeit.de/wr</a>.</p> 
        </div>
        <!-- /.country_de -->
        
        <div class="country country_at" style="display: none;">
			<h2 class="liefer_header">Lieferbedingungen</h2>

			  <p>Ich erhalte DIE&nbsp;ZEIT mit den Österreich-Extraseiten von der nächsterreichbaren Ausgabe an 4&nbsp;Wochen lang gratis frei Haus sowie kostenlosen Zugang zum Komplett-Archiv der ZEIT und den Newsletter ZEIT-Brief.  Wenn mir DIE&nbsp;ZEIT gefällt, brauche ich nichts weiter zu tun. Ich erhalte dann DIE&nbsp;ZEIT inkl. ZEITmagazin 52x im Jahr für zzt. nur 4,25&nbsp;&euro; pro Ausgabe frei Haus statt 4,60&nbsp;&euro; im Einzelverkauf und spare &uuml;ber 7&nbsp;%. Ansonsten genügt eine kurze Mitteilung nach Erhalt der 3.&nbsp;Ausgabe an den Leser-Service. Mein Abonnement ist jederzeit kündbar.<br /><br />
 
Wenn ich Student bin erhalte ich DIE&nbsp;ZEIT nach dem Test 52x im Jahr für zzt. 3,05&nbsp;&euro; pro Ausgabe inkl. 6x im Jahr ZEIT&nbsp;CAMPUS. Ich spare &uuml;ber 33&nbsp;%. Meine Immatrikulationsbescheinigung sende ich per Post an DIE&nbsp;ZEIT, Leserservice, 20080&nbsp;Hamburg, Deutschland.<br /><br />

				Diese Bestellung kann selbstverständlich binnen 14&nbsp;Tagen ab Erhalt der 1.&nbsp;Ausgabe ohne Angabe von Gründen formlos widerrufen werden. Ihr Abonnement ist natürlich auch danach jederzeit kündbar. Ausführliche Informationen zum Widerrufsrecht unter <a href="http://www.zeit.de/wr" target="_blank">www.zeit.de/wr</a>.</p> 
        </div>
        <!-- /.country_at -->
        
        <div class="country country_ch" style="display: none;">
			<h2 class="liefer_header">Lieferbedingungen</h2>

			  <p>Ich erhalte DIE&nbsp;ZEIT mit den Schweiz-Extraseiten von der nächsterreichbaren Ausgabe an 4&nbsp;Wochen lang gratis frei Haus sowie kostenlosen Zugang zum Komplett-Archiv der ZEIT und den Newsletter ZEIT-Brief. Wenn mir DIE&nbsp;ZEIT gefällt, brauche ich nichts weiter zu tun. Ich erhalte dann DIE&nbsp;ZEIT inkl. ZEITmagazin 52x im Jahr für zzt. nur sFr&nbsp;6.20 pro Ausgabe frei Haus statt sFr&nbsp;7.30 im Einzelverkauf und spare 15&nbsp;%. Ansonsten genügt eine kurze Mitteilung nach Erhalt der 3.&nbsp;Ausgabe an den Leser-Service. Mein Abonnement ist jederzeit kündbar.<br /><br />
 
Wenn ich Student bin erhalte ich DIE&nbsp;ZEIT nach dem Test 52x im Jahr für zzt. sFr&nbsp;5.&ndash; pro Ausgabe inkl. 6x im Jahr ZEIT&nbsp;CAMPUS. Ich spare &uuml;ber 31&nbsp;%. Meine Immatrikulationsbescheinigung sende ich per Post an DIE&nbsp;ZEIT, Leserservice, 20080&nbsp;Hamburg, Deutschland.<br /><br />

				Diese Bestellung kann selbstverständlich binnen 14&nbsp;Tagen ab Erhalt der 1.&nbsp;Ausgabe ohne Angabe von Gründen formlos widerrufen werden. Ihr Abonnement ist natürlich auch danach jederzeit kündbar. Ausführliche Informationen zum Widerrufsrecht unter <a href="http://www.zeit.de/wr" target="_blank">www.zeit.de/wr</a>.</p> 
        </div>
        <!-- /.country_ch -->

      <input class="button btn_bestellen" type="image" src="//leserservice.zeit.de/angebote/smartphone/img/btn_bestellen.png" alt="" />
      </div>
      <!-- /#secret_LB_space -->

</form>

<script type="text/javascript"><!--

 $(document).ready( function(){
//	adjust_form_fringes();
 });

//--></script>
			</div>
	<!-- /#content -->
	
	<div id="footer" style="display: none;">
    	<a href="//leserservice.zeit.de/static/impressum.html" class="popup" target="_blank">Impressum</a> | <a href="//leserservice.zeit.de/static/agb.html" target="_blank" class="popup">AGB</a> | <a href="http://www.zeitabo.de" target="_blank">www.zeitabo.de</a>
        
        	</div>
	<!-- /#footer -->
	
</div>
<!-- /#main -->


<script type="text/javascript">
	var wt_page_id = "angebot_smartphone";
</script>

</body>
</html>