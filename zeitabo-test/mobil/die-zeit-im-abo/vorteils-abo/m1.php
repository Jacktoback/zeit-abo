<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>DIE ZEIT - Abonnement - Vorteils-Abo</title>
	<meta name="description" content="Lesen Sie die preisgekrönte Wochenzeitung DIE ZEIT inkl. ZEITmagazin und allen Sonderbeilagen, und erhalten Sie dauerhaft 11 % Rabatt!">

	<meta name="keywords" content="ZEIT Vorteilsabo, Jahresabo, Jahresabonnement, Vollabo">


<link rel="SHORTCUT ICON" href="//leserservice.zeit.de/static/img/favicon.ico" />
<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/css/reset.css" />
<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/css/jquery-mobile.css" media="all">

<meta name="viewport" media="@media screen and (orientation: portrait)"  content="initial-scale=1, user-scalable=no">

<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/css/ie6.css" />
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/css/ie7.css" />
<![endif]-->

<script type="text/javascript" src="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/js/jquery.js"></script>
<script type="text/javascript" src="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/js/trivials.js"></script>
<script type="text/javascript" src="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/js/jquery-mobile-simpler.js"></script>
<script type="text/javascript">
	function popup (url) {
		fenster = window.open(url, "fenster1", "width=610,height=550,status=no,scrollbars=yes,resizable=no");
		fenster.focus();
	}
	
	function toggleCountries(){
		var country = $('#country option:selected').attr('id');
		$('.country').hide();
		
		$('#zipcode').attr('maxlength', 10);
		
		if(country){
			$('.'+country).show();
			
			if(country == 'country_de'){
				$('#zipcode').attr('maxlength', 5);
			} else if((country == 'country_at') || (country == 'country_ch')){
				$('#zipcode').attr('maxlength', 4);
			}

		} else {
			$('.country_eu').show();
		}

	}
	
	$(document).ready(function(){
		$('a.popup').click(function(){
			var url = $(this).attr('href');
			fenster = window.open(url, "fenster1", "width=610,height=550,status=no,scrollbars=yes,resizable=no");
			fenster.focus();
			return false;
		});
		
		$('#country').change(toggleCountries).trigger('change').blur(toggleCountries);	


		 // damit funktionieren die AGB/LB-Links sowohl bei aktiviertem wie auch bei deaktiviertem JavaScript.
		$('a.no_js_fix').each(function(){
			var agb_or_lb = 
			  ( $(this).hasClass('agb') )
			    ? 'AGB'
			    : 'LB';

			$(this)
			  .attr('href', '#'+ agb_or_lb)
			  .attr('target', null)
			  .click(function(){
				show_legal_info('#secret_'+ agb_or_lb +'_space');
			  });
		});


	});
</script>

</head>

<body id="start">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-8FR8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
{'gtm.start': new Date().getTime(),event:'gtm.js'}
);var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-8FR8');</script>
<!-- End Google Tag Manager -->


<img id="background_magic" src="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/img/hintergrund.jpg" />

<div id="main">
	<div id="header">
    </div>
    <!-- /#header -->
	
	
	<div id="content">
	
					
    
        <h2 style="margin-left: 20px;">DIE ZEIT − aktuell,<br />tiefgehend, vielfältig</h2>
    
        <p style="max-width: 385px; margin-left: 20px; padding-right: 33px;">Deutschlands größte Qualitätszeitung bietet Ihnen sorgsam recherchierte Hintergrundinformationen, präzise Analysen, herausfordernde Kommentare und konträre Sichtweisen.</p>
        
        <div style="width: 240px; margin-left: 20px;">
        	<a href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/index.php?abo<?= isset($_REQUEST['r']) ? '&r='.$_REQUEST['r'] : '' ?>">
            	<img src="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/img/text_lesen-sie.png" alt="Lesen Sie jetzt DIE ZEIT und sparen Sie rund 7%" style="margin-top: 25px; margin-bottom: 20px;" />
            </a>
        </div>
    

		<div style="width: 312px; margin-left: -10px;">
	        <a href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/index.php?ressorts<?= isset($_REQUEST['r']) ? '&r='.$_REQUEST['r'] : '' ?>" class="button btn_about">Über DIE ZEIT</a>
    	    <a href="//leserservice.zeit.de/angebote/smartphone-vorteilsabo/index.php?abo<?= isset($_REQUEST['r']) ? '&r='.$_REQUEST['r'] : '' ?>" class="button btn_bestellen">Jetzt Bestellen</a>
        </div>
    
        <div class="clear" style="margin-bottom: 10px;"></div>

       	<a class="link_desktop" href="http://www.zeitabo.de/die-zeit-im-abo/vorteils-abo.html">Zur Desktop-Version</a>
			</div>
	<!-- /#content -->
	
	<div id="footer" style="display: none;">
    	<a href="//leserservice.zeit.de/static/impressum.html" class="popup" target="_blank">Impressum</a> | <a href="//leserservice.zeit.de/static/agb.html" target="_blank" class="popup">AGB</a> | <a href="http://www.zeitabo.de" target="_blank">www.zeitabo.de</a>
        
        	</div>
	<!-- /#footer -->
	
</div>
<!-- /#main -->


<script type="text/javascript">
	var wt_page_id = "angebot_mobile_festabo";
</script>

</body>
</html>