<?php
return array(
	'BE' => array(
		'disable_exec_function' => '0',
		'fileCreateMask' => '0664',
		'folderCreateMask' => '0775',
		'installToolPassword' => '$1$r.aE2Uwq$r4Np1YDmoQYkQ832r9Ydd1',
		'lockSSL' => '0',
		'loginSecurityLevel' => 'rsa',
		'maxFileSize' => '20000',
		'versionNumberInFilename' => '0',
	),
	'DB' => array(
		'database' => 'zeitabo3',
		'extTablesDefinitionScript' => 'extTables.php',
		'host' => 'localhost',
		'password' => '123456',
		'socket' => '',
		'username' => 'root',
	),
	'EXT' => array(
		'extConf' => array(
			'cwmobileredirect' => 'a:18:{s:12:"standard_url";s:14:"www.zeitabo.de";s:10:"mobile_url";s:14:"www.zeitabo.de";s:12:"maintain_url";s:1:"0";s:10:"use_cookie";s:1:"0";s:11:"cookie_name";s:19:"tx_cwmobileredirect";s:15:"cookie_lifetime";s:4:"3600";s:14:"is_mobile_name";s:8:"isMobile";s:14:"no_mobile_name";s:8:"noMobile";s:14:"use_typoscript";s:1:"1";s:5:"debug";s:1:"0";s:9:"error_log";s:0:"";s:19:"add_value_to_params";s:1:"1";s:17:"detection_enabled";s:1:"1";s:19:"redirection_enabled";s:1:"1";s:11:"http_status";s:15:"HTTP_STATUS_303";s:7:"regexp1";s:319:"(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino";s:7:"regexp2";s:1606:"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-";s:19:"redirect_exceptions";s:9:"typo3conf";}',
			'formhandler' => 'a:0:{}',
			'https_enforcer' => 'a:0:{}',
			'phpmyadmin' => 'a:5:{s:12:"hideOtherDBs";s:1:"1";s:9:"uploadDir";s:21:"uploads/tx_phpmyadmin";s:10:"allowedIps";s:0:"";s:12:"useDevIpMask";s:1:"0";s:10:"ajaxEnable";s:1:"1";}',
			'realurl' => 'a:5:{s:10:"configFile";s:26:"typo3conf/realurl_conf.php";s:14:"enableAutoConf";s:1:"1";s:14:"autoConfFormat";s:1:"0";s:12:"enableDevLog";s:1:"0";s:19:"enableChashUrlDebug";s:1:"0";}',
			'rsaauth' => 'a:1:{s:18:"temporaryDirectory";s:8:"/var/opt";}',
			'saltedpasswords' => 'a:2:{s:3:"FE.";a:2:{s:7:"enabled";s:1:"0";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}s:3:"BE.";a:1:{s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}}',
			'seo_basics' => 'a:2:{s:10:"xmlSitemap";s:1:"0";s:16:"sourceFormatting";s:1:"1";}',
			'static_info_tables' => 'a:3:{s:13:"enableManager";s:1:"0";s:7:"charset";s:5:"utf-8";s:12:"usePatch1822";s:1:"0";}',
			'static_info_tables_de' => 'a:1:{s:5:"dummy";s:1:"1";}',
			't3quixplorer' => 'a:10:{s:9:"no_access";s:5:"^\\.ht";s:11:"show_hidden";s:1:"1";s:8:"home_url";s:0:"";s:8:"home_dir";s:0:"";s:11:"show_thumbs";s:1:"1";s:15:"textarea_height";s:2:"30";s:14:"auto_highlight";s:1:"1";s:12:"disable_text";s:1:"1";s:12:"editable_ext";s:215:"\\.phpcron$|\\.ts$|\\.tmpl$|\\.txt$|\\.php$|\\.php3$|\\.phtml$|\\.inc$|\\.sql$|\\.pl$|\\.htm$|\\.html$|\\.shtml$|\\.dhtml$|\\.xml$|\\.js$|\\.css$|\\.cgi$|\\.cpp$\\.c$|\\.cc$|\\.cxx$|\\.hpp$|\\.h$|\\.pas$|\\.p$|\\.java$|\\.py$|\\.sh$\\.tcl$|\\.tk$";s:8:"php_path";s:3:"php";}',
			'templavoila' => 'a:3:{s:7:"enable.";a:3:{s:13:"oldPageModule";s:1:"1";s:19:"selectDataStructure";s:1:"0";s:15:"renderFCEHeader";s:1:"0";}s:9:"staticDS.";a:3:{s:6:"enable";s:1:"0";s:8:"path_fce";s:27:"fileadmin/templates/ds/fce/";s:9:"path_page";s:28:"fileadmin/templates/ds/page/";}s:13:"updateMessage";s:1:"0";}',
		),
	),
	'FE' => array(
		'pageOverlayFields' => 'uid,title,subtitle,nav_title,keywords,description,abstract,author,author_email,url,urltype,shortcut,shortcut_mode,tx_realurl_pathsegment',
	),
	'GFX' => array(
		'TTFdpi' => '96',
		'gdlib_png' => '1',
		'im_noScaleUp' => '1',
		'im_path' => '/usr/bin/',
		'im_path_lzw' => '/usr/bin/',
		'im_v5effects' => -1,
		'im_version_5' => 'gm',
		'jpg_quality' => '100',
		'png_truecolor' => '1',
	),
	'INSTALL' => array(
		'wizardDone' => array(
			'TYPO3\CMS\Install\Updates\FilemountUpdateWizard' => 1,
			'TYPO3\CMS\Install\Updates\TceformsUpdateWizard' => 'tt_content:image,pages:media,pages_language_overlay:media',
			'TYPO3\CMS\Install\Updates\TruncateSysFileProcessedFileTable' => 1,
		),
	),
	'MAIL' => array(
		'defaultMailFromAddress' => 'noreply@zeit.de',
		'defaultMailFromName' => 'DIE ZEIT',
	),
	'SYS' => array(
		'compat_version' => '6.2',
		'devIPmask' => '*',
		'displayErrors' => '1',
		'enableDeprecationLog' => '',
		'encryptionKey' => 'f200a20ebe4515e482ba12cc8ede5c43',
		'sitename' => 'www.zeitabo.de',
	),
);
?>