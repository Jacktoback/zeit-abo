<?php

########################################################################
# Extension Manager/Repository config file for ext: "yejj_swfobject"
#
# Auto generated 30-01-2009 20:44
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Flash Player Integration (SWF Object)',
	'description' => 'This plugin inserts a Flash movie on pages using the SWFObject method. The extension is based on rb_flashobject, but now includes the latest SWFObject version with static and dynamic publishing and provides solutions for dynamic and alternative content.',
	'category' => 'plugin',
	'shy' => 0,
	'version' => '1.0.5',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Visay Keo, Violeng Mam',
	'author_email' => 'web@yejj.com',
	'author_company' => 'Web Department, Yejj Info Ltd., www.yejj.com',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:18:{s:12:"ext_icon.gif";s:4:"9319";s:17:"ext_localconf.php";s:4:"4c23";s:14:"ext_tables.php";s:4:"18cd";s:14:"ext_tables.sql";s:4:"d91d";s:28:"ext_typoscript_constants.txt";s:4:"23c7";s:24:"ext_typoscript_setup.txt";s:4:"94c1";s:15:"flexform_ds.xml";s:4:"6bc5";s:31:"icon_tx_yejjswfobject_movie.gif";s:4:"9d90";s:13:"locallang.php";s:4:"1bd1";s:16:"locallang_db.php";s:4:"70cf";s:7:"tca.php";s:4:"1ff4";s:14:"doc/manual.sxw";s:4:"2d59";s:15:"js/swfobject.js";s:4:"eaa5";s:14:"pi1/ce_wiz.gif";s:4:"9319";s:34:"pi1/class.tx_yejjswfobject_pi1.php";s:4:"031e";s:42:"pi1/class.tx_yejjswfobject_pi1_wizicon.php";s:4:"cd7f";s:17:"pi1/locallang.php";s:4:"ce39";s:22:"res/expressInstall.swf";s:4:"204f";}',
);

?>