<?php
/***************************************************************
*  Copyright notice
*  
*  (c) 2008 Visay Keo, Violeng Mam (web[at]yejj.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************
*
*  This script is a continuation of the work initiated by Richard Bausek from his extension rb_flashobject.
*
***************************************************************/
/** 
 * Plugin 'Flash Movie' for the 'yejj_swfobject' extension.
 *
 * @author	Visay Keo, Violeng Mam <web[at]yejj.com>
 */


require_once(PATH_tslib."class.tslib_pibase.php");

class tx_yejjswfobject_pi1 extends tslib_pibase {
	var $prefixId = "tx_yejjswfobject_pi1";		// Same as class name
	var $scriptRelPath = "pi1/class.tx_yejjswfobject_pi1.php";	// Path to this script relative to the extension dir.
	var $extKey = "yejj_swfobject";	// The extension key.
	
	var $allowCaching;
	var $includeJS;
	var $params = array();
	var $flashVars = array();
	var $attribs = array();
	var $dbTable = 'tx_yejjswfobject_movie';
	var $movieFolder = 'uploads/tx_yejjswfobject/';
	var $idString;
	var $movieIDPrefix = 'swf_';
	
	var $extPath;
	var $defer = '';
	
	var $ffValue = array();

	function main($content,$conf)	{

		$this->init($conf);
	  // get flexform values
    $this->ffValue['ffPublishMethod'] = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'publishMethod', 'sDEF'))? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'publishMethod', 'sDEF') : $this->conf['ts_content.']['publishMethod'];
    $this->ffValue['expressInstall']  = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'expressInstall', 'sDEF') != '')? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'expressInstall', 'sDEF') : $this->conf['ts_content.']['expressInstall'];
   
    $this->ffValue['alternativeContent'] = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'alternativeContent', 'sDEF'))? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'alternativeContent', 'sDEF') : $this->conf['altContent'];
    
		if ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'flashContentId', 'sAttribute')) {
        $this->ffValue['flashContentId']  = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'flashContentId', 'sAttribute');
    } else {
        $this->ffValue['flashContentId'] = ($this->conf['ts_content.']['flashContentId'])? ($this->conf['ts_content.']['flashContentId']) : 'yejjFlashContent'.'_'.$this->idString;
    }
    $this->ffValue['attrName']        = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'attrName', 'sAttribute'))? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'attrName', 'sAttribute') : $this->conf['ts_content.']['attrName'];
    $this->ffValue['attrClass']       = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'attrClass', 'sAttribute'))? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'attrClass', 'sAttribute') : $this->conf['ts_content.']['attrClass'];
    $this->ffValue['attrAlign']       = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'attrAlign', 'sAttribute'))? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'attrAlign', 'sAttribute') : $this->conf['ts_content.']['attrAlign'];
    		   
	  $this->ffValue['bgColor']         = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'bgColor', 'sParameter'))? $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'bgColor', 'sParameter') : $this->conf['ts_content.']['bgColor'];
    
    if (($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'quality', 'sParameter')) != '') {
        $this->ffValue['quality'] = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'quality', 'sParameter') == 0)? 'high' : 'low';
    } else {
        $this->ffValue['quality'] = $this->conf['ts_content.']['quality'];
    }
    
    if (($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'displayMenu', 'sParameter')) != '') {
        $this->ffValue['displayMenu'] = ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'displayMenu', 'sParameter') == 0)? 'false' : 'true';
    } else {
        $this->ffValue['displayMenu'] = $this->conf['ts_content.']['displayMenu'];
    }
    
    $this->ffValue['flashVars']['ff'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'flashVariables', 'sFlashVars');
    $this->ffValue['flashVars']['ts'] = $this->conf['ts_content.']['flashVars.'];
    $this->ffValue['flashParams']['ff'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'flashParameter', 'sParameter');
    $this->ffValue['flashParams']['ts'] = $this->conf['ts_content.']['flashParams'];
	  $output = $this->writeFlashMovie($this->ffValue['ffPublishMethod']);
		return $output;	  
  }
	
	function init($conf) {
	
		$this->conf = $conf; //store configuration
		$this->pi_loadLL(); // Loading language-labels
		$this->pi_setPiVarDefaults(); // Set default piVars from TS
		$this->pi_initPIflexForm(); // Init FlexForm configuration for plugin

		// Configure caching
		$this->allowCaching = $this->conf['allowCaching']?1:0;
		if (!$this->allowCaching) {
			$GLOBALS['TSFE']->set_no_cache();
		}
		//prefix for flash movies (DOM ID)
		$this->movieIDPrefix = (isset($this->conf['movieIDPrefix'])) ? $this->conf['movieIDPrefix'] : $this->movieIDPrefix;
        		    		
		//set id of container div
		$this->idString = substr(md5 (uniqid (rand())), 0, 10);	
	}
	
	function writeFlashMovie ($ffPublishMethod) {
		$ffVars =array();
    if ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'movieSelection', 'sDEF')) {
        //get flexform values				    		
        $ffVars['selection'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'movieSelection', 'sDEF');       		
    	  
        //load the movie data
    	  $singleWhere = $this->dbTable.'.uid=' . intval($ffVars['selection']);
    	  $singleWhere .= $this->cObj->enableFields($this->dbTable);
    	  $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', $this->dbTable, $singleWhere);
    	  $movieValues = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
	  } elseif ($this->conf['flash_record.']['flashmovie']) {
	      $movieValues = $this->conf['flash_record.'];
	      $this->movieFolder = '';
	  } else {
        //get movie uid	
        $ffVars['selection'] = $this->conf['flashMovieUid'];
    	  
        //load the movie data
    	  $singleWhere = $this->dbTable.'.uid=' . intval($ffVars['selection']);
    	  $singleWhere .= $this->cObj->enableFields($this->dbTable);
    	  $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', $this->dbTable, $singleWhere);
    	  $movieValues = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);        
    }
	  
	  //check flexform for alternative content: if value isset get it from the flexform otherwise from movie record
    if($this->ffValue['alternativeContent']) {
        $movieValues['alternativecontent'] = $this->ffValue['alternativeContent'];
    }
	  
    $output = '';
	  if(is_array($movieValues)){
	  	$movieValues['flashmovie'] = $this->movieFolder.$movieValues['flashmovie'];	  	  
	  	$this->includeJSFile($movieValues);
	  	if ($ffPublishMethod == 0){
     		$output .= $this->writeAlternativeContent($movieValues);          		          		
     	}
		  else {
      	$output .= $this->writeStaticFlashJS($movieValues);
     	}
	  } else {
	  	//no flash movie record is there, display error message
			$output = $this->errorMessage();
	  }
	  return $output;	
	}
	
	function includeJSFile($contentConf){
		//get flash version
		$version = ($contentConf['requiredversion']) ? $contentConf['requiredversion'] : $this->conf['default.']['version'];
    
		$this->extPath = substr(t3lib_extMgm::extPath($this->extKey), strlen(PATH_site));
		if (!$GLOBALS['TSFE']->additionalHeaderData[$this->extKey]) {
      $jsFilePath = $this->extPath . 'js/swfobject.js';
		  $extraJS = '<script src="' . $jsFilePath . '" type="text/javascript"><!-- //--></script>';
		}
        
    $extraJS .= chr(10).'<script type="text/javascript">'.chr(10);
    if ($this->ffValue['ffPublishMethod'] == 1) {
    	if ($this->ffValue['expressInstall'] == 1) {
     		$extraJS .= 'swfobject.registerObject("'.$this->ffValue['flashContentId'].'", "'.$version.'", "'.$this->extPath.'res/expressInstall.swf");'.chr(10);
    	}
			else {
     		$extraJS .= 'swfobject.registerObject("'.$this->ffValue['flashContentId'].'", "'.$version.'");'.chr(10);
    	}
    } else {
			$extraJS .= $this->writeFlashJS($contentConf);
		} 
   	$extraJS .= '</script>'.chr(10);
    
    $GLOBALS['TSFE']->additionalHeaderData[$this->extKey] .= $extraJS;    	
	}
	
	function writeAlternativeContent ($contentConf){	
	  // if alternative content is set in the flexform
	  if ($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'alternativeContent', 'sDEF')) { 
      $this->conf['altContent.']['source'] = $contentConf['alternativecontent'];
      $altContent = '<div' . $this->pi_classParam("swf_altcontent") . ' id="' . $this->movieIDPrefix . $this->idString . '">';
      $altContent .= $this->cObj->RECORDS($this->conf['altContent.']);
      $altContent .= '</div>';
      return $altContent;
    }

    // if alternative content is set in TS
    if ($this->conf['altContent']) {
      $altContent = '<div' . $this->pi_classParam("swf_altcontent") . ' id="' . $this->movieIDPrefix . $this->idString . '">';
      $altContent .= $this->cObj->stdWrap($this->cObj->cObjGetSingle($this->conf['altContent'],$this->conf['altContent.']), $this->conf['altContent_stdWrap.']);
      $altContent .= '</div>';
		  return $altContent;       
    }	  
	  
	  // Otherwise get alternative content from flash movie record  
    $this->conf['altContent.']['source'] = $contentConf['alternativecontent'];
    $altContent = '<div' . $this->pi_classParam("swf_altcontent") . ' id="' . $this->movieIDPrefix . $this->idString . '">';
    $altContent .= $this->cObj->RECORDS($this->conf['altContent.']);
    $altContent .= '</div>';
    return $altContent;

	}
	
	function writeFlashJS ($contentConf) {	
		//get flash version
		$version = ($contentConf['requiredversion']) ? $contentConf['requiredversion'] : $this->conf['default.']['version'];
		
    //add additional variables		
		$this->addFlashVarsFromTS($this->ffValue['flashVars']['ts']);
		$this->addFlashVars($this->ffValue['flashVars']['ff']);
		$jsCode .= $this->writeFlashVars();
    
    foreach ($this->ffValue as $key => $value){
      if ($key == "bgColor" && $value != ""){
        $this->addFlashParam('bgcolor', $value);
      }
      
      if ($key == "quality" && $value !="high"){
        $this->addFlashParam('quality', $value);
      }
      
      if ($key == "displayMenu" && $value !="false"){
        $this->addFlashParam('menu', $value);
      }
      
      //add attributes
      if ($key == "flashContentId" && $value !=""){
        $this->addFlashAttrib('id', $value);
      }
      
      if ($key == "attrName" && $value !=""){
        $this->addFlashAttrib('name', $value);
      }
      
      if ($key == "attrClass" && $value !=""){
        $this->addFlashAttrib('styleclass', $value);
      }
    } 

		//add additional parameters
		$this->addFlashParams($this->ffValue['flashParams']['ts']);
		$this->addFlashParams($this->ffValue['flashParams']['ff']);
		$jsCode .= $this->writeFlashParams();
    	
		
    switch ($this->ffValue['attrAlign']){
			case 1:
			case 'middle':
				$this->addFlashAttrib('align','middle');
				break;
		 	case 2:
		 	case 'left':
				$this->addFlashAttrib('align','left');
				break;      
		 	case 3:
		 	case 'right':
				$this->addFlashAttrib('align','right');
		  	break;      
		  case 4:
		  case 'top':
				$this->addFlashAttrib('align','top');
				break;      
		  case 5:
		  case 'bottom':
				$this->addFlashAttrib('align','bottom');
				break;      
		  default:
				break;        
		}				      				 		 		
    $jsCode .= $this->writeFlashAtrribs();
		
		if ($this->ffValue['expressInstall'] == 1) {		
			$jsCode .= 'swfobject.embedSWF("' . $contentConf['flashmovie'] . '", "' . $this->movieIDPrefix.$this->idString . '", "' . $contentConf['width'] . '", "' . $contentConf['height'] . '", "' . $version . '", "'.$this->extPath.'res/expressInstall.swf", flashvars, params, attributes);' . chr(10);
    } else {
    	$jsCode .= 'swfobject.embedSWF("'. $contentConf['flashmovie'] .'", "'.$this->movieIDPrefix.$this->idString .'", "'.$contentConf['width'].'", "'.$contentConf['height'].'", "'.$version.'", "false", flashvars, params, attributes);' . chr(10);      
    }
		return $jsCode;
	}
	
	function writeStaticFlashJS ($contentConf){	
    $name = '';
    $align = '';
    $class = '';	
		//get flash version
		$version = ($contentConf['requiredversion']) ? $contentConf['requiredversion'] : $this->conf['default.']['version'];

		switch($this->ffValue['attrAlign']){
			case 0:
				$this->ffValue['attrAlign'] = '';
				break;
			case 1:
				$this->ffValue['attrAlign'] = 'middle';
				break;
			case 2:
				$this->ffValue['attrAlign'] = 'left';
				break;
			case 3:
				$this->ffValue['attrAlign'] = 'right';
				break;
			case 4:
				$this->ffValue['attrAlign'] = 'top';
				break;
			case 5:
				$this->ffValue['attrAlign'] = 'bottom';
				break;
			default:
				break;
		}
		
		if($this->ffValue['attrAlign']!= ""){
      $align = ' align="'.$this->ffValue['attrAlign'].'"';
    }
    
    if($this->ffValue['attrName']!= ""){
      $name = ' name="'.$this->ffValue['attrName'].'"';
    }
    
    if($this->ffValue['attrClass']!= ""){
      $class = ' class="'.$this->ffValue['attrClass'].'"';
    }
    
    //if()
		$jsCode = chr(10).'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'.$contentConf['width'].'" height="'.$contentConf['height'].'" id="'.$this->ffValue['flashContentId'].'"'.$name.$class.$align.'>' . chr(10);
    $jsCode .= '<param name="movie" value="'.$contentConf['flashmovie'].'" />' . chr(10);

	  $this->addFlashParam('bgcolor',$this->ffValue['bgColor']);
    $this->addFlashParam('quality',$this->ffValue['quality']);
	  $this->addFlashParam('menu',$this->ffValue['displayMenu']); 

		//add additional parameters
		$this->addFlashParams($this->ffValue['flashParams']['ts']);
		$this->addFlashParams($this->ffValue['flashParams']['ff']);
		$jsCode .= $this->writeFlashParams();
		
    //add additional variables
		$this->addFlashVarsFromTS($this->ffValue['flashVars']['ts']);
		$this->addFlashVars($this->ffValue['flashVars']['ff']);
		$jsCode .= $this->writeFlashVars();		
      	    
		$jsCode .= '<!--[if !IE]>-->' . chr(10);
    $jsCode .= '<object type="application/x-shockwave-flash" data="'.$contentConf['flashmovie'].'" width="'.$contentConf['width'].'" height="'.$contentConf['height'].'"'.$class.$align.'>' . chr(10);
		
	  $this->addFlashParam('bgcolor',$this->ffValue['bgColor']);
    $this->addFlashParam('quality',$this->ffValue['quality']);
	  $this->addFlashParam('menu',$this->ffValue['displayMenu']); 

		//add additional parameters
		$this->addFlashParams($this->ffValue['flashParams']['ts']);
		$this->addFlashParams($this->ffValue['flashParams']['ff']);
		$jsCode .= $this->writeFlashParams();
		
    //add additional variables		
		$this->addFlashVarsFromTS($this->ffValue['flashVars']['ts']);
		$this->addFlashVars($this->ffValue['flashVars']['ff']);
		$jsCode .= $this->writeFlashVars();		
		
    $jsCode .= '<!--<![endif]-->' . chr(10);
    $jsCode .= $this->writeAlternativeContent($contentConf) . chr(10);
    $jsCode .= '<!--[if !IE]>-->' . chr(10);
    $jsCode .= '</object>' . chr(10);
    $jsCode .= '<!--<![endif]-->' . chr(10);		
   	$jsCode .= '</object>' . chr(10);
		return $jsCode;
	}	
	
	function addFlashVarsFromTS ($contentConf) {
	  if (! is_array($contentConf)) {
      return;
    }
    foreach ($contentConf as $key => $value) {
		  if (substr($key, -1) == '.') {
        continue;
      }
		  $this->addFlashVar($key, $this->cObj->cObjGetSingle($value, $contentConf[$key.'.']));
    }
	}
	
	function addFlashVars ($contentConf){
		$flashVars = t3lib_div::trimExplode(chr(10), $contentConf, 1);
		if(is_array($flashVars)){		
			while (list(, $val) = each($flashVars)) {
				$var = explode('|', $val, 2);
				if(is_array($var)){
					$this->addFlashVar(trim($var[0]), trim($var[1]));
				}	
			}
		}		
	}
	
	function addFlashVar ($key, $value){		
		//check if data is a getText or a regular string
		$gTStr = 'GT:';
		$gTStrLength = strlen($gTStr);
		if(substr(strtoupper($value), 0,$gTStrLength) == $gTStr){
			$gT = substr($value,$gTStrLength);
			$this->flashVars[$key] = $this->cObj->getData(trim($gT),'');
		} else {
			$this->flashVars[$key] = $value;
		}
	}
	
	function writeFlashVars (){
		if ($this->ffValue['ffPublishMethod'] == 0) {
			$content = 'var flashvars = {};' . chr(10);
			foreach($this->flashVars as $k => $v){
				$content .= 'flashvars.' . htmlspecialchars($k).' = "'.htmlspecialchars($v).'";' . chr(10);			
			}
		} else {
		    if(sizeof($this->flashVars) > 0){
		      $content = '<param name="flashvars" value="';
			    foreach($this->flashVars as $k => $v){
				    $content .= htmlspecialchars($k). '=' .htmlspecialchars($v). '&amp;';			
			    }
			   $content = substr($content,0,-5).'" />'.chr(10);
			 }
		}		
		return $content;
	}

	function addFlashParams ($contentConf){
		$flashParams = t3lib_div::trimExplode(chr(10), $contentConf, 1);
		//add base param
		$this->addFlashParam('base', t3lib_div::getIndpEnv ('TYPO3_SITE_URL'));
		if(is_array($flashParams)){		
			while (list(, $val) = each($flashParams)) {
				$var = t3lib_div::trimExplode('|', $val, 1);
				$this->addFlashParam($var[0], $var[1]);
			}
		}		
	}
	
	function addFlashParam ($key, $value){
		$this->params[$key] = $value;
	}
	
	function writeFlashParams () {
		if ($this->ffValue['ffPublishMethod'] == 0) {
			$content = 'var params = {};'.chr(10);
			foreach($this->params as $k => $v){
				$content .= 'params.'.htmlspecialchars($k).' = "'.htmlspecialchars($v).'";'.chr(10);			
			}
		} else {			
			foreach($this->params as $k => $v){
				$content .= '<param name="'.htmlspecialchars($k).'" value="'.htmlspecialchars($v).'" />'.chr(10);			
		  }			
		}      
		return $content;
	}
	
	function addFlashAttrib ($key, $value){
		$this->attribs[$key] = $value;
	}
	
	function writeFlashAtrribs () {
		$content = 'var attributes = {};' . chr(10);
		foreach($this->attribs as $k => $v){
		  $content .= 'attributes.' . htmlspecialchars($k).' = "'.htmlspecialchars($v).'";' . chr(10);			
		}	  
		return $content;
	}	
	
	function errorMessage(){
		$output = '<div' . $this->pi_classParam("error") . '>';
	  	$output .= $this->pi_getLL('error_msg','No Flash movie to display');
	  	$output .= '</div>';
	  	return $output;
	}
}

if (defined("TYPO3_MODE") && $TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/yejj_swfobject/pi1/class.tx_yejjswfobject_pi1.php"])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/yejj_swfobject/pi1/class.tx_yejjswfobject_pi1.php"]);
}

?>