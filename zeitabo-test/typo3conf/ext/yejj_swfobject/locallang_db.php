<?php
/**
 * Language labels for database tables/fields belonging to extension "yejj_swfobject"
 * 
 * This file is detected by the translation tool.
 */

$LOCAL_LANG = Array (
	"default" => Array (
		"tx_yejjswfobject_movie" => "Flash Movie",
		"tx_yejjswfobject_movie.description" => "Description/name of the Flash movie",
		"tx_yejjswfobject_movie.flashmovie" => "Flash file (.swf)",
		"tx_yejjswfobject_movie.width" => "Flash movie width",
		"tx_yejjswfobject_movie.height" => "Flash movie height",
		"tx_yejjswfobject_movie.requiredversion" => "Required Flash Version",
		"tx_yejjswfobject_movie.alternativecontent" => "Alternative content",        	
		"tt_content.list_type_pi1" => "Flash Movie",	

		'tt_content.pi_flexform.sheet_general' => 'Flash Movie Settings',
		'tt_content.pi_flexform.movie_selection' => 'Flash Movie to display:',
    	'tt_content.pi_flexform.publishmethod' => 'Publishing Method:',
    	'tt_content.pi_flexform.publishmethod.dynamic' => 'Dynamic',
    	'tt_content.pi_flexform.publishmethod.static' => 'Static',    
    	'tt_content.pi_flexform.expressinstall' => 'ExpressInstall?',
    	'tt_content.pi_flexform.alternativecontent' => 'Alternative Content (this will overwrite the alternative content in the flash movie record).',
    
		'tt_content.pi_flexform.sheet_attribute' => 'Attributes',
		'tt_content.pi_flexform.flashcontentid' => 'Flash content id',
		'tt_content.pi_flexform.attrname' => 'Name',
		'tt_content.pi_flexform.attrclass' => 'Class',
		'tt_content.pi_flexform.attralign' => 'Alignment',
		'tt_content.pi_flexform.attralign.choose' => 'Choose...',
		'tt_content.pi_flexform.attralign.middle' => 'middle',
		'tt_content.pi_flexform.attralign.left' => 'left',
		'tt_content.pi_flexform.attralign.right' => 'right',
		'tt_content.pi_flexform.attralign.top' => 'top',
		'tt_content.pi_flexform.attralign.bottom' => 'bottom',
		
		'tt_content.pi_flexform.sheet_parameter' => 'Parameters',
		'tt_content.pi_flexform.flashparameter' => 'Additional parameters for the Flash movie. Each parameter in one line, name and value separated by pipe character  (eg. wmode|transparent)',
		'tt_content.pi_flexform.quality' => 'Quality',
		'tt_content.pi_flexform.quality.high' => 'High',
		'tt_content.pi_flexform.quality.low' => 'Low',
		'tt_content.pi_flexform.displaymenu' => 'Display menu',
		'tt_content.pi_flexform.bgcolor' => 'Background color of Flash movie',    
		
		'tt_content.pi_flexform.sheet_flashvars' => 'Flash Variables',
		'tt_content.pi_flexform.flashvariables' => 'Flash variables (Flashvars) to pass to the Flash movie. Each variable in one line, name and value separated by pipe character (eg. type|normal)',
        		
	),
);
?>