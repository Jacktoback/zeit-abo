<?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");

$TCA["tx_yejjswfobject_movie"] = Array (
	"ctrl" => $TCA["tx_yejjswfobject_movie"]["ctrl"],
	"interface" => Array (
		"showRecordFieldList" => "hidden,description,flashmovie,width,height,requiredversion,alternativecontent"
	),
	"feInterface" => $TCA["tx_yejjswfobject_movie"]["feInterface"],
	"columns" => Array (
		"description" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:yejj_swfobject/locallang_db.php:tx_yejjswfobject_movie.description",
			"config" => Array (
				"type" => "input",
				"size" => "30",
				"eval" => "required",
			)
		),
		"flashmovie" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:yejj_swfobject/locallang_db.php:tx_yejjswfobject_movie.flashmovie",
			"config" => Array (
				"type" => "group",
				"internal_type" => "file",
				"allowed" => 'swf',
				"max_size" => 100000,
				"uploadfolder" => "uploads/tx_yejjswfobject",
				"size" => 1,
				"minitems" => 0,
				"maxitems" => 1,
				"eval" => "required",
			)
		),
		"width" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:yejj_swfobject/locallang_db.php:tx_yejjswfobject_movie.width",
			"config" => Array (
				"type" => "input",	
				"size" => "10",	
				"eval" => "required",
			)
		),
		"height" => Array (		
			"exclude" => 1,		
			"label" => "LLL:EXT:yejj_swfobject/locallang_db.php:tx_yejjswfobject_movie.height",
			"config" => Array (
				"type" => "input",	
				"size" => "10",	
				"eval" => "required",
			)
		),
		"requiredversion" => Array (
			"exclude" => 1,		
			"label" => "LLL:EXT:yejj_swfobject/locallang_db.php:tx_yejjswfobject_movie.requiredversion",
			"config" => Array (
				"type" => "input",	
				"size" => "5",
			)
		),
		"alternativecontent" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:yejj_swfobject/locallang_db.php:tx_yejjswfobject_movie.alternativecontent",
			"config" => Array (
				"type" => "group",
				"internal_type" => "db",
				"allowed" => "tt_content",
				"size" => 20,
				"minitems" => 0,
				"maxitems" => 50,
			)
		),
	),
	"types" => Array (
		"0" => Array("showitem" => "hidden;;;;1-1-1, description;;;;1-1-1, flashmovie, width, height, requiredversion;;;;1-1-1, alternativecontent")
	),
	"palettes" => Array (
	)
);
?>