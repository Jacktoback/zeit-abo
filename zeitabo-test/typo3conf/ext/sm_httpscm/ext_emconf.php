<?php

########################################################################
# Extension Manager/Repository config file for ext "sm_httpscm".
#
# Auto generated 27-10-2011 09:57
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Page HTTP/HTTPS Enforcer via Clickmenu',
	'description' => 'Adds an item to the CM to quickly en-/disable HTTPS for pages (depends on HTTP/HTTPS Enforcer)',
	'category' => 'be',
	'shy' => 0,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => 'cm1',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author' => 'Steffen Müller',
	'author_email' => 'steffen@mail.kommwiss.fu-berlin.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '1.0.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '3.5.0-0.0.0',
			'php' => '3.0.0-0.0.0',
			'https_enforcer' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:9:{s:26:"class.tx_smhttpscm_cm1.php";s:4:"06cc";s:12:"cm_icon0.gif";s:4:"09b1";s:12:"cm_icon1.gif";s:4:"b429";s:12:"ext_icon.gif";s:4:"927b";s:14:"ext_tables.php";s:4:"9a4d";s:13:"locallang.php";s:4:"1aa3";s:14:"doc/manual.sxw";s:4:"edff";s:19:"doc/wizard_form.dat";s:4:"11a1";s:20:"doc/wizard_form.html";s:4:"b2b6";}',
);

?>