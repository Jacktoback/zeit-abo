<?php
/***************************************************************
*  Copyright notice
*  
*  (c) 2004 Steffen M�ller (steffen@mail.kommwiss.fu-berlin.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Adds a new item to the pages ClickMenu (CM) to choose HTTP/HTTPS.
 * Depends on the extension https_enforcer
 *
 * @author		Steffen M�ller <steffen@mail.kommwiss.fu-berlin.de>
 * @package		TYPO3
 * @subpackage		tx_smhttpscm
 */

class tx_smhttpscm_cm1	{

	/**
	 * Adds a new item to the Pages CM.
	 * This item changes the DB value for HTTPS and then reloads pagetree
	 *
	 * @param	object		$backRef: The CM object (see class.alt_clickmenu)
	 * @param	array		$menuItems: Imported item array
	 * @param	string		$table: Name of the table which the selected section refers to
	 * @param	string		$uid: UID of the selected page
	 * @return	array		$menuItems: Extended item array, element in $menuItems
	 */
	function main (&$backRef, $menuItems, $table, $uid)	{
		global $BE_USER,$TCA,$LANG;
		$localItems = Array();
		$field = 'tx_httpsenforcer_force_secure';

			// Checks if the CM is first level (cmLevel=0) or second level
		if (!$backRef->cmLevel)	{

				// Checks if the editing of the element is OK
			if ($backRef->editOK)	{

					// Restrict the new item to CM item
					// Returns directly, if ...
					//  1) the CM is not within the "pages" table
				if ($table!="pages")	return $menuItems;
					//  2) the doktype did not match (standard, advanced or not_in_menu)
				if (!in_array($this->ext_getPagesField($uid, 'doktype'), array(1,2,5)))	return $menuItems;
					//  3) the BE_user is not allowed to edit the page
				if (!$pageinfo = t3lib_BEfunc::readPageAccess($uid,$GLOBALS['BE_USER']->getPagePermsClause(2))) return $menuItems;

					// the CM icons/labels should reflect the actual page configuration
					// Check page condition: if HTTPS is on, prefix=0. If HTTPS is off, prefix=1
				$prefix = ($this->ext_getPagesField($uid, $field))?0:1;

					// Fetches localized labels
				$LL = $this->includeLL();

					// Defines the function of the new item:
					// change DB value on click and then reload pagetree
				$editOnClick='';
				$loc='top.content'.($this->listFrame && !$this->alwaysContentFrame ?'.list_frame':'');
				$editOnClick='if('.$loc.'){'.$loc.".document.location=top.TS.PATH_typo3+'tce_db.php?redirect='+top.rawurlencode(".$backRef->frameLocation($loc.'.document').")+'"."&data[".$table.']['.$uid.']['.$field.']='.$prefix.'&prErr=1&vC='.$GLOBALS['BE_USER']->veriCode()."';hideCM();}";

					// Spend some icons, labels and a link target for the new CM item.
				$localItems[] = $backRef->linkItem(
				$GLOBALS["LANG"]->getLLL("cm1_title".$prefix,$LL),
				$backRef->excludeIcon('<img src="'.t3lib_extMgm::extRelPath("sm_httpscm").'cm_icon'.$prefix.'.gif" width="15" height="12" border=0 align=top>'),
				$editOnClick.'return false;',
				0	// 0 to disable, 1 to enable the item in the top-bar.
				);

					// Find position of "hide" element: NOT PERFECTLY WORKING FOR NON ADMIN USERS
				reset($menuItems);
				$c=0;
				while(list($k)=each($menuItems))	{
					$c++;
					if (!strcmp($k,"hide"))	break;
				}
					// .. subtract five (delete item + divider line + edit page header + visibility settings + hide)
				$c-=5;
					// ... and insert the items just before the delete element.
				array_splice(
					$menuItems,
					$c,
					0,
					$localItems
				);
			}
		}
		return $menuItems;
	}

	/**
	 * Fetches the value of a field in table "pages"
	 * In other words: SELECT $field FROM pages WHERE uid=$uid
	 *
	 * @param	integer		$uid: UID of the selected page
	 * @param	string		$field: field name in "pages"
	 * @return	integer		$row[$field]: value of the selected field
	 */
	function ext_getPagesField($uid, $field)	{
		$row = t3lib_BEfunc::getRecord('pages',$uid,$field);
		return $row[$field];
	}

	/**
	 * Includes the [extDir]/locallang.php and
	 * returns the $LOCAL_LANG array found in that file.
	 *
	 * @return	array		$LOCAL_LANG: array of localized variables
	 */
	function includeLL()	{
		include(t3lib_extMgm::extPath("sm_httpscm")."locallang.php");
		return $LOCAL_LANG;
	}
} 

if (defined("TYPO3_MODE") && $TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/sm_httpscm/class.tx_smhttpscm_cm1.php"])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/sm_httpscm/class.tx_smhttpscm_cm1.php"]);
}

?>