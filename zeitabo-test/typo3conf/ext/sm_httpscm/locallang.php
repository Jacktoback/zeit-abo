<?php
/**
 * Language labels for extension "https_cm"
 * 
 * This file is detected by the translation tool.
 */

$LOCAL_LANG = Array (
	'default' => Array (
		'cm1_title0' => 'Turn off SSL',
		'cm1_title1' => 'Turn on SSL',
	),
	'dk' => Array (
	),
	'de' => Array (
		'cm1_title0' => 'Schalte SSL aus',
		'cm1_title1' => 'Schalte SSL an',
	),
	'no' => Array (
	),
	'it' => Array (
	),
	'fr' => Array (
	),
	'es' => Array (
	),
	'nl' => Array (
	),
	'cz' => Array (
	),
	'pl' => Array (
	),
	'si' => Array (
	),
	'fi' => Array (
		'cm1_title0' => 'SSL p��lle',
		'cm1_title1' => 'SSL pois p��lt�',
	),
	'tr' => Array (
	),
	'se' => Array (
	),
	'pt' => Array (
	),
	'ru' => Array (
	),
	'ro' => Array (
	),
	'ch' => Array (
	),
	'sk' => Array (
	),
	'lt' => Array (
	),
	'is' => Array (
	),
	'hr' => Array (
	),
	'hu' => Array (
		'cm1_title0' => 'SSL kikapcsol�sa',
		'cm1_title1' => 'SSL bekapcsol�sa',
	),
	'gl' => Array (
	),
	'th' => Array (
	),
	'gr' => Array (
	),
	'hk' => Array (
	),
	'eu' => Array (
	),
	'bg' => Array (
	),
	'br' => Array (
	),
	'et' => Array (
	),
	'ar' => Array (
	),
	'he' => Array (
	),
	'ua' => Array (
	),
	'lv' => Array (
	),
	'jp' => Array (
	),
	'vn' => Array (
	),
	'ca' => Array (
	),
	'ba' => Array (
	),
	'kr' => Array (
	),
);
?>