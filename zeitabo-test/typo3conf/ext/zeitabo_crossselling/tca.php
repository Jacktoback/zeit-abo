<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA['tx_zeitabocrossselling_products'] = array (
	'ctrl' => $TCA['tx_zeitabocrossselling_products']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,title,product'
	),
	'feInterface' => $TCA['tx_zeitabocrossselling_products']['feInterface'],
	'columns' => array (
		't3ver_label' => array (		
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array (
				'type' => 'input',
				'size' => '30',
				'max'  => '30',
			)
		),
		'sys_language_uid' => array (		
			'exclude' => 1,
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array (
				'type'                => 'select',
				'foreign_table'       => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				)
			)
		),
		'l10n_parent' => array (		
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude'     => 1,
			'label'       => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config'      => array (
				'type'  => 'select',
				'items' => array (
					array('', 0),
				),
				'foreign_table'       => 'tx_zeitabocrossselling_products',
				'foreign_table_where' => 'AND tx_zeitabocrossselling_products.pid=###CURRENT_PID### AND tx_zeitabocrossselling_products.sys_language_uid IN (-1,0)',
			)
		),
		'l10n_diffsource' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		'starttime' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config'  => array (
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'default'  => '0',
				'checkbox' => '0'
			)
		),
		'endtime' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config'  => array (
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'checkbox' => '0',
				'default'  => '0',
				'range'    => array (
					'upper' => mktime(3, 14, 7, 1, 19, 2038),
					'lower' => mktime(0, 0, 0, date('m')-1, date('d'), date('Y'))
				)
			)
		),
		'title' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_products.title',		
			'config' => array (
				'type' => 'input',	
				'size' => '48',	
				'eval' => 'required,uniqueInPid',
			)
		),
		'product' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_products.product',		
			'config' => array (
				'type' => 'group',	
				'internal_type' => 'db',	
				'allowed' => 'tt_content',	
				'size' => 1,	
				'minitems' => 0,
				'maxitems' => 1,
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title;;;;2-2-2, product;;;;3-3-3')
	),
	'palettes' => array (
		'1' => array('showitem' => 'starttime, endtime')
	)
);



$TCA['tx_zeitabocrossselling_targets'] = array (
	'ctrl' => $TCA['tx_zeitabocrossselling_targets']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,title,formfield,formvalue,product'
	),
	'feInterface' => $TCA['tx_zeitabocrossselling_targets']['feInterface'],
	'columns' => array (
		't3ver_label' => array (		
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array (
				'type' => 'input',
				'size' => '30',
				'max'  => '30',
			)
		),
		'sys_language_uid' => array (		
			'exclude' => 1,
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array (
				'type'                => 'select',
				'foreign_table'       => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				)
			)
		),
		'l10n_parent' => array (		
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude'     => 1,
			'label'       => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config'      => array (
				'type'  => 'select',
				'items' => array (
					array('', 0),
				),
				'foreign_table'       => 'tx_zeitabocrossselling_targets',
				'foreign_table_where' => 'AND tx_zeitabocrossselling_targets.pid=###CURRENT_PID### AND tx_zeitabocrossselling_targets.sys_language_uid IN (-1,0)',
			)
		),
		'l10n_diffsource' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		'starttime' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config'  => array (
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'default'  => '0',
				'checkbox' => '0'
			)
		),
		'endtime' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config'  => array (
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'checkbox' => '0',
				'default'  => '0',
				'range'    => array (
					'upper' => mktime(3, 14, 7, 1, 19, 2038),
					'lower' => mktime(0, 0, 0, date('m')-1, date('d'), date('Y'))
				)
			)
		),
		'title' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_targets.title',		
			'config' => array (
				'type' => 'input',	
				'size' => '48',	
				'eval' => 'required,uniqueInPid',
			)
		),
		'formfield' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_targets.formfield',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'nospace',
			)
		),
		'formvalue' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_targets.formvalue',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'nospace',
			)
		),
		'product' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_targets.product',		
			'config' => array (
				'type' => 'group',	
				'internal_type' => 'db',	
				'allowed' => 'tt_content',	
				'size' => 1,	
				'minitems' => 0,
				'maxitems' => 1,
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title;;;;2-2-2, formfield;;;;3-3-3, formvalue, product')
	),
	'palettes' => array (
		'1' => array('showitem' => 'starttime, endtime')
	)
);



$TCA['tx_zeitabocrossselling_platform'] = array (
	'ctrl' => $TCA['tx_zeitabocrossselling_platform']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,title,platform,platform_value,product'
	),
	'feInterface' => $TCA['tx_zeitabocrossselling_platform']['feInterface'],
	'columns' => array (
		't3ver_label' => array (		
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => array (
				'type' => 'input',
				'size' => '30',
				'max'  => '30',
			)
		),
		'sys_language_uid' => array (		
			'exclude' => 1,
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array (
				'type'                => 'select',
				'foreign_table'       => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				)
			)
		),
		'l10n_parent' => array (		
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude'     => 1,
			'label'       => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config'      => array (
				'type'  => 'select',
				'items' => array (
					array('', 0),
				),
				'foreign_table'       => 'tx_zeitabocrossselling_platform',
				'foreign_table_where' => 'AND tx_zeitabocrossselling_platform.pid=###CURRENT_PID### AND tx_zeitabocrossselling_platform.sys_language_uid IN (-1,0)',
			)
		),
		'l10n_diffsource' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		'starttime' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config'  => array (
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'default'  => '0',
				'checkbox' => '0'
			)
		),
		'endtime' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config'  => array (
				'type'     => 'input',
				'size'     => '8',
				'max'      => '20',
				'eval'     => 'date',
				'checkbox' => '0',
				'default'  => '0',
				'range'    => array (
					'upper' => mktime(3, 14, 7, 1, 19, 2038),
					'lower' => mktime(0, 0, 0, date('m')-1, date('d'), date('Y'))
				)
			)
		),
		'title' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_platform.title',		
			'config' => array (
				'type' => 'input',	
				'size' => '48',	
				'eval' => 'required',
			)
		),
		'platform' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_platform.platform',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'nospace',
			)
		),
		'platform_value' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_platform.platform_value',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
			)
		),
		'product' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_platform.product',		
			'config' => array (
				'type' => 'group',	
				'internal_type' => 'db',	
				'allowed' => 'tt_content',	
				'size' => 1,	
				'minitems' => 0,
				'maxitems' => 1,
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title;;;;2-2-2, platform;;;;3-3-3, platform_value, product')
	),
	'palettes' => array (
		'1' => array('showitem' => 'starttime, endtime')
	)
);

$TCA['tx_zeitabocrossselling_data'] = array (
    'ctrl' => $TCA['tx_zeitabocrossselling_data']['ctrl'],
    'interface' => array (
        'showRecordFieldList' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,abo,sbpid,primary_products,secondary_products,targetgroups,locations,platforms'
    ),
    'feInterface' => $TCA['tx_zeitabocrossselling_data']['feInterface'],
    'columns' => array (
        't3ver_label' => array (
            'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
            'config' => array (
                'type' => 'input',
                'size' => '30',
                'max'  => '30',
            )
        ),
        'sys_language_uid' => array (
            'exclude' => 1,
            'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => array (
                'type'                => 'select',
                'foreign_table'       => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
                )
            )
        ),
        'l10n_parent' => array (
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude'     => 1,
            'label'       => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config'      => array (
                'type'  => 'select',
                'items' => array (
                    array('', 0),
                ),
                'foreign_table'       => 'tx_zeitabocrossselling_data',
                'foreign_table_where' => 'AND tx_zeitabocrossselling_data.pid=###CURRENT_PID### AND tx_zeitabocrossselling_data.sys_language_uid IN (-1,0)',
            )
        ),
        'l10n_diffsource' => array (
            'config' => array (
                'type' => 'passthrough'
            )
        ),
        'hidden' => array (
            'exclude' => 1,
            'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config'  => array (
                'type'    => 'check',
                'default' => '0'
            )
        ),
        'starttime' => array (
            'exclude' => 1,
            'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
            'config'  => array (
                'type'     => 'input',
                'size'     => '8',
                'max'      => '20',
                'eval'     => 'date',
                'default'  => '0',
                'checkbox' => '0'
            )
        ),
        'endtime' => array (
            'exclude' => 1,
            'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
            'config'  => array (
                'type'     => 'input',
                'size'     => '8',
                'max'      => '20',
                'eval'     => 'date',
                'checkbox' => '0',
                'default'  => '0',
                'range'    => array (
                    'upper' => mktime(3, 14, 7, 1, 19, 2038),
                    'lower' => mktime(0, 0, 0, date('m')-1, date('d'), date('Y'))
                )
            )
        ),
        'abo' => array (
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.abo',
            'config' => array (
                'type' => 'select',
                'foreign_table' => 'tx_zeitabo_abonnements',
                'foreign_table_where' => 'AND tx_zeitabo_abonnements.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabo_abonnements.abo_name',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            )
        ),
		'sbpid' => array (
			'exclude' => 0,
			'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.sbpid',
			'config' => array (
				'type' => 'group',
				'internal_type' => 'db',
				'allowed' => 'pages',
				'size' => 1,
				'minitems' => 0,
				'maxitems' => 1,
			)
		),

        'primary_products' => array (
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.primary',
            'config' => array (
                'type' => 'select',
                'foreign_table' => 'tx_zeitabocrossselling_products',
                'foreign_table_where' => 'AND tx_zeitabocrossselling_products.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_products.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabocrossselling_products.title',
                'size' => 10,
                'minitems' => 0,
                'maxitems' => 100,
            )
        ),
        'secondary_products' => array (
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.secondary',
            'config' => array (
                'type' => 'select',
                'foreign_table' => 'tx_zeitabocrossselling_products',
                'foreign_table_where' => 'AND tx_zeitabocrossselling_products.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_products.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabocrossselling_products.title',
                'size' => 10,
                'minitems' => 0,
                'maxitems' => 100,
            )
        ),
        'targetgroups' => array (
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.targetgroups',
            'config' => array (
                'type' => 'select',
                'foreign_table' => 'tx_zeitabocrossselling_targets',
                'foreign_table_where' => 'AND tx_zeitabocrossselling_targets.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_targets.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabocrossselling_targets.title',
                'size' => 10,
                'minitems' => 0,
                'maxitems' => 100,
            )
        ),
        'locations' => array (
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.locations',
            'config' => array (
                'type' => 'select',
                'foreign_table' => 'tx_zeitabocrossselling_targets',
                'foreign_table_where' => 'AND tx_zeitabocrossselling_targets.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_targets.sys_language_uid=###REC_FIELD_sys_language_uid###  ORDER BY tx_zeitabocrossselling_targets.title',
                'size' => 10,
                'minitems' => 0,
                'maxitems' => 100,
            )
        ),
        'platforms' => array (
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data.platforms',
            'config' => array (
                'type' => 'select',
                'foreign_table' => 'tx_zeitabocrossselling_platform',
                'foreign_table_where' => 'AND tx_zeitabocrossselling_platform.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_platform.sys_language_uid=###REC_FIELD_sys_language_uid###  ORDER BY tx_zeitabocrossselling_platform.title',
                'size' => 10,
                'minitems' => 0,
                'maxitems' => 100,
            )
        ),
    ),
    'types' => array (
        '0' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, abo;;;;2-2-2,sbpid,
primary_products,secondary_products, targetgroups, locations, platforms')
    ),
    'palettes' => array (
        '1' => array('showitem' => 'starttime, endtime')
    )
);
?>