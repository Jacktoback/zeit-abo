<?php

########################################################################
# Extension Manager/Repository config file for ext "zeitabo_crossselling".
#
# Auto generated 10-08-2012 06:49
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Zeit Aboshop Cross-Selling',
	'description' => 'Zeit Aboshop Cross-Selling mit Matching von Primär- und Sekundär-Produkten sowie weitere 
Matchings (Zielgruppen, Locations, Plattformen)',
	'category' => 'plugin',
	'author' => 'Jens Büchel',
	'author_email' => 'j.buechel@divine.de',
	'shy' => '',
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
			'cms' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:19:{s:9:"ChangeLog";s:4:"d23e";s:10:"README.txt";s:4:"9fa9";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"2f43";s:14:"ext_tables.php";s:4:"3fb4";s:14:"ext_tables.sql";s:4:"aa24";s:40:"icon_tx_zeitabocrossselling_platform.gif";s:4:"475a";s:40:"icon_tx_zeitabocrossselling_products.gif";s:4:"475a";s:39:"icon_tx_zeitabocrossselling_targets.gif";s:4:"475a";s:13:"locallang.xml";s:4:"46bb";s:16:"locallang_db.xml";s:4:"ee28";s:7:"tca.php";s:4:"1284";s:19:"doc/wizard_form.dat";s:4:"7ace";s:20:"doc/wizard_form.html";s:4:"1915";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:40:"pi1/class.tx_zeitabocrossselling_pi1.php";s:4:"7685";s:48:"pi1/class.tx_zeitabocrossselling_pi1_wizicon.php";s:4:"9671";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"a5a0";}',
);

?>