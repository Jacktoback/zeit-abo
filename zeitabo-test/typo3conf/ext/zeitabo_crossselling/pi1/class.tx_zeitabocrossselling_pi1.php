<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Jens Büchel <j.buechel@divine.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'Zeit Aboshop Cross-Selling' for the 'zeitabo_crossselling' extension.
 *
 * @author	Jens Büchel <j.buechel@divine.de>
 * @package	TYPO3
 * @subpackage	tx_zeitabocrossselling
 */
class tx_zeitabocrossselling_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_zeitabocrossselling_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_zeitabocrossselling_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'zeitabo_crossselling';	// The extension key.
	var $pi_checkCHash = true;
	
	/**
	 * Initialize plugin and get configuration values.
	 *
	 * @param	array		$conf : configuration array from TS
	 */
	function init($conf) {
		// Store configuration
		$this->conf = $conf;

		// Loading language-labels
		$this->pi_loadLL();

		// Set default piVars from TS
		$this->pi_setPiVarDefaults();

		// Init and get the flexform data of the plugin
		$this->pi_initPIflexForm();

		// Assign the flexform data to a local variable for easier access
		$piFlexForm = $this->cObj->data['pi_flexform'];

		// Array for local configuration
		$this->lConf = array();
		// load available syslanguages
		$this->initLanguages();
		// sys_language_mode defines what to do if the requested translation is not found
		$this->sys_language_mode = $this->conf['sys_language_mode']?$this->conf['sys_language_mode'] : $GLOBALS['TSFE']->sys_language_mode;
		
		$this->sys_language_uid = $GLOBALS['TSFE']->config['config']['sys_language_uid'];    //Get site-language 
         
        if($this->sys_language_uid == ''){ 
            $this->sys_language_uid = 0; 
        }
	}
	
	/**
	 * fills the internal array '$this->langArr' with the available syslanguages
	 *
	 * @return	void
	 */
	function initLanguages () {

		$lres = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'sys_language',
			'1=1' . $this->cObj->enableFields('sys_language'));


		$this->langArr = array();
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($lres)) {
			$this->langArr[$row['uid']] = $row;
		}
	}
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->init($conf);
		
		$conf['actPage'] = $GLOBALS['TSFE']->id;


        $conf['Aktionsnummer']= 0;
        $item = t3lib_div::_GP('item');
        if($item != ''){
            $zeit_user = $this->getUserData($item);

			if($zeit_user['Aktionsnummer'] == 'mag'){
				$conf['Aktionsnummer'] = 'mag';
				$resAbo = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_singlebooks', 'uid=' . intval($zeit_user['Zugaben_Artikelnummer']));
				$arrAboData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resAbo);
				$conf['sbPid'] = $arrAboData['pid'];
			} elseif(preg_match('/tx_zeitabo_abonnements/i',$zeit_user['Aktionsnummer'])){
				list($string,$auid) = mb_split('tx_zeitabo_abonnements_',$zeit_user['Aktionsnummer']);
				if(!empty($auid)){
					$conf['Aktionsnummer'] = $auid;
				} else {
					$resAbo = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_singlebooks', 'uid=' . intval($zeit_user['Zugaben_Artikelnummer']));
					$arrAboData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resAbo);
					$conf['sbPid'] = $arrAboData['pid'];
				}
			} else {
				$conf['Aktionsnummer'] = $zeit_user['Aktionsnummer'];
			}

        }

        $pagedata = $this->getCrosssellingDatas($conf);

		$content='';
		
		$conf['FirstRule'] = $conf['FirstRule'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'FirstRule', 'sForm');
		$conf['SecondRule'] = $conf['SecondRule'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'SecondRule', 'sForm');
		$conf['ThirdRule'] = $conf['ThirdRule'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'ThirdRule', 'sForm');
		$conf['targetPage'] = $conf['targetPage'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'targetPage', 'sForm');
		$conf['fallback'] = $conf['fallback'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'fallback', 'sForm');
		
		$conf['crossselling']['primary'] = $pagedata['primary_products'];
		$conf['crossselling']['secondary'] = $pagedata['secondary_products'];
		$conf['crossselling']['targetgroups'] = $pagedata['targetgroups'];
		$conf['crossselling']['locations'] = $pagedata['locations'];
		$conf['crossselling']['platforms'] = $pagedata['platforms'];

		$content = $this->getCrosssellingData($content, $conf);
		
		if (t3lib_extMgm::isLoaded('formhandler')) {
			require_once(t3lib_extMgm::extPath('formhandler').'pi1/class.tx_formhandler_pi1.php');
			$this->formhandler = t3lib_div::makeInstance('tx_formhandler_pi1');
			#var_dump($GLOBALS['TSFE']->tmpl->setup['plugin.']['Tx_Formhandler.']["settings."]["predef."]["default."]["finishers."]["2."]);
		}
		if($content == '' && $conf['ThirdRule'] != ''){
			$conf['ThirdRule'] = NULL;
			$content = $this->getCrosssellingData($content, $conf);
			if($content == '' && $conf['SecondRule'] != ''){
				$conf['SecondRule'] = NULL;
				$content = $this->getCrosssellingData($content, $conf);
			}
		}
		
		if($content == '' && $conf['fallback'] != ''){
			$content = $this->getCrosssellingContent($conf['fallback']);
		}
	
		return $this->pi_wrapInBaseClass($content);
	}
	
	function getCrosssellingData($content, $conf){
		$modul = '';
		$crossDatas = '';
		$conf['targetTable'] = array();
		
		if($conf['ThirdRule'] !== NULL){
			$conf['targetTable'][3] = 'tx_zeitabocrossselling_products';
			if($conf['ThirdRule'] == 'targetgroups' || $conf['ThirdRule'] == 'locations'){
				$conf['targetTable'][3] = 'tx_zeitabocrossselling_targets';
			} elseif($conf['ThirdRule'] == 'platforms'){
				$conf['targetTable'][3] = 'tx_zeitabocrossselling_platform';
			} 
			$crossDatas[3] = $conf['crossselling'][$conf['ThirdRule']];
		} 
		if($conf['SecondRule'] !== NULL){
			$conf['targetTable'][2] = 'tx_zeitabocrossselling_products';
			if($conf['SecondRule'] == 'targetgroups' || $conf['SecondRule'] == 'locations'){
				$conf['targetTable'][2] = 'tx_zeitabocrossselling_targets';
			} elseif($conf['SecondRule'] == 'platforms'){
				$conf['targetTable'][2] = 'tx_zeitabocrossselling_platform';
			}
			$crossDatas[2] = $conf['crossselling'][$conf['SecondRule']];
		} 
		if($conf['FirstRule'] !== NULL){
			$conf['targetTable'][1] = 'tx_zeitabocrossselling_products';
			if($conf['FirstRule'] == 'targetgroups' || $conf['FirstRule'] == 'locations'){
				$conf['targetTable'][1] = 'tx_zeitabocrossselling_targets';
			} elseif($conf['FirstRule'] == 'platforms'){
				$conf['targetTable'][1] = 'tx_zeitabocrossselling_platform';
			}
			$crossDatas[1] = $conf['crossselling'][$conf['FirstRule']];
		}
		
		$findData = 0;
		
		foreach($conf['targetTable'] as $key => $table){
			#$table = $conf['targetTable'];
			
			
			
			if($findData == 0){
				if($table == 'tx_zeitabocrossselling_targets'){
					$item = t3lib_div::_GP('item');
					if($item != ''){
						$zeit_user = $this->getUserData($item);
					}
				}
				
				$tags = t3lib_div::trimExplode(',',$crossDatas[$key],1);
				
				if($tags){
					foreach($tags as $t){
						$row_data = $this->pi_getRecord($table,$t,0);
						
						if ($this->versioningEnabled) {
							// get workspaces Overlay
							$GLOBALS['TSFE']->sys_page->versionOL($table, $row_data);
						}
						if($table == 'tx_zeitabocrossselling_targets'){
							if($row_data['formfield'] != ''){
								if(strpos($row_data['formvalue'],'|') !== false){
									$targets = t3lib_div::trimExplode('|',$row_data['formvalue']);
									$negate = 0;
									if(strpos($row_data['formvalue'],'!') !== false){
										$negate = 1;
										$targets[0] = substr($targets[0],1);
									}
									$skip = 0;
									foreach($targets as $tget){
										if($skip == 0){
											if($negate == 1){
												if($zeit_user[$row_data['formfield']] != $tget){
													if($row_data['product'] != ''){
														$modul = $this->getCrosssellingContent($row_data['product']);
														$findData = 1;
														
													}
												} else {
													$skip = 1;
												}
											} else {
												if($zeit_user[$row_data['formfield']] == $tget){
													if($row_data['product'] != ''){
														$modul = $this->getCrosssellingContent($row_data['product']);
														$findData = 1;
													}
												} else {
													$skip = 1;
												}
											}
										}
									}
								} else {
									if($zeit_user[$row_data['formfield']] == $row_data['formvalue']){
										if($row_data['product'] != ''){
											$modul = $this->getCrosssellingContent($row_data['product']);
											$findData = 1;
										}
									}
								}
							}
						} elseif($table == 'tx_zeitabocrossselling_platform'){
							$platforms = t3lib_div::trimExplode('|',$row_data['platform_value']);
							foreach($platforms as $platform){
								if(strstr(strtolower($_SERVER[$row_data['platform']]),strtolower($platform))){
									
									if($row_data['product'] != ''){
										$modul = $this->getCrosssellingContent($row_data['product']);
										$findData = 1;
									}
								}
							}
							
						} elseif($row_data['product'] != ''){
							$modul = $this->getCrosssellingContent($row_data['product']);
							$findData = 1;
						}
					}
				}
			}
			
		}
		if($findData == 0){
			$modul = $this->getCrosssellingContent($conf['fallback']);
		}

		return $modul;
	}

    function getCrosssellingDatas($conf){
        $cdata = array();

        $selectConf = array();
        $selectConf['selectFields'] = '*';
        $selectConf['fromTable'] = 'tx_zeitabocrossselling_data';
        $selectConf['where']  = 'pid =  \''.$conf['actPage'].'\'';
		if(!empty($conf['sbPid'])){
			$selectConf['where']  .= ' AND sbpid =  \''.$conf['sbPid'].'\'';
		} else {
			$selectConf['where']  .= ' AND abo =  \''.$conf['Aktionsnummer'].'\'';
		}

        $selectConf['where'] .= ' ' . $this->cObj->enableFields('tx_zeitabocrossselling_data');
        $selectConf['groupBy'] = '';
        $selectConf['orderBy'] = '';
        $selectConf['limit'] = '1';

        #$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = 1;
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
        #echo $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery;

        while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
            if ($GLOBALS['TSFE']->sys_language_content) {
                // prevent link targets from being changed in localized records
                $row = $GLOBALS['TSFE']->sys_page->getRecordOverlay('tx_zeitabocrossselling_data', $row, $GLOBALS['TSFE']->sys_language_content, $GLOBALS['TSFE']->sys_language_contentOL, '');
            }
            if ($this->versioningEnabled) {
                // get workspaces Overlay
                $GLOBALS['TSFE']->sys_page->versionOL('tx_zeitabocrossselling_data', $row);
            }
            $cdata = $row;
			if(!empty($conf['sbPid'])){
				$conf['Aktionsnummer'] = $row['abo'];
			}
        }

        $GLOBALS['TYPO3_DB']->sql_free_result($res);

        return $cdata;
    }

	/**
	 * Render the Crossselling Content 
	 *
	 * @param	integer		$contentUid: The tt_content uid
	 * @return	The form content that is displayed on the website
	 */
	function getCrosssellingContent($crossUid) {
		$fcontent = '';
		if($crossUid){
			$formcontent = array('tables' => 'tt_content','source' => $crossUid,'dontCheckPid' => 0);
			$fcontent = $this->cObj->RECORDS($formcontent); 
		}	
		
		return $fcontent;
		
	}
	
	function getUserData($itemhash){
		$zeit_user = array();
		
		$res_zeit_user = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'zeit_user','hash=\''.$itemhash.'\'', '', '1');
		while ($row_zeit_user = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res_zeit_user)) {
			$zeit_user = $row_zeit_user;
		}
		return $zeit_user;
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabo_crossselling/pi1/class.tx_zeitabocrossselling_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabo_crossselling/pi1/class.tx_zeitabocrossselling_pi1.php']);
}

?>