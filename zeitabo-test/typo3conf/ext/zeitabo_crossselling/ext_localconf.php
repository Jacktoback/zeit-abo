<?php
if (!defined ('TYPO3_MODE')) {
 	die ('Access denied.');
}
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabocrossselling_products=1
');
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabocrossselling_targets=1
');
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabocrossselling_platform=1
');
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabocrossselling_data=1
');

t3lib_extMgm::addPItoST43($_EXTKEY, 'pi1/class.tx_zeitabocrossselling_pi1.php', '_pi1', 'list_type', 1);
?>