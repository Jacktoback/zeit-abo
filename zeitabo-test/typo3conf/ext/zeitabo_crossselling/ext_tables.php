<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}
//Settings Cross Selling
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pi1']='pi_flexform';
t3lib_extMgm::addPiFlexFormValue($_EXTKEY.'_pi1','FILE:EXT:'.$_EXTKEY.'/flexform_ds_pi1.xml');

t3lib_extMgm::allowTableOnStandardPages('tx_zeitabocrossselling_products');


t3lib_extMgm::addToInsertRecords('tx_zeitabocrossselling_products');

$TCA['tx_zeitabocrossselling_products'] = array (
	'ctrl' => array (
		'title'     => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_products',		
		'label'     => 'title',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'versioningWS' => TRUE, 
		'origUid' => 't3_origuid',
		'languageField'            => 'sys_language_uid',	
		'transOrigPointerField'    => 'l10n_parent',	
		'transOrigDiffSourceField' => 'l10n_diffsource',	
		'sortby' => 'sorting',	
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',	
			'starttime' => 'starttime',	
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabocrossselling_products.gif',
	),
);


t3lib_extMgm::allowTableOnStandardPages('tx_zeitabocrossselling_targets');


t3lib_extMgm::addToInsertRecords('tx_zeitabocrossselling_targets');

$TCA['tx_zeitabocrossselling_targets'] = array (
	'ctrl' => array (
		'title'     => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_targets',		
		'label'     => 'title',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'versioningWS' => TRUE, 
		'origUid' => 't3_origuid',
		'languageField'            => 'sys_language_uid',	
		'transOrigPointerField'    => 'l10n_parent',	
		'transOrigDiffSourceField' => 'l10n_diffsource',	
		'sortby' => 'sorting',	
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',	
			'starttime' => 'starttime',	
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabocrossselling_targets.gif',
	),
);


t3lib_extMgm::allowTableOnStandardPages('tx_zeitabocrossselling_platform');


t3lib_extMgm::addToInsertRecords('tx_zeitabocrossselling_platform');

$TCA['tx_zeitabocrossselling_platform'] = array (
	'ctrl' => array (
		'title'     => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_platform',		
		'label'     => 'title',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'versioningWS' => TRUE, 
		'origUid' => 't3_origuid',
		'languageField'            => 'sys_language_uid',	
		'transOrigPointerField'    => 'l10n_parent',	
		'transOrigDiffSourceField' => 'l10n_diffsource',	
		'sortby' => 'sorting',	
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',	
			'starttime' => 'starttime',	
			'endtime' => 'endtime',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabocrossselling_platform.gif',
	),
);

t3lib_extMgm::allowTableOnStandardPages('tx_zeitabocrossselling_data');


t3lib_extMgm::addToInsertRecords('tx_zeitabocrossselling_data');

$TCA['tx_zeitabocrossselling_data'] = array (
    'ctrl' => array (
        'title'     => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tx_zeitabocrossselling_data',
        'label'     => 'abo',
        'tstamp'    => 'tstamp',
        'crdate'    => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => TRUE,
        'origUid' => 't3_origuid',
        'languageField'            => 'sys_language_uid',
        'transOrigPointerField'    => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'sortby' => 'sorting',
        'delete' => 'deleted',
        'enablecolumns' => array (
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
        'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabocrossselling_data.gif',
    ),
);

$tempColumns = array (
	'tx_zeitabocrossselling_primary' => array (		
		'exclude' => 0,		
		'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:pages.tx_zeitabocrossselling_crossselling_primary',		
		'config' => array (
			'type' => 'select',	
			'foreign_table' => 'tx_zeitabocrossselling_products',	
			'foreign_table_where' => 'AND tx_zeitabocrossselling_products.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_products.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabocrossselling_products.title',	
			'size' => 10,	
			'minitems' => 0,
			'maxitems' => 100,
		)
	),
	'tx_zeitabocrossselling_secondary' => array (		
		'exclude' => 0,		
		'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:pages.tx_zeitabocrossselling_crossselling_secondary',		
		'config' => array (
			'type' => 'select',	
			'foreign_table' => 'tx_zeitabocrossselling_products',	
			'foreign_table_where' => 'AND tx_zeitabocrossselling_products.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_products.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabocrossselling_products.title',	
			'size' => 10,	
			'minitems' => 0,
			'maxitems' => 100,
		)
	),
	'tx_zeitabocrossselling_targetgroups' => array (		
		'exclude' => 0,		
		'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:pages.tx_zeitabocrossselling_crosselling_targetgroups',		
		'config' => array (
			'type' => 'select',	
			'foreign_table' => 'tx_zeitabocrossselling_targets',	
			'foreign_table_where' => 'AND tx_zeitabocrossselling_targets.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_targets.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_zeitabocrossselling_targets.title',	
			'size' => 10,	
			'minitems' => 0,
			'maxitems' => 100,
		)
	),
	'tx_zeitabocrossselling_locations' => array (		
		'exclude' => 0,		
		'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:pages.tx_zeitabocrossselling_crossselling_locations',		
		'config' => array (
			'type' => 'select',	
			'foreign_table' => 'tx_zeitabocrossselling_targets',	
			'foreign_table_where' => 'AND tx_zeitabocrossselling_targets.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_targets.sys_language_uid=###REC_FIELD_sys_language_uid###  ORDER BY tx_zeitabocrossselling_targets.title',	
			'size' => 10,	
			'minitems' => 0,
			'maxitems' => 100,
		)
	),
	'tx_zeitabocrossselling_platforms' => array (		
		'exclude' => 0,		
		'label' => 'LLL:EXT:zeitabo_crossselling/locallang_db.xml:pages.tx_zeitabocrossselling_crossselling_platforms',		
		'config' => array (
			'type' => 'select',	
			'foreign_table' => 'tx_zeitabocrossselling_platform',	
			'foreign_table_where' => 'AND tx_zeitabocrossselling_platform.pid=###PAGE_TSCONFIG_ID### AND tx_zeitabocrossselling_platform.sys_language_uid=###REC_FIELD_sys_language_uid###  ORDER BY tx_zeitabocrossselling_platform.title',	
			'size' => 10,	
			'minitems' => 0,
			'maxitems' => 100,
		)
	),
);

t3lib_div::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi1']='layout,select_key';


t3lib_extMgm::addPlugin(array(
	'LLL:EXT:zeitabo_crossselling/locallang_db.xml:tt_content.list_type_pi1',
	$_EXTKEY . '_pi1',
	t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');


if (TYPO3_MODE == 'BE') {
	$TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']['tx_zeitabocrossselling_pi1_wizicon'] = t3lib_extMgm::extPath($_EXTKEY).'pi1/class.tx_zeitabocrossselling_pi1_wizicon.php';
}
?>