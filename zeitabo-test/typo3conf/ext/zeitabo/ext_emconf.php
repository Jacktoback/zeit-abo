<?php

########################################################################
# Extension Manager/Repository config file for ext: "zeitabo"
#
# Auto generated 12-09-2007 16:29
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'DIE ZEIT Abo-Shop',
	'description' => 'Verwaltung und Darstellung der Abonnements und Produkte des ZEIT Abo-Shops',
	'category' => 'plugin',
	'author' => 'Patrick Bisplinghoff',
	'author_email' => 'patrick.bisplinghoff@clicktivities.net',
	'shy' => '',
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author_company' => 'clicktivities ag',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'cms' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'suggests' => array(
	),
	'_md5_values_when_last_written' => 'a:66:{s:9:"ChangeLog";s:4:"2711";s:10:"README.txt";s:4:"3672";s:17:"abo_images_ds.xml";s:4:"8a2c";s:32:"class.tx_zeitabo_BEfunctions.php";s:4:"3991";s:12:"ext_icon.gif";s:4:"2980";s:17:"ext_localconf.php";s:4:"308a";s:14:"ext_tables.php";s:4:"203e";s:14:"ext_tables.sql";s:4:"24b0";s:15:"flexform_ds.xml";s:4:"ca88";s:23:"flexform_ds.xml.bak.xml";s:4:"6bca";s:31:"icon_tx_zeitabo_abonnements.gif";s:4:"475a";s:28:"icon_tx_zeitabo_products.gif";s:4:"475a";s:31:"icon_tx_zeitabo_singlebooks.gif";s:4:"475a";s:13:"images_ds.xml";s:4:"d3f8";s:13:"locallang.xml";s:4:"bbe7";s:16:"locallang_db.xml";s:4:"e3ee";s:7:"tca.php";s:4:"d20f";s:19:"doc/wizard_form.dat";s:4:"66f6";s:20:"doc/wizard_form.html";s:4:"66cc";s:14:"pi1/ce_wiz.gif";s:4:"8bb7";s:28:"pi1/class.tx_zeitabo_pi1.php";s:4:"0923";s:36:"pi1/class.tx_zeitabo_pi1_wizicon.php";s:4:"3895";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"5e1a";s:17:"pi1/template.html";s:4:"904a";s:24:"pi1/static/editorcfg.txt";s:4:"32e0";s:20:"pi1/static/setup.txt";s:4:"f6a2";s:17:"res/b_aendern.gif";s:4:"b00d";s:16:"res/b_weiter.gif";s:4:"54d5";s:17:"res/b_zurueck.gif";s:4:"5ebf";s:13:"res/close.gif";s:4:"7958";s:14:"res/close2.gif";s:4:"196f";s:19:"res/close_weiss.jpg";s:4:"324d";s:24:"res/h_alte_anschrift.gif";s:4:"48ae";s:19:"res/h_anschrift.gif";s:4:"c319";s:28:"res/h_anschrift_abonnent.gif";s:4:"81cd";s:38:"res/h_anschrift_praemienempfaenger.gif";s:4:"5bce";s:31:"res/h_anschrift_schenkender.gif";s:4:"8400";s:17:"res/h_auswahl.gif";s:4:"e93f";s:19:"res/h_gutschein.gif";s:4:"085e";s:25:"res/h_lieferanschrift.gif";s:4:"d416";s:24:"res/h_neue_anschrift.gif";s:4:"d44f";s:24:"res/h_neuer_abonnent.gif";s:4:"44d3";s:29:"res/h_praemien_empfaenger.gif";s:4:"2eb6";s:28:"res/h_rechnungsanschrift.gif";s:4:"11fe";s:21:"res/h_schenkender.gif";s:4:"3edc";s:15:"res/h_umzug.gif";s:4:"c3b3";s:24:"res/h_zahlungswunsch.gif";s:4:"a791";s:25:"res/h_zahlungswunsch2.gif";s:4:"3ea8";s:22:"res/include_header.php";s:4:"1de0";s:16:"res/link_ico.gif";s:4:"dde8";s:18:"res/link_ico_2.gif";s:4:"1dc4";s:18:"res/link_ico_3.gif";s:4:"3445";s:14:"res/marquee.js";s:4:"6e2f";s:13:"res/order.css";s:4:"5c2a";s:16:"res/pfeile_l.gif";s:4:"2b66";s:16:"res/pfeile_r.gif";s:4:"85ca";s:17:"res/s_auswahl.gif";s:4:"880e";s:19:"res/s_auswahl_a.gif";s:4:"c506";s:15:"res/s_check.gif";s:4:"141c";s:17:"res/s_check_a.gif";s:4:"13a4";s:15:"res/s_daten.gif";s:4:"72ac";s:17:"res/s_daten_a.gif";s:4:"97de";s:17:"res/s_zahlart.gif";s:4:"8338";s:19:"res/s_zahlart_a.gif";s:4:"2b59";s:13:"res/trans.gif";s:4:"93e6";}',
);

?>