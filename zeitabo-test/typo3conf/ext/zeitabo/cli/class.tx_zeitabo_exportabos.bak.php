<?php


// Include basis cli class
// require_once( t3lib_extMgm::extPath( "scheduler" ).'class.tx_scheduler_task.php');

class tx_zeitabo_exportabos extends tx_scheduler_Task {
  var $fields = array(
    'Zahler-Anrede-Bezeichnung' => 'Zahler_Anrede_Bezeichnung',
    'Zahler-Vorname' => 'Zahler_Vorname',
    'Zahler-Name' => 'Zahler_Name',
    'Zahler-Strasse' => 'Zahler_Strasse',
    'Zahler-Firma' => 'Zahler_Adresszusatz',
    'Zahler-PLZ' => 'Zahler_PLZ',
    'Zahler-Ort' => 'Zahler_Ort',
    'Zahler-Land' => 'Zahler_Land',
    'Zahler-Geburtsdatum' => 'Zahler_Geburtsdatum',
    'Zahler-Telefon-Nr-1' => 'Zahler_Telefon_Nr_1',
    'Zahler-Email-Adresse' => 'Zahler_Email_Adresse',
    'Zahler-Zahlungsryhthmus' => 'Zahler_Zahlungsryhthmus',
    'Zahler-Zahlungsweg' => 'Zahler_Zahlungsweg',
    'Zahler-Konto-Inhaber' => 'Zahler_Konto_Inhaber',
    'Zahler-Konto' => 'Zahler_Konto',
    'Zahler-BLZ' => 'Zahler_BLZ',
    'Zahler-Kreditkartennummer' => 'Zahler_Kreditkartennummer',
    'Zahler-Kreditkarten-Institut' => 'Zahler_Kreditkarten_Institut',
    'Zahler-Kreditkarten-Inhaber' => 'Zahler_Kreditkarten_Inhaber',
    'Zahler-Kreditkarte-gueltig' => 'Zahler_Kreditkarte_gueltig',
    'Zugaben-Artikelnummer' => 'Zugaben_Artikelnummer',
    'Praemien-Artikelnummer' => 'Praemien_Artikelnummer',
    'Aktionsnummer' => 'Aktionsnummer',
    'Empfaenger-Anrede-Bezeichnung' => 'Empfaenger_Anrede_Bezeichnung',
    'Empfaenger-Vorname' => 'Empfaenger_Vorname',
    'Empfaenger-Name' => 'Empfaenger_Name',
    'Empfaenger-Strasse' => 'Empfaenger_Strasse',
    'Empfaenger-Firma' => 'Empfaenger_Adresszusatz',
    'Empfaenger-PLZ' => 'Empfaenger_PLZ',
    'Empfaenger-Ort' => 'Empfaenger_Ort',
    'Empfaenger-Land' => 'Empfaenger_Land',
    'Empfaenger-Geburtsdatum' => 'Empfaenger_Geburtsdatum',
    'Empfaenger-Telefon-Nr-1' => 'Empfaenger_Telefon_Nr_1',
    'Empfaenger-Email-Adresse' => 'Empfaenger_Email_Adresse',
    'Werber-Anrede-Bezeichnung' => 'Werber_Anrede_Bezeichnung',
    'Werber-Vorname' => 'Werber_Vorname',
    'Werber-Name' => 'Werber_Name',
    'Werber-Strasse' => 'Werber_Strasse',
    'Werber-Firma' => 'Werber_Adresszusatz',
    'Werber-PLZ' => 'Werber_PLZ',
    'Werber-Ort' => 'Werber_Ort',
    'Werber-Land' => 'Werber_Land',
    'Werber-Geburtsdatum' => 'Werber_Geburtsdatum',
    'Auftragsbeginn' => 'Auftragsbeginn',
    'Werbecode' => 'Werbecode',
    'Zahler-Kreditkarten-Referenz-ID' => 'Zahler_Kreditkarten_Referenz_ID',
    'anzahl' => 'anzahl',
    'Auftragsbeginn' => 'Auftragsbeginn',
    'Upselling_product' => 'Upselling_product',
    'uid' => 'uid',
  );
  
  function quote_fields( $s ) {
    return '`'.$s.'`';
  }

  function write_header() {
    $to_write = array();
    foreach ( $this->export_fields as $field ) {
      foreach ( $this->fields as $k => $v ) {
        if ( $field == $v ) {
          $to_write[] = $k;
          break;
        }
      }
    }
    fwrite( $this->file_h, implode( ';', $to_write ) . "\n" );      
  }

  function clean_string( $s ) {
     $s = utf8_decode( $s );
     $s = preg_replace( '/;/', ',', $s );
     $s = preg_replace( '/\n/', ' ', $s ); 
     
     return $s;
  }
  
  function get_abo_no_for_id( $id ) {
    if ( $id ) {
      $sql = "SELECT abo_no FROM tx_zeitabo_abonnements WHERE uid=$id";
      $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ));
      return $row['abo_no'];
    }
    else {
      return '';
    }
  }
  
  function get_mag_no_for_id( $id ) {
    if ( $id ) {
      $sql = "SELECT mag_no FROM tx_zeitabo_singlebooks WHERE uid=$id";
      $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ));
      //debug( $row );
      //debug( $sql );
      return $row['mag_no'];
    }
    else {
      return '';
    }
  }
  
  function get_prod_no_for_id( $id ) {
    if ( $id ) {
      $sql = "SELECT prod_no_1 FROM tx_zeitabo_products WHERE uid=$id";
      $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ));
      return $row['prod_no_1'];
    }
    else {
      return '';
    }
  }

  function get_address_for_row( $row, $k ) { 
	return $row[$k];

	$sql = "SELECT * FROM zeit_user WHERE uid={$row['uid']}";
        $row2 = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ));
	$v = '';
	switch ( $k ) {
		case 'Zahler_Strasse':
	    		$v = $row['Zahler_Strasse'];
			if ( $row2['Zahler_Adresszusatz'] ) {
				$v .= ', '.$row2['Zahler_Adresszusatz'];
			}
			break;
		case 'Empfaenger_Strasse':
                        $v = $row['Empfaenger_Strasse'];
			if ( $row2['Empfaenger_Adresszusatz'] ) {
				$v .= ', '.$row2['Empfaenger_Adresszusatz'];
			}
                        break;
		case 'Werber_Strasse':
                        $v = $row2['Werber_Strasse'];
			if ( $row2['Werber_Adresszusatz'] ) {
				$v .= ', '.$row2['Werber_Adresszusatz'];
			}
			break;
	}
	return $v;
  }
 
  function get_special_columns( $row ) {
    if ( preg_match( '/^tx_zeitabo_abonnements_/', $row['Aktionsnummer'] )) {
      $row['Aktionsnummer'] = $this->get_mag_no_for_id( $row['Zugaben_Artikelnummer'] );
      $row['Zugaben_Artikelnummer'] = '';
    }
    else {
	#if ( $row['uid'] == 270 ) print_r( $row );
      foreach ( $row as $k => $v ) {
        switch ( $k ) {
          case 'Aktionsnummer':
            $v = $this->get_abo_no_for_id( $v );
            break;
          case 'Zugaben_Artikelnummer':
          case 'Praemien_Artikelnummer':
            $v = $this->get_prod_no_for_id( $v );
            break;
        }
        $row[$k] = $v; 
      }
    }
    return $row;
  }
  
  function write_row( $row ) {
print_r( $row );
print "<br />";
    $to_write = array();
    $row = $this->get_special_columns( $row );
    foreach( $this->export_fields as $field ) {
      $to_write[] = $this->clean_string( $row[$field] );
    }
    fwrite( $this->file_h, implode( ';', $to_write ) . "\n" );      
  }

  function encrypt( $f_name ) {
    $output = array();
    exec( "/usr/bin/gpg --output {$f_name}.gpg --encrypt --recipient kimba_test@guj.de --yes {$f_name} 2>&1", $output, $ret );
    $msg = implode( "<br />", $output );
    
    if ( $ret <> 0 ) {
      throw new Exception( "gpg failed ($ret): $msg" );
    }
    return $ret;    
  }
      
  function execute() {
    $this->db = $GLOBALS['TYPO3_DB'];

    $f_name = '/var/www/zeitabo-test/fileadmin/export/export.csv';
    $this->file_h = fopen( $f_name, 'w' );
$this->fields['x']='y';    
print_r( $this->fields );
    $this->export_fields = array_values( $this->fields );
print_r( $this->export_fields );
    $select = implode( ',', array_map( array( $this, 'quote_fields' ), $this->export_fields ));
    $sql = "
      SELECT 
        $select
      FROM
        zeit_user
      ORDER BY
        timestamp DESC
    ";
   print "$sql\n"; 
    $this->write_header();
    
    $res = $this->db->sql_query( $sql );
    while ( $row = $this->db->sql_fetch_assoc( $res )) {
      $this->write_row( $row );
    } 
    fclose( $this->file_h );
    
    $ret = $this->encrypt( $f_name );

    return $ret == 0;
  }

}

?>
