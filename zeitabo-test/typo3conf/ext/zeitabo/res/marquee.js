var step = 1, time = 20, mx, actX = false, dragObjects = false, oldDetails = false;

function moveObjects() {
	var objects = document.getElementById('marquee').getElementsByTagName('div');
	for(i=0;i<objects.length;i++) {
		objects[i].style.left = parseInt(objects[i].style.left) - step;
		if(parseInt(objects[i].style.left) <= -200) {
			objects[i].style.left = productWidth - 200;
		}
		if(parseInt(objects[i].style.left) > productWidth - 200) {
			window.status = objects[i].style.left;
			objects[i].style.left = -200;
		}
	}
}

function start() {
	dragObjects = false;
	actX = false;
	lb = window.setInterval("moveObjects()", time);
}
			
function intercept() {
	window.clearInterval(lb);
}

function showDetails(obj) {
	if(oldDetails) {
		hideDetails(oldDetails);
	}
	var theObj = document.getElementById(obj).style;
	theObj.visibility = 'visible';
	oldDetails = obj;
}

function hideDetails(obj) {
	var theObj = document.getElementById(obj).style;
	theObj.visibility = 'hidden';
}

function selectProduct(product) {
	document.getElementById('product-selected').src = document.getElementById(product).src;
	document.productselection.selection.selectedIndex = 1;
}