<?php

class tx_zeitabo_userfuncs {

  function sanitize_link( $content, $conf ) {
    return preg_replace( '/&amp;amp;/', '&amp;', $content[ 'TAG' ] );
  }

  function landingpage_redir( $content, $conf ) {
    $arr = array( );
    $arr[ 'abo' ] = $this->cObj->data[ 'field_content' ];
    $arr[ 'no_back_button' ] = 1;

    if( $this->cObj->data[ 'field_product' ] ) {
      $resProds = $GLOBALS[ 'TYPO3_DB' ]->exec_SELECT_mm_query(
              'tx_zeitabo_products.uid', 'tx_zeitabo_abonnements', 'tx_zeitabo_abonnements_abo_products_mm', 'tx_zeitabo_products', 'AND tx_zeitabo_abonnements.uid = ' . $this->cObj->data[ 'field_content' ] . ' AND tx_zeitabo_products.hidden=0 AND tx_zeitabo_products.deleted=0', '', 'tx_zeitabo_abonnements_abo_products_mm.sorting ASC', ''
      );
      $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $resProds );
      $arr[ 'selection' ] = $row[ 'uid' ];
    }

    $lnk = $this->cObj->currentPageUrl( $arr, $this->cObj->data[ 'field_form_page' ] );
    header( 'Location: /' . $lnk );
    die();
  }

  function load_user_item( $content, $conf ) {
//    debug($conf);
    $hash = t3lib_div::_GP( 'item' );

    if( $GLOBALS[ 'TSFE' ]->register[ 'tx_zeitabo_items' ][ $hash ] ) {
      $row = $GLOBALS[ 'TSFE' ]->register[ 'tx_zeitabo_items' ][ $hash ];
    }
    else {
      $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( '*', 'zeit_user', "hash='$hash'" );
//    debug( $q );
      $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
      $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res );

      $GLOBALS[ 'TSFE' ]->register[ 'tx_zeitabo_items' ][ $hash ] = $row;
    }

    $content = $GLOBALS[ 'TSFE' ]->cObj->stdWrap( $row[ $conf[ 'field' ] ], $conf[ 'stdWrap.' ] );
    return $content;
  }

  function get_foreign_price_js( $content, $conf ) {
    $arrPrices = $GLOBALS[ 'TSFE' ]->tmpl->setup[ 'plugin.' ][ 'tx_zeitabo_pi1.' ][ 'prices.' ][ 'foreign.' ];


    $ret = 'var prices = new Array();';
    foreach( $arrPrices as $key1 => $val1 ) {
      $ret .= chr( 10 ) . 'prices["' . $key1 . '"] = new Array();';
      foreach( $val1 as $key2 => $val2 ) {
        if(!is_array($val2)){
            $ret .= chr( 10 ) . 'prices["' . $key1 . '"]["' . $key2 . '"] = "' . $val2 . '";';
        } else {
            foreach($val2 as $key3 => $val3){
                $ret .= chr( 10 ) . 'prices["' . $key1 . '"]["' . $key2 . '"] = new Array();';
                $ret .= chr( 10 ) . 'prices["' . $key1 . '"]["' . $key2 . '"]["' . $key3 . '"] = "' . $val3 . '";';
            }
        }
      }
    }
      $is_aw = 0;
      $adwords = $GLOBALS["TSFE"]->fe_user->getKey("ses","getvars");
      if(!empty($adwords)){
          $adWordsData = unserialize($adwords);

          $adWordID = NULL;
          foreach($adWordsData as $k => $v){
              if($k == 'r' && $v == 'aw'){
                  $is_aw = 1;
              }
          }
      }
      if($is_aw == 1){
          $ret .= chr( 10 ) . 'var aw = 1;';
      } else {
          $ret .= chr( 10 ) . 'var aw = 0;';
      }
    return $ret;
  }

  function get_foreign_abo_no_from_post( $content, $conf ) {
    $fh = t3lib_div::GPvar( 'formhandler' );
    $sel1 = $fh['selection1'];
    $sel2 = $fh['selection2'];
    
    $arrPrices = $GLOBALS[ 'TSFE' ]->tmpl->setup[ 'plugin.' ][ 'tx_zeitabo_pi1.' ][ 'prices.' ][ 'foreign.' ];

    $abo_no = $arrPrices[$sel1]["{$sel2}."]['abo_no'];
    
    return $abo_no;
  }

    function price_foreign( $content, $conf )
    {
        $price = 0;

        $abo = t3lib_div::_GP('abo');


        $fh = t3lib_div::_GP('formhandler');
        if ($fh['abo'])
            $abo = $fh['abo'];

        $abo = preg_replace('/^.*?(\d+)$/', '\1', $abo);

        $select = "abo_price, abo_price_student";
        $table = 'tx_zeitabo_abonnements';
        $where = " l18n_parent=".$abo ;
        $where .= " AND sys_language_uid = ".$conf['lang']." AND hidden = 0 AND deleted = 0 ";
        $order = '';
        $group = '';
        $limit = '1';
        $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

        $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );

        while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
            if($conf['student'] == 1){
                $price = $row[ 'abo_price_student' ];
            } else {
                $price = $row[ 'abo_price' ];
            }

        }

        return $price;
    }

  function sb_texts_for_all_langs( $content, $conf ) {
    $abo = t3lib_div::_GP( 'abo' );
    $selection = t3lib_div::_GP( 'selection' );

    $fh = t3lib_div::_GP( 'formhandler' );
    if( $fh[ 'abo' ] )
      $abo = $fh[ 'abo' ];
    if( $fh[ 'selection' ] )
      $selection = $fh[ 'selection' ];

    $abo = preg_replace( '/^.*?(\d+)$/', '\1', $abo );
    $selection = (int)$selection;

    // get texts from (shadow-)abo
    $select = "abo_txt_order, sys_language_uid";
    $table = 'tx_zeitabo_abonnements';
    $where = "(uid=$abo OR l18n_parent=$abo)";
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = 'sys_language_uid';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
    $texts = array(
        'regular' => array( ),
        'student' => array( )
    );
    while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
      $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_order' ];
//      $texts[ 'student' ][ $row[ 'sys_language_uid' ] ] = '';
    }

    // overlay with texts from singlebook if present
    $select = "abo_txt_order, sys_language_uid";
    $table = 'tx_zeitabo_singlebooks';
    $where = "(uid=$selection OR l18n_parent=$selection)";
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = 'sys_language_uid';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
    while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
      if( $row[ 'abo_txt_order' ] ) {
        $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_order' ];
      }
//      $texts[ 'student' ][ $row[ 'sys_language_uid' ] ] = '';
    }


    foreach( array( '0', '1', '2' ) as $lang ) {
      $default_text = array( );
      foreach( array( 'regular' ) as $abo_type ) {
        $style_extension = ( $abo_type == 'regular' ? '' : "_$abo_type");
        switch( $lang ) {
          case '1': $country = "country_at$style_extension";
            break;
          case '2': $country = "country_ch$style_extension";
            break;
          default: $country = "country_de$style_extension";
            $default_text[ $abo_type ] = $texts[ $abo_type ][ $lang ];
            break;
        }
//      $dsp = ' style="display:none;"';
        if( $GLOBALS[ 'TSFE' ]->sys_language_uid == $row[ 'sys_language_uid' ] )
          $dsp = "";

        $text = $texts[ $abo_type ][ $lang ];
        if( !$text )
          $text = $default_text[ $abo_type ];
        if( $conf[ 'parseFunc.' ] ) {
          $text = $this->cObj->parseFunc( $text, $conf[ 'parseFunc.' ] );
        }
        $content .= "<div class=\"countries $country\" $dsp>$text</div>";
      }
    }
    return $content;
  }

  function sb_delivery_texts_for_all_langs( $content, $conf ) {
    $abo = t3lib_div::_GP( 'abo' );
    $selection = t3lib_div::_GP( 'selection' );

    $fh = t3lib_div::_GP( 'formhandler' );
    if( $fh[ 'abo' ] )
      $abo = $fh[ 'abo' ];
    if( $fh[ 'selection' ] )
      $selection = $fh[ 'selection' ];

    $abo = preg_replace( '/^.*?(\d+)$/', '\1', $abo );
    $selection = (int)$selection;

    // get texts from (shadow-)abo
    $select = "abo_txt_delivery, sys_language_uid";
    $table = 'tx_zeitabo_abonnements';
    $where = "(uid=$abo OR l18n_parent=$abo)";
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = 'sys_language_uid';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
    $texts = array(
        'regular' => array( ),
        'student' => array( )
    );
    while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
//      debug( $row );
      $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_delivery' ];
//      $texts[ 'student' ][ $row[ 'sys_language_uid' ] ] = '';
    }

    // overlay with texts from singlebook if present
    $select = "abo_txt_delivery, sys_language_uid";
    $table = 'tx_zeitabo_singlebooks';
    $where = "(uid=$selection OR l18n_parent=$selection)";
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = 'sys_language_uid';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
    while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
      if( $row[ 'abo_txt_order' ] ) {
        $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_delivery' ];
      }
    }

    $lang = "{$GLOBALS[ 'TSFE' ]->sys_language_uid}";
    $content = $texts[ 'regular' ][ $lang ];
    if( !$content ) {
      $content = $texts[ 'regular' ][ '0' ];
    }
    if( $conf[ 'parseFunc.' ] ) {
      $content = $this->cObj->parseFunc( $content, $conf[ 'parseFunc.' ] );
    }
    return $content;
  }

  function abo_texts_for_all_langs( $content, $conf ) {
    $abo = (int)t3lib_div::_GP( 'abo' );
    $selection = (int)t3lib_div::_GP( 'selection' );

    $fh = t3lib_div::_GP( 'formhandler' );
    if( $fh[ 'abo' ] )
      $abo = $fh[ 'abo' ];
    if( $fh[ 'selection' ] )
      $selection = $fh[ 'selection' ];

    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery(
            'prod_type', 'tx_zeitabo_products', "uid=$selection" . $GLOBALS[ 'TSFE' ]->sys_page->enableFields( 'tx_zeitabo_products' )
    );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
    $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res );

    $fields = "abo_txt_order,abo_txt_student_order";
    if( $row[ 'prod_type' ] == 2 ) {
      $fields = 'abo_txt_order2 as abo_txt_order, abo_txt_student_order2 as abo_txt_student_order';
    }

    $select = "$fields, sys_language_uid";
    $table = 'tx_zeitabo_abonnements';
    $where = "(uid=$abo OR l18n_parent=$abo)";
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = 'sys_language_uid';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
    $texts = array(
        'regular' => array( ),
        'student' => array( )
    );
    while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
      $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_order' ];
      $texts[ 'student' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_student_order' ];
    }

    foreach( array( '0', '1', '2' ) as $lang ) {
      $default_text = array( );
      foreach( array( 'regular', 'student' ) as $abo_type ) {
        $style_extension = ( $abo_type == 'regular' ? '' : "_$abo_type");
        switch( $lang ) {
          case '1': $country = "country_at$style_extension";
            break;
          case '2': $country = "country_ch$style_extension";
            break;
          default: $country = "country_de$style_extension";
            $default_text[ $abo_type ] = $texts[ $abo_type ][ $lang ];
            break;
        }
//      $dsp = ' style="display:none;"';
        if( $GLOBALS[ 'TSFE' ]->sys_language_uid == $row[ 'sys_language_uid' ] )
          $dsp = "";

        $text = $texts[ $abo_type ][ $lang ];
        if( !$text )
          $text = $default_text[ $abo_type ];
        if( $conf[ 'parseFunc.' ] ) {
          $text = $this->cObj->parseFunc( $text, $conf[ 'parseFunc.' ] );
        } $content .= "<div class=\"countries $country\" $dsp>$text</div>";
      }
    }
    return $content;
  }

    function abo_hints_for_all_langs( $content, $conf ) {
        $abo = (int)t3lib_div::_GP( 'abo' );
        $selection = (int)t3lib_div::_GP( 'selection' );

        $fh = t3lib_div::_GP( 'formhandler' );
        if( $fh[ 'abo' ] )
            $abo = $fh[ 'abo' ];
        if( $fh[ 'selection' ] )
            $selection = $fh[ 'selection' ];

        $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery(
            'prod_type', 'tx_zeitabo_products', "uid=$selection" . $GLOBALS[ 'TSFE' ]->sys_page->enableFields( 'tx_zeitabo_products' )
        );

        $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
        $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res );

        $fields = "abo_txt_order2,abo_txt_student_order2";


        $select = "$fields, sys_language_uid";
        $table = 'tx_zeitabo_abonnements';
        $where = "(uid=$abo OR l18n_parent=$abo)";
        $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
        $order = 'sys_language_uid';
        $group = '';
        $limit = '';
        $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

        $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
        $texts = array(
            'regular' => array( ),
            'student' => array( )
        );
        while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
            $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_order2' ];
            $texts[ 'student' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_student_order2' ];
        }

        foreach( array( '0', '1', '2' ) as $lang ) {
            $default_text = array( );
            foreach( array( 'regular', 'student' ) as $abo_type ) {
                $style_extension = ( $abo_type == 'regular' ? '' : "_$abo_type");
                switch( $lang ) {
                    case '1': $country = "countries country_at$style_extension pricehint_at$style_extension";
                        break;
                    case '2': $country = "countries country_ch$style_extension pricehint_ch$style_extension";
                        break;
                    default: $country = "countries country_de$style_extension pricehint_de$style_extension";
                    $default_text[ $abo_type ] = $texts[ $abo_type ][ $lang ];
                    break;
                }
//      $dsp = ' style="display:none;"';
                if( $GLOBALS[ 'TSFE' ]->sys_language_uid == $row[ 'sys_language_uid' ] )
                    $dsp = "";

                $text = $texts[ $abo_type ][ $lang ];
                if( !$text )
                    $text = $default_text[ $abo_type ];
                if( $conf[ 'parseFunc.' ] ) {
                    $text = $this->cObj->parseFunc( $text, $conf[ 'parseFunc.' ] );
                } $content .= "<div class=\"pricehints $country\" $dsp>$text</div>";
            }
        }
        return $content;
    }

//        10 < lib.abo_records_abo_txt_order
//      10 {
//        override_language = 1
//        override_language.with = 0
//        wrap = <div class="countries country_de">|</div>
//      }
//      20 < lib.abo_records_abo_txt_order
//      20 {
//        override_language = 1
//        override_language.with = 1
//        wrap = <div class="countries country_at">|</div>
//      }
//      30 < lib.abo_records_abo_txt_order
//      30 {
//        override_language = 1
//        override_language.with = 2
//        wrap = <div class="countries country_ch">|</div>
//      }

    function abo_hints_for_all_langs_geraete( $content, $conf ) {
        $abo = (int)t3lib_div::_GP( 'abo' );
        $selection = (int)t3lib_div::_GP( 'selection' );

        $fh = t3lib_div::_GP( 'formhandler' );
        if( $fh[ 'abo' ] )
            $abo = $fh[ 'abo' ];
        if( $fh[ 'selection' ] )
            $selection = $fh[ 'selection' ];

        $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery(
            'prod_type', 'tx_zeitabo_products', "uid=$selection" . $GLOBALS[ 'TSFE' ]->sys_page->enableFields( 'tx_zeitabo_products' )
        );

        $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
        $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res );

        $fields = "abo_txt_student_order";


        $select = "$fields, sys_language_uid";
        $table = 'tx_zeitabo_abonnements';
        $where = "(uid=$abo OR l18n_parent=$abo)";
        $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
        $order = 'sys_language_uid';
        $group = '';
        $limit = '';
        $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

        $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
        $texts = array(
            'regular' => array( ),
            'student' => array( )
        );
        while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
            $texts[ 'regular' ][ $row[ 'sys_language_uid' ] ] = $row[ 'abo_txt_student_order' ];
        }

        foreach( array( '0', '1', '2' ) as $lang ) {
            $default_text = array( );
            foreach( array( 'regular', 'student' ) as $abo_type ) {
                $style_extension = ( $abo_type == 'regular' ? '' : "_$abo_type");
                switch( $lang ) {
                    case '1': $country = "pricehint_at$style_extension";
                        break;
                    case '2': $country = "pricehint_ch$style_extension";
                        break;
                    default: $country = "pricehint_de$style_extension";
                    $default_text[ $abo_type ] = $texts[ $abo_type ][ $lang ];
                    break;
                }
//      $dsp = ' style="display:none;"';
                if( $GLOBALS[ 'TSFE' ]->sys_language_uid == $row[ 'sys_language_uid' ] )
                    $dsp = "";

                $text = $texts[ $abo_type ][ $lang ];
                if( !$text )
                    $text = $default_text[ $abo_type ];
                if( $conf[ 'parseFunc.' ] ) {
                    $text = $this->cObj->parseFunc( $text, $conf[ 'parseFunc.' ] );
                } $content .= "<div class=\"pricehints $country\" $dsp>$text</div>";
            }
        }
        return $content;
    }

  function cObj_fixed_language_wrapper( $content, $conf ) {
    debug( $conf[ 'tables' ], 'tables' );
    debug( $conf[ 'override_language' ], 'override_language' );
    debug( $conf[ 'override_language.' ], 'override_language.' );
    debug( $conf[ 'conf.' ] );
    if( $conf[ 'override_language' ] ) {
      $lang = $conf[ 'override_language.' ][ 'with' ];

      $uid = $GLOBALS[ 'TSFE' ]->sys_language_uid;
      $cnt = $GLOBALS[ 'TSFE' ]->sys_language_content;

      $GLOBALS[ 'TSFE' ]->sys_language_uid = $lang;
      $GLOBALS[ 'TSFE' ]->sys_language_content = $lang;
      $content = $GLOBALS[ 'TSFE' ]->cObj->cObjGetSingle( $conf[ 'base_cObject' ], $conf );

      $GLOBALS[ 'TSFE' ]->sys_language_uid = $uid;
      $GLOBALS[ 'TSFE' ]->sys_language_content = $cnt;
    }
    else {
      $content = $GLOBALS[ 'TSFE' ]->cObj->cObjGetSingle( $conf[ 'base_cObject' ], $conf );
    }
    debug( $content, 'content' );
    return $content;
  }

}

?>
