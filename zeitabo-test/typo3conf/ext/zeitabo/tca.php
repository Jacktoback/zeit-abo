<?php

if (!defined('TYPO3_MODE'))
    die('Access denied.');

$TCA['tx_zeitabo_blz'] = array(
    'ctrl' => $TCA['tx_zeitabo_blz']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,starttime,endtime,blz,merkmal,bezeichnung,plz,ort,kurzbezeichnung,pan,bic,methode,datensatznr,aenderungskennzeichen,blzloeschung,nachfolgeblz'
    ),
    'feInterface' => $TCA['tx_zeitabo_blz']['feInterface'],
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
                )
            )
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_zeitabo_blz',
                'foreign_table_where' => 'AND tx_zeitabo_blz.pid=###CURRENT_PID### AND tx_zeitabo_blz.sys_language_uid IN (-1,0)',
            )
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough'
            )
        ),
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config' => array(
                'type' => 'check',
                'default' => '0'
            )
        ),
        'starttime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => '8',
                'max' => '20',
                'eval' => 'date',
                'default' => '0',
                'checkbox' => '0'
            )
        ),
        'endtime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => '8',
                'max' => '20',
                'eval' => 'date',
                'checkbox' => '0',
                'default' => '0',
                'range' => array(
                    'upper' => mktime(3, 14, 7, 1, 19, 2038),
                    'lower' => mktime(0, 0, 0, date('m') - 1, date('d'), date('Y'))
                )
            )
        ),
        'blz' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.blz',
            'config' => array(
                'type' => 'input',
                'size' => '10',
                'max' => '10',
                'eval' => 'required,trim',
            )
        ),
        'merkmal' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.merkmal',
            'config' => array(
                'type' => 'input',
                'size' => '10',
                'max' => '10',
                'eval' => 'required,trim',
            )
        ),
        'bezeichnung' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.bezeichnung',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '255',
                'eval' => 'trim',
            )
        ),
        'plz' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.plz',
            'config' => array(
                'type' => 'input',
                'size' => '10',
                'max' => '10',
                'eval' => 'trim',
            )
        ),
        'ort' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.ort',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '100',
                'eval' => 'trim',
            )
        ),
        'kurzbezeichnung' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.kurzbezeichnung',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '100',
                'eval' => 'trim',
            )
        ),
        'pan' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.pan',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '50',
                'eval' => 'trim',
            )
        ),
        'bic' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.bic',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '50',
                'eval' => 'trim',
            )
        ),
        'methode' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.methode',
            'config' => array(
                'type' => 'input',
                'size' => '10',
                'max' => '10',
                'eval' => 'trim',
            )
        ),
        'datensatznr' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.datensatznr',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '100',
            )
        ),
        'aenderungskennzeichen' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.aenderungskennzeichen',
            'config' => array(
                'type' => 'input',
                'size' => '48',
                'max' => '100',
                'eval' => 'trim',
            )
        ),
        'blzloeschung' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.blzloeschung',
            'config' => array(
                'type' => 'input',
                'size' => '10',
                'max' => '10',
                'eval' => 'trim',
            )
        ),
        'nachfolgeblz' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz.nachfolgeblz',
            'config' => array(
                'type' => 'input',
                'size' => '10',
                'max' => '10',
                'eval' => 'trim',
            )
        ),
    ),
    'types' => array(
        '0' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, blz, merkmal, bezeichnung, plz, ort, kurzbezeichnung, pan, bic, methode, datensatznr, aenderungskennzeichen, blzloeschung, nachfolgeblz')
    ),
    'palettes' => array(
        '1' => array('showitem' => 'starttime, endtime')
    )
);

$TCA["tx_zeitabo_abonnements"] = Array(
    "ctrl" => $TCA["tx_zeitabo_abonnements"]["ctrl"],
    "interface" => Array(
        "showRecordFieldList" => "sys_language_uid,l18n_parent,l18n_diffsource,hidden,starttime,endtime,fe_group,abo_name,abo_no,abo_no_student,abo_no_kombi_digital,abo_price,abo_price_student,abo_offerform,abo_shipping,abo_img,abo_img_role,abo_hl_long,abo_hl_short,abo_txt_long,abo_txt_middle,abo_txt_short,abo_txt_role,abo_txt_benefits,abo_products,abo_adwords"
    ),
    "feInterface" => $TCA["tx_zeitabo_abonnements"]["feInterface"],
    "columns" => Array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
                )
            )
        ),
        'l18n_parent' => Array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config' => Array(
                'type' => 'select',
                'items' => Array(
                    Array('', 0),
                ),
                'foreign_table' => 'tx_zeitabo_abonnements',
                'foreign_table_where' => 'AND tx_zeitabo_abonnements.pid=###CURRENT_PID### AND tx_zeitabo_abonnements.sys_language_uid IN (-1,0)',
            )
        ),
        'l18n_diffsource' => Array(
            'config' => Array(
                'type' => 'passthrough'
            )
        ),
        "hidden" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
            "config" => Array(
                "type" => "check",
                "default" => "0"
            )
        ),
        "starttime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.starttime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "default" => "0",
                "checkbox" => "0"
            )
        ),
        "endtime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.endtime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "checkbox" => "0",
                "default" => "0",
                "range" => Array(
                    "upper" => mktime(0, 0, 0, 12, 31, 2020),
                    "lower" => mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"))
                )
            )
        ),
        "fe_group" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.fe_group",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("", 0),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login", -1),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.any_login", -2),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.usergroups", "--div--")
                ),
                "foreign_table" => "fe_groups"
            )
        ),
        "abo_name" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_name",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "abo_name2" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_name2",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),
        "abo_no" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_no",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "abo_no_student" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_no_student",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),
		 "abo_no_kombi_digital" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_no_kombi_digital",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),
        "abo_price" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_price",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim,double2",
            )
        ),
        "abo_price_student" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_price_student",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim,double2",
            )
        ),
        "abo_offerform" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.0", "0"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.1", "1"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.2", "2"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.3", "3"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.4", "4"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.5", "5"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.6", "6"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.7", "7"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_offerform.I.8", "8"),
                ),
                "size" => 1,
                "maxitems" => 1,
            )
        ),
        "abo_shipping" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_shipping",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_shipping.I.0", "0"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_shipping.I.1", "1"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_shipping.I.2", "2"),
                ),
                "size" => 1,
                "maxitems" => 1,
            )
        ),
//		"abo_images" => Array (
//			"exclude" => 1,
//			"label" => "Abo-Bilder (in verschiedenen Farbwelten)",
//			"config" => Array (
//				"type" => "flex",
//				"ds" => array(
//					"default" => "FILE:EXT:zeitabo/images_ds.xml",
//				)
//			)
//		),
        "abo_txt_order" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_order",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_order2" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_order2",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_delivery" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_delivery",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_delivery2" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_delivery2",
            "config" => Array(
                "type" => "text",
                "cols" => "30", 
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_student_order" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_student_order",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_student_order2" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_student_order2",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_student_delivery" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_student_delivery",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_student_delivery2" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_student_delivery2",
            "config" => Array(
                "type" => "text",
                "cols" => "30", 
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_txt_benefits" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_benefits",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "abo_products" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_products",
            "config" => Array(
                "type" => "group",
                "internal_type" => "db",
                "allowed" => "tx_zeitabo_products",
                "size" => 20,
                "minitems" => 0,
                "maxitems" => 50,
                "MM" => "tx_zeitabo_abonnements_abo_products_mm",
            )
        ),
        'abo_user_form_image' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_user_form_image',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'file',
                'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
                'uploadfolder' => 'uploads/tx_zeitabo',
                'size' => 2,
                'minitems' => 0,
                'maxitems' => 2,
            )
        ),
        'abo_adwords' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements_adwords.abo_adwords',
            'config' => Array (
                'type' => 'inline',
                'foreign_table' => 'tx_zeitabo_abonnements_adwords',
                'foreign_field' => 'parent_id',
                'foreign_label' => 'adword',
                'maxitems' => 1000,
                'appearance' => Array(
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                    'useSortable' => 1,
                ),
            )
        ),
    ),
    "types" => Array(
        "0" => Array("showitem" => "sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, abo_name, abo_name2, abo_no, abo_no_student, abo_no_kombi_digital, abo_price, abo_price_student, abo_images, abo_txt_order;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], abo_txt_order2;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], abo_txt_delivery;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css],abo_txt_delivery2;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], abo_txt_student_order;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], abo_txt_student_order2;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], abo_txt_student_delivery;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css],abo_txt_student_delivery2;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css],abo_txt_benefits;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css],abo_products,abo_user_form_image, abo_adwords")
    ),
    "palettes" => Array(
        "1" => Array("showitem" => "starttime, endtime, fe_group")
    )
);



$TCA["tx_zeitabo_products"] = Array(
    "ctrl" => $TCA["tx_zeitabo_products"]["ctrl"],
    "interface" => Array(
        "showRecordFieldList" => "sys_language_uid,l18n_parent,l18n_diffsource,hidden,starttime,endtime,fe_group,prod_type,prod_no_1,prod_no_2,prod_no_3,prod_name,prod_img,product_image_mobile,prod_hl,prod_txt,prod_txt_mobile,prod_count,prod_copayment,prod_abos,product_group"
    ),
    "feInterface" => $TCA["tx_zeitabo_products"]["feInterface"],
    "columns" => Array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
                )
            )
        ),
        'l18n_parent' => Array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config' => Array(
                'type' => 'select',
                'items' => Array(
                    Array('', 0),
                ),
                'foreign_table' => 'tx_zeitabo_products',
                'foreign_table_where' => 'AND tx_zeitabo_products.pid=###CURRENT_PID### AND tx_zeitabo_products.sys_language_uid IN (-1,0)',
            )
        ),
        'l18n_diffsource' => Array(
            'config' => Array(
                'type' => 'passthrough'
            )
        ),
        "hidden" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
            "config" => Array(
                "type" => "check",
                "default" => "0"
            )
        ),
        "starttime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.starttime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "default" => "0",
                "checkbox" => "0"
            )
        ),
        "endtime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.endtime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "checkbox" => "0",
                "default" => "0",
                "range" => Array(
                    "upper" => mktime(0, 0, 0, 12, 31, 2020),
                    "lower" => mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"))
                )
            )
        ),
        "fe_group" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.fe_group",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("", 0),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login", -1),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.any_login", -2),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.usergroups", "--div--")
                ),
                "foreign_table" => "fe_groups"
            )
        ),
        "prod_type" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_type",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_type.I.0", "0"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_type.I.1", "1"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_type.I.2", "2"),
                ),
                "size" => 1,
                "maxitems" => 1,
            )
        ),
        "prod_no_1" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_no_1",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "prod_no_2" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_no_2",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),
        "prod_no_3" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_no_3",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),
        "prod_name" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_name",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim,required",
            )
        ),
     /*   "prod_images" => Array(
            "exclude" => 1,
            "label" => "Produkt-Bilder (in verschiedenen Farbwelten)",
            "config" => Array(
                "type" => "flex",
                "ds" => array(
                    "default" => "FILE:EXT:zeitabo/images_ds.xml",
                )
            )
        ), */
        'product_image' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'file',
                'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
                'uploadfolder' => 'uploads/tx_zeitabo',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            )
        ),
        'product_image_detail' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image_detail',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'file',
                'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
                'uploadfolder' => 'uploads/tx_zeitabo',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            )
        ),
        'product_image_thankyou' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image_thankyou',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'file',
                'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
                'uploadfolder' => 'uploads/tx_zeitabo',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            )
        ),
        'product_image_mobile' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image_mobile',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'file',
                'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
                'uploadfolder' => 'uploads/tx_zeitabo',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            )
        ),
        "prod_hl" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_hl",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "prod_txt" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_txt",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
            )
        ),
        "prod_txt_mobile" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_txt_mobile",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
        "prod_count" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_count",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "8",
                "eval" => "int",
                "checkbox" => "0",
                "range" => Array(
                    "upper" => "1000",
                    "lower" => "10"
                ),
                "default" => 0
            )
        ),
        "prod_copayment" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_copayment",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "eval" => "trim",
                "checkbox" => "0",
                "default" => ""
            )
        ),
        "prod_abos" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.prod_abos",
            "config" => Array(
                "type" => "group",
                "internal_type" => "db",
                "allowed" => "tx_zeitabo_abonnements",
                "size" => 20,
                "minitems" => 0,
                "maxitems" => 100,
                "MM" => "tx_zeitabo_products_prod_abos_mm",
            )
        ),
        'product_group' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_group',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_zeitabo_product_groups',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            )
        ),
    ),
    "types" => Array(
        "0" => Array("showitem" => "sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, prod_type, prod_no_1, prod_name, product_image, product_image_detail, product_image_thankyou, product_image_mobile, prod_images, prod_hl, prod_txt, prod_txt_mobile;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], prod_count, prod_copayment, product_group")
    ),
    "palettes" => Array(
        "1" => Array("showitem" => "starttime, endtime, fe_group")
    )
);



$TCA["tx_zeitabo_singlebooks"] = Array(
    "ctrl" => $TCA["tx_zeitabo_singlebooks"]["ctrl"],
    "interface" => Array(
        "showRecordFieldList" => "sys_language_uid,l18n_parent,l18n_diffsource,hidden,starttime,endtime,fe_group,mag_type,mag_no,mag_name,mag_release,mag_img,mag_txt"
    ),
    "feInterface" => $TCA["tx_zeitabo_singlebooks"]["feInterface"],
    "columns" => Array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
                )
            )
        ),
        'l18n_parent' => Array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config' => Array(
                'type' => 'select',
                'items' => Array(
                    Array('', 0),
                ),
                'foreign_table' => 'tx_zeitabo_singlebooks',
                'foreign_table_where' => 'AND tx_zeitabo_singlebooks.pid=###CURRENT_PID### AND tx_zeitabo_singlebooks.sys_language_uid IN (-1,0)',
            )
        ),
        'l18n_diffsource' => Array(
            'config' => Array(
                'type' => 'passthrough'
            )
        ),
        "hidden" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
            "config" => Array(
                "type" => "check",
                "default" => "0"
            )
        ),
        "starttime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.starttime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "default" => "0",
                "checkbox" => "0"
            )
        ),
        "endtime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.endtime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "checkbox" => "0",
                "default" => "0",
                "range" => Array(
                    "upper" => mktime(0, 0, 0, 12, 31, 2020),
                    "lower" => mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"))
                )
            )
        ),
        "fe_group" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.fe_group",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("", 0),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login", -1),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.any_login", -2),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.usergroups", "--div--")
                ),
                "foreign_table" => "fe_groups"
            )
        ),
        "mag_type" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.0", "0"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.1", "1"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.2", "2"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.3", "3"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.4", "4"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.5", "5"),
                    Array("LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_type.I.6", "6"),
                ),
                "size" => 1,
                "maxitems" => 1,
            )
        ),
        "mag_no" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_no",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "mag_name" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_name",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "mag_release" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_release",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "trim",
                "checkbox" => "0",
                "default" => "0"
            )
        ),
        "mag_images" => Array(
            "exclude" => 1,
            "label" => "Einzelheft-Bilder (in verschiedenen Farbwelten)",
            "config" => Array(
                "type" => "flex",
                "ds" => array(
                    "default" => "FILE:EXT:zeitabo/images_ds.xml",
                )
            )
        ),
    'product_image' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image',
        'config' => array(
            'type' => 'group',
            'internal_type' => 'file',
            'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
            'uploadfolder' => 'uploads/tx_zeitabo',
            'size' => 1,
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
    'product_image_detail' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image_detail',
        'config' => array(
            'type' => 'group',
            'internal_type' => 'file',
            'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
            'uploadfolder' => 'uploads/tx_zeitabo',
            'size' => 1,
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
    'product_image_thankyou' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products.product_image_thankyou',
        'config' => array(
            'type' => 'group',
            'internal_type' => 'file',
            'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
            'uploadfolder' => 'uploads/tx_zeitabo',
            'size' => 1,
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
        "mag_txt" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_txt",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
            )
        ),
        "mag_price" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_price",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "eval" => "trim",
                "checkbox" => "0",
                "default" => ""
            )
        ),
        "mag_shipping" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks.mag_shipping",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "eval" => "trim",
                "checkbox" => "0",
                "default" => ""
            )
        ),
        "abo_txt_order" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_order",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
          "abo_txt_delivery" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_txt_delivery",
            "config" => Array(
                "type" => "text",
                "cols" => "30",
                "rows" => "5",
                "wizards" => Array(
                    "_PADDING" => 2,
                    "RTE" => Array(
                        "notNewRecords" => 1,
                        "RTEonly" => 1,
                        "type" => "script",
                        "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
                        "icon" => "wizard_rte2.gif",
                        "script" => "wizard_rte.php",
                    ),
                ),
            )
        ),
   ),
    "types" => Array(
        "0" => Array("showitem" => "sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, mag_no, mag_name, mag_release, product_image, product_image_detail, product_image_thankyou, mag_txt, mag_price, mag_shipping, abo_txt_order;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css], abo_txt_delivery;;;richtext[cut|copy|paste|bold|italic]:rte_transform[mode=ts_css]")
    ),
    "palettes" => Array(
        "1" => Array("showitem" => "starttime, endtime, fe_group")
    )
);
$TCA['tx_zeitabo_product_groups'] = array(
    'ctrl' => $TCA['tx_zeitabo_product_groups']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'hidden,name'
    ),
    'feInterface' => $TCA['tx_zeitabo_product_groups']['feInterface'],
    'columns' => array(
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config' => array(
                'type' => 'check',
                'default' => '0'
            )
        ),
        'name' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_product_groups.name',
            'config' => array(
                'type' => 'input',
                'size' => '30',
            )
        ),
    ),
    'types' => array(
        '0' => array('showitem' => 'hidden;;1;;1-1-1, name')
    ),
    'palettes' => array(
        '1' => array('showitem' => '')
    )
);

$TCA["tx_zeitabo_abonnements_adwords"] = Array(
    "ctrl" => $TCA["tx_zeitabo_abonnements_adwords"]["ctrl"],
    "interface" => Array(
        "showRecordFieldList" => "sys_language_uid,l18n_parent,l18n_diffsource,hidden,starttime,endtime,fe_group,adword, abo_no, abo_no_student"
    ),
    "feInterface" => $TCA["tx_zeitabo_abonnements_adwords"]["feInterface"],
    "columns" => Array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
                )
            )
        ),
        'l18n_parent' => Array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
            'config' => Array(
                'type' => 'select',
                'items' => Array(
                    Array('', 0),
                ),
                'foreign_table' => 'tx_zeitabo_abonnements_adwords',
                'foreign_table_where' => 'AND tx_zeitabo_abonnements_adwords.pid=###CURRENT_PID### AND tx_zeitabo_abonnements_adwords.sys_language_uid IN (-1,0)',
            )
        ),
        'l18n_diffsource' => Array(
            'config' => Array(
                'type' => 'passthrough'
            )
        ),
        "hidden" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
            "config" => Array(
                "type" => "check",
                "default" => "0"
            )
        ),
        "starttime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.starttime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "default" => "0",
                "checkbox" => "0"
            )
        ),
        "endtime" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.endtime",
            "config" => Array(
                "type" => "input",
                "size" => "8",
                "max" => "20",
                "eval" => "date",
                "checkbox" => "0",
                "default" => "0",
                "range" => Array(
                    "upper" => mktime(0, 0, 0, 12, 31, 2020),
                    "lower" => mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"))
                )
            )
        ),
        "fe_group" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:lang/locallang_general.xml:LGL.fe_group",
            "config" => Array(
                "type" => "select",
                "items" => Array(
                    Array("", 0),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login", -1),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.any_login", -2),
                    Array("LLL:EXT:lang/locallang_general.xml:LGL.usergroups", "--div--")
                ),
                "foreign_table" => "fe_groups"
            )
        ),
        "adword" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.adword",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "abo_no" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_no",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "required,trim",
            )
        ),
        "abo_no_student" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_no_student",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),
        /*"abo_no_kombi_digital" => Array(
            "exclude" => 1,
            "label" => "LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements.abo_no_kombi_digital",
            "config" => Array(
                "type" => "input",
                "size" => "30",
                "eval" => "trim",
            )
        ),*/
        'parent_id' => array(
            'config' => array(
                'type' => 'passthrough',
            )
        ),
    ),
    "types" => Array(
        "0" => Array("showitem" => "sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, adword, abo_no, abo_no_student")
    ),
    "palettes" => Array(
        "1" => Array("showitem" => "starttime, endtime, fe_group")
    )
);
?>
