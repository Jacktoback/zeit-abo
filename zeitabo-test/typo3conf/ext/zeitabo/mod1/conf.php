<?php
    // DO NOT REMOVE OR CHANGE THESE 2 LINES:
define('TYPO3_MOD_PATH', '../typo3conf/ext/zeitabo/mod1/');
$BACK_PATH = '../../../../typo3/';
$MCONF['name'] = 'web_txzeitabomoduleM1';
$MCONF['script'] = 'index.php';
    
$MCONF['access'] = 'user,group';

$MLANG['default']['tabs_images']['tab'] = 'moduleicon.gif';
$MLANG['default']['ll_ref'] = 'LLL:EXT:zeitabo/mod1/locallang_mod.xml';
?>