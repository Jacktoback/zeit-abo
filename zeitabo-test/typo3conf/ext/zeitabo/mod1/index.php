<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Jens Büchel <j.buechel@divien.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
ini_set("display_errors",1);
error_reporting(E_ALL);
ini_set("max_execution_time",3600);


unset($MCONF);
require_once('conf.php');

require_once($BACK_PATH . 'init.php');
require_once($BACK_PATH . 'template.php');

$LANG->includeLLFile('EXT:zeitabo/mod1/locallang.xml');

require_once(PATH_t3lib . 'class.t3lib_scbase.php');
$BE_USER->modAccess($MCONF, 1);	// This checks permissions and exits if the users has no permission for entry.

// DEFAULT initialization of a module [END]


/**
 * Module 'Zeitabo User Data Manager' for the 'zeitabo' extension.
 *
 * @author	Jens Büchel <j.buechel@divine.de>
 * @package	TYPO3
 * @subpackage	tx_zeitabo
 */
class tx_zeitabo_module1 extends t3lib_SCbase {
	var $pageinfo;
	var $fields;
	var $fields_aischa;
	var $anrede_schluessel;
	var $optin;
	var $optin_abgelehnt;
	var $db;
    var $dev = false;
    var $_iban_registry = array();
	/**
	 * Initializes the Module
	 * @return	void
	 */
	function init()	{
		global $BE_USER, $LANG, $BACK_PATH, $TCA_DESCR, $TCA, $CLIENT, $TYPO3_CONF_VARS;

		parent::init();

		/*
		 if (t3lib_div::_GP('clear_all_cache'))	{
		 $this->include_once[] = PATH_t3lib.'class.t3lib_tcemain.php';
		 }
		 */
	}

	/**
	 * Adds items to the ->MOD_MENU array. Used for the function menu selector.
	 *
	 * @return	void
	 */
	function menuConfig()	{
		global $LANG;
		$this->MOD_MENU = Array (
						'function' => Array (
							'1' => $LANG->getLL('function1'),
							'2' => $LANG->getLL('function2'),
							'3' => $LANG->getLL('function3'),
		)
		);
		parent::menuConfig();
	}

    function _iban_load_registry() {

        # if the registry is not yet loaded, or has been corrupted, reload
        if(!is_array($this->_iban_registry) || count($this->_iban_registry)<1) {
            $data = file_get_contents(dirname(__FILE__) . '/registry.txt');
            $lines = explode("\n",$data);
            array_shift($lines); # drop leading description line
            # loop through lines
            foreach($lines as $line) {
                if($line!='') {
                    # split to fields
                    $old_display_errors_value = ini_get('display_errors');
                    ini_set('display_errors',false);
                    $old_error_reporting_value = ini_get('error_reporting');
                    ini_set('error_reporting',false);
                    list($country,$country_name,$domestic_example,$bban_example,$bban_format_swift,$bban_format_regex,$bban_length,$iban_example,$iban_format_swift,$iban_format_regex,$iban_length,$bban_bankid_start_offset,$bban_bankid_stop_offset,$bban_branchid_start_offset,$bban_branchid_stop_offset,$registry_edition,$country_sepa) = explode('|',$line);
                    ini_set('display_errors',$old_display_errors_value);
                    ini_set('error_reporting',$old_error_reporting_value);
                    # assign to registry
                    $this->_iban_registry[$country] = array(
                        'country'			=>	$country,
                        'country_name'			=>	$country_name,
                        'country_sepa'			=>	$country_sepa,
                        'domestic_example'		=>	$domestic_example,
                        'bban_example'			=>	$bban_example,
                        'bban_format_swift'		=>	$bban_format_swift,
                        'bban_format_regex'		=>	$bban_format_regex,
                        'bban_length'			=>	$bban_length,
                        'iban_example'			=>	$iban_example,
                        'iban_format_swift'		=>	$iban_format_swift,
                        'iban_format_regex'		=>	$iban_format_regex,
                        'iban_length'			=>	$iban_length,
                        'bban_bankid_start_offset'	=>	$bban_bankid_start_offset,
                        'bban_bankid_stop_offset'	=>	$bban_bankid_stop_offset,
                        'bban_branchid_start_offset'	=>	$bban_branchid_start_offset,
                        'bban_branchid_stop_offset'	=>	$bban_branchid_stop_offset,
                        'registry_edition'		=>	$registry_edition
                    );
                }
            }
        }
    }

	/**
	 * Main function of the module. Write the content to $this->content
	 * If you chose "web" as main module, you will need to consider the $this->id parameter which will contain the uid-number of the page clicked in the page tree
	 *
	 * @return	[type]		...
	 */
	function main()	{
		global $BE_USER, $LANG, $BACK_PATH, $TCA_DESCR, $TCA, $CLIENT, $TYPO3_CONF_VARS;


        $this->_iban_load_registry();


		// Access check!
		// The page will show only if there is a valid page and if this page may be viewed by the user
		$this->pageinfo = t3lib_BEfunc::readPageAccess($this->id, $this->perms_clause);
		$access = is_array($this->pageinfo) ? 1 : 0;

		if (($this->id && $access) || ($BE_USER->user['admin'] && !$this->id))	{
			
			$this->anrede_schluessel = array(
				0 => 'unbekannt',
				1 => 'Herr',
				2 => 'Frau'
			);
			$this->optin = array(
				'' => '',
				'801' => 'ok'
			);
			
			$this->optin_abgelehnt = array(
				'' => '',
				'N' => 'ok'
			);
			
			$this->fields_aischa = array(
				'TEMP-ID' => array( 0 => 'ID', 1 => ''),
				'TEMP-DATE' => array( 0 => 'D', 1 => 'timestamp'),
				#'Allgemeine-Auftragsnummer' => array( 0 => 'L', 1 => ''),
				#'Bestellzeichen' => array( 0 => 'L', 1 => ''),
				'Aktionsnummer' => array( 0 => 'D', 1 => 'Aktionsnummer'),
				'Liefermenge' => array( 0 => 'L', 1 => '1'),
				'Auftragsbeginn' => array( 0 => 'D', 1 => 'Auftragsbeginn'),
				'Zugaben-Artikelnummer' => array( 0 => 'D', 1 => 'Zugaben_Artikelnummer'),
				'Zugaben-Artikelnummer-Z1' => array( 0 => 'L', 1 => ''),
				#'Zugaben-Artikelnummer-Z2' => array( 0 => 'L', 1 => ''),
				#'Zugaben-Artikelnummer-Z3' => array( 0 => 'L', 1 => ''),
				#'Zugaben-Artikelnummer-Z4' => array( 0 => 'L', 1 => ''),
				#'Zugaben-Artikelnummer-Z5' => array( 0 => 'L', 1 => ''),
				'Praemien-Artikelnummer' => array( 0 => 'D', 1 => 'Praemien_Artikelnummer'),
				'Zahler-Zahlungsweg' => array( 0 => 'D', 1 => 'Zahler_Zahlungsweg'),
				'Zahler-Zahlungsryhthmus' => array( 0 => 'D', 1 => 'Zahler_Zahlungsryhthmus'),
				'Zahler-Bank-Land' => array( 0 => 'Z', 1 => 'Zahler_Land', 2 => 'Zahler_Zahlungsweg', 3 => 2),
				'Zahler-Konto' => array( 0 => 'D', 1 => 'Zahler_Konto'),
				'Zahler-Konto-Inhaber' => array( 0 => 'D', 1 => 'Zahler_Konto_Inhaber'),
				'Zahler-BLZ' => array( 0 => 'D', 1 => 'Zahler_BLZ'),
                'Zahler-BIC' => array( 0 => 'D', 1 => 'Zahler_BIC'),
                'Zahler-IBAN' => array( 0 => 'D', 1 => 'Zahler_IBAN'),
				'Zahler-Kreditkartennummer' => array( 0 => 'D', 1 => 'Zahler_Kreditkartennummer'),
				'Zahler-Kreditkarten-Inhaber' => array( 0 => 'D', 1 => 'Zahler_Kreditkarten_Inhaber'),
				'Zahler-Kreditkarten-Institut' => array( 0 => 'D', 1 => 'Zahler_Kreditkarten_Institut'),
				'Zahler-Kreditkarte-gueltig' => array( 0 => 'D', 1 => 'Zahler_Kreditkarte_gueltig'),
				#'Zahler-Kreditkarten-Referenz-ID' => array( 0 => 'L', 1 => ''),
				#'Zahler-Firma' => array( 0 => 'L', 1 => ''),
				#'Zahler-Abteilung' => array( 0 => 'L', 1 => ''),
				'Zahler-Anrede-Schluessel' => array( 0 => 'S', 1 => 'Zahler_Anrede_Bezeichnung', 2 => $this->anrede_schluessel, 3 => ''),
				'Zahler-Anrede-Bezeichnung' => array( 0 => 'D', 1 => 'Zahler_Titel'),
				'Zahler-Vorname' => array( 0 => 'D', 1 => 'Zahler_Vorname'),
				'Zahler-Name' => array( 0 => 'D', 1 => 'Zahler_Name'),
				'Zahler-Name-Zusatz' => array( 0 => 'D', 1 => 'Zahler_Adresszusatz'),
				#'Zahler-Adress-Zusatz-2' => array( 0 => 'L', 1 => ''),
				#'Zahler-Adress-Zusatz-3' => array( 0 => 'L', 1 => ''),
				'Zahler-Strasse' => array( 0 => 'D', 1 => 'Zahler_Strasse'),
				'Zahler-PLZ' => array( 0 => 'D', 1 => 'Zahler_PLZ'),
				'Zahler-Ort' => array( 0 => 'D', 1 => 'Zahler_Ort'),
				'Zahler-Land' => array( 0 => 'D', 1 => 'Zahler_Land'),
				#'Zahler-Geburtsdatum' => array( 0 => 'D', 1 => 'Zahler_Geburtsdatum'),
				'Zahler-Email-Adresse' => array( 0 => 'D', 1 => 'Zahler_Email_Adresse'),
				'Zahler-Telefon-Nr-1' => array( 0 => 'D', 1 => 'Zahler_Telefon_Nr_1'),
				'Zahler-Telefon-Nr-2' => array( 0 => 'D', 1 => 'Kontakt_Telefon'),
				#'Empfaenger-Firma' => array( 0 => 'L', 1 => ''),
				#'Empfaenger-Abteilung' => array( 0 => 'L', 1 => ''),
				'Empfaenger-Anrede-Schluessel' => array( 0 => 'S', 1 => 'Empfaenger_Anrede_Bezeichnung', 2 => $this->anrede_schluessel, 3 => ''),
				'Empfaenger-Anrede-Bezeichnung' => array( 0 => 'D', 1 => 'Empfaenger_Titel'),
				'Empfaenger-Vorname' => array( 0 => 'D', 1 => 'Empfaenger_Vorname'),
				'Empfaenger-Name' => array( 0 => 'D', 1 => 'Empfaenger_Name'),
				'Empfaenger-Name-Zusatz' => array( 0 => 'D', 1 => 'Empfaenger_Adresszusatz'),
				'Empfaenger-Strasse' => array( 0 => 'D', 1 => 'Empfaenger_Strasse'),
				'Empfaenger-PLZ' => array( 0 => 'D', 1 => 'Empfaenger_PLZ'),
				'Empfaenger-Ort' => array( 0 => 'D', 1 => 'Empfaenger_Ort'),
				'Empfaenger-Land' => array( 0 => 'D', 1 => 'Empfaenger_Land'),
				#'Empfaenger-Geburtsdatum' => array( 0 => 'D', 1 => 'Empfaenger_Geburtsdatum'),
				'Empfaenger-Email-Adresse' => array( 0 => 'D', 1 => 'Empfaenger_Email_Adresse'),
				'Empfaenger-Telefon-Nr-1' => array( 0 => 'D', 1 => 'Empfaenger_Telefon_Nr_1'),
				#'Werber-Firma' => array( 0 => 'L', 1 => ''),
				#'Werber-Abteilung' => array( 0 => 'L', 1 => ''),
				'Werber-Anrede-Schluessel' => array( 0 => 'S', 1 => 'Werber_Anrede_Bezeichnung', 2 => $this->anrede_schluessel, 3 => ''),
				'Werber-Anrede-Bezeichnung' => array( 0 => 'L', 1 => ''),
				'Werber-Vorname' => array( 0 => 'D', 1 => 'Werber_Vorname'),
				'Werber-Name' => array( 0 => 'D', 1 => 'Werber_Name'),
				'Werber-Name-Zusatz' => array( 0 => 'D', 1 => 'Werber_Adresszusatz'),
				'Werber-Strasse' => array( 0 => 'D', 1 => 'Werber_Strasse'),
				'Werber-PLZ' => array( 0 => 'D', 1 => 'Werber_PLZ'),
				'Werber-Ort' => array( 0 => 'D', 1 => 'Werber_Ort'),
				'Werber-Land' => array( 0 => 'D', 1 => 'Werber_Land'),
				#'Werber-Geburtsdatum' => array( 0 => 'D', 1 => 'Werber_Geburtsdatum'),
				'Werber-Email-Adresse' => array( 0 => 'L', 1 => ''),
				'Werber-Telefon-Nr-1' => array( 0 => 'L', 1 => ''),
				#'Kontaktergebnis' => array( 0 => 'L', 1 => ''),
				#'Kontaktnummer' => array( 0 => 'L', 1 => ''),
				#'Befristung' => array( 0 => 'L', 1 => ''),
				#'Befristung-Ausgaben' => array( 0 => 'L', 1 => ''),
				#'Bescheinigung' => array( 0 => 'L', 1 => ''),
				'Zahler-Adress-Art' => array( 0 => 'L', 1 => '5'),
				'Empfaenger-Adress-Art' => array( 0 => 'C', 1 => '5', 2 => 'Empfaenger'),
				'Werber-Adress-Art' => array( 0 => 'C', 1 => '5', 2 => 'Werber'),
				'OptIn-Abgelehnt' => array( 0 => 'S', 1 => 'callback', 2 => $this->optin_abgelehnt,3 => ''),
				#'OptIn-Agentur-ID' => array( 0 => 'L', 1 => ''),
				#'OptIn-Erfassungszeit' => array( 0 => 'L', 1 => ''),
				'OptIn-ID' => array( 0 => 'S', 1 => 'callback', 2 => $this->optin,3 => ''),
				#'OptIn-Rolle' => array( 0 => 'L', 1 => ''),
				#'OptIn-Voicefile-Datum' => array( 0 => 'L', 1 => ''),
				#'OptIn-Voicefile-ID' => array( 0 => 'L', 1 => ''),
				#'Agenten-Nr' => array( 0 => 'L', 1 => ''),
				#'Geburtsdatum3(TTMMJJJJ) ' => array( 0 => 'L', 1 => ''),
				#'Hostessennummer' => array( 0 => 'L', 1 => ''),
				#'ICODE' => array( 0 => 'L', 1 => ''),
				
				'Bestellweise'=> array( 0 => 'L', 1 => 'Internet')
			);
			
			 $this->fields = array(
				'Zahler-Anrede-Bezeichnung' => 'Zahler_Anrede_Bezeichnung',
                'Zahler-Titel' => 'Zahler_Titel',
				'Zahler-Vorname' => 'Zahler_Vorname',
				'Zahler-Name' => 'Zahler_Name',
				'Zahler-Strasse' => 'Zahler_Strasse',
				'Zahler-Firma' => 'Zahler_Adresszusatz',
				'Zahler-PLZ' => 'Zahler_PLZ',
				'Zahler-Ort' => 'Zahler_Ort',
				'Zahler-Land' => 'Zahler_Land',
				'Zahler-Geburtsdatum' => 'Zahler_Geburtsdatum',
				'Zahler-Telefon-Nr-1' => 'Zahler_Telefon_Nr_1',
				'Zahler-Email-Adresse' => 'Zahler_Email_Adresse',
				'Zahler-Zahlungsryhthmus' => 'Zahler_Zahlungsryhthmus',
				'Zahler-Zahlungsweg' => 'Zahler_Zahlungsweg',
				'Zahler-Konto-Inhaber' => 'Zahler_Konto_Inhaber',
				'Zahler-Konto' => 'Zahler_Konto',
				'Zahler-BLZ' => 'Zahler_BLZ',
				'Zahler-Kreditkartennummer' => 'Zahler_Kreditkartennummer',
				'Zahler-Kreditkarten-Institut' => 'Zahler_Kreditkarten_Institut',
				'Zahler-Kreditkarten-Inhaber' => 'Zahler_Kreditkarten_Inhaber',
				'Zahler-Kreditkarte-gueltig' => 'Zahler_Kreditkarte_gueltig',
				'Zugaben-Artikelnummer' => 'Zugaben_Artikelnummer',
				'Praemien-Artikelnummer' => 'Praemien_Artikelnummer',
				'Aktionsnummer' => 'Aktionsnummer',
				'Empfaenger-Anrede-Bezeichnung' => 'Empfaenger_Anrede_Bezeichnung',
				'Empfaenger-Vorname' => 'Empfaenger_Vorname',
				'Empfaenger-Name' => 'Empfaenger_Name',
				'Empfaenger-Strasse' => 'Empfaenger_Strasse',
				'Empfaenger-Firma' => 'Empfaenger_Adresszusatz',
				'Empfaenger-PLZ' => 'Empfaenger_PLZ',
				'Empfaenger-Ort' => 'Empfaenger_Ort',
				'Empfaenger-Land' => 'Empfaenger_Land',
				'Empfaenger-Geburtsdatum' => 'Empfaenger_Geburtsdatum',
				'Empfaenger-Telefon-Nr-1' => 'Empfaenger_Telefon_Nr_1',
				'Empfaenger-Email-Adresse' => 'Empfaenger_Email_Adresse',
				'Werber-Anrede-Bezeichnung' => 'Werber_Anrede_Bezeichnung',
				'Werber-Vorname' => 'Werber_Vorname',
				'Werber-Name' => 'Werber_Name',
				'Werber-Strasse' => 'Werber_Strasse',
				'Werber-Firma' => 'Werber_Adresszusatz',
				'Werber-PLZ' => 'Werber_PLZ',
				'Werber-Ort' => 'Werber_Ort',
				'Werber-Land' => 'Werber_Land',
				'Werber-Geburtsdatum' => 'Werber_Geburtsdatum',
				'Auftragsbeginn' => 'Auftragsbeginn',
				'Werbecode' => 'Werbecode',
				'Zahler-Kreditkarten-Referenz-ID' => 'Zahler_Kreditkarten_Referenz_ID',
				'anzahl' => 'anzahl',
				'Auftragsbeginn' => 'Auftragsbeginn',
				'Upselling_product' => 'Upselling_product',
				'uid' => 'uid',
				'callback' => 'callback',
				'Telefon-Permission' => 'Kontakt_Telefon',
				'Absendedatum' => 'timestamp',
				'IP' => 'ip',
				'Device' => 'Device',
                'Refferer' => 'Refferer',
			);
			 $this->db = $GLOBALS[ 'TYPO3_DB' ];
			 
			// Draw the header.
			$this->doc = t3lib_div::makeInstance('mediumDoc');
			$this->doc->backPath = $BACK_PATH;
			#$this->doc->form='<form action="" method="POST">';

			// JavaScript
			$this->doc->JScode = '
							<script language="javascript" type="text/javascript">
								script_ended = 0;
								function jumpToUrl(URL)	{
									document.location = URL;
								}
							</script>
						';
			$this->doc->postCode='
							<script language="javascript" type="text/javascript">
								script_ended = 1;
								if (top.fsMod) top.fsMod.recentIds["web"] = 0;
							</script>
						';

			//$headerSection = $this->doc->getHeader('pages',$this->pageinfo,$this->pageinfo['_thePath']).'<br />'.$LANG->sL('LLL:EXT:lang/locallang_core.xml:labels.path').': '.t3lib_div::fixed_lgd_pre($this->pageinfo['_thePath'],50);

			$this->content.=$this->doc->startPage($LANG->getLL('title'));
			$this->content.=$this->doc->header($LANG->getLL('title'));
			$this->content.=$this->doc->spacer(5);
			$this->content.=$this->doc->section('', $this->doc->funcMenu('', t3lib_BEfunc::getFuncMenu($this->id, 'SET[function]', $this->MOD_SETTINGS['function'], $this->MOD_MENU['function'])));
			$this->content.=$this->doc->divider(5);


			$this->content.=$this->moduleContent();


			// ShortCut
			if ($BE_USER->mayMakeShortcut())	{
				$this->content.=$this->doc->spacer(20).$this->doc->section('', $this->doc->makeShortcutIcon('id', implode(',', array_keys($this->MOD_MENU)), $this->MCONF['name']));
			}

			$this->content.=$this->doc->spacer(10);
		} else {
			// If no access or if ID == zero

			$this->doc = t3lib_div::makeInstance('mediumDoc');
			$this->doc->backPath = $BACK_PATH;

			$this->content.=$this->doc->startPage($LANG->getLL('title'));
			$this->content.=$this->doc->header($LANG->getLL('title'));
			$this->content.=$this->doc->spacer(5);
			$this->content.=$LANG->getLL('no_id');
			$this->content.=$this->doc->spacer(10);
		}
	}

	/**
	 * Prints out the module HTML
	 *
	 * @return	void
	 */
	function printContent()	{

		$this->content.=$this->doc->endPage();
		echo $this->content;
	}

	/**
	 * Generates the module content
	 *
	 * @return	void
	 */
	function moduleContent()	{
		
		switch ((string)$this->MOD_SETTINGS['function'])	{
			case 1:
				$content = $this->getExport();
				$this->content .= $this->doc->section('', $content, 0, 1);
				break;
			case 2:
				$content = $this->getDelete();
				$this->content .= $this->doc->section('', $content, 0, 1);
				break;
			case 3:
				$content = $this->getOverview();
				$this->content .= $this->doc->section('', $content, 0, 1);
				break;
		}
	}
	
	/**
	 * Gets the buttons that shall be rendered in the docHeader.
	 *
	 * @return	array		Available buttons for the docHeader
	 */
	protected function getDocHeaderButtons() {
		$buttons = array(
			'csh'      => t3lib_BEfunc::cshItem('_MOD_web_txzeitmoduleM1', '', $this->backPath),
			'shortcut' => $this->getShortcutButton(),
		);
		return $buttons;
	}

	/**
	 * Gets the button to set a new shortcut in the backend (if current user is allowed to).
	 *
	 * @return	string		HTML representiation of the shortcut button
	 */
	protected function getShortcutButton() {
		$result = '';
		if ($GLOBALS['BE_USER']->mayMakeShortcut()) {
			$result = $this->doc->makeShortcutIcon('id', implode(',', array_keys($this->MOD_MENU)), $this->MCONF['name']);
		}

		return $result;
	}
	protected function getDelete(){
		$content = '<div class="wrapper" style="padding: 40px;">';
		$deleteDatas = $this->collectExportToDeleteDatas();
		
		if(t3lib_div::_GP("selection") == 'delete'){
			$tstamp = time();
			$tstamp -= (60*60*24*7);
			$today = date('Y-m-d H:i:s',$tstamp);
			$delete_sql = '';
			if(is_array($deleteDatas) && count($deleteDatas) > 0){
				if($deleteDatas[0]['export_datas'] != ''){
					$delUserDatas = unserialize($deleteDatas[0]['export_datas']);
					$deletedUser = array();
					foreach($delUserDatas as $UserID => $delUserTs){
						$deletedUser[$UserID] = $delUserTs;
						$delData = $GLOBALS['TYPO3_DB']->exec_DELETEquery('zeit_user','uid='.intval($UserID));
						$content .= '<p>Datensatz '.$UserID.' wurde gelöscht</p>';
					}
					$resetExportData = array();
					$resetExportData['timestamp'] = $today;
					$resetExportData['export_datas'] = '';
					$resetExports = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('zeit_exports','DATE(timestamp) = CURDATE()',$resetExportData);
					
					$delData = serialize($deletedUser);
					$updateExportData = array();
					$updateExportData['timestamp'] = $today;
					$updateExportData['export_datas'] = '';
					$updateExportData['deleted_datas'] = $delData;
					$delData = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('zeit_exports','uid='.intval(t3lib_div::_GP("exportid")),$updateExportData);
				}
			}
		} else {
			if(is_array($deleteDatas) && count($deleteDatas) > 0){

				if($deleteDatas[0]['export_datas'] != ''){

					$delUserDatas = unserialize($deleteDatas[0]['export_datas']);

					if(is_array($delUserDatas) && count($delUserDatas) > 0){
						$nodeletes = 1;
						foreach($delUserDatas as $UserID => $delUserTs){
							$userData = $this->getUserData($UserID);
							if(is_array($userData) && count($userData) > 0){
								$nodeletes = 0;
							}
						}
						if($nodeletes == 0){
							$content .= '<script type="text/javascript">
											function confirmation( )
											{
													var answer = confirm(\'Sind Sie sicher alle unten aufgeführen Datensätze unwiederruflich zu löschen?\');
													if (answer){
														return true;
													}
													else{
														return false;
													}
												
											}
											</script>';
							
							$content .= '<form action="'.$_SERVER['PHP_SELF'].'?M=web_txzeitabomduleM1" method="POST" name="zeitaboexport" onsubmit="return confirmation( );">';
							$content .= '<input type="hidden" name="selection" value="delete">';
							$content .= '<input type="hidden" name="exportid" value="'.$deleteDatas[0]['uid'].'">';
							$content .= '<br /><br /><input type="submit" value="Daten unwiederruflich löschen" />';
							$content .= '</form>';
						
							$content .= '<h2>Folgende Daten können gelöscht werden</h2>';
							
							$content .= '<table border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border: 1px solid #000000;">';
							$content .= '<thead>';
							foreach($this->fields as $fkey=> $fhead){
								$content .= '<th>'.$fkey.'</th>';
							}
							$content .= '</thead>';
							$z = 1;
                            $even = true;
							foreach($delUserDatas as $UserID => $delUserTs){
								if($z < 100){
									$userData = $this->getUserData($UserID);
									if(is_array($userData) && count($userData) > 0){
										$userData = $this->get_special_columns( $userData );
										$is_abo = true;
										if( $userData[ 'Aktionsnummer' ] == 'mag' || preg_match( '/^tx_zeitabo_abonnements_/', $userData[ 'Aktionsnummer' ] ) ) {
											 $userData[ 'Aktionsnummer' ] = $this->get_mag_no_for_id( $userData[ 'Zugaben_Artikelnummer' ] );
											 $userData[ 'Zugaben_Artikelnummer' ] = '';
										  $is_abo = false;
										}
                                        if(!empty($userData[ 'Aktionsnummer_Adword'])){
                                            $is_student = 0;
                                            if($userData['student' ] == 'ok') $is_student = 1;
                                            $newAktionsnummer = $this->get_abo_no_for_adword( $userData[ 'Aktionsnummer_Adword'], $is_student, $userData['Praemien_Artikelnummer'], $userData['Praemien_Artikelnummer_orig']);
                                            if(!empty($newAktionsnummer) && $newAktionsnummer != 0 && $newAktionsnummer != ''){
                                                $userData[ 'Aktionsnummer' ] = $newAktionsnummer;
                                            }
                                        }
										if(!$even) {
											$class="odd";
											$even = !$even;
										} else {
											$class="even";
											$even = !$even;
										}
										$content .= '<tr class="'.$class.'">';	
										foreach($this->fields as $fkey=> $fhead){
											$content .= '<td>'.$userData[$fhead].'</td>';
										}
										$content .= '</tr>';	
									}
								}
								$z++;
							}
							$content .= '</table>';
						} else {
							$content = '<h2>Es liegen für den heutigen Tag keine Daten zum Löschen vor.</h2>';
						}
					} else {
						$content = '<h2>Es liegen für den heutigen Tag keine Daten zum Löschen vor.</h2>';
					}
				} else {
					$content = '<h2>Es liegen für den heutigen Tag keine Daten zum Löschen vor.</h2>';
				}
			} else {
				$content = '<h2>Es liegen für den heutigen Tag noch keine Daten zum Löschen vor. Bitte zuerst Daten exportieren.</h2>';
			}
		}
		
		$content .= '</div>';
		
		return $content;
	}
	
	protected function getExport(){
		$content = '<div class="wrapper" style="padding: 40px;">';

		$tstamp = time();
		$tstamp -= (60*60*24*7);
		$today = date('Y-m-d H:i:s',$tstamp);

		$content .= $today;
		
		if(t3lib_div::_GP("selection") == 'export'){
			$content .= '<p>Export wurde gestartet<br></p>';
			
			$root = $_SERVER['DOCUMENT_ROOT'];
			$exportIds = array();
			$exportfiles = array();
			$export_sqlfile = '';
			
			if(t3lib_div::_GP("csv") == '1'){
				$f_name_abo = $root.'/fileadmin/export/export.abo.csv';
				$f_name_einzelheft = $root.'/fileadmin/export/export.einzelheft.csv';
				$f_name_callback = $root.'/fileadmin/export/export.callback.csv';
				
				$f_name_abo_gpg = $root.'/fileadmin/export/export.abo.csv.gpg';
				$f_name_einzelheft_gpg = $root.'/fileadmin/export/export.einzelheft.csv.gpg';
				$f_name_callback_gpg = $root.'/fileadmin/export/export.callback.csv.gpg';
				
				$exportfiles[] = $f_name_abo;
				$exportfiles[] = $f_name_einzelheft;
				$exportfiles[] = $f_name_callback;
				$exportfiles[] = $f_name_abo_gpg;
				$exportfiles[] = $f_name_einzelheft_gpg;
				$exportfiles[] = $f_name_callback_gpg;

				#$f_name_abo = '/var/www/zeitabo-test/fileadmin/export/export.abo.csv';
				#$f_name_einzelheft = '/var/www/zeitabo-test/fileadmin/export/export.einzelheft.csv';
				#$f_name_callback = '/var/www/zeitabo-test/fileadmin/export/export.callback.csv';
				
				
				$this->file_abo = fopen( $f_name_abo, 'w' );
				$this->file_einzelheft = fopen( $f_name_einzelheft, 'w' );
				$this->file_callback = fopen( $f_name_callback, 'w' );
			
				$this->export_fields = array_values( $this->fields );
				$select = implode( ',', array_map( array( $this, 'quote_fields' ), $this->export_fields ) );
                $select .= ',student,Aktionsnummer_Adword';
                $sql = "
				  SELECT 
					$select
				  FROM
					zeit_user
				  ORDER BY
					timestamp DESC
				";
				$this->write_header();
			
				$res = $this->db->sql_query( $sql );
				
				while( $row = $this->db->sql_fetch_assoc( $res ) ) {
                    /*
                    $zKonto = false;
                    $zBLZ = false;
                    if(!empty($row['Zahler_Konto'])){
                        $zKonto = $this->test_iban($row['Zahler_Konto']);
                    }
                    if(!empty($row['Zahler_BLZ'])) {
                        $zBLZ = $this->test_bic($row['Zahler_BLZ']);
                    }
                    if($zKonto && $zBLZ){
                        $row['Zahler_IBAN'] = $row['Zahler_Konto'];
                        unset($row['Zahler_Konto']);
                        $row['Zahler_BIC'] = $row['Zahler_BLZ'];
                        unset($row['Zahler_BLZ']);
                    }
                    */
				  $this->write_row( $row );
				  if($row['timestamp'] < $today){
					  $exportIds[$row['uid']] = $row['timestamp'];
				  }
				}
				fclose( $this->file_abo );
				fclose( $this->file_einzelheft );
				fclose( $this->file_callback );
				
				$content .= '<p>CSV-Export wurde erfolgreich durchgeführt<br><br></p>';
			} else {
				$this->export_fields = array_values( $this->fields );
				$select = implode( ',', array_map( array( $this, 'quote_fields' ), $this->export_fields ) );
				$sql = "
				  SELECT 
					$select
				  FROM
					zeit_user
				  ORDER BY
					timestamp DESC
				";
				
				$res = $this->db->sql_query( $sql );
				
				while( $row = $this->db->sql_fetch_assoc( $res ) ) {
					if($row['timestamp'] < $today){

					  $exportIds[$row['uid']] = $row['timestamp'];
					}
				}
			}
			
			$exportDatas = serialize($exportIds);
			$sql = " INSERT INTO zeit_exports (`export_datas`) VALUES ('".$exportDatas."');";
			$res = $this->db->sql_query( $sql );
			
			if(t3lib_div::_GP("aischa") == '1'){
				$f_name_aischa = $root.'/fileadmin/export/export.aischa.csv';
				#$f_name_aischa = $root.'/typo3conf/ext/zeitabo/mod1/export.aischa.csv';
				$exportfiles[] =$f_name_aischa;
				
				$this->file_aischa = fopen( $f_name_aischa, 'w' );
				
				$this->export_fields_aischa = array_values( $this->fields_aischa );
				
				#$select = implode( ',', array_map( array( $this, 'quote_fields' ), $this->export_fields_aischa ) );
				$sql = "
				  SELECT 
					*
				  FROM
					zeit_user
				  WHERE 
				    (Error_digital = '' AND Abo_Nummer = '') OR (Error_digital LIKE '%<error%' AND Abo_Nummer = '') OR (Abo_Nummer_Kombi != '' AND Ist_Abonnent = 0 AND Error_digital LIKE '%<order id%') OR (Ist_Upselling != 0) OR (Upselling_product != '')
				  ORDER BY
					timestamp DESC
				";
				
				$this->write_header_aischa();
				
				$res = $this->db->sql_query( $sql );
				
				while( $row = $this->db->sql_fetch_assoc( $res ) ) {
                    $zKonto = false;
                    $zBLZ = false;
                    if(!empty($row['Zahler_Konto'])){
                        $zKonto = $this->test_iban($row['Zahler_Konto']);
                    }
                    if(!empty($row['Zahler_BLZ'])) {
                        $zBLZ = $this->test_bic($row['Zahler_BLZ']);
                    }
                    if($zKonto && $zBLZ){
                        $row['Zahler_IBAN'] = $row['Zahler_Konto'];
                        unset($row['Zahler_Konto']);
                        $row['Zahler_BIC'] = $row['Zahler_BLZ'];
                        unset($row['Zahler_BLZ']);
                    }


				  $this->write_row_aischa( $row, 1 );
				}
				
				fclose( $this->file_aischa );
				
			}
			
				
			if(t3lib_div::_GP("mysql") == '1'){
				if($this->dev == true){
                    /****DEV ***/
                    $dbUser = 'zeitabo3';
                    $dbPass = 'UsichVurdEv7';
                    $dbName = 'zeitabo3_dev';
                    $tableName = 'zeit_user';
                    /****DEV ***/
                } else {
                    $dbUser = 'zeitabo3';
                    $dbPass = 'UsichVurdEv7';
                    $dbName = 'zeitabo3';
                    $tableName = 'zeit_user';
                }


				$destination = $root.'/fileadmin/export/';
				system("mysqldump --user=".$dbUser." --password=".$dbPass." --opt -B ".$dbName." --tables ".$tableName ." > ".$destination.date('Ymd_His')."_".$tableName ."_bak.sql");
				$content .= '<p>MySQL-Export wurde erfolgreich durchgeführt<br><br></p>';
				$exportfiles[] = $destination.date('Ymd_His')."_".$tableName ."_bak.sql";
				$export_sqlfile = $destination.date('Ymd_His')."_".$tableName ."_bak.sql";
			}
			if(count($exportfiles) > 0){
				$destination = $root.'/fileadmin/export/';
				$filenameZip = 'Export_'.date('Ymd_His').".zip";
				$filenamePath = $destination.$filenameZip;
				
				$zip = new ZipArchive();
				
				if ($zip->open($filenamePath,ZIPARCHIVE::CREATE)===TRUE) {
					foreach($exportfiles as $exportfile){
						$dirparts = explode('/',$exportfile);
						$filename = $dirparts[count($dirparts)-1];
						if(is_file($exportfile)){
							if($zip->addFile($exportfile,$filename)) {
								$content .= '<p>'.$filename.' wurde zum Zip-Archiv hinzugefügt<br></p>';
							}
						}
					}
					if($zip->close()) {
						if($export_sqlfile != ''){
							unlink($export_sqlfile);
						}
						$content .= '<p>Export_Daten stehen zum Download zur Verfügung:<br><br><a href="/fileadmin/export/'.$filenameZip.'" target="_blank" style="font-weight: bold;">Zip-Archiv downloaden</a><br></p>';	
						$content .= '<p><br><br><a href="'.$_SERVER['PHP_SELF'].'?M=web_txzeitabomduleM1&delete_zipfile='.$filenameZip.'" target="content" style="font-weight: bold;">Zip-Archiv löschen</a><br></p>';		
					} else {
						$content .= 'The zip archive contains '.$zip->numFiles.' files with a status of '.$zip->status;
					}
				}
			}
			
		} else {
			if(t3lib_div::_GP("delete_zipfile") != ''){
				$root = $_SERVER['DOCUMENT_ROOT'];
				$delete_zipfile = $root.'/fileadmin/export/'.t3lib_div::_GP("delete_zipfile");
				unlink($delete_zipfile);
				$content .= '<p><br>Zip-Archiv '.t3lib_div::_GP("delete_zipfile").' wurde gelöscht<br></p>';		
			}
			$content .= '<form action="'.$_SERVER['PHP_SELF'].'?M=web_txzeitabomduleM1" method="POST" name="zeitaboexport">';
			$content .= '<input type="hidden" name="selection" value="export">';
			$content .= '<br /><input type="checkbox" name="csv" value="1" checked="checked">&nbsp;<label>Exportiere CSV-Dateien</label>';
			$content .= '<br /><br /><input type="checkbox" name="aischa" value="1" checked="checked">&nbsp;<label>Exportiere Aischa-Dateien</label>';
			$content .= '<br /><br /><input type="checkbox" name="mysql" value="1" checked="checked">&nbsp;<label>Exportiere MySQL-Dump</label>';
			
			$content .= '<br /><br /><input type="submit" value="Starte Export" />';
			$content .= '</form>';
			$content .= '</div>';
		}
		return $content;
	}
	protected function getOverview() {
		$content = '<table border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border: 1px solid #000;">';
		$content .= '<thead>';
		foreach($this->fields as $fkey=> $fhead){
			$content .= '<th>'.$fkey.'</th>';
		}
		$content .= '</thead>';
		$userDatas = $this->collectUserDatas();
		
		
		 
		$even = false;
		foreach($userDatas as $userData){
			$userData = $this->get_special_columns( $userData );
			$is_abo = true;
			if( $userData[ 'Aktionsnummer' ] == 'mag' || preg_match( '/^tx_zeitabo_abonnements_/', $userData[ 'Aktionsnummer' ] ) ) {
				 $userData[ 'Aktionsnummer' ] = $this->get_mag_no_for_id( $userData[ 'Zugaben_Artikelnummer' ] );
			     $userData[ 'Zugaben_Artikelnummer' ] = '';
			  $is_abo = false;
			}
            if(!empty($userData[ 'Aktionsnummer_Adword'])){
                $is_student = 0;
                if($userData['student' ] == 'ok') $is_student = 1;
                $newAktionsnummer = $this->get_abo_no_for_adword( $userData[ 'Aktionsnummer_Adword'], $is_student, $userData['Praemien_Artikelnummer'], $userData['Praemien_Artikelnummer_orig']);
                if(!empty($newAktionsnummer) && $newAktionsnummer != 0 && $newAktionsnummer != ''){
                    $userData[ 'Aktionsnummer' ] = $newAktionsnummer;
                }
            }
			if(!$even) {
				$class="odd";
				$even = !$even;
			} else {
				$class="even";
				$even = !$even;
			}
			$content .= '<tr class="'.$class.'">';	
			foreach($this->fields as $fkey=> $fhead){
				$content .= '<td>'.$userData[$fhead].'</td>';
			}
			$content .= '</tr>';	
		}
		
		$content .= '</table>';
		
		return $content;
		
	}
	
	protected function getUserData($uid = 0) {
		
		if($uid != 0){
			$userData = array();
			$selectConf = array();
			$selectConf['selectFields'] = '*';
			$selectConf['fromTable'] = 'zeit_user';
			$selectConf['where'] .= ' uid='.$uid.' '; 
			$selectConf['groupBy'] = '';
			$selectConf['orderBy'] = 'timestamp';
			$selectConf['limit'] = '1';
			
			$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
			
			while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
				$userData = $row;
			}
			
			$GLOBALS['TYPO3_DB']->sql_free_result($res);
			
			return $userData;
		}
	}


	protected function collectExportToDeleteDatas() {
		
		
		$exportDatas = array();
		$selectConf = array();
		$selectConf['selectFields'] = '*';
		$selectConf['fromTable'] = 'zeit_exports';
		$selectConf['where'] .= ' export_datas != \'\' AND DATE(timestamp) = CURDATE()'; 
		$selectConf['groupBy'] = '';
		$selectConf['orderBy'] = 'timestamp DESC';
		$selectConf['limit'] = '';
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
		
		while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
			$exportDatas[] = $row;
		}
		
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
		
		return $exportDatas;
	}

    # Get information from the IBAN registry by country / code combination
    function _iban_country_get_info($country,$code) {
        $country = strtoupper($country);
        $code = strtolower($code);
        if(array_key_exists($country,$this->_iban_registry)) {
            if(array_key_exists($code,$this->_iban_registry[$country])) {
                return $this->_iban_registry[$country][$code];
            }
        }
        return false;
    }

    # Get the IBAN length for an IBAN country
    function iban_country_get_iban_length($iban_country) {
        return $this->_iban_country_get_info($iban_country,'iban_length');
    }

    function test_iban( $iban ) {
        $iban = strtoupper(str_replace( ' ', '', $iban ));
        $country = substr($iban,0,2);

        if(strlen($iban)!=$this->iban_country_get_iban_length($country)) { return false; }

        $iban1 = substr( $iban,4 )
            . strval( ord( $iban{0} )-55 )
            . strval( ord( $iban{1} )-55 )
            . substr( $iban, 2, 2 );

        for( $i = 0; $i < strlen($iban1); $i++) {
            if(ord( $iban1{$i} )>64 && ord( $iban1{$i} )<91) {
                $iban1 = substr($iban1,0,$i) . strval( ord( $iban1{$i} )-55 ) . substr($iban1,$i+1);
            }
        }
        $rest=0;
        for ( $pos=0; $pos<strlen($iban1); $pos+=7 ) {
            $part = strval($rest) . substr($iban1,$pos,7);
            $rest = intval($part) % 97;
        }
        $pz = sprintf("%02d", 98-$rest);

        if ( substr($iban,2,2)=='00')
            return substr_replace( $iban, $pz, 2, 2 );
        else
            return ($rest==1) ? true : false;
    }

    function test_bic($bic)
    {
        $bic = strtoupper(str_replace( ' ', '', $bic));
        if(!preg_match("/^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/", $bic))
        {

            return false;

        } else {

            return true;

        }

    }

	protected function collectUserDatas() {
		
		
		$userDatas = array();
		$selectConf = array();
		$selectConf['selectFields'] = '*';
		$selectConf['fromTable'] = 'zeit_user';
		$selectConf['where'] .= ' 1=1 '; 
		$selectConf['groupBy'] = '';
		$selectConf['orderBy'] = 'timestamp';
		$selectConf['limit'] = '100';
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
		
		while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
            $zKonto = false;
            $zBLZ = false;
            if(!empty($row['Zahler_Konto'])){
                $zKonto = $this->test_iban($row['Zahler_Konto']);
            }
            if(!empty($row['Zahler_BLZ'])) {
                $zBLZ = $this->test_bic($row['Zahler_BLZ']);
            }
            if($zKonto && $zBLZ){
                $row['Zahler_IBAN'] = $row['Zahler_Konto'];
                unset($row['Zahler_Konto']);
                $row['Zahler_BIC'] = $row['Zahler_BLZ'];
                unset($row['Zahler_BLZ']);
            }

			$userDatas[$row['uid']] = $row;
		}

		$GLOBALS['TYPO3_DB']->sql_free_result($res);
		
		return $userDatas;
	}
	
	function quote_fields( $s ) {
    return '`' . $s . '`';
  }
  
  function write_header_aischa() {
    $to_write = array( );
    foreach( $this->fields_aischa as $k => $v ) {
    	$to_write[ ] = $k;
    }
    fwrite( $this->file_aischa, implode( ';', $to_write ) . "\n" );
  }
  
  
  function write_header() {
    $to_write = array( );
    foreach( $this->export_fields as $field ) {
      foreach( $this->fields as $k => $v ) {
        if( $field == $v ) {
          $to_write[ ] = $k;
          break;
        }
      }
    }
    fwrite( $this->file_abo, implode( ';', $to_write ) . "\n" );
    fwrite( $this->file_einzelheft, implode( ';', $to_write ) . "\n" );
    fwrite( $this->file_callback, implode( ';', $to_write ) . "\n" );
  }

  function clean_string( $s ) {
    $s = utf8_decode( $s );
    $s = preg_replace( '/;/', ',', $s );
    $s = preg_replace( '/\n/', ' ', $s );
    $s = preg_replace( '/\r/', ' ', $s );

    return $s;
  }

  function get_abo_no_for_id( $id ) {
    if( $id ) {
      $sql = "SELECT abo_no FROM tx_zeitabo_abonnements WHERE uid=$id";
      $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
      return $row[ 'abo_no' ];
    }
    else {
      return '';
    }
  }

  function get_abo_no_for_row( $orig_row, $id ) {
    $sql = "select student from zeit_user where uid={$orig_row[ 'uid' ]}";
    $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
    if( $row[ 'student' ] ) {
      $field = 'abo_no_student as abo_no';
    }
    else {
      /* this is a bit weird. switching art_no inplace from uid to real no.
        uid is saved in _orig. but we still need to lookup the original if
        conversion hasn't been processed, yet.
       */

      $art_no = $orig_row[ 'Praemien_Artikelnummer_orig' ];
      if( !$art_no )
        $art_no = $orig_row[ 'Praemien_Artikelnummer' ];

      $sql = "select prod_type from tx_zeitabo_products where uid={$art_no}";
      $prod_row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
      if( $prod_row[ 'prod_type' ] == 2 ) {
        $field = 'abo_no_student as abo_no';
      }
      else {
        $field = 'abo_no';
      }
    }

    $sql = "SELECT $field FROM tx_zeitabo_abonnements WHERE uid=$id";
    $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
//    debug( $row );
    return $row[ 'abo_no' ];
  }

  function get_mag_no_for_id( $id ) {
    if( $id ) {
      $sql = "SELECT mag_no FROM tx_zeitabo_singlebooks WHERE uid=$id";
      $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
      //debug( $row );
      //debug( $sql );
      return $row[ 'mag_no' ];
    }
    else {
      return '';
    }
  }

  function get_prod_no_for_id( $id ) {
    if( $id ) {
      $sql = "SELECT prod_no_1 FROM tx_zeitabo_products WHERE uid=$id";
      $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
      return $row[ 'prod_no_1' ];
    }
    else {
      return '';
    }
  }

    function get_abo_no_for_adword( $id, $is_student,$Praemien_Artikelnummer, $Praemien_Artikelnummer_orig) {
        if( $id ) {
            $sql = "SELECT * FROM tx_zeitabo_abonnements_adwords WHERE uid=$id";
            $row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
            if($is_student == 1){
                return $row[ 'abo_no_student' ];
            } else {
                $art_no = $Praemien_Artikelnummer_orig;
                if( !$art_no ) $art_no = $Praemien_Artikelnummer;

                $sql = "select prod_type from tx_zeitabo_products where uid={$art_no}";
                $prod_row = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
                if( $prod_row[ 'prod_type' ] == 2 ) {
                    return $row[ 'abo_no_student' ];
                } else {
                    return $row[ 'abo_no' ];
                }

            }
        }
        else {
            return '';
        }
    }

  function get_address_for_row( $row, $k ) {
    return $row[ $k ];

    $sql = "SELECT * FROM zeit_user WHERE uid={$row[ 'uid' ]}";
    $row2 = $this->db->sql_fetch_assoc( $this->db->sql_query( $sql ) );
    $v = '';
    switch( $k ) {
      case 'Zahler_Strasse':
        $v = $row[ 'Zahler_Strasse' ];
        if( $row2[ 'Zahler_Adresszusatz' ] ) {
          $v .= ', ' . $row2[ 'Zahler_Adresszusatz' ];
        }
        break;
      case 'Empfaenger_Strasse':
        $v = $row[ 'Empfaenger_Strasse' ];
        if( $row2[ 'Empfaenger_Adresszusatz' ] ) {
          $v .= ', ' . $row2[ 'Empfaenger_Adresszusatz' ];
        }
        break;
      case 'Werber_Strasse':
        $v = $row2[ 'Werber_Strasse' ];
        if( $row2[ 'Werber_Adresszusatz' ] ) {
          $v .= ', ' . $row2[ 'Werber_Adresszusatz' ];
        }
        break;
    }
    return $v;
  }

  function get_special_columns( $row ) {
    $mag = false;
    if( $row[ 'Aktionsnummer' ] == 'mag' || preg_match( '/^tx_zeitabo_abonnements_/', $row[ 'Aktionsnummer' ] ) ) {
      $row[ 'Aktionsnummer' ] = $this->get_mag_no_for_id( $row[ 'Zugaben_Artikelnummer' ] );
      $row[ 'Zugaben_Artikelnummer' ] = '';
    }
    elseif($row['Abo_Nummer_Kombi'] != ''){
	  $row[ 'Aktionsnummer' ] = $row['Abo_Nummer_Kombi'];
      $row[ 'Zugaben_Artikelnummer' ] = '';
	} else {
      #if ( $row['uid'] == 270 ) print_r( $row );
      foreach( $row as $k => $v ) {
        switch( $k ) {
          case 'Aktionsnummer':
            $row[ "{$k}_orig" ] = $v;
            $v = $this->get_abo_no_for_row( $row, $v );
            break;
          case 'Zugaben_Artikelnummer':
          case 'Praemien_Artikelnummer':
            $row[ "{$k}_orig" ] = $v;
            $v = $this->get_prod_no_for_id( $v );
            break;
        }
        $row[ $k ] = $v;
      }
    }
    if(!empty($row[ 'Aktionsnummer_Adword'])){
      $is_student = 0;
      if($row['student' ] == 'ok') $is_student = 1;
      $newAktionsnummer = $this->get_abo_no_for_adword( $row[ 'Aktionsnummer_Adword'], $is_student, $row['Praemien_Artikelnummer'], $row['Praemien_Artikelnummer_orig']);
      if(!empty($newAktionsnummer) && $newAktionsnummer != 0 && $newAktionsnummer != ''){
          $row[ 'Aktionsnummer' ] = $newAktionsnummer;
      }
    }
    return $row;
  }
  
  function write_row_aischa( $row, $checkspecial = 0 ) {
	#print_r( $row );
	#print "<br />";
    $to_write = array( );

    $is_abo = true;
    if( $row[ 'Aktionsnummer' ] == 'mag' || preg_match( '/^tx_zeitabo_abonnements_/', $row[ 'Aktionsnummer' ] ) ) {
      $is_abo = false;
    }


      if( $row[ 'Aktionsnummer' ] == '385' || $row[ 'Aktionsnummer' ] == '438' || $row[ 'Aktionsnummer' ] == '383' || $row[ 'Aktionsnummer' ] == '423'|| $row[ 'Aktionsnummer' ] == '384'  ) {
          if (isset($row['is_upselling']) && $row['is_upselling'] != 0) {
              $is_abo = true;
          } else {
              $is_abo = false;
          }

      }

	if($checkspecial == 1){
	    $row = $this->get_special_columns( $row );
	}
    if(!empty($row[ 'Aktionsnummer_Adword'])){
      $is_student = 0;
      if($row['student' ] == 'ok') $is_student = 1;
        $newAktionsnummer = $this->get_abo_no_for_adword( $row[ 'Aktionsnummer_Adword'], $is_student, $row['Praemien_Artikelnummer'], $row['Praemien_Artikelnummer_orig']);
        if(!empty($newAktionsnummer) && $newAktionsnummer != 0 && $newAktionsnummer != ''){
            $row[ 'Aktionsnummer' ] = $newAktionsnummer;
        }
    }
    foreach( $this->export_fields_aischa as $field ) {
        if( $is_abo ) {
            if ($field[0] == 'D') {
                $to_write[] = $this->clean_string($row[$field[1]]);
            } elseif ($field[0] == 'S') {
                $value = $this->clean_string($row[$field[1]]);
                if (is_array($field[2])) {
                    $rvalue = '';
                    if ($field[3] != '') {
                        $rvalue = $field[3];
                    }
                    foreach ($field[2] as $k => $v) {
                        if ($value == $v) {
                            $rvalue = $k;
                        }
                    }
                }
                $to_write[] = $rvalue;
            } elseif ($field[0] == 'L') {
                $to_write[] = $field[1];
            } elseif ($field[0] == 'C') {
                if ($row[$field[2] . '_Vorname'] != '' && $row[$field[2] . '_Name'] != '') {
                    $to_write[] = $field[1];
                } else {
                    $to_write[] = '';
                }
            } elseif ($field[0] == 'Z') {
                if ($row[$field[2]] == $field[3]) {
                    $to_write[] = $row[$field[1]];
                } else {
                    $to_write[] = '';
                }
            } elseif ($field[0] == 'ID') {
                if (isset($row['is_upselling']) && $row['is_upselling'] != 0) {
                    $to_write[] = 'zeit-aboshop-upselling-' . $row['uid'];
                } else {
                    $to_write[] = 'zeit-aboshop-' . $row['uid'];
                }
            }
        }
    }
	if( $row[ 'Upselling_product' ] != '' ) {
		$row_upselling = $row;
		$row_upselling['Aktionsnummer'] = $row['Upselling_product'];
        $row_upselling['Aktionsnummer_Adword'] = '';
		$row_upselling['Upselling_product'] = '';
		$row_upselling['Zugaben_Artikelnummer'] = '';
		$row_upselling['Praemien_Artikelnummer'] = '';
		$row_upselling['Werber_Anrede_Bezeichnung'] = '';
		$row_upselling['Werber_Vorname'] = '';
		$row_upselling['Werber_Name'] = '';
		$row_upselling['Werber_Adresszusatz'] = '';
		$row_upselling['Werber_Strasse'] = '';
		$row_upselling['Werber_PLZ'] = '';
		$row_upselling['Werber_Ort'] = '';
		$row_upselling['Werber_Land'] = '';
		$row_upselling['Werber_Geburtsdatum'] = '';
		$row_upselling['is_upselling'] = 1;
        #print_r($row_upselling);
		$this->write_row_aischa( $row_upselling, 0 );
	}
    if( $is_abo ) {
      fwrite( $this->file_aischa, implode( ';', $to_write ) . "\n" );
    }
   
  }
  
				
  function write_row( $row ) {
//print_r( $row );
//print "<br />";
    $to_write = array( );

    $is_abo = true;
    if( $row[ 'Aktionsnummer' ] == 'mag' || preg_match( '/^tx_zeitabo_abonnements_/', $row[ 'Aktionsnummer' ] ) ) {
      $is_abo = false;
    }

    $is_callback = false;
    if( $row[ 'callback' ] || $row[ 'Kontakt_Telefon' ] ) {
      $is_callback = true;
    }

    $row = $this->get_special_columns( $row );
    if(!empty($row[ 'Aktionsnummer_Adword'])){
      $is_student = 0;
      if($row['student' ] == 'ok') $is_student = 1;
        $newAktionsnummer = $this->get_abo_no_for_adword( $row[ 'Aktionsnummer_Adword'], $is_student, $row['Praemien_Artikelnummer'], $row['Praemien_Artikelnummer_orig']);
        if(!empty($newAktionsnummer) && $newAktionsnummer != 0 && $newAktionsnummer != ''){
            $row[ 'Aktionsnummer' ] = $newAktionsnummer;
        }
    }
   
    foreach( $this->export_fields as $field ) {
      $to_write[ ] = $this->clean_string( $row[ $field ] );
    }

    if( $is_abo ) {
      fwrite( $this->file_abo, implode( ';', $to_write ) . "\n" );
    }
    else {
      fwrite( $this->file_einzelheft, implode( ';', $to_write ) . "\n" );
    }

    if( $is_callback ) {
      fwrite( $this->file_callback, implode( ';', $to_write ) . "\n" );
    }
  }

  function encrypt( $f_name ) {
    return 0;

    $output = array( );
    exec( "/usr/bin/gpg --output {$f_name}.gpg --encrypt --recipient kimba_test@guj.de --yes {$f_name} 2>&1", $output, $ret );
    $msg = implode( "<br />", $output );

    if( $ret <> 0 ) {
      throw new Exception( "gpg failed ($ret): $msg" );
    }
    return $ret;
  }

}



if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/zeitabo/mod1/index.php'])) {
	include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/zeitabo/mod1/index.php']);
}




	// Make instance:
$SOBE = t3lib_div::makeInstance('tx_zeitabo_module1');
$SOBE->init();

	// Include files?
foreach ($SOBE->include_once as $INC_FILE) {
	include_once($INC_FILE);
}

$SOBE->main();
$SOBE->printContent();

?>