<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabo_abonnements=1
');
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabo_products=1
');
t3lib_extMgm::addUserTSConfig('
	options.saveDocNew.tx_zeitabo_singlebooks=1
');

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
t3lib_extMgm::addTypoScript($_EXTKEY,'editorcfg','
	tt_content.CSS_editor.ch.tx_zeitabo_pi1 = < plugin.tx_zeitabo_pi1.CSS_editor
',43);


t3lib_extMgm::addPItoST43($_EXTKEY,'pi1/class.tx_zeitabo_pi1.php','_pi1','list_type',1);


t3lib_extMgm::addTypoScript($_EXTKEY,'setup','
	tt_content.shortcut.20.0.conf.tx_zeitabo_abonnements = < plugin.'.t3lib_extMgm::getCN($_EXTKEY).'_pi1
	tt_content.shortcut.20.0.conf.tx_zeitabo_abonnements.CMD = singleView
',43);


$TYPO3_CONF_VARS['FE']['eID_include']['blz'] = 'EXT:zeitabo/blz.php';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_zeitabo_exportabos'] = array(
  'extension' => $_EXTKEY, // Selbsterklärend
  'title' => 'Export Abos', // Der Titel der Aufgabe
  'description' => 'Abos exportieren und verschicken', // Die Beschreibung der Aufgabe
  // 'additionalFields' => 'tx_extkey_TaskName_AdditionalFieldProvider' // Zusätzliche Felder
);

?>