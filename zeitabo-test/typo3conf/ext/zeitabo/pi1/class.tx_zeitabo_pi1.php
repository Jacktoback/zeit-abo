<?php

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2007 Patrick Bisplinghoff <patrick.bisplinghoff@clicktivities.net>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */
/**
 * Plugin 'DIE ZEIT Abo-Shop' for the 'zeitabo' extension.
 *
 * @author    Patrick Bisplinghoff <patrick.bisplinghoff@clicktivities.net>
 */
require_once(PATH_tslib . 'class.tslib_pibase.php');
require_once( t3lib_extMgm::extPath('zeitabo') . '/class.tx_zeitabo_base.php' );

class tx_zeitabo_pi1 extends tx_zeitabo_base {

  var $prefixId = 'tx_zeitabo_pi1';        // Same as class name
  var $scriptRelPath = 'pi1/class.tx_zeitabo_pi1.php';    // Path to this script relative to the extension dir.
  var $extKey = 'zeitabo';    // The extension key.
  var $pi_checkCHash = TRUE;

  /**
   * Main method of your PlugIn
   *
   * @param    string        $content: The content of the PlugIn
   * @param    array        $conf: The PlugIn Configuration
   * @return    The content that should be displayed on the website
   */
  function main($content, $conf) {
    $this->conf = $conf;
    $this->pi_setPiVarDefaults();
    $this->pi_loadLL();
    $this->pi_initPIFlexForm();

    # debugging
    #t3lib_div::debug($this->conf);
    #t3lib_div::debug($this->cObj->data);
    #t3lib_div::debug($this->cObj->data['starttime']);
    #t3lib_div::debug($this->cObj->data['pages']);
    #t3lib_div::debug($this->cObj->data['list_type']);
    #t3lib_div::debug($this->cObj->data['pi_flexform']);
    #t3lib_div::debug($this->piVars);
    # get template
    if ($conf['templatefile']) {
      $this->template=$this->cObj->fileResource($conf['templatefile']);
    } else {
      $this->template=$this->cObj->fileResource('typo3conf/ext/zeitabo/pi1/template.html');
    }

    # get display
    $displaySelect = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'displayselect', 'abo');

    #t3lib_div::debug($displaySelect);
    # get colors from colorscheme
    $this->colors = array();
    $this->colorscheme = ($this->cObj->data['tx_add2tca_colorscheme'] != '') ? $this->cObj->data['tx_add2tca_colorscheme'] : $this->cObj->TEXT(array('data' => $this->conf['colors.']['inheritedColorscheme.']['data']));
    $this->colorscheme = str_replace('-', '', $this->colorscheme);
    if ($this->colorscheme == '') {
      $this->colorscheme = 'zblue';
    }

    $this->colors[0] = $this->conf['colors.'][$this->colorscheme . '.']['1'];
    $this->colors[1] = $this->conf['colors.'][$this->colorscheme . '.']['2'];
    $this->colors[2] = $this->conf['colors.'][$this->colorscheme . '.']['3'];
    $this->colors[3] = $this->conf['colors.'][$this->colorscheme . '.']['4'];
    $this->colors[4] = $this->conf['colors.'][$this->colorscheme . '.']['5'];
    $this->colors[5] = $this->conf['colors.'][$this->colorscheme . '.']['6'];
    $this->colors[6] = $this->conf['colors.'][$this->colorscheme . '.']['7'];

    #t3lib_div::debug($this->colors);
    #t3lib_div::debug($this->colorscheme);
    # switch to funtions
    switch ($displaySelect) {
      // case 'hero':
      //  return $this->displayHero($content, $conf, false);
      // break;
      //
      // case 'hero_small':
      //  return $this->displayHero($content, $conf, true);
      // break;
      //
      // case 'highlight':
      //  return $this->displayHighlight($content, $conf);
      // break;
      //
      // case 'highlightfull':
      //  return $this->displayHighlight($content, $conf, true);
      // break;
      //
      // case 'teaser':
      //  return $this->displayTeaser($content, $conf);
      // break;

      case 'treadmill':
        return $this->displayTreadmill($content, $conf);
        break;

      // case 'abo_prod_h':
      //  return $this->displayHorizontal($content, $conf);
      // break;
      //
      // case 'abo_only':
      //  return $this->displaySingle($content, $conf);
      // break;
      //
      // case 'abo_double':
      //  return $this->displayDouble($content, $conf);
      // break;
      //
      // case 'abo_tripple':
      //  return $this->displayTripple($content, $conf);
      // break;
      //
      // case 'singlezeit':
      //  return $this->displaySingleZeit($content, $conf);
      // break;
      //
      case 'singlebooks':
        return $this->displaySinglebooks($content, $conf);
        break;
      //
      // case 'singlebooks_single':
      //  return $this->displaySingle($content, $conf, true);
      // break;
      //
      case 'foreign':
        return $this->displayForeign($content, $conf);
        break;

      case 'landingpage1':
        return $this->displayLandingpage(1, $content, $conf);
        break;

      case 'landingpage1 (3 prod) - v2':
        return $this->displayLandingpage('1_v2', $content, $conf);
        break;

      case 'landingpage2':
        return $this->displayLandingpage(2, $content, $conf);
        break;

      case 'landingpage3':
        return $this->displayLandingpage(3, $content, $conf);
        break;
	  case 'digital':
      	return $this->displayDigital($content, $conf);
      	break;
      // case 'landingpage4':
      //  return $this->displayLandingpage(4, $content, $conf);
      // break;
      //
      // case 'order':
      //  return $this->displayOrder($content, $conf);
      // break;
      //
      // case 'interface':
      //  return $this->displayInterface($content, $conf);
      // break;
      //
      // case 'email':
      //  return $this->displayEMail($content, $conf);
      // break;
      //
      // default:
      //  return $this->displaySingle($content, $conf);
      // break;
    }
  }
  
  function displayDigital($content, $conf) {
    # get abo data
    $arrAbo = $this->getAboData(true, false);
    // debug( $arrAbo );
    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo, 'all_products');
    $arrMarker['###L###'] = $GLOBALS['TSFE']->sys_language_uid;

    $arrMarker['###FORM_LINK###'] = $this->pi_getPageLink($this->cObj->data['tx_add2tca_formpage_id']);
    $arrMarker['###ABO_ID###'] = $arrAbo['ff_uid'];

    # marker top
    $arrMarker['###COLOR_1###'] = $this->colors[0];
    $arrMarker['###HEADLINE_ABO###'] = $arrAbo['ff_abo_hl'];
    $arrMarker['###SUB_HEADLINE_ABO###'] = $arrAbo['ff_abo_sub_hl'];
    $arrMarker['###BUTTON_COLOR###'] = $conf['buttoncolor'];
      if ($conf['mobile']) {
          $arrMarker['###IMAGE_ABO###'] = '<img src="uploads/tx_zeitabo/'.$arrAbo['abo_img'][0].'" alt="'.$arrAbo['ff_abo_hl'].'" />';
      } else {
          $arrMarker['###IMAGE_ABO###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['treadmill_intro.'], $this->colors[0]);
      }


    $arrMarker['###TEXT_ABO###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###HEADLINE_PRODUCT###'] = $arrAbo['ff_prod_headline'];
    $arrMarker['###TEXT_ABO_2###'] = $this->pi_RTEcssText($arrAbo['ff_prod_txt']);
    $arrMarker['###IMAGE_PRODUCT_SELECTED###'] = $this->getAboImage($arrAbo['abo_img'][1], $this->conf['images.']['product_selected.'], $this->colors[0]);
    $arrMarker['###HEADLINE_SELECT###'] = ($arrAbo['all_products'][0]['prod_type'] == '0') ? 'Ihr Geschenk' : 'Ihre Pr&auml;mie';

    $arrMarker['###PRODUCT_LIST###'] = '';
    $arrMarker['###PRODUCT_ARRAY###'] = '';
	$arrMarker['###SELECTED_ITEM###'] = 0;
    $arrMarker['###SELECTED_PRODUCT###'] = 0;
    $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
    $arrMarker['###IS_PRE_SELECTION###'] = 'false';
    $arrMarker['###BTN_BACK###'] = '/fileadmin/templates_new/img/button_zurueck.png';
    $arrMarker['###BTN_FORWARD###'] = '/fileadmin/templates_new/img/button_vor.png';

    $this->collect_product_js_array($arrAbo, $conf, $arrMarker);

      if ($conf['mobile']) {
          #$this->collect_products_array($arrAbo, $conf, $arrMarker);
          $arrMarker['###PRICE###'] =  number_format($arrAbo['abo_price'],2,',','.');
          $arrMarker['###PRICE_STUDENT###'] =  number_format($arrAbo['abo_price_student'],2,',','.');

          $arrMarker['###BENEFITS###'] =  $this->pi_RTEcssText($arrAbo['abo_txt_benefits']);
          if (isset($conf['hidebenefit']) && $conf['hidebenefit'] == 1) {
              $arrMarker['###BENEFITS###'] =  '';
          }
      }

    # praemien abo add text
    switch ($arrAbo['all_products'][0]['prod_type']) {
      case '1': $addText = ' f&uuml;r ein 1 Jahres-Abo!';
        break;
      case '2': $addText = ' f&uuml;r ein 2 Jahres-Abo!';
        break;
      default: $addText = '';
    }

    $arrMarker['###ZUZAHLUNG###'] = ($arrAbo['abo_offerform'] == '5') ? 'Ohne Zuzahlung' : '';
    $arrMarker['###PRODUCT_COPAYMENT###'] = (intval($arrAbo['all_products'][0]['prod_copayment']) > 0) ? 'Zuzahlung nur &euro; ' . $arrAbo['all_products'][0]['prod_copayment'] . ' ' . $addText : $arrMarker['###ZUZAHLUNG###'] . $addText;

    $arrMarker['###TEXT###'] = $arrAbo['abo_txt_teaser'];

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###DIGITAL###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }
  
  function displaySinglebooks($content, $conf) {
    # get abo data
    $arrSB = $this->getSBdata($this->cObj->data['pages']);
//        debug($arrSB);
    #t3lib_div::debug($arrSB[1]['mag_price']);
//        $arrSB['ff_abo_img_ov'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_img_overwrite', 'overwrite');
//        $arrSB['ff_prod_txt'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'prod_txt', 'prod_content');
//
//        # set marker array
//        $arrMarker = array();
//
//        # order form
//        $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
//        $lConf = array('parameter' => $this->conf['orderFormId']);
//        $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);
//
//        # hidden fields
//        $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrSB, 'singlebooks');
//
//        # marker top 
//        $arrMarker['###COLOR_1###'] = $this->colors[0];
//        debug( $this->cObj->data['pi_flexform']);

    $arrMarker['###FORM_LINK###'] = $this->pi_getPageLink($this->cObj->data['tx_add2tca_formpage_id']);
    $arrMarker['###L###'] = $GLOBALS['TSFE']->sys_language_uid;

    $arrMarker['###HEADLINE_ABO###'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_headline', 'abo_content');
    $arrMarker['###SUB_HEADLINE_ABO###'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_sub_headline', 'abo_content');

    $strMagImage = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_img_overwrite', 'overwrite');

    $arrMarker['###IMAGE_ABO###'] = $this->getAboImage($strMagImage, $this->conf['images.']['treadmill_intro.']);
    $arrMarker['###TEXT_ABO###'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_txt', 'abo_content');
//        $strSBHeadline = ($arrSB[0]['mag_type'] == '2') ? 'Klassensatzbestellung' : 'Einzelheftbestellung';
    $arrMarker['###HEADLINE_PRODUCT###'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'prod_headline', 'prod_content');
    $arrMarker['###TEXT_ABO_2###'] = $this->pi_RTEcssText($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'prod_txt', 'prod_content'));
//        $arrMarker['###IMAGE_PRODUCT_SELECTED###'] = $this->getProductImageSmall($arrSB[0]['mag_img'][0], $this->conf['images.']['product_selected.'], $this->colors[0]);
    $arrMarker['###HEADLINE_SELECT###'] = 'Ausgabe';

    $aboArr = $this->getAboData(true, false);
    $arrMarker['###ABO_ID###'] = 'tx_zeitabo_abonnements_' . $aboArr['ff_uid'];
    ;

        $arrMarker['###ABO_ID###'] = 'mag';
        
//        $arrMarker['###PRODUCT_LIST###'] = '';
    $count = 0;
    $arrMarker['###PRODUCT_ARRAY###'] = "var all_products = new Array();\n";
	$arrMarker['###SELECTED_ITEM###'] = 0;
    $arrMarker['###SELECTED_PRODUCT###'] = 0;
    $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
    $arrMarker['###IS_PRE_SELECTION###'] = 'false';
    $arrMarker['###BTN_BACK###'] = '/fileadmin/templates_new/img/button_zurueckmagazine.png';
    $arrMarker['###BTN_FORWARD###'] = '/fileadmin/templates_new/img/button_weiteremagazine.png';

    foreach ($arrSB as $prods) {
      // debug( $prods );
      $arrMarker['###PRODUCT_ARRAY###'] .= "all_products[$count] = {
                uid: {$prods['uid']},
                image: '" . $this->getProductImageSmallResource($prods['mag_img'][0], $this->conf['images.']['product_treadmill.']) . "',
                big_image: '" . $this->getProductImageSmallResource($prods['mag_img'][1], $this->conf['images.']['product_treadmill_top.']) . "',
                headline: '" . preg_replace("/\r\n/", '<br />', addslashes($prods['mag_name'])) . "',
				group: '',
                text: '" . preg_replace("/\r\n/", '<br />', addslashes($this->addSpecialsChars($prods['mag_txt']))) . "'
                };\n";
      // $arrMarker['###PRODUCT_ARRAY###'] .= 'products['.$prods['uid'].'] = new Array('.$count.', "'.$prods['prod_no_1'].'","'.$prods['prod_no_2'].'","'.$prods['prod_no_3'].'", "'.$prods['prod_copayment'].'", "'.$this->getProductImageSmallResource($prods['prod_img'][0], $this->conf['images.']['product_selected.'], $this->colors[0]).'", "'.$prods['prod_type'].'");';
      $arrMarker['###PRODUCT_LIST###'] .= '<option value="' . $prods['uid'] . '">' . $prods['mag_name'] . '</option>' . chr(10);
      $count++;
    }


    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###TREADMILL###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayTreadmill($content, $conf) {
    # get abo data
    $arrAbo = $this->getAboData(true, false);
    // debug( $arrAbo );
    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo, 'all_products');
    $arrMarker['###L###'] = $GLOBALS['TSFE']->sys_language_uid;

    $arrMarker['###FORM_LINK###'] = $this->pi_getPageLink($this->cObj->data['tx_add2tca_formpage_id']);
    $arrMarker['###ABO_ID###'] = $arrAbo['ff_uid'];

    # marker top
    $arrMarker['###COLOR_1###'] = $this->colors[0];
    $arrMarker['###HEADLINE_ABO###'] = $arrAbo['ff_abo_hl'];
    $arrMarker['###SUB_HEADLINE_ABO###'] = $arrAbo['ff_abo_sub_hl'];
    $arrMarker['###BUTTON_COLOR###'] = $conf['buttoncolor'];
    if ($conf['mobile']) {
        $arrMarker['###IMAGE_ABO###'] = '<img src="uploads/tx_zeitabo/'.$arrAbo['abo_img'][0].'" alt="'.$arrAbo['ff_abo_hl'].'" />';
    } else {
        $arrMarker['###IMAGE_ABO###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['treadmill_intro.'], $this->colors[0]);
    }

    $arrMarker['###TEXT_ABO###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###HEADLINE_PRODUCT###'] = $arrAbo['ff_prod_headline'];
    $arrMarker['###TEXT_ABO_2###'] = $this->pi_RTEcssText($arrAbo['ff_prod_txt']);
    $arrMarker['###IMAGE_PRODUCT_SELECTED###'] = $this->getAboImage($arrAbo['abo_img'][1], $this->conf['images.']['product_selected.'], $this->colors[0]);
    $arrMarker['###HEADLINE_SELECT###'] = ($arrAbo['all_products'][0]['prod_type'] == '0') ? 'Ihr Geschenk' : 'Ihre Pr&auml;mie';

    $arrMarker['###PRODUCT_LIST###'] = '';
	$arrMarker['###SELECTED_ITEM###'] = 0;
    $arrMarker['###SELECTED_PRODUCT###'] = 0;
    $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
    $arrMarker['###IS_PRE_SELECTION###'] = 'false';
    $arrMarker['###PRODUCT_ARRAY###'] = '';
	$praembutton = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'prod_praemie_buttons', 'overwrite');
	if($praembutton == 1){
		$arrMarker['###BTN_BACK###'] = '/fileadmin/templates_new/img/button_zurueck_praemie.png';
    	$arrMarker['###BTN_FORWARD###'] = '/fileadmin/templates_new/img/button_vor_praemie.png';
		$arrMarker['###BTN_UP###'] = '/fileadmin/templates_new/img/button_up_praemie.png';
	} else {
	    $arrMarker['###BTN_BACK###'] = '/fileadmin/templates_new/img/button_zurueck.png';
    	$arrMarker['###BTN_FORWARD###'] = '/fileadmin/templates_new/img/button_vor.png';
		$arrMarker['###BTN_UP###'] = '/fileadmin/templates_new/img/button_up.png';
	}

    $this->collect_product_js_array($arrAbo, $conf, $arrMarker);

    if ($conf['mobile']) {
        $this->collect_products_array($arrAbo, $conf, $arrMarker);
        $arrMarker['###PRICE###'] =  number_format($arrAbo['abo_price'],2,',','.');
        $arrMarker['###PRICE_STUDENT###'] =  number_format($arrAbo['abo_price_student'],2,',','.');
        $arrMarker['###BENEFITS###'] =  $this->pi_RTEcssText($arrAbo['abo_txt_benefits']);
        if (isset($conf['hidebenefit']) && $conf['hidebenefit'] == 1) {
            $arrMarker['###BENEFITS###'] =  '';
        }
    }

    # praemien abo add text
    switch ($arrAbo['all_products'][0]['prod_type']) {
      case '1': $addText = ' f&uuml;r ein 1 Jahres-Abo!';
        break;
      case '2': $addText = ' f&uuml;r ein 2 Jahres-Abo!';
        break;
      default: $addText = '';
    }

    $arrMarker['###ZUZAHLUNG###'] = ($arrAbo['abo_offerform'] == '5') ? 'Ohne Zuzahlung' : '';
    $arrMarker['###PRODUCT_COPAYMENT###'] = (intval($arrAbo['all_products'][0]['prod_copayment']) > 0) ? 'Zuzahlung nur &euro; ' . $arrAbo['all_products'][0]['prod_copayment'] . ' ' . $addText : $arrMarker['###ZUZAHLUNG###'] . $addText;

    $arrMarker['###TEXT###'] = $arrAbo['abo_txt_teaser'];



    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###TREADMILL###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function get_product_js_string($prods, $count, $group_str) {
    // debug( $prods['prod_txt']);
	#headline: '" . preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', addslashes($prods['prod_hl']))) . "',
    return "{
                uid: {$prods['uid']},
                image: '" . $this->getProductImageSmallResource($prods['prod_img'][0], $this->conf['images.']['product_treadmill.']) . "',
                big_image: '" . $this->getProductImageSmallResource($prods['prod_img'][1], $this->conf['images.']['product_treadmill_top.']) . "',
                
				headline: '" . preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', $this->addSpecialsChars($prods['prod_name']))) . "',
                text: '" . preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', $prods['prod_txt'])) . "',
                group: $group_str
                }\n";
  }

  function collect_products_array($arrAbo, $conf, &$arrMarker) {
      $groups = array();
      $products = array();
      $carousel_item = $this->cObj->getSubpart($this->template, '###CAROUSEL_ITEM###');

      $selection = t3lib_div::removeXSS(strip_tags(t3lib_div::_GP('selection')));

      foreach ($arrAbo['all_products'] as $prods) {
          if (!$prods['product_group']) {
              $products[] = $prods;
          } else {
              if (!array_key_exists($prods['product_group'], $groups)) {
                  $products[] = $prods;
              }
              $groups[$prods['product_group']][] = $prods;
          }
      }

      foreach ($products as $key => $value) {
          if ($value['product_group'] && sizeof($groups[$value['product_group']]) == 1) {
              $products[$key]['product_group'] = null;
              unset($groups[$value['product_group']]);
          }
      }
      $count = 0;
      $totalCount = 0;
      $prodCount = 0;
      $prodTitle = '';
      $prodDesc = '';
      foreach ($products as $prods) {
          $newProduct = array();

          $newProduct['active'] = 'active';
          if($totalCount > 0) $newProduct['active'] = '';

          if ($prods['product_group']) {
              $group_count = 0;
              foreach ($groups[$prods['product_group']] as $group_prod) {
                  $newProduct['artikelnummer'] = $group_prod['uid'];
                  $img = $group_prod['prod_img'][1];
                  if(!empty($group_prod['product_image_mobile' ])){
                      $img = $group_prod['product_image_mobile' ];
                  }
                  $newProduct['image'] = $this->getProductImageSmallResource($img, $this->conf['images.']['product_treadmill.']);

                  $newProduct['title'] = stripslashes(preg_replace("/xA0/", " ", preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', $this->addSpecialsChars($group_prod['prod_name'])))));
                  $newProduct['image_alt'] = stripslashes($newProduct['title']);
                  $ptext = ($group_prod['prod_txt_mobile'] != '') ? $group_prod['prod_txt_mobile'] : $group_prod['prod_txt'];
                  $newProduct['text'] = stripslashes(preg_replace("/xA0/", " ", preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', $ptext))));
                  if($totalCount == 0){
                      $prodTitle = $newProduct['title'];
                      $prodDesc = $newProduct['text'];
                      $arrMarker['###SELECTION_ID###'] = $newProduct['artikelnummer'];
                  }
                  if($totalCount > 0) $newProduct['active'] = '';
                  if($group_count == 0 && $prodCount < 4) {
                      $arrMarker['###CAROUSEL###'] .= $this->cObj->substituteMarkerArray($carousel_item,$newProduct,'###|###',$uppercase=1);
                      $prodCount++;
                  }
                  $group_count++;
                  $totalCount++;
              }
          } else {
              $newProduct['artikelnummer'] = $prods['uid'];
              $img = $prods['prod_img'][1];
              if(!empty($prods['product_image_mobile' ])){
                  $img = $prods['product_image_mobile' ];
              }
              $newProduct['image'] = $this->getProductImageSmallResource($img, $this->conf['images.']['product_treadmill.']);

              $newProduct['title'] = stripslashes(preg_replace("/xA0/", " ", preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', $this->addSpecialsChars($prods['prod_name'])))));
              $newProduct['image_alt'] = stripslashes($newProduct['title']);
              $ptext = ($prods['prod_txt_mobile'] != '') ? $prods['prod_txt_mobile'] : $prods['prod_txt'];
              $newProduct['text'] = stripslashes(preg_replace("/\xA0/", " ", preg_replace("/'/", "\\'", preg_replace("/\r\n/", '<br />', $ptext))));
              if($totalCount == 0){
                  $prodTitle = $newProduct['title'];
                  $prodDesc = $newProduct['text'];
                  $arrMarker['###SELECTION_ID###'] = $newProduct['artikelnummer'];

              }
              if($prodCount < 4){
                  $arrMarker['###CAROUSEL###'] .= $this->cObj->substituteMarkerArray($carousel_item,$newProduct,'###|###',$uppercase=1);
                  $prodCount++;
              }
              $totalCount++;
          }


          $count++;

      }
      $arrMarker['###SELECTED_ARTICLE_HEADLINE###'] = $prodTitle;
      $arrMarker['###SELECTED_ARTICLE_TEXT###'] = $prodDesc;

      $arrMarker['###ARTICLES_TOTAL###'] = $prodCount;

      #var_dump($arrMarker);


  }

  function collect_product_js_array($arrAbo, $conf, &$arrMarker) {
    $groups = array();
    $products = array();

    $options = array();
    $selection = t3lib_div::removeXSS(strip_tags(t3lib_div::_GP('selection')));

    $arrMarker['###SELECTED_ITEM###'] = 0;

    foreach ($arrAbo['all_products'] as $prods) {
      $prods['prod_images'] = '';
	  $options[$prods['prod_name']] = '<option value="' . $prods['uid'] . '">' . preg_replace("/\\\\xA0/","&nbsp;",$prods['prod_name']) . '</option>' . chr(10);
      #$options[$prods['prod_name']] = '<option value="' . $prods['uid'] . '">' . $prods['prod_name'] . '</option>' . chr(10);

      if (!$prods['product_group']) {
        $products[] = $prods;
      } else {
        if (!array_key_exists($prods['product_group'], $groups)) {
          $products[] = $prods;
        }
        $groups[$prods['product_group']][] = $prods;
      }
      if(!empty($prods['uid']) && !empty($selection)){
        if( $selection == $prods['uid']){
          $arrMarker['###SELECTED_PRODUCT###'] =  $prods['uid'];
          $options[$prods['prod_name']] = '<option value="' . $prods['uid'] . '" selected="selected">' . preg_replace("/\\\\xA0/","&nbsp;",$prods['prod_name']) . '</option>' . chr(10);
        }
      }
    }

    ksort($options);
    $arrMarker['###PRODUCT_LIST###'] = '';
    foreach ($options as $option) {
      $arrMarker['###PRODUCT_LIST###'] .= $option;
    }

    foreach ($products as $key => $value) {
      if ($value['product_group'] && sizeof($groups[$value['product_group']]) == 1) {
        $products[$key]['product_group'] = null;
        unset($groups[$value['product_group']]);
      }
    }

    foreach ($groups as $x => $pgroup) {

      foreach($pgroup as $k => $pg){

        if(!empty($pg['uid']) && !empty($selection)){
          if( $selection == $pg['uid']){

            $arrMarker['###SELECTED_ITEM###'] =  $x;
          }
        }
      }

    }
    $count = 0;

    $arrMarker['###PRODUCT_ARRAY###'] = "var all_products = new Array();\n";


    foreach ($products as $prods) {
      $group_str = "[]";
      if ($prods['product_group']) {
        $group_str = '[';
        $group_count = 0;
        foreach ($groups[$prods['product_group']] as $group_prod) {
          $group_str .= "\n" . $this->get_product_js_string($group_prod, $group_count, '[]') . ",";
          $group_count++;
          if(!empty($group_prod['uid']) && !empty($selection)){
            if( $selection == $group_prod['uid']){
              $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
              $arrMarker['###IS_PRE_SELECTION###'] = 'true';
              $arrMarker['###SELECTED_ITEM###'] =  $count;
            }
          }
        }
        $group_str = substr($group_str, 0, strlen($group_str) - 1);
        $group_str .= "]";

      } else {
        if(!empty($prods['uid']) && !empty($selection)){
          if( $selection == $prods['uid']){
            $arrMarker['###SELECTED_ITEM###'] =  $count;
          }
        }
      }
      $arrMarker['###PRODUCT_ARRAY###'] .= "all_products[$count] = " . $this->get_product_js_string($prods, $count, $group_str) . ";\n";

      $count++;
    }
  }

  function displayHero($content, $conf, $small = false) {
    # get abo data
    $arrAbo = $this->getAboData();

    # set marker array
    $arrMarker = array();
    $arrMarker['###COLOR###'] = $this->colors[0];
    if ($small) {
      $arrMarker['###IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['herosmall.'], $this->colors[0]);
    } else {
      $arrMarker['###IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    }
    $arrMarker['###HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_link', 'abo_content'), $this->conf['links.'], $this->colors[4], $this->colors[0], $this->colors[6]);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###HERO###');

    # replace markers with marker array
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayHighlight($content, $conf, $full = false) {

    $imgNo = 1;
    if ($full) {
      $this->colors[1] = $this->colors[0];
      $imgNo = 0;
      #    $this->colors[4] = $this->colors[3];
      #    $this->colors[6] = $this->colors[5];
    }
    # get abo data
    $arrAbo = $this->getAboData();

    # set marker array
    $arrMarker = array();
    $arrMarker['###COLOR###'] = $this->colors[1];
    $arrMarker['###ADD_CLASS###'] = '';
    #if($this->colorscheme == 'z-white') {
    #    $arrMarker['###ADD_CLASS###'] = '-z-white';
    #}
    $arrMarker['###IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][$imgNo], $this->conf['images.']['highlight.'], $this->colors[1], $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_link', 'abo_content'));
    $arrMarker['###TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_link', 'abo_content'), $this->conf['links.'], $this->colors[4], $this->colors[1], $this->colors[6]);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###HIGHLIGHT###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayTeaser($content, $conf) {
    # get abo data
    $arrAbo = $this->getAboData();

    # set marker array
    $arrMarker = array();
    $arrMarker['###COLOR###'] = $this->colors[2];
    $arrMarker['###TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_link', 'abo_content'), $this->conf['links.'], $this->colors[4], $this->colors[2], $this->colors[6]);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###TEASER###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayHorizontal($content, $conf) {
    # get order data
    $arrAbo = $this->getAboData(false, true);
    #t3lib_div::debug($arrAbo['special_products']);
    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo, 'special_products');

    # abo data
    $arrMarker['###BACKCOLOR_1###'] = $this->colors[0];
    $arrMarker['###ABO_IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);

    # product data
    $arrMarker['###BACKCOLOR_2###'] = $this->colors[1];
    $arrMarker['###PRODUCT_IMAGE###'] = $this->getAboImage($arrAbo['special_products'][0]['prod_img'][1], $this->conf['images.']['hero.'], $this->colors[1]);
    $strProductHeadline = ($arrAbo['special_products'][0]['prod_type'] == '0') ? 'Ihr Geschenk' : 'Ihre Prämie';
    $arrMarker['###PRODUCT_HEADLINE###'] = $this->getAboHeadline($strProductHeadline, $this->conf['header.']['hero.'], $this->colors[4], $this->colors[1]);
    $arrMarker['###PRODUCT_TEXT###'] = $this->pi_RTEcssText($arrAbo['special_products'][0]['prod_txt']);

    # link / submit
    $arrMarker['###ABO_LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:document.productselection.submit();', $this->conf['links.'], $this->colors[4], $this->colors[1], $this->colors[6], false);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###HORIZONTAL###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displaySingle($content, $conf, $sb = false) {
    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    if (!$sb) {
      # get order data
      $arrAbo = $this->getAboData();

      # hidden fields
      $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo);
    } else {
      # singlebook
      $arrSB = $this->getSBData($this->cObj->data['pages']);
      $arrAbo['abo_img'][0] = $arrSB[0]['mag_img'][0];
      $arrAbo['ff_abo_hl'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_headline', 'abo_content');
      $arrAbo['ff_abo_txt'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_txt', 'abo_content');
      $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrSB, 'singlebooks');
    }

    # abo data
    $arrMarker['###BACKCOLOR###'] = $this->colors[0];
    $arrMarker['###ABO_IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);

    # link / submit
    $arrMarker['###ABO_LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:document.productselection.submit();', $this->conf['links.'], $this->colors[4], $this->colors[0], $this->colors[6], false);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###SINGLE###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayDouble($content, $conf) {
    # get order data
    $arrAbo = $this->getAboData(false, true);

    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo, 'special_products');

    # abo data
    $arrMarker['###BACKCOLOR_1###'] = $this->colors[0];
    $arrMarker['###ABO_IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);

    # product data
    $arrMarker['###BACKCOLOR_2###'] = $this->colors[1];
    $arrMarker['###PRODUCT_ARRAY###'] = '';
	$arrMarker['###SELECTED_ITEM###'] = 0;
    $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
    $arrMarker['###IS_PRE_SELECTION###'] = 'false';
    $arrMarker['###SELECTED_PRODUCT###'] =  0;
    for ($i = 0; $i < 2; $i++) {
      # fill js array
      $arrMarker['###PRODUCT_ARRAY###'] .= 'products["' . $arrAbo['special_products'][$i]['uid'] . '"] = new Array("' . $arrAbo['special_products'][$i]['prod_no_1'] . '", "' . $arrAbo['special_products'][$i]['prod_no_2'] . '", "' . $arrAbo['special_products'][$i]['prod_no_3'] . '", "' . $arrAbo['special_products'][$i]['copayment'] . '");' . chr(10);

      # contents
      $arrMarker['###PRODUCT_HEADLINE_' . $i . '###'] = $this->getAboHeadline($arrAbo['special_products'][$i]['prod_name'], $this->conf['header.']['small.'], $this->colors[4], $this->colors[1]);
      $arrMarker['###PRODUCT_IMAGE_' . $i . '###'] = $this->getAboImage($arrAbo['special_products'][$i]['prod_img'][1], $this->conf['images.']['double.'], $this->colors[1]);
      $arrMarker['###PRODUCT_TEXT_' . $i . '###'] = $this->pi_RTEcssText($arrAbo['special_products'][$i]['prod_txt']);

      # link / submit
      $arrMarker['###ABO_LINK_' . $i . '###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:setProduct("' . $arrAbo['special_products'][$i]['uid'] . '");document.productselection.submit();', $this->conf['links.'], $this->colors[4], $this->colors[1], $this->colors[6], false);
    }

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###DOUBLE###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayTripple($content, $conf) {
    # get order data
    $arrAbo = $this->getAboData(false, true);

    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo, 'special_products');

    # abo data
    $arrMarker['###BACKCOLOR_1###'] = $this->colors[0];
    $arrMarker['###ABO_IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);

    # product data
    $arrMarker['###BACKCOLOR_2###'] = $this->colors[1];
    $arrMarker['###PRODUCT_ARRAY###'] = '';
	$arrMarker['###SELECTED_ITEM###'] = 0;
    $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
    $arrMarker['###IS_PRE_SELECTION###'] = 'false';
    $arrMarker['###SELECTED_PRODUCT###'] =  0;
    for ($i = 0; $i < 3; $i++) {
      # fill js array
      $arrMarker['###PRODUCT_ARRAY###'] .= 'products["' . $arrAbo['special_products'][$i]['uid'] . '"] = new Array("' . $arrAbo['special_products'][$i]['prod_no_1'] . '", "' . $arrAbo['special_products'][$i]['prod_no_2'] . '", "' . $arrAbo['special_products'][$i]['prod_no_3'] . '", "' . $arrAbo['special_products'][$i]['copayment'] . '");' . chr(10);

      # contents
      $arrMarker['###PRODUCT_HEADLINE_' . $i . '###'] = $this->getAboHeadline($arrAbo['special_products'][$i]['prod_name'], $this->conf['header.']['small2.'], $this->colors[4], $this->colors[1]);
      $arrMarker['###PRODUCT_HEADLINE2_' . $i . '###'] = $arrAbo['special_products'][$i]['prod_name'];
      $arrMarker['###PRODUCT_IMAGE_' . $i . '###'] = $this->getAboImage($arrAbo['special_products'][$i]['prod_img'][1], $this->conf['images.']['tripple.'], $this->colors[1]);
      $arrMarker['###PRODUCT_TEXT_' . $i . '###'] = $this->pi_RTEcssText($arrAbo['special_products'][$i]['prod_txt']);

      # link / submit
      $arrMarker['###ABO_LINK_' . $i . '###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:setProduct("' . $arrAbo['special_products'][$i]['uid'] . '");document.productselection.submit();', $this->conf['links.'], $this->colors[4], $this->colors[1], $this->colors[6], false);
    }

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###TRIPPLE###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displaySingleZeit($content, $conf) {
    # get order data
    $arrAbo = $this->getAboData();
    $arrSBZ = $this->getSBdata($this->cObj->data['pages']);

    $arrAbo['abo_uid'] = $arrSBZ[0]['uid'];
    $arrAbo['abo_offerform'] = 'singlezeit';
    $arrAbo['abo_img'] = $arrSBZ[0]['mag_img'];

    #t3lib_div::debug($arrAbo);
    #t3lib_div::debug($arrSBZ);
    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = '<input type="hidden" name="tx_zeitabo_pi1[abo_uid]" value ="' . $arrAbo['abo_uid'] . '">' . chr(10);
    $arrMarker['###HIDDEN_FIELDS###'] .= '<input type="hidden" name="tx_zeitabo_pi1[abo_offerform]" value ="' . $arrAbo['abo_offerform'] . '">' . chr(10);
    $arrMarker['###HIDDEN_FIELDS###'] .= '<input type="hidden" name="tx_zeitabo_pi1[return_url]" value ="' . $this->pi_linkTP_keepPIvars_url() . '">' . chr(10);

    $week = 7 * 24 * 60 * 60;
    $arrMarker['###OPTIONS###'] = '';
    for ($i = 0; $i < 26; $i++) {
      $arrMarker['###OPTIONS###'] .= '<option value="' . date('W/Y', time() - ($i * $week)) . '">DIE ZEIT Ausgabe ' . date('W / Y', time() - ($i * $week)) . '</option>' . chr(10);
    }

    # abo data
    $arrMarker['###BACKCOLOR###'] = $this->colors[0];
    $arrMarker['###ABO_IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);

    # link / submit
    $arrMarker['###ABO_LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:document.productselection.submit();', $this->conf['links.'], $this->colors[4], $this->colors[0], $this->colors[6], false);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###SINGLEZEIT###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayForeign($content, $conf) {
    # get abo data
    $arrAbo = $this->getAboData(true, true);
//debug( $arrAbo );
    # set marker array
    $arrMarker = array();

    # order form
    $arrMarker['###ORDERFORM_ID###'] = $this->conf['orderFormId'];
    $lConf = array('parameter' => $this->conf['orderFormId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    # hidden fields
    $arrMarker['###HIDDEN_FIELDS###'] = $this->getHiddenFields($arrAbo);

    # prices
    $arrPrices = $this->conf['prices.']['foreign.'];

    $arrMarker['###FIRST###'] = $arrPrices['austria.']['year'];

    $arrMarker['###PRICES###'] = 'var prices = new Array();';
    foreach ($arrPrices as $key1 => $val1) {
      $arrMarker['###PRICES###'] .= chr(10) . 'prices["' . $key1 . '"] = new Array();';
      foreach ($val1 as $key2 => $val2) {
        $arrMarker['###PRICES###'] .= chr(10) . 'prices["' . $key1 . '"]["' . $key2 . '"] = "' . $val2 . '";';
      }
    }

    # marker top
    $arrMarker['###COLOR_1###'] = $this->colors[0];
    $arrMarker['###HEADLINE_ABO###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###IMAGE_ABO###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['hero2.'], $this->colors[0]);
    $arrMarker['###TEXT_ABO###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###HEADLINE_PRODUCT###'] = $this->getAboHeadline('Abo-Auswahl', $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
    $arrMarker['###TEXT_ABO_2###'] = $this->pi_RTEcssText($arrAbo['ff_prod_txt']);
    $arrMarker['###HEADLINE_SELECT###'] = 'Ihre Auswahl:';

    $arrMarker['###PRODUCT_LIST###'] = '';

    $arrMarker['###PRODUCT_COPAYMENT###'] = '&nbsp;';

    $arrMarker['###TEXT###'] = $arrAbo['abo_txt_teaser'];
    $arrMarker['###LINK###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:document.productselection.submit();', $this->conf['links.'], $this->colors[4], $this->colors[0], $this->colors[6], false);

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###FOREIGN###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayLandingpage($type, $content, $conf) {
    # get abo data
    $arrAbo = $this->getAboData(true, false);
//        t3lib_div::debug($this->conf);
//        t3lib_div::debug($arrAbo['all_products']);


    $arrMarker['###HEADLINE_ABO###'] = $arrAbo['ff_abo_hl'];
    $arrMarker['###SUB_HEADLINE_ABO###'] = $arrAbo['ff_abo_sub_hl'];
    $arrMarker['###IMAGE_ABO###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['treadmill_intro.'], $this->colors[0]);
    $arrMarker['###TEXT_ABO###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
    $arrMarker['###HEADLINE_PRODUCT###'] = $arrAbo['ff_prod_headline'];
    $arrMarker['###TEXT_ABO_2###'] = $this->pi_RTEcssText($arrAbo['ff_prod_txt']);
    $arrMarker['###IMAGE_PRODUCT_SELECTED###'] = $this->getAboImage($arrAbo['abo_img'][1], $this->conf['images.']['product_selected.'], $this->colors[0]);
    $arrMarker['###HEADLINE_SELECT###'] = ($arrAbo['all_products'][0]['prod_type'] == '0') ? 'Ihr Geschenk' : 'Ihre Pr&auml;mie';

    $arrMarker['###PRODUCT_LIST###'] = '';
    $arrMarker['###PRODUCT_ARRAY###'] = '';
	$arrMarker['###SELECTED_ITEM###'] = 0;
    $arrMarker['###IS_GROUP_SELECTION###'] = 'false';
    $arrMarker['###IS_PRE_SELECTION###'] = 'false';
    $arrMarker['###SELECTED_PRODUCT###'] =  0;
    foreach ($arrAbo['all_products'] as $ix => $product) {
      $arrMarker["###HEADLINE_$ix###"] = $product['prod_name'];
      $arrMarker["###TEXT_$ix###"] = $product['prod_txt'];
      $arrMarker["###COPAYMENT_$ix###"] = $product['prod_copayment'];
      $arrMarker["###IMAGE_{$ix}_BIG###"] = $this->getProductImageSmallResource($product['prod_img'][1], $this->conf['images.']['product_treadmill_top.'] );
      $arrMarker["###IMAGE_{$ix}_THUMB###"] = $this->getProductImageSmallResource($product['prod_img'][0], $this->conf['images.']['product_treadmill.'] );
      $arrMarker["###LINK_$ix###"] = $this->pi_getPageLink(
                      $this->cObj->data['tx_add2tca_formpage_id'],
                      '',
                      array(
                          'abo' => $arrAbo['ff_uid'],
                          'selection' => $product['uid'],
                      )
      );
    }
    # praemien abo add text
    switch ($arrAbo['all_products'][0]['prod_type']) {
      case '1': $addText = ' f&uuml;r ein 1 Jahres-Abo!';
        break;
      case '2': $addText = ' f&uuml;r ein 2 Jahres-Abo!';
        break;
      default: $addText = '';
    }

    $arrMarker['###TEXT###'] = $arrAbo['abo_txt_teaser'];

    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, "###LANDINGPAGE_$type###");

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;


//        # set marker array
//        $arrMarker = array();
//        $arrMarker['###ORDERFORM###'] = $this->conf['orderFormId'];
//        $lConf = array('parameter' => $this->conf['orderFormId']);
//        $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);
//
//        $arrMarker['###HIDDEN_FIELDS###'] = ($type == 4) ? $this->getHiddenFields($arrAbo) : $this->getHiddenFields($arrAbo, 'special_products');
//
//        $arrMarker['###SCRIPT###'] = '';
//        for ($i = 0; $i < 3; $i++) {
//            $arrMarker['###SCRIPT###'] .= chr(10) . 'products[' . $i . '] = new Array("' . $arrAbo['special_products'][$i]['uid'] . '","' . $arrAbo['special_products'][$i]['prod_no_1'] . '","' . $arrAbo['special_products'][$i]['prod_no_2'] . '","' . $arrAbo['special_products'][$i]['prod_no_3'] . '","' . $arrAbo['special_products'][$i]['prod_copayment'] . '","' . htmlspecialchars($arrAbo['special_products'][$i]['prod_name']) . '");';
//        }
//        $arrMarker['###COLOR###'] = $this->colors[0];
//        $arrMarker['###IMAGE###'] = $this->getAboImage($arrAbo['abo_img'][0], $this->conf['images.']['lp1.'], $this->colors[0]);
//        $arrMarker['###HEADLINE###'] = $this->getAboHeadline($arrAbo['ff_abo_hl'], $this->conf['header.']['hero.'], $this->colors[4], $this->colors[0]);
//        $arrMarker['###TEXT###'] = $this->pi_RTEcssText($arrAbo['ff_abo_txt']);
//        #$arrMarker['###AUSWAHL###'] = ($type == 4) ? '<p>&nbsp;</p>' : 'Ihre Auswahl:';
//        $arrMarker['###PRODUCT_0###'] = ($type == 4) ? '' : $arrAbo['special_products'][0]['prod_name'];
//        $arrMarker['###LINK###'] = ($type == 4) ? $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:document.landingpage.submit();', $this->conf['links.'], $this->colors[4], $this->colors[0], $this->colors[6]) : '';
//
//        # select template subpart
//        $subpart = $this->cObj->getSubpart($this->template, '###LANDINGPAGE_TOP###');
//
//        # replace markers with marker array
//        $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);
//
//        # set marker array
//        $arrMarker = array();
//        $arrMarker['###COLOR###'] = $this->colors[1];
//
//        switch ($type) {
//            case 1:
//                $strImageFormat = 'lp2.';
//                $strHeaderFormat = 'small.';
//                break;
//            case 2:
//                $strImageFormat = 'lp3.';
//                $strHeaderFormat = 'small.';
//                break;
//            case 3:
//                $strImageFormat = 'lp4.';
//                $strHeaderFormat = 'hero.';
//                break;
//            default:
//                $strImageFormat = 'lp2.';
//                $strHeaderFormat = 'small.';
//        }
//
//        $count = 0;
//        if (is_array($arrAbo['special_products'])) {
//            foreach ($arrAbo['special_products'] as $spp) {
//                $arrMarker['###HEADLINE_' . $count . '###'] = $this->getAboHeadline($spp['prod_name'], $this->conf['header.'][$strHeaderFormat], $this->colors[4], $this->colors[1]);
//                $arrMarker['###IMAGE_' . $count . '###'] = $this->getAboImage($spp['prod_img'][1], $this->conf['images.'][$strImageFormat], $this->colors[1]);
//                $arrMarker['###TEXT_' . $count . '###'] = $this->pi_RTEcssText($spp['prod_txt']);
//                #$arrMarker['###LINK_'.$count.'###'] = $this->getAboLink('Als Geschenk auswählen', 'javascript:setProduct('.$count.');', $this->conf['links.'], $this->colors[4], $this->colors[1], $this->colors[6]);
//                $arrMarker['###LINK_' . $count . '###'] = $this->getAboLink($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_linktext', 'abo_content'), 'javascript:setProduct(' . $count . ');document.landingpage.submit();', $this->conf['links.'], $this->colors[4], $this->colors[1], $this->colors[6]);
//                $count++;
//            }
//        }
//        # select template subpart
//        switch ($type) {
//            case 1:
//                $subpart = $this->cObj->getSubpart($this->template, '###LANDINGPAGE_1###');
//                break;
//            case 2:
//                $subpart = $this->cObj->getSubpart($this->template, '###LANDINGPAGE_2###');
//                break;
//            case 3:
//                $subpart = $this->cObj->getSubpart($this->template, '###LANDINGPAGE_3###');
//                break;
//            case 4:
//                $subpart = $this->cObj->getSubpart($this->template, '###LANDINGPAGE_4###');
//                break;
//            default:
//                $subpart = $this->cObj->getSubpart($this->template, '###LANDINGPAGE_4###');
//        }
//
//        # replace markers with marker array
//        $content .= $this->cObj->substituteMarkerArray($subpart, $arrMarker);
//
//        return $content;
  }

  function displayOrder($content, $conf) {

    #t3lib_div::debug($this->conf);
    #t3lib_div::debug($this->piVars);
    # reset session data
    $this->orderData = array();
    $this->orderData = $GLOBALS['TSFE']->fe_user->getKey('ses', 'orderData');

    if ($this->piVars['abo_offerform'] == '6') {
      $this->orderData['foreign_name'] = $this->piVars['foreign_name'];
      $this->orderData['foreign_price'] = $this->piVars['foreign_price'];
    }
    $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);
    #t3lib_div::debug($this->orderData);
    #t3lib_div::debug($GLOBALS['TSFE']->fe_user->getKey('ses','orderData'));

    $content = '';
    $strError = '<h2>ERROR: missing parameter!</h2>';

    # set marker array
    $arrMarker = array();

    # init product marker
    $arrMarker['###LINE_DISPLAY###'] = 'none';
    $arrMarker['###HEADLINE_CHOICE###'] = '';
    $arrMarker['###PRODUCT_IMAGE###'] = '';
    $arrMarker['###PRODUCT_HEADLINE###'] = '';
    $arrMarker['###PRODUCT_TEXT###'] = '';
    $arrMarker['###ORDER_PROD_NR###'] = '';
    $arrMarker['###ORDER_PROD_NAME###'] = '';
    $arrMarker['###ORDER_PROD_COPAYMENT###'] = '';
    $arrMarker['###ORDER_ABO_OFFERFORM###'] = '';
    $arrMarker['###ORDER_PRICE###'] = '';
    $arrMarker['###ORDER_SHIPPING###'] = '';
    $arrMarker['###RETURN_ABO_URL###'] = $this->piVars['return_url'];
    $arrMarker['###RETURN_URL###'] = $this->pi_linkTP_keepPIvars_url();
    $arrMarker['###STEP1_URL###'] = $this->pi_linkTP_keepPIvars_url();

    # set order process interface or email
    $arrMarker['###INTERFACE_ID###'] = $this->conf['interfaceId'];
    $lConf = array('parameter' => $this->conf['interfaceId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    if ($this->piVars['mag_type'] && $this->piVars['mag_type'] != '0') {
      $arrMarker['###INTERFACE_ID###'] = $this->conf['orderEmailId'];
      $lConf = array('parameter' => $this->conf['orderEmailId']);
      $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);
    }
    if ($this->piVars['abo_offerform'] == '6') {
      $arrMarker['###INTERFACE_ID###'] = $this->conf['orderEmailId'];
      $lConf = array('parameter' => $this->conf['orderEmailId']);
      $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);
    }
    if ($this->piVars['abo_offerform'] == 'singlezeit') {
      $arrMarker['###INTERFACE_ID###'] = $this->conf['orderEmailId'];
      $lConf = array('parameter' => $this->conf['orderEmailId']);
      $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);
    }

    # check piVars
    if ($this->piVars['cmd'] == 'order') {
      if (isset($this->piVars['abo_uid'])) {
        if ($this->piVars['abo_offerform'] == 'singlezeit') {
          # get abo data
          $resAbo = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_singlebooks', 'uid=' . intval($this->piVars['abo_uid']));
          if ($GLOBALS['TYPO3_DB']->sql_error())
            debug(array($GLOBALS['TYPO3_DB']->sql_error(), $query));
          $arrAboData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resAbo);
          #t3lib_div::debug($arrAboData);

          $this->orderData['product_name'] = $arrAboData['mag_name'] . ' Ausgabe ' . $this->piVars['ausgabe'];
          $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);

          $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAboData['mag_name'] . ' Einzelheft', $this->conf['header.']['small.'], '#000033', '#f0f0f0');
          ;

          $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText('<b>Ausgabe ' . $this->piVars['ausgabe'] . '</b>');
          $arrMarker['###ABO_TEXT###'] .= $this->pi_RTEcssText($this->addSpecialsChars($arrAboData['mag_txt']));

          # form fields
          $arrMarker['###ORDER_ABO_NR###'] = $arrAboData['mag_no'];
          $arrMarker['###ORDER_ABO_NR_STUDENT###'] = '';
          $arrMarker['###ORDER_ABO_OFFERFORM###'] = $this->piVars['abo_offerform'];
          $arrMarker['###INFO_TEXT###'] = '';
        }
        else {
          # get abo db values
          $resAbo = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_abonnements', 'uid=' . intval($this->piVars['abo_uid']));
          if ($GLOBALS['TYPO3_DB']->sql_error())
            debug(array($GLOBALS['TYPO3_DB']->sql_error(), $query));
          $arrAboData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resAbo);
          #t3lib_div::debug($arrAboData);

          $this->orderData['product_name'] = $arrAboData['abo_name'];
          $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);
          $aboName = ($arrAboData['abo_name2'] != '') ? $arrAboData['abo_name2'] : $arrAboData['abo_name'];
          $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($aboName, $this->conf['header.']['small.'], '#000033', '#f0f0f0');
          ;
          $arrMarker['###ABO_TEXT###'] = '';
          if ($this->piVars['foreign_name'] != '' && $this->piVars['foreign_price'] != '') {
            $arrMarker['###ABO_TEXT###'] .= $this->pi_RTEcssText('<b>' . $this->piVars['foreign_name'] . '<br>' . $this->piVars['foreign_price'] . '</b>');
          }
          $arrMarker['###ABO_TEXT###'] .= $this->pi_RTEcssText($this->addSpecialsChars($arrAboData['abo_txt_order']));

          # form fields
          $arrMarker['###ORDER_ABO_NR###'] = $arrAboData['abo_no'];
          $arrMarker['###ORDER_ABO_NR_STUDENT###'] = ($arrAboData['abo_offerform'] == '5') ? '' : $arrAboData['abo_no_student'];
          $arrMarker['###ORDER_ABO_OFFERFORM###'] = $arrAboData['abo_offerform'];
          $arrMarker['###INFO_TEXT###'] = '';
        }
      } else {
        if ($this->piVars['mag_order'] == 'true') {
          #t3lib_div::debug($this->piVars);
          #$resSB = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_singlebooks', 'mag_no='.$this->piVars['prod_no_1']);
          $resSB = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_singlebooks', 'uid=' . intval($this->piVars['prod_uid']));
          if ($GLOBALS['TYPO3_DB']->sql_error())
            debug(array($GLOBALS['TYPO3_DB']->sql_error(), $query));
          $arrSBData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resSB);
          #t3lib_div::debug($arrSBData);

          $strSBHeadline = ($arrSBData['mag_type'] == '2') ? 'Klassensatz: ' : 'Einzelheft: ';
          $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($strSBHeadline . $arrSBData['mag_name'], $this->conf['header.']['small.'], '#000033', '#f0f0f0');
          ;
          $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($this->addSpecialsChars($arrSBData['mag_txt']));
          # set name
          $this->orderData['product_name'] = 'Einzelheftbestellung ' . $arrSBData['mag_name'];
          $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);

          # form fields
          $arrMarker['###ORDER_ABO_NR###'] = '';
          $arrMarker['###ORDER_PROD_NR###'] = $arrSBData['mag_no'];
          $arrMarker['###ORDER_ABO_NR_STUDENT###'] = '';
          $arrMarker['###INFO_TEXT###'] = 'Einzelpreis &euro; ' . $arrSBData['mag_price'] . ' zzgl. &euro; ' . $arrSBData['mag_shipping'] . ' Versand pro Bestellung</b>';
          $arrMarker['###ORDER_PRICE###'] = $arrSBData['mag_price'];
          $arrMarker['###ORDER_SHIPPING###'] = $arrSBData['mag_shipping'];
        }
        else {
          $content = $strError;
        }
      }

      if ($this->piVars['prod_uid'] != '' && $this->piVars['mag_order'] != 'true') {
        # get product db values
        $resProd = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_zeitabo_products', 'uid=' . intval($this->piVars['prod_uid']));
        if ($GLOBALS['TYPO3_DB']->sql_error())
          debug(array($GLOBALS['TYPO3_DB']->sql_error(), $query));
        $arrProdData = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resProd);
        #t3lib_div::debug($arrProdData);

        $arrMarker['###LINE_DISPLAY###'] = 'block';
        $strHeadlineChoice = ($arrProdData['prod_type'] == '0') ? 'Ihr Geschenk' : 'Ihre Prämie';
        $arrMarker['###HEADLINE_CHOICE###'] = $this->getAboHeadline($strHeadlineChoice, $this->conf['header.']['small.'], '#000033', '#f0f0f0');
        $this->cObj->data['prod_images'] = $arrProdData['prod_images'];
        $this->pi_initPIFlexForm('prod_images');
        $arrMarker['###PRODUCT_IMAGE###'] = $this->getAboImage($this->pi_getFFvalue($this->cObj->data['prod_images'], 'img_1', 'zgrey'), $this->conf['images.']['product_order.'], '#f0f0f0');
        $arrMarker['###PRODUCT_HEADLINE###'] = $this->getAboHeadline($arrProdData['prod_name'], $this->conf['header.']['small.'], '#000033', '#f0f0f0');
        $arrMarker['###PRODUCT_TEXT###'] = $this->pi_RTEcssText($this->addSpecialsChars($arrProdData['prod_txt']));

        # form fields
        $arrMarker['###ORDER_PROD_NR###'] = $arrProdData['prod_no_1'];
        $arrMarker['###ORDER_PROD_NAME###'] = htmlspecialchars($arrProdData['prod_name']);
        $arrMarker['###ORDER_PROD_COPAYMENT###'] = $arrProdData['prod_copayment'];

        # praemien abo switch
        #t3lib_div::debug($arrAboData);
        switch ($arrProdData['prod_type']) {
          case '1':
            #$arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline('DIE ZEIT 1-Jahres-Prämien-Abo', $this->conf['header.']['small.'], '#000033', '#f0f0f0');
            $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAboData['abo_name2'] . ' 1-Jahres-Prämien-Abo', $this->conf['header.']['small.'], '#000033', '#f0f0f0');

            $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($this->addSpecialsChars($arrAboData['abo_txt_order']));
            $arrMarker['###ORDER_ABO_NR###'] = $arrAboData['abo_no'];
            break;
          case '2':
            #$arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline('DIE ZEIT 2-Jahres-Prämien-Abo', $this->conf['header.']['small.'], '#000033', '#f0f0f0');
            $arrMarker['###ABO_HEADLINE###'] = $this->getAboHeadline($arrAboData['abo_name2'] . ' 2-Jahres-Prämien-Abo', $this->conf['header.']['small.'], '#000033', '#f0f0f0');
            $arrMarker['###ABO_TEXT###'] = $this->pi_RTEcssText($this->addSpecialsChars($arrAboData['abo_txt_order2']));
            $arrMarker['###ORDER_ABO_NR###'] = $arrAboData['abo_no_student'];
            break;
        }
      }
    } else {
      $content = $strError;
    }

    $arrMarker['###ORDER_PRICEGROUP###'] = '0';
    if ($this->piVars['abo_offerform'] == '6') {
      $arrMarker['###ORDER_PRICEGROUP###'] = $this->piVars['pricegroup'];
    }
    $arrMarker['###ORDER_COUPON###'] = '0';

    #t3lib_div::debug($arrAboData);
    #t3lib_div::debug($arrProdData);
    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###SELECTION###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function displayInterface($content, $conf) {
    #t3lib_div::debug($this->piVars);
    #t3lib_div::debug(count(t3lib_div::_GET()));
    # set marker array
    $arrMarker = array();

    # end of order
    if (count(t3lib_div::_GET()) == 0 || $this->piVars['finish'] != '') {
      # type of result
      switch ($this->piVars['finish']) {
        case 'ok':
          $arrMarker['###HEADLINE###'] = $this->getAboHeadline('Vielen Dank für Ihre Bestellung', $this->conf['header.']['hero.'], $this->colors[4], $this->colors[2]);
          $arrMarker['###TEXT###'] = $this->pi_RTEcssText('<b>Ihr Auftrag ist bei unserem Leser-Service eingegangen und wird umgehend bearbeitet.</b>');
          $arrMarker['###LINK###'] = '';
          break;
        case 'error_order':
          $arrMarker['###HEADLINE###'] = $this->getAboHeadline('Bei Ihrer Bestellung ist ein Fehler aufgetreten!', $this->conf['header.']['hero.'], $this->colors[4], $this->colors[2]);
          $arrMarker['###TEXT###'] = $this->pi_RTEcssText('<b>Bitte versuchen Sie es erneut oder wenden Sie sich an unseren Leser-Service.<br>Telefon: 0180 - 52 52 909 (0,14 &euro;/Min. aus dem deutschen Festnetz; Mobilfunkpreise können abweichen.)</b>');
          $arrMarker['###LINK###'] = '';
          break;
        case 'error_404':
          $arrMarker['###HEADLINE###'] = $this->getAboHeadline('Bei Ihrer Bestellung ist ein Fehler aufgetreten!', $this->conf['header.']['hero.'], $this->colors[4], $this->colors[2]);
          $arrMarker['###TEXT###'] = $this->pi_RTEcssText('<b>Bitte versuchen Sie es erneut oder wenden Sie sich an unseren Leser-Service.<br>Telefon: 0180 - 52 52 909 (0,14 &euro;/Min. aus dem deutschen Festnetz; Mobilfunkpreise können abweichen.)</b>');
          $arrMarker['###LINK###'] = '';
          break;
        case 'error_500':
          $arrMarker['###HEADLINE###'] = $this->getAboHeadline('Bei Ihrer Bestellung ist ein Fehler aufgetreten!', $this->conf['header.']['hero.'], $this->colors[4], $this->colors[2]);
          $arrMarker['###TEXT###'] = $this->pi_RTEcssText('<b>Um sicher zu gehen, ob Ihre Bestellung korrekt &uuml;bermittelt wurde, wenden Sie sich bitte an unseren Leser-Service.<br>Telefon: 0180 - 52 52 909 (0,14 &euro;/Min. aus dem deutschen Festnetz; Mobilfunkpreise können abweichen.)<br>oder per Mail an <a href="mailto:abo@zeit.de">abo@zeit.de</a></b>');
          $arrMarker['###LINK###'] = '';
          break;
        default:
          $arrMarker['###HEADLINE###'] = $this->getAboHeadline('Unbekannter Fehler!', $this->conf['header.']['hero.'], $this->colors[4], $this->colors[2]);
          $arrMarker['###TEXT###'] = $this->pi_RTEcssText('<b>Bitte &uuml;berpr&uuml;fen Sie Ihre Eingaben.</b>');
          $arrMarker['###LINK###'] = '';
          break;
      }

      # select template subpart
      $subpart = $this->cObj->getSubpart($this->template, '###RESULT###');

      # replace markers with marker values
      $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);
    } else {
      # fill order params
      $strOrderParams = '';
      $strOrderParams .= '?abo_nr=' . urlencode($this->piVars['abo_nr']);
      $strOrderParams .= '&abo_nr_student=' . urlencode($this->piVars['abo_nr_student']);
      $strOrderParams .= '&prod_nr=' . urlencode($this->piVars['prod_nr']);
      $strOrderParams .= '&prod_name=' . urlencode(utf8_decode($this->piVars['prod_name']));
      $strOrderParams .= '&prod_copayment=' . urlencode($this->piVars['prod_copayment']);
      $strOrderParams .= '&pricegroup=' . urlencode($this->piVars['pricegroup']);
      $strOrderParams .= '&coupon=' . urlencode($this->piVars['coupon']);
      $strOrderParams .= '&referer_url=' . urlencode(t3lib_div::getIndpEnv('HTTP_REFERER'));

      $lConf = array('parameter' => $this->conf['returnUrl']);
      $strOrderParams .= '&redirect_url=' . urlencode($this->cObj->typoLink_URL($lConf));

      header('Location: ' . t3lib_div::locationHeaderUrl($this->conf['interfaceUrl'] . $strOrderParams));
      exit;

      #$content = '<iframe src="'.$this->conf['interfaceUrl'].$strOrderParams.'" width="784" height="498" scrolling="no" marginheight="0" marginwidth="0" frameborder="0"></iframe>';
    }
    return $content;
  }

  function displayEMail($content, $conf) {
    # set session data

    foreach ($this->piVars as $key => $value) {
      $this->piVars[$key] = htmlspecialchars($value);
    }

    $this->orderData = array_merge($GLOBALS['TSFE']->fe_user->getKey('ses', 'orderData'), $this->piVars);

    #t3lib_div::debug($this->piVars);
    #t3lib_div::debug($this->orderData);

    if ($this->piVars['sendmail'] == 'true') {
      return $this->sendOrderMail($content, $conf);
    } else {
      switch ($this->orderData['mode']) {
        case 'init':
          return $this->renderOrder2($content, $conf, false);
          break;
        case 'back2':
          return $this->renderOrder2($content, $conf, false);
          break;
        case 'step2':
          return $this->renderOrder2($content, $conf, true);
          break;
        case 'back3':
          return $this->renderOrder3($content, $conf, false);
          break;
        case 'step3':
          return $this->renderOrder3($content, $conf, false);
          break;
        case 'step4':
          return $this->renderOrder3($content, $conf, true);
          break;
      }
    }
  }

  function renderOrder2($content, $conf, $check) {

    $this->orderData['mode'] = 'step2';
    $this->orderData['step2_url'] = tslib_pibase::pi_getPageLink($this->conf['orderEmailId']);

    # set marker array
    $arrMarker = array();
    $lConf = array('parameter' => $this->conf['orderEmailId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);
    $arrMarker['###HIDDEN_FIELDS###'] = '';

    # error markers
    $arrMarker['###MSG_ERROR###'] = 'none';
    $arrMarker['###ANREDE_ERROR###'] = '';
    $arrMarker['###NAME_ERROR###'] = '';
    $arrMarker['###NAME_ERROR###'] = '';
    $arrMarker['###STRASSENR_ERROR###'] = '';
    $arrMarker['###PLZORT_ERROR###'] = '';
    $arrMarker['###PLZORT_ERROR###'] = '';
    $arrMarker['###EMAIL_ERROR###'] = '';
    $arrMarker['###DISPLAY_AUSLAND###'] = ($this->orderData['abo_offerform'] == '6') ? 'none' : 'block;';
    $arrMarker['###ANZAHL_DISPLAY###'] = ($this->orderData['abo_offerform'] == '6') ? 'none' : 'block';
    $arrMarker['###STUDENT_DISPLAY###'] = (strpos($this->orderData['foreign_name'], 'STUDENT') > 0) ? 'block' : 'none';

    if ($check) {
      $formError = false;

      if ($this->piVars['anrede'] == '') {
        $arrMarker['###ANREDE_ERROR###'] = 'error';
        $formError = true;
      }
      if ($this->piVars['vorname'] == '') {
        $arrMarker['###NAME_ERROR###'] = 'error';
        $formError = true;
      }
      if ($this->piVars['nachname'] == '') {
        $arrMarker['###NAME_ERROR###'] = 'error';
        $formError = true;
      }
      if ($this->piVars['strassenr'] == '') {
        $arrMarker['###STRASSENR_ERROR###'] = 'error';
        $formError = true;
      }
      if ($this->piVars['plz'] == '') {
        $arrMarker['###PLZORT_ERROR###'] = 'error';
        $formError = true;
      }
      if ($this->piVars['ort'] == '') {
        $arrMarker['###PLZORT_ERROR###'] = 'error';
        $formError = true;
      }
      if (!$this->checkEmail($this->piVars['email'])) {
        $arrMarker['###EMAIL_ERROR###'] = 'error';
        $formError = true;
      }
      if (!$formError) {
        return $this->renderOrder3($content, $conf, false);
      }
    }

    # fill markers
    $arrMarker['###ANREDE_1###'] = ($this->orderData['anrede'] == 'Herr') ? 'checked' : '';
    $arrMarker['###ANREDE_2###'] = ($this->orderData['anrede'] == 'Frau') ? 'checked' : '';
    $arrMarker['###FIRMA###'] = $this->orderData['firma'];
    $arrMarker['###VORNAME###'] = $this->orderData['vorname'];
    $arrMarker['###NACHNAME###'] = $this->orderData['nachname'];
    $arrMarker['###STRASSENR###'] = $this->orderData['strassenr'];
    $arrMarker['###ADRESSZUSATZ###'] = $this->orderData['adresszusatz'];
    $arrMarker['###PLZ###'] = $this->orderData['plz'];
    $arrMarker['###ORT###'] = $this->orderData['ort'];
    $arrMarker['###LAND_1###'] = $this->orderData['land'];
    $arrMarker['###TELEFON###'] = $this->orderData['telefon'];
    $arrMarker['###EMAIL###'] = $this->orderData['email'];
    $arrMarker['###INFO###'] = $this->orderData['info'];
    $arrMarker['###ANZAHL###'] = ($this->orderData['anzahl'] == '') ? '1' : $this->orderData['anzahl'];
    $arrMarker['###INFO_CHECKED###'] = ($this->orderData['info'] == 'info') ? 'checked' : '';
    $arrMarker['###STUDENT_CHECKED###'] = (strpos($this->orderData['foreign_name'], 'STUDENT') > 0) ? 'checked' : '';

    $arrMarker['###RETURN_URL###'] = $this->orderData['step1_url'];
    $arrMarker['###STEP2_URL###'] = $this->orderData['step2_url'];

    # set session array
    $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);
    #t3lib_div::debug($GLOBALS['TSFE']->fe_user->getKey('ses','orderData'));
    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###ORDER_EMAIL###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function renderOrder3($content, $conf, $check) {

    $this->orderData['mode'] = 'step4';
    $this->orderData['step3_url'] = tslib_pibase::pi_getPageLink($this->conf['orderEmailId']);

    # set marker array
    $arrMarker = array();

    $lConf = array('parameter' => $this->conf['orderEmailId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    $arrMarker['###HIDDEN_FIELDS###'] = '';

    # error markers
    $arrMarker['###MSG_ERROR###'] = 'none';
    $arrMarker['###SELECT_ERROR###'] = '';
    $arrMarker['###KONTO_ERROR###'] = '';
    $arrMarker['###INHABER_ERROR###'] = '';
    $arrMarker['###BLZ_ERROR###'] = '';
    $arrMarker['###INSTITUT_ERROR###'] = '';
    $arrMarker['###PLZORT_ERROR###'] = '';
    $arrMarker['###EMAIL_ERROR###'] = '';

    if ($check) {
      $formError = false;

      if ($this->piVars['zahlung'] == '') {
        $arrMarker['###SELECT_ERROR###'] = 'error';
        $formError = true;
      }
      if ($this->piVars['zahlung'] == 'Bankeinzug') {
        if ($this->piVars['konto'] == '') {
          $arrMarker['###KONTO_ERROR###'] = 'error';
          $formError = true;
        }
        if ($this->piVars['inhaber'] == '') {
          $arrMarker['###INHABER_ERROR###'] = 'error';
          $formError = true;
        }
        if ($this->piVars['blz'] == '') {
          $arrMarker['###BLZ_ERROR###'] = 'error';
          $formError = true;
        }
        if ($this->piVars['institut'] == '') {
          $arrMarker['###INSTITUT_ERROR###'] = 'error';
          $formError = true;
        }
      }
      if (!$formError) {
        return $this->renderOrder4($content, $conf);
      }
    } else {
      # set checkboxes
      $this->orderData['info'] = ($this->piVars['info'] == 'info') ? $this->piVars['info'] : '';
      $this->orderData['student'] = ($this->piVars['student'] == 'student') ? $this->piVars['student'] : '';
    }

    # fill markers
    $arrMarker['###CHECKED_1###'] = ($this->orderData['zahlung'] == 'Bankeinzug') ? 'checked' : '';
    $arrMarker['###CHECKED_2###'] = ($this->orderData['zahlung'] == 'Rechnung') ? 'checked' : '';
    $arrMarker['###BANKEINZUG_DISPLAY###'] = 'block';
    if ($this->orderData['abo_offerform'] == '6') {
      $arrMarker['###BANKEINZUG_DISPLAY###'] = 'none';
      $arrMarker['###CHECKED_2###'] = 'checked';
    }
    $arrMarker['###KONTO###'] = ($this->orderData['zahlung'] == 'Rechnung') ? '' : $this->orderData['konto'];
    $arrMarker['###INHABER###'] = ($this->orderData['zahlung'] == 'Rechnung') ? '' : $this->orderData['inhaber'];
    $arrMarker['###BLZ###'] = ($this->orderData['zahlung'] == 'Rechnung') ? '' : $this->orderData['blz'];
    $arrMarker['###INSTITUT###'] = ($this->orderData['zahlung'] == 'Rechnung') ? '' : $this->orderData['institut'];
    $arrMarker['###RETURN_URL###'] = $this->orderData['step2_url'] . '?tx_zeitabo_pi1[mode]=back2';

    # set session array
    $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);
    #t3lib_div::debug($GLOBALS['TSFE']->fe_user->getKey('ses','orderData'));
    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###ORDER_EMAIL_2###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function renderOrder4($content, $conf) {
    $this->orderData['mode'] = 'step4';
    $this->orderData['step4_url'] = tslib_pibase::pi_getPageLink($this->conf['orderEmailId']);

    # set marker array
    $arrMarker = array();

    $lConf = array('parameter' => $this->conf['orderEmailId']);
    $arrMarker['###ACTION###'] = $this->cObj->typoLink_URL($lConf);

    $arrMarker['###HIDDEN_FIELDS###'] = '';

    $arrMarker['###PRODUKT###'] = '';
    $arrMarker['###PRODUKT###'] .= $this->orderData['product_name'] . '<br><br>';
    if ($this->orderData['abo_offerform'] == '6') {
      $arrMarker['###PRODUKT###'] .= $this->orderData['foreign_name'] . '<br>';
      $arrMarker['###PRODUKT###'] .= $this->orderData['foreign_price'] . '<br><br>';
    } else {
      $arrMarker['###PRODUKT###'] .= '<b>Anzahl:</b><br>' . $this->orderData['anzahl'] . ' St&uuml;ck<br><br>';
    }
    if ($this->orderData['student'] == 'student') {
      #$arrMarker['###PRODUKT###'] .= 'Student<br><br>';
    }
    $arrMarker['###RECHNUNG###'] = '';
    $arrMarker['###RECHNUNG###'] .= $this->orderData['anrede'] . '<br>';
    $arrMarker['###RECHNUNG###'] .= $this->orderData['vorname'] . ' ' . $this->orderData['nachname'] . '<br>';
    $arrMarker['###RECHNUNG###'] .= $this->orderData['firma'] . '<br>';
    $arrMarker['###RECHNUNG###'] .= $this->orderData['strassenr'] . '<br>';
    $arrMarker['###RECHNUNG###'] .= $this->orderData['adresszusatz'] . '<br>';
    $arrMarker['###RECHNUNG###'] .= $this->orderData['plz'] . ' ' . $this->orderData['ort'] . '<br>';

    $arrMarker['###ZAHLUNG###'] = '';
    $arrMarker['###ZAHLUNG###'] .= 'per ' . $this->orderData['zahlung'] . '<br><br>';
    if ($this->orderData['zahlung'] == 'Bankeinzug') {
      $arrMarker['###ZAHLUNG###'] .= '<b>Kontonummer</b>:<br>' . $this->orderData['konto'] . '<br>';
      $arrMarker['###ZAHLUNG###'] .= '<b>Kontoinhaber</b>:<br>' . $this->orderData['inhaber'] . '<br>';
      $arrMarker['###ZAHLUNG###'] .= '<b>Bankleitzahl</b>:<br>' . $this->orderData['blz'] . '<br>';
      $arrMarker['###ZAHLUNG###'] .= '<b>Geldinstitut</b>:<br>' . $this->orderData['institut'] . '<br>';
    }

    $arrMarker['###RETURN_URL###'] = $this->orderData['step3_url'] . '?tx_zeitabo_pi1[mode]=back3';
    $arrMarker['###STEP1_URL###'] = $this->orderData['step2_url'] . '?tx_zeitabo_pi1[mode]=back2';
    $arrMarker['###STEP2_URL###'] = $this->orderData['step2_url'] . '?tx_zeitabo_pi1[mode]=back2';
    $arrMarker['###STEP3_URL###'] = $this->orderData['step2_url'] . '?tx_zeitabo_pi1[mode]=back3';
    if ($this->orderData['abo_offerform'] == '6' || $this->orderData['abo_offerform'] == 'singlezeit') {
      $arrMarker['###STEP1_URL###'] = $this->orderData['step1_url'];
    }
    # set session array
    $GLOBALS['TSFE']->fe_user->setKey('ses', 'orderData', $this->orderData);
    #t3lib_div::debug($GLOBALS['TSFE']->fe_user->getKey('ses','orderData'));
    # select template subpart
    $subpart = $this->cObj->getSubpart($this->template, '###ORDER_EMAIL_3###');

    # replace markers with marker values
    $content = $this->cObj->substituteMarkerArray($subpart, $arrMarker);

    return $content;
  }

  function sendOrderMail($content, $conf) {
    $mailData = array();
    foreach ($this->orderData as $key => $value) {
      $mailData[$key] = utf8_decode($value);
    }
    #t3lib_div::debug($mailData);

    $orderMail = t3lib_div::makeInstance(t3lib_htmlmail);

    $orderMail->add_header('From: DIE ZEIT-Leser-Service <abo@zeit.de>');

    $orderMail->recipient = $mailData['email'];
    $orderMail->recipient_copy = 'abo@zeit.de';
    #$orderMail->recipient_copy = 'patrick.bisplinghoff.net';

    $orderMail->subject = 'DIE ZEIT - Leserservice: ' . $mailData['product_name'];

    $orderMail->add_message('Sehr geehrte(r) ' . $mailData['anrede'] . ' ' . $mailData['nachname'] . ',');
    $orderMail->add_message('');
    $orderMail->add_message('vielen Dank für Ihre Bestellung und das uns entgegengebrachte Vertrauen. Ihr Auftrag ist bei unserem Kunden-Service eingegangen und wird nach Ihren Wünschen bearbeitet.');
    $orderMail->add_message('');
    $orderMail->add_message('Ihre Daten wurden von uns wie folgt aufgenommen:');
    $orderMail->add_message('');
    $orderMail->add_message('Art: ' . $mailData['product_name']);
    if ($this->orderData['abo_offerform'] == '6') {
      $orderMail->add_message('Bestellnummer: ' . $mailData['abo_nr']);
      $orderMail->add_message('Abo: ' . $mailData['foreign_name']);
      $orderMail->add_message('Preis: ' . $mailData['foreign_price']);
      if ($mailData['student'] == 'student') {
        $orderMail->add_message('Hinweis STUDENT akzeptiert.');
      }
    } elseif ($this->orderData['abo_offerform'] == 'singlezeit') {
      $orderMail->add_message('Anzahl: ' . $mailData['anzahl']);
    } else {
      $orderMail->add_message('Bestellnummer: ' . $mailData['prod_nr']);
      $orderMail->add_message('Anzahl: ' . $mailData['anzahl']);
    }
    if ($this->orderData['prod_price'] != '') {
      $orderMail->add_message('Einzelpreis: ' . $mailData['prod_price']);
      $orderMail->add_message('Versandkosten: ' . $mailData['prod_shipping']);
    }
    $orderMail->add_message('');

    $orderMail->add_message('');
    $orderMail->add_message('LIEFERANSCHRIFT');
    $orderMail->add_message('');
    $orderMail->add_message('Anrede: ' . $mailData['anrede']);
    $orderMail->add_message('Vorname: ' . $mailData['vorname']);
    $orderMail->add_message('Nachname: ' . $mailData['nachname']);
    $orderMail->add_message('Firma: ' . $mailData['firma']);
    $orderMail->add_message('Adresse: ' . $mailData['strassenr']);
    $orderMail->add_message('Adresszusatz: ' . $mailData['adresszusatz']);
    $orderMail->add_message('PLZ: ' . $mailData['plz']);
    $orderMail->add_message('Ort: ' . $mailData['ort']);
    $orderMail->add_message('Land: ' . $mailData['land']);
    $orderMail->add_message('Telefon: ' . $mailData['telefon']);
    $orderMail->add_message('E-Mail: ' . $mailData['email']);
    if ($this->orderData['info'] == 'info') {
      $orderMail->add_message('');
      $orderMail->add_message('Weitere Infos: erwünscht');
    }
    $orderMail->add_message('');
    $orderMail->add_message('ZAHLUNGSWUNSCH');
    $orderMail->add_message('');
    $orderMail->add_message('per ' . $mailData['zahlung']);
    if ($this->orderData['zahlung'] == 'Bankeinzug') {
      $orderMail->add_message('Kontonummer: ' . $mailData['konto']);
      $orderMail->add_message('Kontoinhaber: ' . $mailData['inhaber']);
      $orderMail->add_message('BLZ: ' . $mailData['blz']);
      $orderMail->add_message('Geldinstitut: ' . $mailData['institut']);
    }
    $orderMail->add_message('');
    $orderMail->add_message('Sollten Sie weitere Fragen zu Ihrer Bestellung haben, steht Ihnen unser Leser-Service jederzeit zur Verfügung.');
    $orderMail->add_message('DIE ZEIT, Leser-Service, 20080 Hamburg.');
    $orderMail->add_message('Fax: 0180 - 52 52 908*');
    $orderMail->add_message('Tel: 0180 - 52 52 909*');
    $orderMail->add_message('Email: abo@zeit.de');
    $orderMail->add_message('');
    $orderMail->add_message('* 14 Cent/Min. aus dem deutschen Festnetz');
    $orderMail->add_message('');
    $orderMail->add_message('Genießen Sie DIE ZEIT');
    $orderMail->add_message('Ihr Leserservice');
    $orderMail->add_message('');
    $orderMail->add_message('');

    #t3lib_div::debug($this->orderData);
    #t3lib_div::debug($orderMail);

    if ($orderMail->sendTheMail()) {
      $link = tslib_pibase::pi_getPageLink($this->conf['thanxEmailPageId'], '', array('tx_zeitabo_pi1[finish]' => 'ok'));
    } else {
      $link = tslib_pibase::pi_getPageLink($this->conf['errorEmailPageId'], '', array('tx_zeitabo_pi1[finish]' => 'error_order'));
    }
    #t3lib_div::debug($link);
    return '<script>self.location.href="/' . $link . '";</script>';
  }

  /**
   * Returns a string with multiple hidden fields
   * 
   * @param    array        $abo:    the abo-data array.
   * @param    string    $product:    either 'all_products' or 'special_products'.
   * @return string    $hiddenFields: HTML-inputfields, type "hidden". 
   */
  function getHiddenFields($abo, $product = '') {
    $hiddenFields = '';

    # retur url
    $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[return_url]" value="' . $this->pi_linkTP_keepPIvars_url() . '">' . chr(10);

    # abo data
    if ($product != 'singlebooks') {
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[abo_offerform]" value="' . $abo['abo_offerform'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[abo_uid]" value="' . $abo['uid'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[abo_no]" value="' . $abo['abo_no'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[abo_no_student]" value="' . $abo['abo_no_student'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[abo_allowstudents]" value="' . $abo['abo_allowstudents'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[abo_bankcollection]" value="' . $abo['abo_bankcollection'] . '">' . chr(10);
    }
    # product data
    if ($product && $product != 'singlebooks') {
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_uid]" value="' . $abo[$product][0]['uid'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_no_1]" value="' . $abo[$product][0]['prod_no_1'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_no_2]" value="' . $abo[$product][0]['prod_no_2'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_no_3]" value="' . $abo[$product][0]['prod_no_3'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_copayment]" value="' . $abo[$product][0]['prod_copayment'] . '">' . chr(10);
    }
    # singlebook data
    if ($product == 'singlebooks') {
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[mag_order]" value="true">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[mag_type]" value="' . $abo[0]['mag_type'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_uid]" value="' . $abo[0]['uid'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_no_1]" value="' . $abo[0]['mag_no'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_no_2]" value="">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_no_3]" value="">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_copayment]" value="">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_price]" value="' . $abo[0]['mag_price'] . '">' . chr(10);
      $hiddenFields .= '<input type="hidden" name="' . $this->prefixId . '[prod_shipping]" value="' . $abo[0]['mag_shipping'] . '">' . chr(10);
    }
    #t3lib_div::debug($hiddenFields);
    return $hiddenFields;
  }

  /**
   * Returns GIFBUILDER-Images from the the given text and colors.
   * see plugin.tx_zeitabo_pi1. in pi1/static/setup.txt for basic $conf
   *
   * @param    array        $conf:    typoscript configuration.
   * @param    string    $fontColor:    TEXT fontColor property.
   * @param    string    $backColor:    GIFBUILDER backColor property.
   * @param    string    $icon:    FILE link icon switch.
   *
   * @return string    the image tag created by the cObj IMAGE method.
   */
  function getAboHeadline($strText, $conf, $fontColor, $backColor) {
    $conf['altText'] = $strText;
    $conf['file.']['10.']['text'] = $strText;
    $conf['file.']['10.']['fontColor'] = $fontColor;
    $conf['file.']['backColor'] = $backColor;
    return $this->cObj->IMAGE($conf);
  }

  function getExtension($filename) {
    // Find the last dot
    $pos = strrpos($filename, '.');

    // Check if the needle has been found in the haystack
    if ($pos === false)
      return NULL;

    // Cut off and return the characters to the right of the dot
    return substr($filename, $pos + 1);
  }

  function getAllowedFormat($extension, $default) {
    // Convert extension to lowercase
    $extension = strtolower($extension);

    // Define the allowed formats for the GIFBUILDER
    $allowedFormats = array('gif', 'jpg', 'png');

    // Check if the extension matches an allowed format
    if (in_array($extension, $allowedFormats))
      return $extension;

    // Extension does not match an allowed format, so let's look if a default has been set otherwise return 'gif'
    return isset($default) ? $default : 'gif';
  }

  function getAboImage($strImage, $conf, $backColor, $link = false) {
    // Define the output format of the GIFBUILDER based on the file extension of the input file
    $conf['file.']['format'] = $this->getAllowedFormat(
                    $this->getExtension($strImage),
                    $conf['file.']['format']
    );

    if ($link) {
      $conf['stdWrap.']['typolink.']['parameter'] = $link;
    }
    $conf['file.']['backColor'] = $backColor;
    $conf['file.']['10.']['file'] = 'uploads/tx_zeitabo/' . $strImage;
    return $this->cObj->IMAGE($conf['file.']['10.']);
  }

  function getAboLink($strText, $strLink, $conf, $fontColor, $backColor, $icon = 'black', $linkParams = false, $tagParams = false) {
    $conf['file.']['backColor'] = $backColor;
    $conf['stdWrap.']['typolink.']['parameter'] = $strLink;
    $conf['stdWrap.']['typolink.']['extTarget'] = '_self';
    if ($linkParams) {
      $conf['stdWrap.']['typolink.']['additionalParams'] = $linkParams;
    }
    if ($tagParams) {
      $conf['stdWrap.']['typolink.']['extTarget'] = '_self';
      $conf['stdWrap.']['typolink.']['ATagParams'] = $tagParams;
    }
    switch ($icon) {
      case 'blue':
        $conf['file.']['10.']['file'] = 'typo3conf/ext/zeitabo/res/link_ico.gif';
        break;
      case 'black':
        $conf['file.']['10.']['file'] = 'typo3conf/ext/zeitabo/res/link_ico_2.gif';
        break;
      case 'white':
        $conf['file.']['10.']['file'] = 'typo3conf/ext/zeitabo/res/link_ico_3.gif';
        break;
    }
    $conf['file.']['20.']['text'] = $strText;
    $conf['file.']['20.']['fontColor'] = $fontColor;
    return $this->cObj->IMAGE($conf);
  }

  function getProductImageSmall($strImage, $conf, $backColor, $params = false, $strText = '') {
    // Define the output format of the GIFBUILDER based on the file extension of the input file
    $conf['file.']['format'] = $this->getAllowedFormat(
                    $this->getExtension($strImage),
                    $conf['file.']['format']
    );

    $conf['altText'] = $strText;
    $conf['file.']['backColor'] = $backColor;
    if ($params) {
      $conf['params'] = $params;
    }
    $conf['file.']['10.']['file'] = 'uploads/tx_zeitabo/' . $strImage;
    return $this->cObj->IMAGE($conf);
  }

  function getProductImageSmallText($strImage, $conf, $backColor, $params = false, $text = '', $strText = '') {
    $conf['altText'] = $strText;
    $conf['file.']['backColor'] = $backColor;
    if ($params) {
      $conf['params'] = $params;
    }
    $conf['file.']['9.']['text'] = $text;
    $conf['file.']['10.']['file'] = 'uploads/tx_zeitabo/' . $strImage;
    return $this->cObj->IMAGE($conf);
  }

  function checkEmail($email) {
    // RegEx begin
    $nonascii = "\x80-\xff"; # Non-ASCII-Chars are not allowed

    $nqtext = "[^\\\\$nonascii\015\012\"]";
    $qchar = "\\\\[^$nonascii]";

    $protocol = '(?:mailto:)';

    $normuser = '[a-zA-Z0-9][a-zA-Z0-9_.-]*';
    $quotedstring = "\"(?:$nqtext|$qchar)+\"";
    $user_part = "(?:$normuser|$quotedstring)";

    $dom_mainpart = '[a-zA-Z0-9][a-zA-Z0-9._-]*\\.';
    $dom_subpart = '(?:[a-zA-Z0-9][a-zA-Z0-9._-]*\\.)*';
    $dom_tldpart = '[a-zA-Z]{2,5}';
    $domain_part = "$dom_subpart$dom_mainpart$dom_tldpart";

    $regex = "$protocol?$user_part\@$domain_part";
    // RegEx end

    return preg_match("/^$regex$/", $email);
  }

  /**
   * Returns a string with special Chars like nonbreaking-space
   *
   * @param    string    $txtContent:    like prod_txt.
   * @return string    $prodContent with nonbreaking-spaces, €, etc.
   */
  function addSpecialsChars($txtContent) {
    $prodContent = preg_replace('/DIE ZEIT/', 'DIE&nbsp;ZEIT', $txtContent);
    $prodContent = preg_replace('/EUR /', '€', $prodContent);
    $prodContent = preg_replace('/Euro /', '€', $prodContent);

    return $prodContent;
  }

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabo/pi1/class.tx_zeitabo_pi1.php']) {
  include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabo/pi1/class.tx_zeitabo_pi1.php']);
}
?>
