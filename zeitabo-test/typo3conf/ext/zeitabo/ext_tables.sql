CREATE TABLE tx_zeitabo_blz (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l10n_parent int(11) DEFAULT '0' NOT NULL,
    l10n_diffsource mediumtext,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    starttime int(11) DEFAULT '0' NOT NULL,
    endtime int(11) DEFAULT '0' NOT NULL,
    blz varchar(10) DEFAULT '' NOT NULL,
    merkmal varchar(10) DEFAULT '' NOT NULL,
    bezeichnung varchar(255) DEFAULT '' NOT NULL,
    plz varchar(10) DEFAULT '' NOT NULL,
    ort varchar(100) DEFAULT '' NOT NULL,
    kurzbezeichnung varchar(100) DEFAULT '' NOT NULL,
    pan varchar(50) DEFAULT '' NOT NULL,
    bic varchar(50) DEFAULT '' NOT NULL,
    methode varchar(10) DEFAULT '' NOT NULL,
    datensatznr tinytext,
    aenderungskennzeichen varchar(100) DEFAULT '' NOT NULL,
    blzloeschung varchar(10) DEFAULT '' NOT NULL,
    nachfolgeblz varchar(10) DEFAULT '' NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY blz_merkmal (blz,merkmal)
);

#
# Table structure for table 'tx_zeitabo_abonnements_abo_products_mm'
# 
#
CREATE TABLE tx_zeitabo_abonnements_abo_products_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);



#
# Table structure for table 'tx_zeitabo_abonnements'
#
CREATE TABLE tx_zeitabo_abonnements (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l18n_parent int(11) DEFAULT '0' NOT NULL,
	l18n_diffsource mediumblob NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group int(11) DEFAULT '0' NOT NULL,
	abo_name varchar(255) DEFAULT '' NOT NULL,
	abo_name2 varchar(255) DEFAULT '' NOT NULL,
	abo_no varchar(255) DEFAULT '' NOT NULL,
	abo_no_student varchar(255) DEFAULT '' NOT NULL,
	abo_no_kombi_digital varchar(255) DEFAULT '' NOT NULL,
	abo_offerform int(11) DEFAULT '0' NOT NULL,
	abo_shipping int(11) DEFAULT '0' NOT NULL,
  abo_price decimal(19,2) DEFAULT '0.00' NOT NULL,
  abo_price_student decimal(19,2) DEFAULT '0.00' NOT NULL,
	abo_images mediumtext NOT NULL,
	abo_txt_order text NOT NULL,
	abo_txt_order2 text NOT NULL,
	abo_txt_delivery text NOT NULL,
	abo_txt_delivery2 text NOT NULL,
	abo_txt_student_order text NOT NULL,
	abo_txt_student_order2 text NOT NULL,
	abo_txt_student_delivery text NOT NULL,
	abo_txt_student_delivery2 text NOT NULL,
  abo_txt_benefits text NOT NULL,
	abo_products int(11) DEFAULT '0' NOT NULL,
  abo_user_form_image text,
  abo_adwords  text,

	
	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid)
);



#
# Table structure for table 'tx_zeitabo_abonnements_adwords'
#
CREATE TABLE tx_zeitabo_abonnements_adwords (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    t3ver_oid int(11) DEFAULT '0' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_wsid int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(30) DEFAULT '' NOT NULL,
    t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
    t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
    t3ver_count int(11) DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
    t3_origuid int(11) DEFAULT '0' NOT NULL,
    sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l18n_parent int(11) DEFAULT '0' NOT NULL,
    l18n_diffsource mediumblob NOT NULL,
    sorting int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    starttime int(11) DEFAULT '0' NOT NULL,
    endtime int(11) DEFAULT '0' NOT NULL,
    fe_group int(11) DEFAULT '0' NOT NULL,
    adword tinytext,
    abo_no varchar(255) DEFAULT '' NOT NULL,
	  abo_no_student varchar(255) DEFAULT '' NOT NULL,
	  abo_no_kombi_digital varchar(255) DEFAULT '' NOT NULL,
  	parent_id tinytext,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY t3ver_oid (t3ver_oid,t3ver_wsid)
) ENGINE=InnoDB;

#
# Table structure for table 'tx_zeitabo_products_prod_abos_mm'
# 
#
CREATE TABLE tx_zeitabo_products_prod_abos_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);



#
# Table structure for table 'tx_zeitabo_products'
#
CREATE TABLE tx_zeitabo_products (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l18n_parent int(11) DEFAULT '0' NOT NULL,
	l18n_diffsource mediumblob NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	fe_group int(11) DEFAULT '0' NOT NULL,
	prod_type int(11) DEFAULT '0' NOT NULL,
	prod_no_1 varchar(255) DEFAULT '' NOT NULL,
	prod_no_2 varchar(255) DEFAULT '' NOT NULL,
	prod_no_3 varchar(255) DEFAULT '' NOT NULL,
	prod_name tinytext NOT NULL,
        product_image text,
        product_image_detail text,
        product_image_thankyou text,
  product_image_mobile text,
        product_group text,
	prod_images mediumtext NOT NULL,
	prod_hl varchar(255) DEFAULT '' NOT NULL,
	prod_txt text NOT NULL,
  prod_txt_mobile text NOT NULL,
	prod_count int(11) DEFAULT '0' NOT NULL,
	prod_copayment varchar(20) DEFAULT '' NOT NULL,
	prod_abos int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid)
);


#
# Table structure for table 'tx_zeitabo_product_groups'
#
CREATE TABLE tx_zeitabo_product_groups (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    name tinytext,

    PRIMARY KEY (uid),
    KEY parent (pid)
);


#
# Table structure for table 'tx_zeitabo_singlebooks'
#
CREATE TABLE tx_zeitabo_singlebooks (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l18n_parent int(11) DEFAULT '0' NOT NULL,
	l18n_diffsource mediumblob NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,
	fe_group int(11) DEFAULT '0' NOT NULL,
  product_image text,
  product_image_detail text,
  product_image_thankyou text,
	mag_type int(11) DEFAULT '0' NOT NULL,
	mag_no varchar(255) DEFAULT '' NOT NULL,
	mag_name varchar(255) DEFAULT '' NOT NULL,
	mag_release varchar(20) DEFAULT '' NOT NULL,
	mag_images mediumtext NOT NULL,
	mag_txt text NOT NULL,
	mag_price varchar(20) DEFAULT '' NOT NULL,
	mag_shipping varchar(20) DEFAULT '' NOT NULL,
        abo_txt_order text NOT NULL,
        abo_txt_delivery text NOT NULL,
	
	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid)
);
