<?php

$feUserObj = tslib_eidtools::initFeUser(); // Initialize FE user object
tslib_eidtools::connectDB(); //Connect to database

if (preg_match('/\d+/', t3lib_div::_GP('blz'), $matches)) {
  $where = $matches[0];
}
$where = "blz='$where' AND merkmal=1";
if(strip_tags(intval(t3lib_div::_GP('iban'))) == 1){
    $where .= " AND BIC IS NOT NULL ";
}


$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
                'kurzbezeichnung, bic', // SELECT
                'tx_zeitabo_blz', // FROM
                $where, // WHERE
                'blz' // GROUP BY
);
$result = array();
while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
    $result['institut'] = $row['kurzbezeichnung'];
    $result['bic'] = $row['bic'];
}
$GLOBALS['TYPO3_DB']->sql_free_result($res);

print json_encode($result);

?>
