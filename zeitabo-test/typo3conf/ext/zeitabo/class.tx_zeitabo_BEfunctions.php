<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2007 Patrick Bisplinghoff <patrick.bisplinghoff@clicktivities.net>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'DIE ZEIT Abo-Shop' for the 'zeitabo' extension.
 *
 * @author	Patrick Bisplinghoff <patrick.bisplinghoff@clicktivities.net>
 */
 
class tx_zeitabo_BEfunctions {
		
	function setBodytext($conf) {
		
		$uid = $conf['row']['uid'];
		
        // debug( $conf['row'] );
		if ( $conf['row']['pi_flexform'] ) {
    		$piFlexForm = t3lib_div::xml2array($conf['row']['pi_flexform']);
            // debug( $piFlexForm );
    		$newBodyText = $piFlexForm['data']['abo_content']['lDEF']['abo_txt']['vDEF'];
    		$newBodyText .= $piFlexForm['data']['prod_content']['lDEF']['prod_txt']['vDEF'];
    	}
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECT_mm_query(
			'tx_zeitabo_products.prod_name, tx_zeitabo_products.prod_txt',
			'tx_zeitabo_products',
			'tx_zeitabo_products_prod_abos_mm',
			'tx_zeitabo_abonnements',
			'AND tx_zeitabo_abonnements.uid='.$piFlexForm['data']['abo']['lDEF']['aboselect']['vDEF'].' AND tx_zeitabo_products.hidden=0 AND tx_zeitabo_products.deleted=0',
			'tx_zeitabo_products.prod_name'														
		);		
		
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$newBodyText .= '<h3>'.$row['prod_name'].'</h3>';
			$newBodyText .= '<p>'.$row['prod_txt'].'</p>';
		}
		
		$GLOBALS['TYPO3_DB']->exec_UPDATEquery('tt_content', 'uid='.$uid, array('bodytext' => $newBodyText));
		
		return date('d.m.Y');		
	}
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/geze_downloads/class.tx_gezedownloads_itemFunctions.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/geze_downloads/class.tx_gezedownloads_itemFunctions.php']);
}
?>