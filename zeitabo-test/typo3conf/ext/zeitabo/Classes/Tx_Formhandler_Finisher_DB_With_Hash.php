<?php

class Tx_Formhandler_Finisher_DB_With_Hash extends Tx_Formhandler_Finisher_DB {

  /**
   * Returns current UID to use for updating the DB.
   * @return int UID
   */
  protected function getUpdateUid() {
    $q = $GLOBALS['TYPO3_DB']->SELECTquery(
                    'uid', 'zeit_user', "hash='" . t3lib_div::GPvar('item') . "'"
    );
    $res = $GLOBALS['TYPO3_DB']->sql_query( $q );
    $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
    return $row['uid'];
  }

}

?>
