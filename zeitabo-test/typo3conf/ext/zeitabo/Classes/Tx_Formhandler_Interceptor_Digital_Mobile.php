<?php

class Tx_Formhandler_Interceptor_Digital_Mobile extends Tx_Formhandler_AbstractInterceptor {
   
   var $headers = array(
			'Accept: application/xml',
			'Content-Type: application/xml',
	);
		
	public function fetchdata($url, $data,$auth){
		$headers = array(
			'Accept: text/xml',
			'Content-Type: text/xml',
			'charset=utf-8'
		);
		
		$handle = curl_init();
		
		
		curl_setopt($handle, CURLOPT_HEADER, false);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
		
		curl_setopt($handle, CURLOPT_POST, true);
		curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
		if($auth != ''){
			$headers = array(
				'Accept: text/xml',
				'Content-Type: text/xml',
				'charset=utf-8',
				'Authorization: Basic '.base64_encode($auth)
			);
			curl_setopt($handle, CURLOPT_USERPWD, $auth);
		}
		curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($handle, CURLOPT_URL, $url);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($handle, CURLOPT_VERBOSE, 1);
		
		$response = curl_exec($handle);
		$code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		$data = array();
		$data['code'] = $code;
		$data['response'] = $response;

		return $data;
	}
	
	public function getItemData($itemhash){
		$q = $GLOBALS['TYPO3_DB']->SELECTquery(
						'*', 'zeit_user', "hash='" . $itemhash . "'"
		);
		$res = $GLOBALS['TYPO3_DB']->sql_query( $q );
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
		return $row;
		//79b3df8e6eb366068a46897cac9fd210
	}
	
	public function updateCheckDitial($itemhash){
		$GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                'zeit_user', "hash='" . $itemhash. "'", array('Check_Digital' => 1)
        );
	}
	public function updateErrorDitial($itemhash, $error_digital){

		$GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                'zeit_user', "hash='" . $itemhash. "'", array('Error_digital' => $error_digital)
        );
	}
	
	public function process() {
		
		$tacAccepted = 'no';
		$moreOffers = 'no';	
		$auth = '';
		$checkdata = true;
		
		$xmlValues = array();
		$errors_messages = array();
		$this->gp['show_response'] = 'display: none;';
		$this->gp['response'] = '';
		$this->gp['value_response'] = '';
		
		$this->updateErrorDitial($this->gp['item'], 'step1');

		if ( !isset($this->gp['isabo'])) {
		  $checkdata = false;
		} else {
			$newdigital = $this->gp['isabo'];
			$xmlValues['productId'] = $this->gp['abonummer'];
			
			if($this->gp['agb'] != 'ok'){
				$xmlValues['tacAccepted'] = 'no';
			} else {
				$xmlValues['tacAccepted'] = 'yes';
				$xmlValues['type'] = 'digital';

                $userdata = $this->getItemData( $this->gp['item']);

                if($newdigital == 1){
					// Bestehender Kunde
					$checkdata = true;
					$customerVal = 'existing';
					$xmlValues['customer'] = 'new';
					$username = $this->gp['isclientusername'];
					$password = $this->gp['isclientpassword'];
					if($username != '' && $password != ''){
						#$auth = base64_encode($username.':'.$password);
						$auth = $username.':'.$password;
					} else {
                        $auth = '';
					}
				} elseif($newdigital == 0){
					// Neuer Kunde
					$checkdata = true;
					$xmlValues['customer'] = 'new';
					if($this->gp['newclientemail'] == $this->gp['newclientemailcheck']){
						$xmlValues['username'] = $this->gp['newclientusername'];
						$xmlValues['email'] = $this->gp['newclientemail'];
						$xmlValues['email'] = $this->gp['newclientemail'];
						
						
						
					} else {
						$checkdata = false;
						$errors_messages[] = 'newclientemail is wrong';
					}
					
				} else {
					$checkdata = false;
				}
				if($this->gp['isabo'] == '1'){
					$xmlValues['type'] = 'upgrade';
					$xmlValues['customer'] = 'new';
					if(isset($this->gp['abonummer_abo']) && $this->gp['abonummer_abo'] != ''){
						$xmlValues['productId'] = $this->gp['abonummer_abo'];
					}
					$xmlValues['subscriptionId'] =  $this->gp['clientabonummer'];
					$xmlValues['v_postalCode'] =  $this->gp['clientaboplz'];

                    $xmlValues['email'] = $this->gp['newclientemail'];
				}
				if($this->gp['zahlart'] == 'bankeinzug'){
					$xmlValues['paymenttype'] = 'directDebit';
					$xmlValues['accountOwner'] =  $this->gp['kontoInhaber'];
					$xmlValues['accountNumber'] =  $this->gp['kontoNummer'];
					$xmlValues['bankId'] =  $this->gp['kontoBLZ'];
					
				} elseif($this->gp['zahlart'] == 'kreditkarte'){
					$xmlValues['paymenttype'] = 'creditCard';
					$xmlValues['cardType'] = $this->gp['kreditkartenTyp'];
					$xmlValues['cardOwner'] = $this->gp['kreditkartenInhaber'];
					$xmlValues['cardNumber'] = $this->gp['kreditkartenNummer'];
					if($this->gp['kreditkartenMonat'] != 'Monat'){
						$xmlValues['expirationMonth'] = $this->gp['kreditkartenMonat'];
					}
					if($this->gp['kreditkartenJahr'] != 'Jahr'){
						$xmlValues['expirationYear'] = $this->gp['kreditkartenJahr'];
					}
					$xmlValues['verificationNumber'] = $this->gp['kreditkartenPruefziffer'];
					
				} elseif($this->gp['zahlart'] == 'rechnung'){
                    $xmlValues['paymenttype'] = 'invoice';
                }

                if(!empty($this->gp['praemie']) && $this->gp['praemie'] != ''){
                    $xmlValues['extras'] = $this->gp['praemie'];
                }


				
				if(isset($userdata['Zahler_Anrede_Bezeichnung']) && $userdata['Zahler_Anrede_Bezeichnung'] != ''){
					$xmlValues['salutation'] = $userdata['Zahler_Anrede_Bezeichnung'];
				}
				if(isset($userdata['Zahler_Vorname']) && $userdata['Zahler_Vorname'] != ''){
					$xmlValues['givenName'] = $userdata['Zahler_Vorname'];
				} else {
					$checkdata = false;
					$errors_messages[] = 'Vorname fehlt';
				}
				if(isset($userdata['Zahler_Name']) && $userdata['Zahler_Name'] != ''){
					$xmlValues['surname'] = $userdata['Zahler_Name'];
				} else {
					$checkdata = false;
					$errors_messages[] = 'Name fehlt';
				}
				if(isset($userdata['Zahler_Strasse']) && $userdata['Zahler_Strasse'] != ''){
					$xmlValues['streetAddress'] = $userdata['Zahler_Strasse'];
				} else {
					$checkdata = false;
					$errors_messages[] = 'Strasse fehlt';
				}
				if(isset($userdata['Zahler_Adresszusatz']) && $userdata['Zahler_Adresszusatz'] != ''){
					$xmlValues['streetAddressExtra'] = $userdata['Zahler_Adresszusatz'];
				}
				if(isset($userdata['Zahler_PLZ']) && $userdata['Zahler_PLZ'] != ''){
					$xmlValues['b_postalCode'] = $userdata['Zahler_PLZ'];
				} else {
					$checkdata = false;
					$errors_messages[] = 'PLZ fehlt';
				}
				if(isset($userdata['Zahler_Ort']) && $userdata['Zahler_Ort'] != ''){
					$xmlValues['locality'] = $userdata['Zahler_Ort'];
				} else {
					$checkdata = false;
					$errors_messages[] = 'Ort fehlt';
				}
				if(isset($userdata['Zahler_Land']) && $userdata['Zahler_Land'] != ''){
					switch($userdata['Zahler_Land']){
						case 'D':
							$xmlValues['country'] = 'DE';
							break;
						default:
							$xmlValues['country'] = $userdata['Zahler_Land'];
							break;
					}
				} else {
					$checkdata = false;
					$errors_messages[] = 'Land fehlt';
				}
				if(isset($userdata['Zahler_Telefon_Nr_1']) && $userdata['Zahler_Telefon_Nr_1'] != ''){
					$xmlValues['phone'] = $userdata['Zahler_Telefon_Nr_1'];
				} else {
					$xmlValues['phone'] = '';
				}
				if(isset($this->gp['callback']) && $this->gp['callback'] == 'ok'){
					$xmlValues['moreOffers'] = 'yes';
				} else {
					$xmlValues['moreOffers'] = 'no';
				}
                if(isset($userdata['Zahler_Adresszusatz']) && $userdata['Zahler_Adresszusatz'] != ''){
                    $xmlValues['streetAddressExtra'] = $userdata['Zahler_Adresszusatz'];
                }
				
			}
		}
        #var_dump($checkdata);
		if($checkdata == true){
			$this->updateErrorDitial($this->gp['item'], $xmlValues['customer']);
			$dom = new DOMDocument("1.0", "utf-8");
			$root = $dom->createElement("order");
			if(isset($xmlValues['customer']) && $xmlValues['customer'] != ''){
				$customer = $dom->createAttribute("customer");
				$customer->value = $xmlValues['customer'];
				$root->appendChild($customer);
				
				$type = $dom->createAttribute("type");
				$type->value = $xmlValues['type'];
				$root->appendChild($type);
				$dom->appendChild($root);
				
				$productId = $dom->createElement("productId");
				$root->appendChild($productId);
				
				$productIdValue = $dom->createTextNode($xmlValues['productId']);
				$productId->appendChild($productIdValue);
				
				
				
				//<!-- type="upgrade" -->
				if($xmlValues['type'] == 'upgrade'){
					$verification = $dom->createElement("verification");
					$root->appendChild($verification);
					
						$subscriptionId = $dom->createElement("subscriptionId");
						$verification->appendChild($subscriptionId);
						
						$subscriptionIdValue = $dom->createTextNode($xmlValues['subscriptionId']);
						$subscriptionId->appendChild($subscriptionIdValue);
						
						$v_postalCode = $dom->createElement("postalCode");
						$verification->appendChild($v_postalCode);
						
						$v_postalCodeValue = $dom->createTextNode($xmlValues['v_postalCode']);
						$v_postalCode->appendChild($v_postalCodeValue);
				} 
				if($xmlValues['customer'] == 'new'){	
					//<!-- customer="new" -->
					$account = $dom->createElement("account");
					$root->appendChild($account);
						
						$username = $dom->createElement("username");
						$account->appendChild($username);
						
						$usernameValue = $dom->createTextNode($xmlValues['username']);
						$username->appendChild($usernameValue);
						
						$email = $dom->createElement("email");
						$account->appendChild($email);
						
						$emailValue = $dom->createTextNode($xmlValues['email']);
						$email->appendChild($emailValue);
				}
				
				
				$billing = $dom->createElement("billing");
				$root->appendChild($billing);
				
					$salutation = $dom->createElement("salutation");
					$billing->appendChild($salutation);
					
					$salutationValue = $dom->createTextNode($xmlValues['salutation']);
					$salutation->appendChild($salutationValue);
					
					$givenName = $dom->createElement("givenName");
					$billing->appendChild($givenName);
					
					$givenNameValue = $dom->createTextNode($xmlValues['givenName']);
					$givenName->appendChild($givenNameValue);
					
					$surname = $dom->createElement("surname");
					$billing->appendChild($surname);
					
					$surnameValue = $dom->createTextNode($xmlValues['surname']);
					$surname->appendChild($surnameValue);
					
					$streetAddress = $dom->createElement("streetAddress");
					$billing->appendChild($streetAddress);
					
					$streetAddressValue = $dom->createTextNode($xmlValues['streetAddress']);
					$streetAddress->appendChild($streetAddressValue);
					
					$streetAddressExtra = $dom->createElement("streetAddressExtra");
					$billing->appendChild($streetAddressExtra);
					
					$streetAddressExtraValue = $dom->createTextNode($xmlValues['streetAddressExtra']);
					$streetAddressExtra->appendChild($streetAddressExtraValue);
					
					$b_postalCode = $dom->createElement("postalCode");
					$billing->appendChild($b_postalCode);
					
					$b_postalCodeValue = $dom->createTextNode($xmlValues['b_postalCode']);
					$b_postalCode->appendChild($b_postalCodeValue);
					
					$locality = $dom->createElement("locality");
					$billing->appendChild($locality);
					
					$localityValue = $dom->createTextNode($xmlValues['locality']);
					$locality->appendChild($localityValue);
					
					$country = $dom->createElement("country");
					$billing->appendChild($country);
					
					$countryValue = $dom->createTextNode($xmlValues['country']);
					$country->appendChild($countryValue);
					
					$phone = $dom->createElement("phone");
					$billing->appendChild($phone);
					
					$phoneValue = $dom->createTextNode($xmlValues['phone']);
					$phone->appendChild($phoneValue);
				
				$payment = $dom->createElement("payment");
				$root->appendChild($payment);
				
				$paymenttype = $dom->createAttribute("type");
				$paymenttype->value = $xmlValues['paymenttype'];
				$payment->appendChild($paymenttype);
					
					$paymentInterval = $dom->createElement("paymentInterval");
					$payment->appendChild($paymentInterval);
					
					$paymentIntervalValue = $dom->createTextNode('12');
					$paymentInterval->appendChild($paymentIntervalValue);
					
					//<!-- type="directdebit" -->
					if($xmlValues['paymenttype'] == 'directDebit'){
						$accountOwner = $dom->createElement("accountOwner");
						$payment->appendChild($accountOwner);
						
						$accountOwnerValue = $dom->createTextNode($xmlValues['accountOwner']);
						$accountOwner->appendChild($accountOwnerValue);
						
						$accountNumber = $dom->createElement("accountNumber");
						$payment->appendChild($accountNumber);
						
						$accountNumberValue = $dom->createTextNode($xmlValues['accountNumber']);
						$accountNumber->appendChild($accountNumberValue);
						
						$bankId = $dom->createElement("bankId");
						$payment->appendChild($bankId);
						
						$bankIdValue = $dom->createTextNode($xmlValues['bankId']);
						$bankId->appendChild($bankIdValue);
					} elseif($xmlValues['paymenttype'] == 'creditCard'){
						//<!-- type="creditCard" -->
						
						$cardType = $dom->createElement("cardType");
						$payment->appendChild($cardType);
						
						$cardTypeValue = $dom->createTextNode($xmlValues['cardType']);
						$cardType->appendChild($cardTypeValue);
						
						$cardOwner = $dom->createElement("cardOwner");
						$payment->appendChild($cardOwner);
						
						$cardOwnerValue = $dom->createTextNode($xmlValues['cardOwner']);
						$cardOwner->appendChild($cardOwnerValue);
						
						$cardNumber = $dom->createElement("cardNumber");
						$payment->appendChild($cardNumber);
						
						$cardNumberValue = $dom->createTextNode($xmlValues['cardNumber']);
						$cardNumber->appendChild($cardNumberValue);
						
						$expirationMonth = $dom->createElement("expirationMonth");
						$payment->appendChild($expirationMonth);
						
						$expirationMonthValue = $dom->createTextNode($xmlValues['expirationMonth']);
						$expirationMonth->appendChild($expirationMonthValue);
						
						$expirationYear = $dom->createElement("expirationYear");
						$payment->appendChild($expirationYear);
						
						$expirationYearValue = $dom->createTextNode($xmlValues['expirationYear']);
						$expirationYear->appendChild($expirationYearValue);
						
						$verificationNumber = $dom->createElement("verificationNumber");
						$payment->appendChild($verificationNumber);
						
						$verificationNumberValue = $dom->createTextNode($xmlValues['verificationNumber']);
						$verificationNumber->appendChild($verificationNumberValue);
					}
					
				$tacAccepted = $dom->createElement("tacAccepted");
				$root->appendChild($tacAccepted);
				
				$tacAcceptedValue = $dom->createTextNode($xmlValues['tacAccepted']);
				$tacAccepted->appendChild($tacAcceptedValue);
				
				$moreOffers = $dom->createElement("moreOffers");
				$root->appendChild($moreOffers);
				
				$moreOffersValue = $dom->createTextNode($xmlValues['moreOffers']);
				$moreOffers->appendChild($moreOffersValue);

                if(!empty($this->gp['praemie']) && $this->gp['praemie'] != ''){
                    $extra = $dom->createElement("extra");
                    $root->appendChild($extra);

                    $extraValue = $dom->createTextNode($xmlValues['extras']);
                    $extra->appendChild($extraValue);

                    #$extrasValue = $dom->createElement("extras");
                    #$extras->appendChild($xmlValues['extras']);
                }



				$xmldatas = $dom->saveXML();
                if($xmlValues['productId'] == '1222025'){

                }

                /*
                ob_start();
                var_dump($xmldatas);

                $resultDump = ob_get_contents();
                ob_end_clean();
                file_put_contents(dirname(__FILE__) . '/result.txt', $resultDump, FILE_APPEND | LOCK_EX);
                */

				$this->updateErrorDitial($this->gp['item'], serialize($xmldatas));
				
				$digital_reponse = $this->fetchdata('https://premium.zeit.de/api/0/orders', $xmldatas,$auth);
				$this->updateErrorDitial($this->gp['item'], serialize($digital_reponse));

				if(empty($digital_reponse['code'])){
					$this->gp['checkdigital'] = 1;
					$this->gp['xmlcheck'] = 1;
					$this->gp['error_check'] = 1;
					$this->updateCheckDitial( $this->gp['item']);
				} elseif($digital_reponse['code'] == 201 && $digital_reponse['error'] == ''){
					$this->gp['checkdigital'] = 1;
					$this->gp['xmlcheck'] = 1;
					$this->gp['error_check'] = 1;
					$this->updateErrorDitial($this->gp['item'], serialize($digital_reponse));
					$this->updateCheckDitial( $this->gp['item']);
				} else {
					
					$xml = simplexml_load_string($digital_reponse['response']);

					if(is_object($xml)){
						if($digital_reponse['code'] == '401'){
							$error = 'Bitte geben Sie eine gültige Kombination aus Postleitzahl und Abonummer ein.';
						} elseif($digital_reponse['code'] == '503'){
                            $error = '503 Service Temporarily Unavailable<br />The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.';
                        } else {
							if(isset($xml->username)){
								$error = $xml->username;
							}
							if(isset($xml->email)){
								$error = $xml->email;
							}
							if(isset($xml->payment)){
								$error = $xml->payment;
							}
							if(isset($xml->verification)){
								$error = $xml->verification;
							}
                            if(isset($xml->accountNumber)){
                                $error = $xml->accountNumber;
                            }
							if(isset($xml->cardType)){
								$error = $xml->cardType;
							}
							if(isset($xml->cardOwner)){
								$error = $xml->cardOwner;
							}
							if(isset($xml->cardNumber)){
								$error = $xml->cardNumber;
							}
							if(isset($xml->expirationMonth)){
								$error = $xml->expirationMonth;
							}
							if(isset($xml->expirationYear)){
								$error = $xml->expirationYear;
							}
							if(isset($xml->verificationNumber)){
								$error = $xml->verificationNumber;
							}
							if(isset($xml->bankId)){
								$error = $xml->bankId;
							}
						}

					}
					$this->gp['checkdigital'] = '';
					$this->gp['response'] = $error;
					$this->gp['value_response'] = $error;
					$this->gp['show_response'] = 'display: block';
					$this->gp['xmlcheck'] = '';
					$this->gp['error_check'] = 0;
					$this->updateErrorDitial($this->gp['item'], serialize($digital_reponse));
					#var_dump($_SESSION['tx_zeitabo_data']);
				}
			}
			
		}
		
		return $this->gp;
	}
	
   
  
  
}

?>