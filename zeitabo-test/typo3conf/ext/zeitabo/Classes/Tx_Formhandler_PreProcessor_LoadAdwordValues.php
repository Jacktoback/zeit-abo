<?php

class Tx_Formhandler_PreProcessor_LoadAdwordValues extends Tx_Formhandler_AbstractPreProcessor {

    public function getAdwordData($adword, $parent_id){
        $q = $GLOBALS['TYPO3_DB']->SELECTquery(
            'uid', 'tx_zeitabo_abonnements_adwords', "adword='" . $adword . "' AND parent_id='". $parent_id . "'",'','',1
        );
        $res = $GLOBALS['TYPO3_DB']->sql_query( $q );
        $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
        return $row;
    }

  public function process() {

    $adwords = $GLOBALS["TSFE"]->fe_user->getKey("ses","getvars");
    if(!empty($adwords)){
        $adWordsData = unserialize($adwords);

        $adWordID = NULL;
        foreach($adWordsData as $aw){
            $adWordResult = $this->getAdwordData($aw, $this->gp['abo']);
            if(!empty($adWordResult)){
                $adWordID = $adWordResult['uid'];
            }

        }
        $this->gp['Aktionsnummer_Adword'] = $adWordID;
    }

    return $this->gp;
  }
  
}

?>