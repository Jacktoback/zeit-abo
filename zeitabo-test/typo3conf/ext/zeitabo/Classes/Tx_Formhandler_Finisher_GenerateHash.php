<?php

/* *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *
 * $Id: Tx_Formhandler_Finisher_ClearCache.php 22614 2009-07-21 20:43:47Z fabien_u $
 *                                                                        */

/**
 * This finisher generates a unique code for a database entry.
 * This can be used for FE user registration or newsletter registration.
 *
 * @author	Reinhard Führicht <rf@typoheads.at>
 * @package	Tx_Formhandler
 * @subpackage	Finisher
 */
class Tx_Formhandler_Finisher_GenerateHash extends Tx_Formhandler_AbstractFinisher {

  /**
   * The main method called by the controller
   *
   * @return array The probably modified GET/POST parameters
   */
  public function process() {
    $firstInsertInfo = array();
    if (is_array($this->gp['saveDB'])) {
      if (isset($this->settings['table'])) {
        foreach ($this->gp['saveDB'] as $insertInfo) {
          if ($insertInfo['table'] == $this->settings['table']) {
            $firstInsertInfo = $insertInfo;
            break;
          }
        }
      }
      if (empty($firstInsertInfo)) {
        reset($this->gp['saveDB']);
        $firstInsertInfo = current($this->gp['saveDB']);
      }
    }

    $table = $firstInsertInfo['table'];
    $uid = $firstInsertInfo['uid'];
    $uidField = $firstInsertInfo['uidField'];
    if ($table && $uid && $uidField) {
      $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', $table, $uidField . '=' . $uid);
      if ($res) {
        $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
        $authCode = md5(serialize($row));
        $this->gp['hash'] = $authCode;
        $GLOBALS['TSFE']->register['tx_zeitabo_hash'] = $authCode;

        $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                $table, $uidField . '=' . $uid, array('hash' => $authCode)
        );
      }
    }
    return $this->gp;
  }

}

?>
