<?php

class Tx_Formhandler_Validator_Student extends Tx_Formhandler_AbstractValidator {

  /**
   * Validates the submitted values using given settings
   *
   * @param array &$errors Reference to the errors array to store the errors occurred
   * @return boolean
   */
  public function validate( &$errors ) {
    $abo = (int)$this->gp[ 'abo' ];

    $select = "abo_no_student";
    $table = 'tx_zeitabo_abonnements';
    $where = "(uid=$abo)";
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = '';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );

    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );

    $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res );
//    debug( $row['abo_no_student']?'true':'false', 'abo_no_student');
    if( !$row[ 'abo_no_student' ] && $this->gp[ 'student' ] ) {
      return false;
    }

    return true;
  }

}

?>