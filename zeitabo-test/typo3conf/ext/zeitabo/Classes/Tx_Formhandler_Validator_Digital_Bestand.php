<?php

class Tx_Formhandler_Validator_Digital_Bestand extends Tx_Formhandler_Validator_Default {
	
	/**
	 * Validates the submitted values using given settings
	 *
	 * @param array &$errors Reference to the errors array to store the errors occurred
	 * @return boolean
	 */
	public function validate(&$errors) {
  	
	if ($this->gp['item']) {

		$itemhash = $this->gp['item'];
		$q = $GLOBALS['TYPO3_DB']->SELECTquery(
							'*', 'zeit_user', "hash='" . $itemhash . "'"
			);
		$res = $GLOBALS['TYPO3_DB']->sql_query( $q );
		$row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
		
		$zeitabodata = unserialize($row['Error_digital']);
		$errors['xmlcheck'] = '';
		unset($errors['xmlcheck']);
        $errors['xmlcheck'][0] = 'required';

		if(isset($zeitabodata['response'])){
			$xml = simplexml_load_string($zeitabodata['response']);

			if(is_object($xml)){
				if($zeitabodata['Check_Digital'] == 1){
					#return true;
					
					return true;
				} else {
					#
					if($zeitabodata['code'] == '401'){
						$errors['xmlcheck'][0] = 'required';
						if($zeitabodata['Ist_Abonnent '] == 1){
							$errors['clientabonummer'][0] = 'required';
							$errors['clientaboplz'][0] = 'required';
						} else {
							$errors['isclientusername'][0] = 'required';
							$errors['isclientpassword'][0] = 'required';
						}
					} elseif($zeitabodata['code'] == '503'){
                        $errors['xmlcheck'][0] = 'required';
                        $this->gp['value_response'] = '503 Service Temporarily Unavailable<br />The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.';
                    } else {
						if(isset($xml->username)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->username;
							#$errors['newclientusername'][0] = 'required';
							$this->gp['value_response'] = $xml->username;
						}
						if(isset($xml->payment)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->payment;
							$this->gp['value_response'] = $xml->payment;
						}
						if(isset($xml->email)){
							$errors['xmlcheck'][0] = 'required';
							$error =  $xml->email;
							$this->gp['value_response'] = $xml->email;
						}
						if(isset($xml->verification)){
							$errors['xmlcheck'][0] = 'required';
							$error =  $xml->verification;
							$this->gp['value_response'] = $xml->verification;
						}
						if(isset($xml->cardType)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->cardType;
							$this->gp['value_response'] = $xml->cardType;
						}
						if(isset($xml->cardOwner)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->cardOwner;
							$this->gp['value_response'] = $xml->cardOwner;
						}
						if(isset($xml->cardNumber)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->cardNumber;
							$this->gp['value_response'] = $xml->cardNumber;
						}
						if(isset($xml->expirationMonth)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->expirationMonth;
							$this->gp['value_response'] = $xml->expirationMonth;
						}
						if(isset($xml->expirationYear)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->expirationYear;
							$this->gp['value_response'] = $xml->expirationYear;
						}
						if(isset($xml->verificationNumber)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->verificationNumber;
							$this->gp['value_response'] = $xml->verificationNumber;
						}
						if(isset($xml->bankId)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->bankId;
							$this->gp['value_response'] = $xml->bankId;
						}
						if(isset($xml->accountNumber)){
							$errors['xmlcheck'][0] = 'required';
							$error = $xml->accountNumber;
							$this->gp['value_response'] = $xml->accountNumber;
						}
					}
				}
			}
		}
	}
	
    
    $res = parent::validate( $errors );
		
	return empty($errors);
	
  }
  
	

}
?>