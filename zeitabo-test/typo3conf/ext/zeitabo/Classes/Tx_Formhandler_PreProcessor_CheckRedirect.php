<?php

class Tx_Formhandler_PreProcessor_CheckRedirect extends Tx_Formhandler_AbstractPreProcessor {
  
  public function process() {
    $land = strtolower( $this->cObj->cObjGetSingle( $this->settings['country'], $this->settings['country.'] ));

    if ( in_array( $land, array( 'at','ch' ))) {
      $id = $this->cObj->data["tx_add2tca_bank_{$land}_id"];
      if ( $id ) {
        $url = $this->cObj->currentPageUrl( array( 'item' => t3lib_div::_GP( 'item' )), $id );
        header('Location: /'. $url );
        exit;
      }
    }
    return $this->gp;
  }
  
}

?>