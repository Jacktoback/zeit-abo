<?php

class Tx_Formhandler_Validator_Default_With_Allow extends Tx_Formhandler_Validator_Default {
	/**
	 * Validates the submitted values using given settings
	 *
	 * @param array &$errors Reference to the errors array to store the errors occurred
	 * @return boolean
	 */
	public function validate(&$errors) {
    if ( !$this->gp['allow'] ) {
      return true;
    }
    $res = parent::validate( $errors );
//    debug( $errors );
    return $res;
	}

}
?>