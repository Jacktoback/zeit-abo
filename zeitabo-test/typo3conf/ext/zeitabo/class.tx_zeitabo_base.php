<?php

class tx_zeitabo_base extends tslib_pibase {

  /**
   * Returns an array from current cObj[pi_flexform] and
   * db values from table tx_zeitabo_abonnements
   * 
   * @return array    $abo Data, values from flexform and db.
   */
  function getAboData( $products = false, $firstProduct = false ) {
    # get flexform values
    $aboFFdata = array( );

    $aboFFdata[ 'ff_uid' ] = $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'aboselect', 'abo' );
    // $aboFFdata['ff_imgselect'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'imgselect', 'abo');

    $aboFFdata[ 'ff_abo_hl' ] = $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'abo_headline', 'abo_content' );
    $aboFFdata[ 'ff_abo_sub_hl' ] = $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'abo_sub_headline', 'abo_content' );

    $aboFFdata[ 'ff_abo_txt' ] = $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'abo_txt', 'abo_content' );

    $aboFFdata[ 'ff_prod_headline' ] = $this->addSpecialsChars( $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'prod_headline', 'prod_content' ) );
    $aboFFdata[ 'ff_prod_txt' ] = $this->addSpecialsChars( $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'prod_txt', 'prod_content' ) );

    $aboFFdata[ 'ff_abo_img_ov' ] = $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'abo_img_overwrite', 'overwrite' );
    $aboFFdata[ 'ff_prod_img_ov' ] = $this->pi_getFFvalue( $this->cObj->data[ 'pi_flexform' ], 'prod_img_overwrite', 'overwrite' );

    # get db values
    $resAbo = $GLOBALS[ 'TYPO3_DB' ]->exec_SELECTquery(
            '*', 'tx_zeitabo_abonnements', 'uid=' . $aboFFdata[ 'ff_uid' ] . $this->cObj->enableFields( 'tx_zeitabo_abonnements' )
    );
    if( $GLOBALS[ 'TYPO3_DB' ]->sql_error() )
      debug( array( $GLOBALS[ 'TYPO3_DB' ]->sql_error(), $query ) );
    $aboDBdata = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $resAbo );

    # set frontend name if exists
    $aboDBdata[ 'abo_name' ] = ($aboDBdata[ 'abo_name2' ] != '') ? $aboDBdata[ 'abo_name2' ] : $aboDBdata[ 'abo_name' ];

    # merge arrays
    $aboData = array_merge( $aboFFdata, $aboDBdata );

    # get images from flexform
    $this->cObj->data[ 'abo_images' ] = $aboData[ 'abo_images' ];
    $this->pi_initPIFlexForm( 'abo_images' );
    $aboData[ 'abo_img' ] = array( );
    $aboData[ 'abo_img' ][ 0 ] = ($aboData[ 'ff_abo_img_ov' ] == '') ? $this->pi_getFFvalue( $this->cObj->data[ 'abo_images' ], 'img_1', $this->colorscheme ) : $aboData[ 'ff_abo_img_ov' ];
    $aboData[ 'abo_img' ][ 1 ] = ($aboData[ 'ff_abo_img_ov' ] == '') ? $this->pi_getFFvalue( $this->cObj->data[ 'abo_images' ], 'img_2', $this->colorscheme ) : $aboData[ 'ff_abo_img_ov' ];
    $aboData[ 'abo_img' ][ 2 ] = ($aboData[ 'ff_abo_img_ov' ] == '') ? $this->pi_getFFvalue( $this->cObj->data[ 'abo_images' ], 'img_3', $this->colorscheme ) : $aboData[ 'ff_abo_img_ov' ];

    # get specific products from db
    $GLOBALS[ 'TYPO3_DB' ]->store_lastBuiltQuery = true;
    if( $firstProduct ) {
//          debug( 'don\'t go here' ); die();
//            $resProds = $GLOBALS['TYPO3_DB']->exec_SELECT_mm_query(
//                'tx_zeitabo_abonnements.uid AS abo_uid, tx_zeitabo_products.uid, prod_type, prod_no_1, prod_no_2, prod_no_3, prod_name, prod_images, prod_hl, prod_txt, prod_copayment',
//                'tx_zeitabo_abonnements',
//                'tx_zeitabo_abonnements_abo_products_mm',
//                'tx_zeitabo_products',
//                'AND tx_zeitabo_abonnements.uid = '.$aboData['uid'].' AND tx_zeitabo_products.hidden=0 AND tx_zeitabo_products.deleted=0',#$this->cObj->enableFields('tx_zeitabo_abonnements').$this->cObj->enableFields('tx_zeitabo_products'),
//                '',
//                'sorting',
//                ''
//            );
//            // debug( $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery, 'first' );
//            while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resProds)) {
//                $aboData['special_products'][] = $row;
//            }
//
//            # get images from flexform
//            $count = 0;
//            if($aboData['special_products']) {
//                foreach($aboData['special_products'] as $prods) {
//                    $this->cObj->data['prod_images_'.$count] = $prods['prod_images'];
//                    $this->pi_initPIFlexForm('prod_images_'.$count);
//                    $aboData['special_products'][$count]['prod_img'] = array();
//                    $aboData['special_products'][$count]['prod_img'][0] = ($aboData['ff_prod_img_ov'] == '') ? $this->pi_getFFvalue($this->cObj->data['prod_images_'.$count], 'img_1', $this->colorscheme) : $aboData['ff_prod_img_ov'];
//                    $aboData['special_products'][$count]['prod_img'][1] = ($aboData['ff_prod_img_ov'] == '') ? $this->pi_getFFvalue($this->cObj->data['prod_images_'.$count], 'img_2', $this->colorscheme) : $aboData['ff_prod_img_ov'];
//                    $aboData['special_products'][$count]['prod_img'][2] = ($aboData['ff_prod_img_ov'] == '') ? $this->pi_getFFvalue($this->cObj->data['prod_images_'.$count], 'img_3', $this->colorscheme) : $aboData['ff_prod_img_ov'];
//                    $count++;
//                }
//            }
    }

    # get all products
    if( $products ) {
      $resProds = $GLOBALS[ 'TYPO3_DB' ]->exec_SELECT_mm_query(
              'tx_zeitabo_products.*, prod_type, prod_no_1, prod_no_2, prod_no_3, prod_name, prod_images, prod_hl, prod_txt, prod_copayment, product_group', 'tx_zeitabo_abonnements', 'tx_zeitabo_abonnements_abo_products_mm', 'tx_zeitabo_products', 'AND tx_zeitabo_abonnements.uid = ' . $aboData[ 'uid' ] . ' AND tx_zeitabo_products.hidden=0 AND tx_zeitabo_products.deleted=0', '', 'tx_zeitabo_abonnements_abo_products_mm.sorting ASC', ''
      );
//        debug( $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery, 'prods' );

      while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $resProds ) ) {
        $aboData[ 'all_products' ][ ] = $row;
      }

      # get images from flexform
      $count = 0;
      if( $aboData[ 'all_products' ] ) {
        foreach( $aboData[ 'all_products' ] as $prods ) {
          // debug( $prods );
          $this->cObj->data[ 'prod_images_' . $count ] = $prods[ 'prod_images' ];
          $this->pi_initPIFlexForm( 'prod_images_' . $count );
          $aboData[ 'all_products' ][ $count ][ 'prod_img' ] = array( );
          if( $prods[ 'product_image' ] )
            $aboData[ 'all_products' ][ $count ][ 'prod_img' ][ 0 ] = $prods[ 'product_image' ];
          if( $prods[ 'product_image_detail' ] )
            $aboData[ 'all_products' ][ $count ][ 'prod_img' ][ 1 ] = $prods[ 'product_image_detail' ];
          $aboData[ 'all_products' ][ $count ][ 'prod_img' ][ 2 ] = ($aboData[ 'ff_prod_img_ov' ] == '') ? $this->pi_getFFvalue( $this->cObj->data[ 'prod_images_' . $count ], 'img_3', $this->colorscheme ) : $aboData[ 'ff_prod_img_ov' ];
          $count++;
        }
      }
    }

    #t3lib_div::debug($aboData);
    #t3lib_div::debug($aboData['abo_type']);
    #t3lib_div::debug($aboData['hidden']);
    #t3lib_div::debug($aboData['starttime']);
    #t3lib_div::debug($aboData['endtime']);
    #t3lib_div::debug($aboData['special_products']);
    #t3lib_div::debug("-----------------------------------------------------------------------------------");
    #t3lib_div::debug($aboData['all_products']);

    return $aboData;
  }

  function getProductImageSmallResource( $strImage, $conf, $backColor = false ) {
    if( $conf[ 'file' ] == 'GIFBUILDER' ) {
      $conf[ 'file.' ][ '10.' ][ 'file' ] = 'uploads/tx_zeitabo/' . $strImage;
    }
    else {
      $conf[ 'file' ] = 'uploads/tx_zeitabo/' . $strImage;
    }
    // debug( $conf, $strImage );
    $ret = $this->cObj->IMG_RESOURCE( $conf );
    // debug( $ret, $strImage ); 
    return $ret;
  }

  //  $select = 'uid, pid, sys_language_uid, text, title'
  //  $table = 'tx_myext_data';
  //// we try to get the default language entry (normal behaviour) or, if not possible, currently the needed language (fallback if no default language entry is available)
  //  $where = '(sys_language_uid IN (-1,0) OR (sys_language_uid = ' .$GLOBALS[ 'TSFE' ]->sys_language_uid. ' AND l18n_parent = 0))';
  //// always (!) use TYPO3 default function for adding hidden = 0, deleted = 0, group and date statements
  //  $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields($table);
  //  $order = '';
  //  $group = '';
  //  $limit = '';
  //  $res = $GLOBALS[ 'TYPO3_DB' ]->exec_SELECTquery($select, $table, $where, $group, $order, $limit);
  //
    //  while ($row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc($res)) {
  //  // check for language overlay if:
  //  // * row is valid
  //  // * row language is different from currently needed language
  //  // * sys_language_contentOL is set
  //  if (is_array($row) && $row[ 'sys_language_uid' ] != $GLOBALS[ 'TSFE' ]->sys_language_content && $GLOBALS[ 'TSFE' ]->sys_language_contentOL) {
  //  $row = $GLOBALS[ 'TSFE' ]->sys_page->getRecordOverlay($table, $row, $GLOBALS[ 'TSFE' ]->sys_language_content, $GLOBALS[ 'TSFE' ]->sys_language_contentOL);
  //}
  //if( $row ) {
  //  // get correct language uid for translated realurl link
  //  $link_uid = ($row[ '_LOCALIZED_UID' ]) ? $row[ '_LOCALIZED_UID' ] : $row[ 'uid' ];
  //
    //  // do stuff with the row entry data like built HTML or prepare further usage
  //}
  //}
  //
  /**
   * Returns an array from table singlebooks
   * where pid eqauls cObj.data.pages
   * 
   * @return array    $sb Data, values from db.
   */
  function getSBData( $pid ) {
    $SBdata = array( );

# get db values
    $select = '*';
    $table = 'tx_zeitabo_singlebooks';
// we try to get the default language entry (normal behaviour) or, if not possible, currently the needed language (fallback if no default language entry is available)
    $where = 'pid=' . (int)$pid;
    $where .= ' AND (sys_language_uid IN (-1,0) OR (sys_language_uid = ' . $GLOBALS[ 'TSFE' ]->sys_language_uid . ' AND l18n_parent = 0))';
// always (!) use TYPO3 default function for adding hidden = 0, deleted = 0, group and date statements
    $where .= $GLOBALS[ 'TSFE' ]->sys_page->enableFields( $table );
    $order = 'sorting';
    $group = '';
    $limit = '';
    $q = $GLOBALS[ 'TYPO3_DB' ]->SELECTquery( $select, $table, $where, $group, $order, $limit );
    $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );

    while( $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res ) ) {
      // check for language overlay if:
      // * row is valid
      // * row language is different from currently needed language
      // * sys_language_contentOL is set
      if( is_array( $row ) && $row[ 'sys_language_uid' ] != $GLOBALS[ 'TSFE' ]->sys_language_content && $GLOBALS[ 'TSFE' ]->sys_language_contentOL ) {
        $row = $GLOBALS[ 'TSFE' ]->sys_page->getRecordOverlay( $table, $row, $GLOBALS[ 'TSFE' ]->sys_language_content, $GLOBALS[ 'TSFE' ]->sys_language_contentOL );
      }
      if( $row ) {
        // get correct language uid for translated realurl link
        $link_uid = ($row[ '_LOCALIZED_UID' ]) ? $row[ '_LOCALIZED_UID' ] : $row[ 'uid' ];
        //
        $SBdata[ ] = $row;
      }
    }

    $count = 0;
    foreach( $SBdata as $sb ) {
      $this->cObj->data[ 'mag_images_' . $count ] = $sb[ 'mag_images' ];
      $this->pi_initPIFlexForm( 'mag_images_' . $count );
      $SBdata[ $count ][ 'mag_img' ] = array( );
      $SBdata[ $count ][ 'mag_img' ][ 0 ] = $sb[ 'product_image' ];
      $SBdata[ $count ][ 'mag_img' ][ 1 ] = $sb[ 'product_image_detail' ];
      $count++;
    }

#t3lib_div::debug($SBdata);
    return $SBdata;
  }

}

?>
