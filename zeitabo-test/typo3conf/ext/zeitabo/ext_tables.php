<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

include_once(t3lib_extMgm::extPath($_EXTKEY).'class.tx_zeitabo_BEfunctions.php');

$TCA["tx_zeitabo_abonnements"] = array (
	"ctrl" => array (
		'title'     => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements',		
		'label'     => 'uid',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'versioningWS' => TRUE, 
		'origUid' => 't3_origuid',
		'languageField'            => 'sys_language_uid',	
		'transOrigPointerField'    => 'l18n_parent',	
		'transOrigDiffSourceField' => 'l18n_diffsource',	
		'default_sortby' => "ORDER BY abo_name,crdate",	
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',	
			'starttime' => 'starttime',	
			'endtime' => 'endtime',	
			'fe_group' => 'fe_group',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabo_abonnements.gif',
	),
	"feInterface" => array (
		"fe_admin_fieldList" => "sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, fe_group, abo_name, abo_no, abo_no_student, abo_offerform, abo_shipping, abo_images, abo_txt_order, abo_products",
	)
);

$TCA["tx_zeitabo_abonnements_adwords"] = array (
    "ctrl" => array (
        'title'     => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_abonnements_adwords',
        'label'     => 'adword',
        'tstamp'    => 'tstamp',
        'crdate'    => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => TRUE,
        'origUid' => 't3_origuid',
        'languageField'            => 'sys_language_uid',
        'transOrigPointerField'    => 'l18n_parent',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'sortby' => 'sorting',
        'delete' => 'deleted',
        'enablecolumns' => array (
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ),
        'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
        'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabo_abonnements_adwords.gif',
    ),
    "feInterface" => array (
        "fe_admin_fieldList" => "sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, fe_group, adword, abo_no, abo_no_student, abo_no_kombi_digital",
    )
);

$TCA["tx_zeitabo_products"] = array (
	"ctrl" => array (
		'title'     => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_products',		
		'label'     => 'uid',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'versioningWS' => TRUE, 
		'origUid' => 't3_origuid',
		'languageField'            => 'sys_language_uid',	
		'transOrigPointerField'    => 'l18n_parent',	
		'transOrigDiffSourceField' => 'l18n_diffsource',	
		'default_sortby' => "ORDER BY prod_name,crdate",	
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',	
			'starttime' => 'starttime',	
			'endtime' => 'endtime',	
			'fe_group' => 'fe_group',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabo_products.gif',
	),
	"feInterface" => array (
		"fe_admin_fieldList" => "sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, fe_group, prod_type, prod_no_1, prod_no_2, prod_no_3, prod_name, prod_image, prod_hl, prod_txt, prod_count, prod_copayment, prod_abos",
	)
);

$TCA["tx_zeitabo_singlebooks"] = array (
	"ctrl" => array (
		'title'     => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_singlebooks',		
		'label'     => 'uid',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'versioningWS' => TRUE, 
		'origUid' => 't3_origuid',
		'languageField'            => 'sys_language_uid',	
		'transOrigPointerField'    => 'l18n_parent',	
		'transOrigDiffSourceField' => 'l18n_diffsource',	
		'sortby' => 'sorting',
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',	
			'starttime' => 'starttime',	
			'endtime' => 'endtime',	
			'fe_group' => 'fe_group',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabo_singlebooks.gif',
	),
	"feInterface" => array (
		"fe_admin_fieldList" => "sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, fe_group, mag_type, mag_no, mag_name, mag_release, mag_images",
	)
);

$TCA['tx_zeitabo_blz'] = array (
    'ctrl' => array (
        'title'     => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_blz',        
        'label'     => 'uid',    
        'tstamp'    => 'tstamp',
        'crdate'    => 'crdate',
        'cruser_id' => 'cruser_id',
        'languageField'            => 'sys_language_uid',    
        'transOrigPointerField'    => 'l10n_parent',    
        'transOrigDiffSourceField' => 'l10n_diffsource',    
        'default_sortby' => 'ORDER BY crdate',    
        'delete' => 'deleted',    
        'enablecolumns' => array (        
            'disabled' => 'hidden',    
            'starttime' => 'starttime',    
            'endtime' => 'endtime',
        ),
        'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
        'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabo_blz.gif',
    ),
);

$TCA['tx_zeitabo_product_groups'] = array (
    'ctrl' => array (
        'title'     => 'LLL:EXT:zeitabo/locallang_db.xml:tx_zeitabo_product_groups',
        'label'     => 'name',
        'tstamp'    => 'tstamp',
        'crdate'    => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'ORDER BY crdate',
        'delete' => 'deleted',
        'enablecolumns' => array (
            'disabled' => 'hidden',
        ),
        'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
        'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_zeitabo_product_groups.gif',
    ),
);


t3lib_div::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi1']='layout,select_key';
#$TCA['tt_content']['types']['list']['showitem'] = 'CType;;4;button;1-1-1, header;;3;;2-2-2, tx_add2tca_colorscheme;;;;1-1-1,tx_add2tca_addline, --div--, list_type;;;;5-5-5, layout, select_key, bodytext, pages;;12';

t3lib_extMgm::addPlugin(array('LLL:EXT:zeitabo/locallang_db.xml:tt_content.list_type_pi1', $_EXTKEY.'_pi1'),'list_type');

// add FlexForm
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pi1'] = 'pi_flexform,tx_add2tca_formpage_id';
t3lib_extMgm::addPiFlexFormValue($_EXTKEY.'_pi1', 'FILE:EXT:'.$_EXTKEY.'/flexform_ds.xml');

// add ABO-Images FlexForm
#$TCA['tx_zeitabo_abonnements']['columns']['list']['subtypes_addlist'][$_EXTKEY.'_pi1'] = 'pi_flexform';
#t3lib_extMgm::addPiFlexFormValue($_EXTKEY.'_pi1', 'FILE:EXT:'.$_EXTKEY.'/abo_images_ds.xml');


t3lib_extMgm::addStaticFile($_EXTKEY,'pi1/static/','DIE ZEIT Abo-Shop');


if (TYPO3_MODE=="BE")	{
	$TBE_MODULES_EXT["xMOD_db_new_content_el"]["addElClasses"]["tx_zeitabo_pi1_wizicon"] = t3lib_extMgm::extPath($_EXTKEY).'pi1/class.tx_zeitabo_pi1_wizicon.php';
	
	t3lib_extMgm::addModulePath('web_txzeitabomoduleM1', t3lib_extMgm::extPath($_EXTKEY) . 'mod1/');
		
	t3lib_extMgm::addModule('web', 'txzeitabomoduleM1', '', t3lib_extMgm::extPath($_EXTKEY) . 'mod1/');
	
}
?>
