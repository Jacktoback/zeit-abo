<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');
$tempColumns = Array (
	"tx_add2tca_colorscheme" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme",		
		"config" => Array (
			"type" => "select",
			"items" => Array (
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.0", "", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_0.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.1", "z-blue", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_1.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.2", "z-orange", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_2.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.3", "z-red", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_3.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.4", "z-green", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_4.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.5", "z-blue2", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_5.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.6", "z-white", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_6.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.7", "z-red2", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_7.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.8", "z-blue3", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_8.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.9", "z-purple", t3lib_extMgm::extRelPath("add2tca")."selicon_pages_tx_add2tca_colorscheme_9.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.10", "zeit"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.11", "zeit-digital"),
				Array("LLL:EXT:add2tca/locallang_db.xml:pages.tx_add2tca_colorscheme.I.12", "magazine"),
			),
			"size" => 1,	
			"maxitems" => 1,
		)
	),
);


t3lib_div::loadTCA("pages");
t3lib_extMgm::addTCAcolumns("pages",$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes("pages","tx_add2tca_colorscheme;;;;1-1-1");

$tempColumns = Array (
	"tx_add2tca_colorscheme" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme",		
		"config" => Array (
			"type" => "select",
			"items" => Array (
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.0", "", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_0.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.1", "z-blue", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_1.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.2", "z-orange", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_2.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.3", "z-red", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_3.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.4", "z-green", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_4.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.5", "z-blue2", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_5.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.6", "z-white", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_6.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.7", "z-red2", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_7.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.8", "z-blue3", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_8.gif"),
				Array("LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_colorscheme.I.9", "z-purple", t3lib_extMgm::extRelPath("add2tca")."selicon_tt_content_tx_add2tca_colorscheme_9.gif"),
			),
			"size" => 1,	
			"maxitems" => 1,
		)
	),
	"tx_add2tca_addline" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_addline",		
		"config" => Array (
			"type" => "check",
		)
	),
	"tx_add2tca_backcolor" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_backcolor",		
		"config" => Array (
			"type" => "check",
		)
	),
    'tx_add2tca_formpage_id' => array (        
        'exclude' => 0,        
        'label' => 'LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_formpage_id',        
        'config' => array (
            'type' => 'group',    
            'internal_type' => 'db',    
            'allowed' => 'pages',    
            'size' => 1,    
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
    'tx_add2tca_bank_at_id' => array (        
        'exclude' => 0,        
        'label' => 'LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_bank_at_id',        
        'config' => array (
            'type' => 'group',    
            'internal_type' => 'db',    
            'allowed' => 'pages',    
            'size' => 1,    
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
        'tx_add2tca_bank_ch_id' => array (        
        'exclude' => 0,        
        'label' => 'LLL:EXT:add2tca/locallang_db.xml:tt_content.tx_add2tca_bank_ch_id',        
        'config' => array (
            'type' => 'group',    
            'internal_type' => 'db',    
            'allowed' => 'pages',    
            'size' => 1,    
            'minitems' => 0,
            'maxitems' => 1,
        )
    ),
    );


t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);
#t3lib_extMgm::addToAllTCAtypes("tt_content","tx_add2tca_colorscheme;;;;1-1-1,tx_add2tca_addline,tx_add2tca_backcolor","","after:header");
//t3lib_extMgm::addToAllTCAtypes("tt_content","tx_add2tca_bank_at_id,tx_add2tca_bank_ch_id","","after:header");
//t3lib_extMgm::addToAllTCAtypes("tt_content","","after:header");

?>