<?php

########################################################################
# Extension Manager/Repository config file for ext: "add2tca"
#
# Auto generated 12-06-2007 17:02
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Add fields to TCA',
	'description' => 'Simple extension to add fields to the TCA',
	'category' => 'misc',
	'author' => 'Patrick Bisplinghoff',
	'author_email' => 'patrick.bisplinghoff@clicktivities.net',
	'shy' => '',
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
			'cms' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'suggests' => array(
	),
	'_md5_values_when_last_written' => 'a:28:{s:9:"ChangeLog";s:4:"8f02";s:10:"README.txt";s:4:"9fa9";s:12:"ext_icon.gif";s:4:"1bdc";s:14:"ext_tables.php";s:4:"b044";s:14:"ext_tables.sql";s:4:"3ab2";s:16:"locallang_db.xml";s:4:"6232";s:42:"selicon_pages_tx_add2tca_colorscheme_0.gif";s:4:"ddb9";s:42:"selicon_pages_tx_add2tca_colorscheme_1.gif";s:4:"cc79";s:42:"selicon_pages_tx_add2tca_colorscheme_2.gif";s:4:"6d82";s:42:"selicon_pages_tx_add2tca_colorscheme_3.gif";s:4:"7058";s:42:"selicon_pages_tx_add2tca_colorscheme_4.gif";s:4:"616b";s:42:"selicon_pages_tx_add2tca_colorscheme_5.gif";s:4:"8114";s:42:"selicon_pages_tx_add2tca_colorscheme_6.gif";s:4:"1f55";s:42:"selicon_pages_tx_add2tca_colorscheme_7.gif";s:4:"df7a";s:42:"selicon_pages_tx_add2tca_colorscheme_8.gif";s:4:"ce4d";s:42:"selicon_pages_tx_add2tca_colorscheme_9.gif";s:4:"bff3";s:47:"selicon_tt_content_tx_add2tca_colorscheme_0.gif";s:4:"ddb9";s:47:"selicon_tt_content_tx_add2tca_colorscheme_1.gif";s:4:"cc79";s:47:"selicon_tt_content_tx_add2tca_colorscheme_2.gif";s:4:"6d82";s:47:"selicon_tt_content_tx_add2tca_colorscheme_3.gif";s:4:"7058";s:47:"selicon_tt_content_tx_add2tca_colorscheme_4.gif";s:4:"616b";s:47:"selicon_tt_content_tx_add2tca_colorscheme_5.gif";s:4:"8114";s:47:"selicon_tt_content_tx_add2tca_colorscheme_6.gif";s:4:"1f55";s:47:"selicon_tt_content_tx_add2tca_colorscheme_7.gif";s:4:"df7a";s:47:"selicon_tt_content_tx_add2tca_colorscheme_8.gif";s:4:"ce4d";s:47:"selicon_tt_content_tx_add2tca_colorscheme_9.gif";s:4:"bff3";s:19:"doc/wizard_form.dat";s:4:"96e7";s:20:"doc/wizard_form.html";s:4:"6b50";}',
);

?>