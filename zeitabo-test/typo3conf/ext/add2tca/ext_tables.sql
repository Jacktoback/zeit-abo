#
# Table structure for table 'pages'
#
CREATE TABLE pages (
	tx_add2tca_colorscheme varchar(14) DEFAULT '' NOT NULL
);



#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	tx_add2tca_colorscheme varchar(14) DEFAULT '' NOT NULL,
	tx_add2tca_addline tinyint(3) DEFAULT '0' NOT NULL,
	tx_add2tca_backcolor tinyint(3) DEFAULT '0' NOT NULL,
  tx_add2tca_formpage_id text,
  tx_add2tca_bank_at_id text,
  tx_add2tca_bank_ch_id text
);