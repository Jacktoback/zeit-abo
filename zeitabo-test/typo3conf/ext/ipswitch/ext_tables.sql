#
# Table structure for table 'tx_ipswitch_data'
#
CREATE TABLE tx_ipswitch_data (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	begin_ip varchar(255) DEFAULT '' NOT NULL,
	end_ip varchar(255) DEFAULT '' NOT NULL,
	begin_num bigint(20) DEFAULT '0' NOT NULL,
	end_num bigint(20) DEFAULT '0' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	
	PRIMARY KEY (uid),
	KEY parent (pid),
  KEY begin_num (begin_num),
  KEY end_num (end_num)
);