<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA['tx_ipswitch_data'] = array (
	'ctrl' => $TCA['tx_ipswitch_data']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'begin_ip,end_ip,begin_num,end_num,country,name'
	),
	'feInterface' => $TCA['tx_ipswitch_data']['feInterface'],
	'columns' => array (
		'begin_ip' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:ipswitch/locallang_db.xml:tx_ipswitch_data.begin_ip',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',
			)
		),
		'end_ip' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:ipswitch/locallang_db.xml:tx_ipswitch_data.end_ip',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',
			)
		),
		'begin_num' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:ipswitch/locallang_db.xml:tx_ipswitch_data.begin_num',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'int',
			)
		),
		'end_num' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:ipswitch/locallang_db.xml:tx_ipswitch_data.end_num',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'int',
			)
		),
		'country' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:ipswitch/locallang_db.xml:tx_ipswitch_data.country',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',
			)
		),
		'name' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:ipswitch/locallang_db.xml:tx_ipswitch_data.name',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'begin_ip;;;;1-1-1, end_ip, begin_num, end_num, country, name')
	),
	'palettes' => array (
		'1' => array('showitem' => '')
	)
);
?>