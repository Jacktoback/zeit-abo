<?php

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Jan Voges <j.voges@divine.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */
require_once(PATH_tslib . 'class.tslib_pibase.php');

/**
 * Plugin 'switch' for the 'ipswitch' extension.
 *
 * @author	Jan Voges <j.voges@divine.de>
 * @package	TYPO3
 * @subpackage	tx_ipswitch
 */
class tx_ipswitch_pi1 extends tslib_pibase {

  var $prefixId = 'tx_ipswitch_pi1';  // Same as class name
  var $scriptRelPath = 'pi1/class.tx_ipswitch_pi1.php'; // Path to this script relative to the extension dir.
  var $extKey = 'ipswitch'; // The extension key.

  /**
   * The main method of the PlugIn
   *
   * @param	string		$content: The PlugIn content
   * @param	array		$conf: The PlugIn configuration
   * @return	The content that is displayed on the website
   */

  function main( $content, $conf ) {
    $ip = t3lib_div::GPvar( 'remote_ip' );

    if( !$ip ) {
      $ip_checked = $GLOBALS[ "TSFE" ]->fe_user->getKey( "ses", "{$this->prefixId}[ip_checked]" );
      if( !$ip_checked ) {
        $ip = t3lib_div::getIndpEnv( 'REMOTE_ADDR' );
      }
    }
    #debug( $ip, 'ip' );

    if( $ip ) {
      $ip_a = preg_split( '/\./', $ip );
      $ip_num = (( $ip_a[ 0 ] * 256 + $ip_a[ 1 ]) * 256 + $ip_a[ 2 ]) * 256 + $ip_a[ 3 ];

      $q = "select country from tx_ipswitch_data where begin_num <= $ip_num and $ip_num <= end_num";
      $res = $GLOBALS[ 'TYPO3_DB' ]->sql_query( $q );
      $row = $GLOBALS[ 'TYPO3_DB' ]->sql_fetch_assoc( $res );

      $country = 'DE';
      $L_new = 0;
      if( $row ) {
        switch( $row[ 'country' ] ) {
          case 'AT':
            $country = 'AT';
            $L_new = 1;
            break;
          case 'CH':
            $country = 'CH';
            $L_new = 2;
            break;
        }
      }


      #debug( $country, 'setting country' );
      $GLOBALS[ "TSFE" ]->fe_user->setKey( "ses", "{$this->prefixId}[ip_checked]", true );

      #debug( $ip_num, $country );
      if( $L_new != $GLOBALS[ 'TSFE' ]->sys_language_uid ) {
        $link = $this->cObj->currentPageUrl( array( 'L' => $L_new ) );
        #debug( $link, 'redirecting to ' . $L_new );
        $GLOBALS[ 'TSFE' ]->fe_user->storeSessionData();
        header( 'Location: /' . $link );
        die();
      }
    }
    return '';
  }

}

if( defined( 'TYPO3_MODE' ) && $TYPO3_CONF_VARS[ TYPO3_MODE ][ 'XCLASS' ][ 'ext/ipswitch/pi1/class.tx_ipswitch_pi1.php' ] ) {
  include_once($TYPO3_CONF_VARS[ TYPO3_MODE ][ 'XCLASS' ][ 'ext/ipswitch/pi1/class.tx_ipswitch_pi1.php' ]);
}
?>
