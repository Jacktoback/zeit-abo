<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2013 Jens Büchel <j.buechel@divine.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'DIE ZEIT - Leserservice - Kontaktformular-Tool' for the 'zeit_leserservice' extension.
 *
 * @author	Jens Büchel <j.buechel@divine.de>
 * @package	TYPO3
 * @subpackage	tx_zeitleserservice
 */
class tx_zeitleserservice_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_zeitleserservice_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_zeitleserservice_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'zeit_leserservice';	// The extension key.
	var $pi_checkCHash = true;

	/**
	 * Initialize plugin and get configuration values.
	 *
	 * @param	array		$conf : configuration array from TS
	 */
	function init($conf) {
		// Store configuration
		$this->conf = $conf;
		
		// Loading language-labels
		$this->pi_loadLL();
		
		// Set default piVars from TS
		$this->pi_setPiVarDefaults();
		
		// Init and get the flexform data of the plugin
		$this->pi_initPIflexForm();
		
		// Assign the flexform data to a local variable for easier access
		$piFlexForm = $this->cObj->data['pi_flexform'];
		
		// Array for local configuration
		$this->lConf = array();
		// load available syslanguages
		$this->initLanguages();
		// sys_language_mode defines what to do if the requested translation is not found
		$this->sys_language_mode = $this->conf['sys_language_mode']?$this->conf['sys_language_mode'] : $GLOBALS['TSFE']->sys_language_mode;
		
		$this->sys_language_uid = $GLOBALS['TSFE']->config['config']['sys_language_uid'];    //Get site-language 
		
		if($this->sys_language_uid == ''){ 
			$this->sys_language_uid = 0; 
		}
	}
	
	/**
	 * fills the internal array '$this->langArr' with the available syslanguages
	 *
	 * @return	void
	 */
	function initLanguages () {
		
		$lres = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'sys_language',
			'1=1' . $this->cObj->enableFields('sys_language'));
		
		$this->langArr = array();
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($lres)) {
			$this->langArr[$row['uid']] = $row;
		}
	}
	
	/**
	 * The selector for the Layout
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->init($conf);
		
		date_default_timezone_set('Europe/Berlin');
		$loc=setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'de_DE.UTF-8');
		
		$content = '';
		
		if ($conf['templatefile']) {
			$this->template=$this->cObj->fileResource($conf['templatefile']);
		} else {
			$this->template=$this->cObj->fileResource('EXT:'.$this->extKey.'/res/html/template.html');
		}
		
		$accordion = $this->cObj->getSubpart($this->template,'###ACCORDION###');
		$accordion_element = $this->cObj->getSubpart($this->template,'###ACCORDION_ELEMENT###');
		$question_selector = $this->cObj->getSubpart($this->template,'###QUESTION_SELECTOR###');
		$one_question = $this->cObj->getSubpart($this->template,'###ONE_QUESTION###');
		$answers = $this->cObj->getSubpart($this->template,'###ANSWERS###');
		
		$markers = array();
		$markers['###ACCORDION_ELEMENTS###'] = '';
		
		
		$categories = $this->collectCategories();
		
		if(count($categories) > 0 ){
			foreach($categories as $category){
				$acc_markers = array();
				$acc_markers['question_section'] = '';
				if($category['popunder'] != ''){
					$popunder_link = $this->pi_getPageLink($category['popunder']);
					$acc_markers['category'] = '<a onclick="vHWin=window.open(\''.$popunder_link.'\',\'FEopenLink\',\'scrollbars=1,resizable=1,width=690,height=600\');vHWin.focus();return false;" target="FEopenLink" href="'.$popunder_link.'">'. $category['title'].'</a>';
				} else {
					$acc_markers['category'] = $category['title'];
				}
                $acc_markers['category_uid'] = $category['uid'];
				$questions = $this->collectQuestions($category['uid']);
				if(count($questions) > 0){
					$question_markers = array();
					$question_markers['list_answers'] = '';
					$question_markers['category_uid'] = $category['uid'];

					$first = true;
					foreach($questions as $question){
						$question_markers['question'] = $question['question'];
						$question_markers['option_list_questions'] .= '<option value="'.$question['uid'].'">'.$question['question'].'</option>';
						$question_markers['answers'] = $this->pi_RTEcssText($question['answer']);
						$answer_markers = array();
						$answer_markers['category_uid'] = $category['uid'];
						$answer_markers['answer_uid'] = $question['uid'];
						$answer_markers['display_answer'] = 'display:none';
						if($first == true){
							$answer_markers['display_answer'] = 'display:block';
							$first = false;
						}
						$answer_markers['question'] = $question['question'];
						$answer_markers['answer'] = $this->pi_RTEcssText($question['answer']);
						$answer_markers['answer_content'] = '';
						$question_markers['list_answers'] .= $this->cObj->substituteMarkerArray($answers, $answer_markers, '###|###',$uppercase=1);
					}
					if(count($questions) == 1){
						$acc_markers['question_section'] .= $this->cObj->substituteMarkerArray($one_question, $question_markers, '###|###',$uppercase=1);
					} else {
						$acc_markers['question_section'] .= $this->cObj->substituteMarkerArray($question_selector, $question_markers, '###|###',$uppercase=1);
					}
					$markers['###ACCORDION_ELEMENTS###'] .= $this->cObj->substituteMarkerArray($accordion_element, $acc_markers, '###|###',$uppercase=1);
				}
				
			}
            if (!$conf['mobile']) {
                $zeitleserserviceCSS = empty($this->conf['zeitleserserviceCSS']) ? 'typo3conf/ext/zeit_leserservice/res/css/zeitleserservice.css' : $this->conf['zeitleserserviceCSS'];
                $GLOBALS['TSFE']->pSetup['includeCSS.'][$this->extKey] = $zeitleserserviceCSS;
                $jqueryUiJS = empty($this->conf['jqueryUiJS']) ? 'http://code.jquery.com/ui/1.10.3/jquery-ui.js' : $this->conf['jqueryUiJS'];
                $GLOBALS['TSFE']->pSetup['includeJS.']['jQueryUiNew'] = $jqueryUiJS;
                $GLOBALS['TSFE']->pSetup['includeJS.']['jQueryUiNew.']['external'] = 1;
                $zeitleserserviceJS = empty($this->conf['zeitleserserviceJS']) ? 'typo3conf/ext/zeit_leserservice/res/js/zeitleserservice.js' : $this->conf['zeitleserserviceJS'];
                $GLOBALS['TSFE']->pSetup['includeJSFooter.'][$this->extKey] = $zeitleserserviceJS;
            }
			
			$content = $this->cObj->substituteMarkerArray($accordion, $markers);
		}
			
		return $this->pi_wrapInBaseClass($content);
	}
	
	function collectCategories(){
		$catData = array();
		
		$selectConf = array();
		$selectConf['selectFields'] = '*';
		$selectConf['fromTable'] = 'tx_zeitleserservice_categories';
		$selectConf['where'] = ' 1 = 1';
		$selectConf['where'] .= $this->cObj->enableFields('tx_zeitleserservice_categories'); 
		$selectConf['groupBy'] = '';
		$selectConf['orderBy'] = 'sorting';
		$selectConf['limit'] = '';
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
		
		while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
			if ($GLOBALS['TSFE']->sys_language_content) {
				// prevent link targets from being changed in localized records
				$row = $GLOBALS['TSFE']->sys_page->getRecordOverlay('tx_zeitleserservice_categories', $row, $GLOBALS['TSFE']->sys_language_content, $GLOBALS['TSFE']->sys_language_contentOL, '');
			}
			if ($this->versioningEnabled) {
				// get workspaces Overlay
				$GLOBALS['TSFE']->sys_page->versionOL('tx_zeitleserservice_categories', $row);
			}
			$catData[] = $row;
		}
		
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
		
		return $catData;
	}
	
	function collectQuestions($cid = 0){
		$questionData = array();
		
		$selectConf = array();
		$selectConf['selectFields'] = '*';
		$selectConf['fromTable'] = 'tx_zeitleserservice_questions';
		if($cid != 0){
			$selectConf['where'] = ' category = '.$cid;
		} else {
			$selectConf['where'] = ' 1 = 1';
		}
		$selectConf['where'] .= $this->cObj->enableFields('tx_zeitleserservice_questions'); 
		$selectConf['groupBy'] = '';
		$selectConf['orderBy'] = 'sorting';
		$selectConf['limit'] = '';
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
		
		while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
			if ($GLOBALS['TSFE']->sys_language_content) {
				// prevent link targets from being changed in localized records
				$row = $GLOBALS['TSFE']->sys_page->getRecordOverlay('tx_zeitleserservice_questions', $row, $GLOBALS['TSFE']->sys_language_content, $GLOBALS['TSFE']->sys_language_contentOL, '');
			}
			if ($this->versioningEnabled) {
				// get workspaces Overlay
				$GLOBALS['TSFE']->sys_page->versionOL('tx_zeitleserservice_questions', $row);
			}
			$questionData[] = $row;
		}
		
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
		
		return $questionData;
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeit_leserservice/pi1/class.tx_zeitleserservice_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeit_leserservice/pi1/class.tx_zeitleserservice_pi1.php']);
}

?>