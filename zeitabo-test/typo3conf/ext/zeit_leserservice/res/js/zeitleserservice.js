$( document ).ready( function() {
	$(".acc-content").accordion({
		heightStyle: "content",
		animate: false,
		active: false,
		collapsible: true,
		beforeActivate: function( event, ui ) {
			var aOldSection = ui.oldPanel.find('.acc-contentsection');
			$(aOldSection).hide();
			var aNewSection = ui.newPanel.find('.acc-contentsection');
			$(aNewSection).hide();
		},
		activate: function( event, ui ) {
			var aSection = ui.newPanel.find('.acc-contentsection');
			$(aSection).show();
		}
	});
	
	$('.question-selector').change(function(){
		var answerId = $(this).val();
		var sectionId = $(this).attr('rel');
		if(answerId != undefined && sectionId != undefined){
			$('.questionsanswer-'+sectionId).hide();
			$('#answer-'+answerId).show();
		}
	});
});