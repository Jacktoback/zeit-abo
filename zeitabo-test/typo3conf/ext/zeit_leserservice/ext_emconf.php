<?php

########################################################################
# Extension Manager/Repository config file for ext "zeit_leserservice".
#
# Auto generated 11-07-2013 14:29
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'DIE ZEIT - Leserservice - Kontakt',
	'description' => 'DIE ZEIT - Leserservice - Kontakt',
	'category' => 'plugin',
	'author' => 'Jens Büchel',
	'author_email' => 'j.buechel@divine.de',
	'shy' => '',
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => 'uploads/tx_zeitleserservice/rte/',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
			'cms' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:18:{s:9:"ChangeLog";s:4:"be37";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"694b";s:14:"ext_tables.php";s:4:"3421";s:14:"ext_tables.sql";s:4:"8425";s:39:"icon_tx_zeitleserservice_categories.gif";s:4:"475a";s:38:"icon_tx_zeitleserservice_questions.gif";s:4:"475a";s:13:"locallang.xml";s:4:"1524";s:16:"locallang_db.xml";s:4:"7839";s:7:"tca.php";s:4:"8738";s:19:"doc/wizard_form.dat";s:4:"126d";s:20:"doc/wizard_form.html";s:4:"62f6";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:37:"pi1/class.tx_zeitleserservice_pi1.php";s:4:"88af";s:45:"pi1/class.tx_zeitleserservice_pi1_wizicon.php";s:4:"0349";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"9cc1";}',
);

?>