<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2013 Jens Büchel <j.buechel@divine.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'ZEIT Abo - Rich Snippet Integration' for the 'zeitabo_richsnipptes' extension.
 *
 * @author	Jens Büchel <j.buechel@divine.de>
 * @package	TYPO3
 * @subpackage	tx_zeitaborichsnipptes
 */
class tx_zeitaborichsnipptes_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_zeitaborichsnipptes_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_zeitaborichsnipptes_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'zeitabo_richsnipptes';	// The extension key.
	var $pi_checkCHash = true;
	
	public function fetchdata($url){
		$headers = array(
			'Accept: text/html',
			'Content-Type: text/html',
			'charset=utf-8'
		);
		
		$handle = curl_init();
		
		
		curl_setopt($handle, CURLOPT_HEADER, false);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
		
		curl_setopt($handle, CURLOPT_POST, false);
		
		curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($handle, CURLOPT_URL, $url);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($handle, CURLOPT_VERBOSE, 1);
		
		$response = curl_exec($handle);
		$code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		$data = array();
		$data['code'] = $code;
		$data['response'] = $response;

		return $data;
	}
	
	/**
	 * Initialize plugin and get configuration values.
	 *
	 * @param	array		$conf : configuration array from TS
	 */
	function init($conf) {
		// Store configuration
		$this->conf = $conf;
		
		// Loading language-labels
		$this->pi_loadLL();
		
		// Set default piVars from TS
		$this->pi_setPiVarDefaults();
		
		// Init and get the flexform data of the plugin
		$this->pi_initPIflexForm();
		
		// Assign the flexform data to a local variable for easier access
		$piFlexForm = $this->cObj->data['pi_flexform'];
		
		// Array for local configuration
		$this->lConf = array();
		// load available syslanguages
		$this->initLanguages();
		// sys_language_mode defines what to do if the requested translation is not found
		$this->sys_language_mode = $this->conf['sys_language_mode']?$this->conf['sys_language_mode'] : $GLOBALS['TSFE']->sys_language_mode;
		
		$this->sys_language_uid = $GLOBALS['TSFE']->config['config']['sys_language_uid'];    //Get site-language 
		
		if($this->sys_language_uid == ''){ 
			$this->sys_language_uid = 0; 
		}
	}
	
	/**
	 * fills the internal array '$this->langArr' with the available syslanguages
	 *
	 * @return	void
	 */
	function initLanguages () {
		
		$lres = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'sys_language',
			'1=1' . $this->cObj->enableFields('sys_language'));
		
		$this->langArr = array();
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($lres)) {
			$this->langArr[$row['uid']] = $row;
		}
	}
	
	function innerHTML( $contentdiv ) {
		$r = '';
		$elements = $contentdiv->childNodes;
			foreach( $elements as $element ) { 
				if ( $element->nodeType == XML_TEXT_NODE ) {
					$text = $element->nodeValue;
					$r .= $text;
				}	 
				elseif ( $element->nodeType == XML_COMMENT_NODE ) {
					$r .= '';
				}	 
				else {
					$r .= '<';
					$r .= $element->nodeName;
					if ( $element->hasAttributes() ) { 
						$attributes = $element->attributes;
						foreach ( $attributes as $attribute )
							$r .= " {$attribute->nodeName}='{$attribute->nodeValue}'" ;
					}	 
					$r .= '>';
					$r .= $this->innerHTML( $element );
					$r .= "</{$element->nodeName}>";
				}	 
			}	 
			return $r;
	}

	/**
	 * The selector for the Layout
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->init($conf);
		
		date_default_timezone_set('Europe/Berlin');
		$loc=setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'de_DE.UTF-8');
		
		$content = '';
		
		$data = $this->fetchdata('http://www.golocal.de/hamburg/sonstige-gewerbe/zeit-die-zeit-zeitverlag-gerd-bucerius-gmbh-co-kg-GO4w/');
		$getData = true;
		$ratingValue = '';
		//Daten von Go local abgreifen
		if($data['code'] == 200){

			$dom = new DOMDocument();
			$dom->loadHTML($data["response"]);
			$dom->preserveWhiteSpace = false;
			
			$detailRating = $dom->getElementById('detailRating');
			$ratingHTML = $this->innerHTML( $detailRating );
			if(isset($ratingHTML)){
				$totalRatings = intval(substr($ratingHTML,38,3));
			} else {
				$getData = false;
			}
			
			if($getData == true){
				$cluetipHTML = '';
				$metas = $detailRating->getElementsByTagName('meta');
				$metatag = '';
				foreach ($metas as $meta) {
					$metatag .= '<';
					$metatag .= $meta->nodeName;
					if ( $meta->hasAttributes() ) { 
						$attributes = $meta->attributes;
						foreach ( $attributes as $attribute )
							$metatag .= " {$attribute->nodeName}='{$attribute->nodeValue}'" ;
					}	 
					$metatag .= '>';				
				}
				
				$divs = $detailRating->getElementsByTagName('div');
				foreach ($divs as $div) {
					if($div->hasAttribute('data-rel')){
						$tooltipUrl = $div->getAttribute('data-rel');
						$ratingValue = $this->innerHTML( $div );
						if(isset($tooltipUrl) && $tooltipUrl != ''){
							$tooltip = $this->fetchdata('http://www.golocal.de'.$tooltipUrl);
							if($tooltip['code'] == 200){
								$dom2 = new DOMDocument();
								$dom2->loadHTML($tooltip["response"]);
								$dom2->preserveWhiteSpace = false;
								$cluetipHTML = $this->innerHTML( $dom2 );
							} else {
								$getData = false;
							}
						} else {
							$getData = false;
						}
					} 
				}
				
				if($getData == true){
					$content = '<div id="richsnippetcontent" itemtype="http://schema.org/LocalBusiness" itemscope="">';
					$content .= '<meta content="Zeitverlag Gerd Bucerius GmbH & Co. KG" itemprop="name">';
					$content .= '<div id="detailRating" class="left rating" itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating">';
					if($metatag != ''){
						$content .= $metatag;
					}
					if($ratingValue != ''){
						$starvalue = round($ratingValue);
						$content .= '<div class="stRtg star'.$starvalue.' addInfoTooltip" itemprop="ratingValue">'.$ratingValue.'</div>';
					} else {
						$content .= $ratingHTML;
					}
					$content .= '</div><div class="left"><h4>&nbsp;'.$totalRatings.' Bewertungen</h4></div>';
					$content .= '<div id="cluetip">';
					$content .= $cluetipHTML;
					$content .= '</div>';
					$content .= '</div>';
				}
			}
			
		} else {
			$getData = false;
		}
		//$getData = false;
		//Notlösung eigene Daten
		if($getData == false){
			
			$conf['star1'] = $conf['star1'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'star1', 'sForm');
			$conf['star2'] = $conf['star2'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'star2', 'sForm');
			$conf['star3'] = $conf['star3'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'star3', 'sForm');
			$conf['star4'] = $conf['star4'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'star4', 'sForm');
			$conf['star5'] = $conf['star5'] ? 0 : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'star5', 'sForm');
			
			$reviewCount = $conf['star1']+$conf['star2']+$conf['star3']+$conf['star4']+$conf['star5'];
			
			$ratingValueTotal = (($conf['star1']*1)+($conf['star2']*2)+($conf['star3']*3)+($conf['star4']*4)+($conf['star5']*5))/$reviewCount;
			
			$ratingValue = number_format($ratingValueTotal,5);
			
			$content = '<div id="richsnippetcontent">';
			$content .= '<div id="detailRating" class="left rating" itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating">';	
			$content .= '	<meta content="'.$reviewCount.'" itemprop="reviewCount">';
			$starvalue = round($ratingValue);
			$content .= '	<div class="stRtg star'.$starvalue.' addInfoTooltip" itemprop="ratingValue">'.$ratingValue.'</div>';
			$content .= '</div><div class="left"><h4>&nbsp;'.$reviewCount.' Bewertungen</h4></div>';
			$content .= '<div id="cluetip">';
			$content .= '<h3>'.$reviewCount.' Bewertungen</h3>
	
		<table>
			<colgroup>
				<col style="width: 110px"></col>
				<col style="width: 170px"></col>
			</colgroup>
			<tr>
				<td><div class="smlRtg star5" style="margin-right: 5px;">5</div></td>
				<td>
					<div class="ReviewDistribmeter ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="550" aria-valuenow="4"><div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="width: 80%;"></div></div>
				<span class="reviewDistCount">('.$conf['star5'].')</span></td>
				
			</tr><tr><td><div class="smlRtg star4" style="margin-right: 5px;">4</div></td>
				<td>
					<div class="ReviewDistribmeter ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="550" aria-valuenow="1"><div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="width: 20%;"></div></div>
				<span class="reviewDistCount">('.$conf['star4'].')</span></td>
			</tr><tr><td><div class="smlRtg star3" style="margin-right: 5px;">3</div></td>
				<td>
					<div class="ReviewDistribmeter ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="550" aria-valuenow="0"><div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="width: 0%;"></div></div>
				<span class="reviewDistCount">('.$conf['star3'].')</span></td>
			</tr><tr><td><div class="smlRtg star2" style="margin-right: 5px;">2</div></td>
				<td>
					<div class="ReviewDistribmeter ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="550" aria-valuenow="0"><div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="width: 0%;"></div></div>
				<span class="reviewDistCount">('.$conf['star2'].')</span></td>
			</tr><tr><td><div class="smlRtg star1" style="margin-right: 5px;">1</div></td>
				<td>
					<div class="ReviewDistribmeter ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="550" aria-valuenow="0"><div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="width: 0%;"></div></div>
				<span class="reviewDistCount">('.$conf['star1'].')</span></td>
			</tr></table>';
			$content .= '</div>';
			$content .= '</div>';
				
		}
		/*if(t3lib_div::_GP('type') == 857){
			$content = $this->saveRating($content, $conf);
			return $content;
		} else {
			$content = $this->getRating($content, $conf);
		
			
		}*/
		return $this->pi_wrapInBaseClass($content);
	}
	
	function getRating($content, $conf){
		if ($conf['templatefile']) {
			$this->template=$this->cObj->fileResource($conf['templatefile']);
		} else {
			$this->template=$this->cObj->fileResource('EXT:'.$this->extKey.'/res/rating.html');
		}
		
		$rating = $this->cObj->getSubpart($this->template,'###RATING###');
		
		$markers = array();
		$markers['###FORM_URL###'] = $this->pi_getPageLink($GLOBALS['TSFE']->id,'',array('type' => 857));
		$markers['###URL###'] = $GLOBALS['TSFE']->tmpl->setup['config.']['baseURL'].$this->pi_getPageLink($GLOBALS['TSFE']->id);
		
		$markers['###RATING_ACT1###'] = '';
		$markers['###RATING_ACT2###'] = '';
		$markers['###RATING_ACT3###'] = '';
		$markers['###RATING_ACT4###'] = '';
		$markers['###RATING_ACT5###'] = '';
		
		$ratingData = $this->getResults($GLOBALS['TSFE']->id);
		$total_points = 0;
		$votes = 0;
		$dec_avg = 0;
		$whole_avg = 0;
		$best = 0;
		
		if(count($ratingData) > 0){
			foreach($ratingData as $rd){
				$total_points +=$rd['rating'];
				if($rd['rating'] > $best){
					$best = $rd['rating'];
				}
				$votes++;
			}
			if($votes != 0){
				$dec_avg = round( $total_points / $votes, 1 );  
				$whole_avg = round( $dec_avg );
			}
		}
		for($k=1;$k<=$whole_avg;$k++){
			$markers['###RATING_ACT'.$k.'###'] = 'rating_act';
		}
		$markers['###AVERAGE###'] = $dec_avg;
		$markers['###BEST###'] = $best;
		$markers['###VOTES###'] = $votes;
		$markers['###PID###'] = $GLOBALS['TSFE']->id;
		
		$content = $this->cObj->substituteMarkerArray($rating, $markers);
		
		return $content;
	}
	
	function saveRating($content, $conf){
		$pid = t3lib_div::_GP('pid');
		
		if(isset($pid) && $pid != ''){
			preg_match('/star_([1-5]{1})/', t3lib_div::_GP('clicked_on'), $match);  
		    $vote = $match[1]; 
			$insertData = array(
							'pid'				=> $pid,
							'tstamp'			=> time(),
							'crdate'			=> time(),
							'sys_language_uid'	=> $this->sys_language_uid,
							'pageid'			=> $pid,
							'rating'			=> $vote,
							'ip'				=>  t3lib_div::getIndpEnv('REMOTE_ADDR'),
			);
			$insertComment = $GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_zeitaborichsnipptes_rating',$insertData);
			
			$ratingData = $this->getResults($pid);
			$total_points = 0;
			$votes = 0;
			$dec_avg = 0;
			$whole_avg = 0;
			$best = 0;
			if(count($ratingData) > 0){
				foreach($ratingData as $rd){
					$total_points +=$rd['rating'];
					if($rd['rating'] > $best){
						$best = $rd['rating'];
					}
					$votes++;
				}
				if($votes != 0){
					$dec_avg = round( $total_points / $votes, 1 );  
					$whole_avg = round( $dec_avg );
				}
			}
			$data = array();
			$data['pid'] = $pid;  
			$data['number_votes'] = $votes;  
			$data['best'] = $best;  
			$data['total_points'] = $total_points;  
			$data['dec_avg'] = $dec_avg;  
			$data['whole_avg'] = $whole_avg;  
			echo json_encode($data);  
		}
	}
	
	function getResults($pid){
		$ratingData = array();
		
		$selectConf = array();
		$selectConf['selectFields'] = '*';
		$selectConf['fromTable'] = 'tx_zeitaborichsnipptes_rating';
		$selectConf['where'] = ' pid ='.$pid;
		
		$selectConf['where'] .= $this->cObj->enableFields('tx_zeitaborichsnipptes_rating'); 
		$selectConf['groupBy'] = '';
		$selectConf['orderBy'] = '';
		$selectConf['limit'] = '';
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery($selectConf['selectFields'], $selectConf['fromTable'], $selectConf['where'], $selectConf['groupBy'], $selectConf['orderBy'], $selectConf['limit']);
		
		while (($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res))) {
			if ($GLOBALS['TSFE']->sys_language_content) {
				// prevent link targets from being changed in localized records
				$row = $GLOBALS['TSFE']->sys_page->getRecordOverlay('tx_zeitaborichsnipptes_rating', $row, $GLOBALS['TSFE']->sys_language_content, $GLOBALS['TSFE']->sys_language_contentOL, '');
			}
			
			$ratingData[] = $row;
		}
		
		$GLOBALS['TYPO3_DB']->sql_free_result($res);
		
		return $ratingData;
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabo_richsnipptes/pi1/class.tx_zeitaborichsnipptes_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabo_richsnipptes/pi1/class.tx_zeitaborichsnipptes_pi1.php']);
}

?>