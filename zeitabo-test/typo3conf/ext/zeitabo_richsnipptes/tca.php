<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA['tx_zeitaborichsnipptes_rating'] = array (
	'ctrl' => $TCA['tx_zeitaborichsnipptes_rating']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,pageid,ip,rating'
	),
	'feInterface' => $TCA['tx_zeitaborichsnipptes_rating']['feInterface'],
	'columns' => array (
		'sys_language_uid' => array (		
			'exclude' => 1,
			'label'  => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => array (
				'type'                => 'select',
				'foreign_table'       => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				)
			)
		),
		'l10n_parent' => array (		
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude'     => 1,
			'label'       => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config'      => array (
				'type'  => 'select',
				'items' => array (
					array('', 0),
				),
				'foreign_table'       => 'tx_zeitaborichsnipptes_rating',
				'foreign_table_where' => 'AND tx_zeitaborichsnipptes_rating.pid=###CURRENT_PID### AND tx_zeitaborichsnipptes_rating.sys_language_uid IN (-1,0)',
			)
		),
		'l10n_diffsource' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		'pageid' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_richsnipptes/locallang_db.xml:tx_zeitaborichsnipptes_rating.pageid',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'int,nospace',
			)
		),
		'ip' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_richsnipptes/locallang_db.xml:tx_zeitaborichsnipptes_rating.ip',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'trim',
			)
		),
		'rating' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:zeitabo_richsnipptes/locallang_db.xml:tx_zeitaborichsnipptes_rating.rating',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'int',
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, pageid, ip, rating')
	),
	'palettes' => array (
		'1' => array('showitem' => '')
	)
);
?>