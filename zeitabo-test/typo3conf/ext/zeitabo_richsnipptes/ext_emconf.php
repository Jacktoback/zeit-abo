<?php

########################################################################
# Extension Manager/Repository config file for ext "zeitabo_richsnipptes".
#
# Auto generated 18-04-2013 12:06
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'ZEIT Abo - Rich Snippet Integration',
	'description' => 'ZEIT Abo - Rich Snippet Integration',
	'category' => 'plugin',
	'author' => 'Jens Büchel',
	'author_email' => 'j.buechel@divine.de',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:17:{s:9:"ChangeLog";s:4:"3912";s:10:"README.txt";s:4:"9fa9";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"3367";s:14:"ext_tables.php";s:4:"55b3";s:14:"ext_tables.sql";s:4:"382b";s:38:"icon_tx_zeitaborichsnipptes_rating.gif";s:4:"475a";s:13:"locallang.xml";s:4:"f1e2";s:16:"locallang_db.xml";s:4:"144c";s:7:"tca.php";s:4:"2ef5";s:19:"doc/wizard_form.dat";s:4:"3738";s:20:"doc/wizard_form.html";s:4:"c796";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:40:"pi1/class.tx_zeitaborichsnipptes_pi1.php";s:4:"4f89";s:48:"pi1/class.tx_zeitaborichsnipptes_pi1_wizicon.php";s:4:"c228";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"58e5";}',
);

?>