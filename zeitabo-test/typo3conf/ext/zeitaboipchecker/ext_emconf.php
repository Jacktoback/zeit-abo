<?php

########################################################################
# Extension Manager/Repository config file for ext "zeitaboipchecker".
#
# Auto generated 05-10-2012 14:23
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'DIE ZEIT Abo-Shop IP-Checker',
	'description' => 'IP-basierte Content-Anzeige anhand des Landes',
	'category' => 'plugin',
	'author' => 'Jens Büchel',
	'author_email' => 'j.buechel@divine.de',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:14:{s:9:"ChangeLog";s:4:"1ea1";s:10:"README.txt";s:4:"9fa9";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"9caf";s:14:"ext_tables.php";s:4:"583f";s:13:"locallang.xml";s:4:"a98b";s:16:"locallang_db.xml";s:4:"2b57";s:19:"doc/wizard_form.dat";s:4:"f5e8";s:20:"doc/wizard_form.html";s:4:"568d";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:34:"pi1/class.tx_zeitaboipchecker_pi1.php";s:4:"b016";s:42:"pi1/class.tx_zeitaboipchecker_pi1_wizicon.php";s:4:"522f";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"9080";}',
);

?>