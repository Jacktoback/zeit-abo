<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Jens Büchel <j.buechel@divine.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'DIE ZEIT Abo-Shop IP-Checked Content' for the 'zeitaboipchecker' extension.
 *
 * @author	Jens Büchel <j.buechel@divine.de>
 * @package	TYPO3
 * @subpackage	tx_zeitaboipchecker
 */
class tx_zeitaboipchecker_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_zeitaboipchecker_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_zeitaboipchecker_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'zeitaboipchecker';	// The extension key.
	var $pi_checkCHash = true;
	
	/**
	 * Initialize plugin and get configuration values.
	 *
	 * @param	array		$conf : configuration array from TS
	 */
	function init($conf) {
		// Store configuration
		$this->conf = $conf;

		// Loading language-labels
		$this->pi_loadLL();

		// Set default piVars from TS
		$this->pi_setPiVarDefaults();

		// Init and get the flexform data of the plugin
		$this->pi_initPIflexForm();

		// Assign the flexform data to a local variable for easier access
		$piFlexForm = $this->cObj->data['pi_flexform'];

		// Array for local configuration
		$this->lConf = array();
		// load available syslanguages
		$this->initLanguages();
		// sys_language_mode defines what to do if the requested translation is not found
		$this->sys_language_mode = $this->conf['sys_language_mode']?$this->conf['sys_language_mode'] : $GLOBALS['TSFE']->sys_language_mode;
		
		$this->sys_language_uid = $GLOBALS['TSFE']->config['config']['sys_language_uid'];    //Get site-language 
         
        if($this->sys_language_uid == ''){ 
            $this->sys_language_uid = 0; 
        }
	}
	
	/**
	 * fills the internal array '$this->langArr' with the available syslanguages
	 *
	 * @return	void
	 */
	function initLanguages () {

		$lres = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'*',
			'sys_language',
			'1=1' . $this->cObj->enableFields('sys_language'));


		$this->langArr = array();
		while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($lres)) {
			$this->langArr[$row['uid']] = $row;
		}
	}
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->init($conf);
		
	
		$conf['CountryCode'] = $conf['CountryCode'] ? $conf['CountryCode'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'CountryCode', 'sForm');
		$conf['ContentCountry'] = $conf['ContentCountry'] ? $conf['ContentCountry'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'ContentCountry', 'sForm');
		$conf['ContentCountrySecondVersion'] = $conf['ContentCountrySecondVersion'] ? $conf['ContentCountrySecondVersion'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'ContentCountrySecondVersion', 'sForm');
		$conf['ContentNoCountry'] = $conf['ContentNoCountry'] ? $conf['ContentNoCountry'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'ContentNoCountry', 'sForm');
		$conf['GaGroup1'] = $conf['GaGroup1'] ? $conf['GaGroup1'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'GaGroup1', 'sForm');
		$conf['GaCode1'] = $conf['GaCode1'] ? $conf['GaCode1'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'GaCode1', 'sForm');
		$conf['GaGroup2'] = $conf['GaGroup2'] ? $conf['GaGroup2'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'GaGroup2', 'sForm');
		$conf['GaCode2'] = $conf['GaCode2'] ? $conf['GaCode2'] : $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'GaCode2', 'sForm');
		
	
		$content='';
		
		$countryUser = $this->ipChecker();
		if(!empty($this->piVars['ccheck'])){
			$countryUser = $this->piVars['ccheck'];
		}

		if(isset($countryUser) && $countryUser == $conf['CountryCode']){
			/* Test ist deaktiviert 
			$group = 1;
			if (isset($_COOKIE['GaGroup1']) && $conf['ContentCountry'] != '') {
				$group = 1;
				$content = $this->getIPContent($conf['ContentCountry']);
			} elseif(isset($_COOKIE['GaGroup2']) && $conf['ContentCountrySecondVersion'] != '') {
				$group = 2;
				$content = $this->getIPContent($conf['ContentCountrySecondVersion']);
			} else {
				$zufall = rand(1,100);	
				if($zufall <=50){
					$group = 1;
					setcookie("GaGroup1","1",time()+60*60*24*7);
					$content = $this->getIPContent($conf['ContentCountry']);
				} else {
					$group = 2;
					setcookie("GaGroup2","1",time()+60*60*24*7);
					$content = $this->getIPContent($conf['ContentCountrySecondVersion']);
				}
			}
			
			
			if(isset($conf['GaCode'.$group]) && $conf['GaCode'.$group] != ''){
				$gaJS = '
				<script type="text/javascript">
				<!--
				'.$conf['GaCode'.$group].'
				//-->
				</script>
				' . chr(10);
				$GLOBALS['TSFE']->additionalHeaderData[$this->extKey]  = $gaJS;
			}
			*/
			$content = $this->getIPContent($conf['ContentCountry']);
		} else {
			$content = $this->getIPContent($conf['ContentNoCountry']);
			#$content = $this->getIPContent($conf['ContentCountry']);
		}
		
		return $this->pi_wrapInBaseClass($content);
	}
	
	
	function ipChecker(){
		if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		} else {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		$license_key = 'WpSJqTq0GtDr';
		$query = "http://geoip.maxmind.com/a?l=" . $license_key . "&i=" . $ipaddress;
		$url = parse_url($query);
		$host = $url["host"];
		$path = $url["path"] . "?" . $url["query"];
		$timeout = 1;
		$fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
		if ($fp) {
		  fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
		  while (!feof($fp)) {
			$buf .= fgets($fp, 128);
		  }
		  $lines = explode("\n", $buf);
		  $data = $lines[count($lines)-1];
		  fclose($fp);
		} else {
		  return false;
		}
		return $data;
	}
	/**
	 * Render the IP based Content 
	 *
	 * @param	integer		$contentUid: The tt_content uid
	 * @return	The ip based content that is displayed on the website
	 */
	function getIPContent($cUid) {
		$tcontent = '';
		$markers = array();
		if($cUid){
			$ipcontent = array('tables' => 'tt_content','source' => $cUid,'dontCheckPid' => 1);
			$tcontent = $this->cObj->RECORDS($ipcontent); 
		}	
		
		return $tcontent;
		
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitaboipchecker/pi1/class.tx_zeitaboipchecker_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitaboipchecker/pi1/class.tx_zeitaboipchecker_pi1.php']);
}

?>