<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "zeitabotopteaser".
 *
 * Auto generated 22-04-2014 16:25
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Zeitabo Topteaser',
	'description' => 'Zeitabo Topteaser',
	'category' => 'plugin',
	'author' => 'Jens Büchel',
	'author_email' => 'j.buechel@divine.de',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:14:{s:9:"ChangeLog";s:4:"046c";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"124b";s:14:"ext_tables.php";s:4:"6b31";s:13:"locallang.xml";s:4:"dcf2";s:16:"locallang_db.xml";s:4:"f829";s:10:"README.txt";s:4:"ee2d";s:19:"doc/wizard_form.dat";s:4:"edbd";s:20:"doc/wizard_form.html";s:4:"7b42";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:37:"pi1/class.tx_zeitabotopteaser_pi1.php";s:4:"1622";s:45:"pi1/class.tx_zeitabotopteaser_pi1_wizicon.php";s:4:"595a";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"e306";}',
);

?>