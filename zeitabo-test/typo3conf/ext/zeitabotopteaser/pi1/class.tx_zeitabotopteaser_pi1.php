<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2014 Jens Büchel <j.buechel@divine.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');




/**
 * Plugin 'Zeitabo Topteaser' for the 'zeitabotopteaser' extension.
 *
 * @author	Jens Büchel <j.buechel@divine.de>
 * @package	TYPO3
 * @subpackage	tx_zeitabotopteaser
 */
class tx_zeitabotopteaser_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_zeitabotopteaser_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_zeitabotopteaser_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'zeitabotopteaser';	// The extension key.
	var $pi_checkCHash = true;
	
	/**
	 * The main method of the PlugIn
	 *36103

    License key

    WpSJqTq0GtDr
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();

        $content = '';
        $city = '';

        $username = '36103';
        $password = 'WpSJqTq0GtDr';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //$ip = '87.155.61.32';
        $query = 'https://geoip.maxmind.com/geoip/v2.0/city/'.$ip;



        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_HTTPHEADER  =>  array(
                'Content-Type: application/vnd.maxmind.com-city+json; charset=UTF-8; version=2.0',
                'Connection: Keep-Alive'
                ),
                CURLOPT_URL => $query,
                CURLOPT_USERAGENT => 'MaxMind',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERPWD => $username . ":" . $password
            )
        );

        $resp = curl_exec($curl);
        $result = json_decode($resp);
        $DACH = 0;
        if(!empty($result)){
            if(is_object($result->city)){
                if(is_object($result->city->names)){
                    if(!empty($result->city->names->de)){
                        $city = $result->city->names->de;
                    }
                }
            }
            if(is_object($result->country)){
                if(!empty($result->country->iso_code)){
                    $isocode = $result->country->iso_code;
                    if($isocode == 'DE' || $isocode == 'AT' || $isocode == 'CH'){
                        $DACH = 1;
                    }
                }
            } elseif(is_object($result->registered_country)){
                if(!empty($result->registered_country->iso_code)){
                    $isocode = $result->registered_country->iso_code;
                    if($isocode == 'DE' || $isocode == 'AT' || $isocode == 'CH'){
                        $DACH = 1;
                    }
                }
            }
        }

        if($city == 'Hamburg'){
            $content='<div style="position:absolute; right:-40px; top:-167px; z-index:100; width: 326px;">
                    <img width="250" height="106" border="0" alt="" src="http://www.zeitabo.de/uploads/pics/DZ-Hamburg-Seiten_ab-jetzt.png">
                  </div>';
        } elseif( $DACH == 1){
            $content = '<div style="position:absolute; right:0; top:-195px; z-index:100;">
            <a href="http://www.zeitabo.de/die-zeit-im-abo/probe-abo.html?selection=316" class="email"><img border="0" alt="" src="http://www.zeitabo.de/fileadmin/templates_new/img/Archiv_DVD.png" /></a>
            </div>';
            $content = '';
        }  else {
            $content = '<div style="position:absolute; right:0px; top:-195px; z-index:100;">
            <a href="http://www.zeitabo.de/die-zeit-im-abo/digital-abo.html" class="email"><img border="0" alt="" src="http://www.zeitabo.de/uploads/pics/Teaser_Digitalpaket_20130723.png" /></a>
            </div>';

        }
	
		return $this->pi_wrapInBaseClass($content);
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabotopteaser/pi1/class.tx_zeitabotopteaser_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitabotopteaser/pi1/class.tx_zeitabotopteaser_pi1.php']);
}

?>