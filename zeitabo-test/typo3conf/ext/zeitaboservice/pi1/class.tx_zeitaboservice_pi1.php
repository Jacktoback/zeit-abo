<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2007 Patrick Bisplinghoff <patrick.bisplinghoff@clicktivities.net>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'DIE ZEIT Customer services' for the 'zeitaboservice' extension.
 *
 * @author	Patrick Bisplinghoff <patrick.bisplinghoff@clicktivities.net>
 * @package	TYPO3
 * @subpackage	tx_zeitaboservice
 */
class tx_zeitaboservice_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_zeitaboservice_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_zeitaboservice_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'zeitaboservice';	// The extension key.
	var $pi_checkCHash = true;
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content,$conf)	{
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		$this->pi_initPIFlexForm();
		#t3lib_div::debug($this->conf);
		$content = '';
		
		# set marker array
		$this->marker = array();
		
		# countries
		$this->marker['###COUNTRIES###'] = '';
		if($this->conf['markers.']['countries']) {
			$this->marker['###COUNTRIES###'] = $this->cObj->PHP_SCRIPT($this->conf['markers.']['countries.']);
		}
		
		# mandatory fields
		$this->marker['###M_1###'] = '*';
		$this->marker['###M_2###'] = '*';
		
		# display 
		$this->marker['###DISPLAY_ABO_NR###'] = 'block';
		
		# set error
		$this->formError = false;
		$this->checkAddress2 = true;
		
		# get template
		$this->template = $this->cObj->fileResource('typo3conf/ext/zeitaboservice/pi1/template.html');
		
		# select template subpart
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_CSS###');
		
		# replace markers with marker values
		$content .= $this->cObj->substituteMarkerArray($subpart, array());
		
		# get display
		$displaySelect = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'serviceselect', 'service');
		
		switch($displaySelect) {
			
			case 'nachsendung':
				$this->thanksPage = $this->conf['thanks.']['urlaub'];
				$this->errorPage = $this->conf['error.']['service'];
				$this->mailSubject = 'URLAUB: Nachsendung';
				$this->mailHL1 = 'HEIMATANSCHRIFT:';
				$this->mailHL2 = 'URLAUBSANSCHRIFT:';
				$this->marker['###HEADLINE_1###'] = 'h_heimatanschrift.gif';
				$this->marker['###HEADLINE_2###'] = 'h_urlaubsanschrift.gif';
				$this->marker['###DISPLAY_ADDRESS_2###'] = 'block';
				$this->displayNachsendung($content, $conf);				
			break;	
			
			case 'patenschaft':
				$this->thanksPage = $this->conf['thanks.']['urlaub'];
				$this->errorPage = $this->conf['error.']['service'];
				$this->mailSubject = 'URLAUB: Patenschaft';
				$this->mailHL1 = 'HEIMATANSCHRIFT:';
				$this->mailHL2 = 'ANSCHRIFT DES PATEN:';
				$this->marker['###HEADLINE_1###'] = 'h_meine_anschrift.gif';
				$this->marker['###HEADLINE_2###'] = 'h_anschrift_freunde.gif';
				$this->marker['###DISPLAY_ADDRESS_2###'] = 'block';
				$this->displayPatenschaft($content, $conf);
			break;
			
			case 'lieferunterbrechung':
				$this->thanksPage = $this->conf['thanks.']['urlaub'];
				$this->errorPage = $this->conf['error.']['service'];
				$this->checkAddress2 = false;
				$this->mailSubject = 'URLAUB: Lieferunterbrechung';
				$this->mailHL1 = 'HEIMATANSCHRIFT:';
				$this->mailHL2 = '';
				$this->marker['###HEADLINE_1###'] = 'h_anschrift.gif';
				$this->marker['###HEADLINE_2###'] = 'trans.gif';
				$this->marker['###DISPLAY_ADDRESS_2###'] = 'none';
				$this->displayLieferunterbrechung($content, $conf);
			break;
			
			case 'zustellreklamation':
				$this->thanksPage = $this->conf['thanks.']['reklamation'];
				$this->errorPage = $this->conf['error.']['service'];
				$this->checkAddress2 = false;
				$this->mailSubject = 'ZUSTELLREKLAMATION';
				$this->mailHL1 = 'KUNDENDATEN:';
				$this->mailHL2 = '';
				$this->marker['###HEADLINE_1###'] = 'h_anschrift.gif';
				$this->marker['###HEADLINE_2###'] = 'trans.gif';
				$this->marker['###DISPLAY_ADDRESS_2###'] = 'none';
				$this->displayZustellreklamation($content, $conf);
			break;
			
			case 'umzugsservice':
				$this->thanksPage = $this->conf['thanks.']['umzug'];
				$this->errorPage = $this->conf['error.']['service'];
				$this->mailSubject = 'UMZUGSSERVICE';
				$this->mailHL1 = 'ALTE ANSCHRIFT:';
				$this->mailHL2 = 'NEUE ANSCHRIFT';
				$this->marker['###HEADLINE_1###'] = 'h_alte_anschrift2.gif';
				$this->marker['###HEADLINE_2###'] = 'h_neue_anschrift2.gif';
				$this->marker['###DISPLAY_ADDRESS_2###'] = 'block';
				$this->displayUmzugsservice($content, $conf);
			break;
			
			case 'singlezeit':
				$this->thanksPage = $this->conf['thanks.']['einzelheft'];
				$this->errorPage = $this->conf['error.']['service'];
				$this->marker['###M_1###'] = '';
				$this->marker['###DISPLAY_ABO_NR###'] = 'none';
				$this->checkAddress2 = false;
				$this->mailSubject = 'Einzelheftbestellung DIE ZEIT';
				$this->mailHL1 = 'KUNDENDATEN:';
				$this->mailHL2 = '';
				$this->marker['###HEADLINE_1###'] = 'h_anschrift.gif';
				$this->marker['###HEADLINE_2###'] = 'trans.gif';
				$this->marker['###DISPLAY_ADDRESS_2###'] = 'none';
				$this->displaySingleZeit($content, $conf);
			break;	
		}
		
			
		$this->marker['###KUNDENNR###'] = htmlspecialchars($this->piVars['kundennr']);
		$this->marker['###ANREDE_1###'] = ($this->piVars['anrede'] == 'Herr') ? 'checked' : '';
		$this->marker['###ANREDE_2###'] = ($this->piVars['anrede'] == 'Frau') ? 'checked' : '';
		$this->marker['###FIRMA###'] = htmlspecialchars($this->piVars['firma']);
		$this->marker['###VORNAME###'] = htmlspecialchars($this->piVars['vorname']);
		$this->marker['###NACHNAME###'] = htmlspecialchars($this->piVars['nachname']);
		$this->marker['###STRASSENR###'] = htmlspecialchars($this->piVars['strassenr']);
		$this->marker['###ZUSATZ###'] = htmlspecialchars($this->piVars['zusatz']);
		$this->marker['###PLZ###'] = htmlspecialchars($this->piVars['plz']);		
		$this->marker['###ORT###'] = htmlspecialchars($this->piVars['ort']);		
		$this->marker['###LAND###'] = htmlspecialchars($this->piVars['land']);
		$this->marker['###VORWAHL###'] = htmlspecialchars($this->piVars['vorwahl']);
		$this->marker['###RUFNUMMER###'] = htmlspecialchars($this->piVars['rufnummer']);
		$this->marker['###FAXVORWAHL###'] = htmlspecialchars($this->piVars['faxvorwahl']);
		$this->marker['###FAXNUMMER###'] = htmlspecialchars($this->piVars['faxnummer']);
		$this->marker['###EMAIL###'] = htmlspecialchars($this->piVars['email']);
		
		$this->marker['###ANREDE2_1###'] = ($this->piVars['anrede2'] == 'Herr') ? 'checked' : '';
		$this->marker['###ANREDE2_2###'] = ($this->piVars['anrede2'] == 'Frau') ? 'checked' : '';
		$this->marker['###FIRMA2###'] = htmlspecialchars($this->piVars['firma2']);
		$this->marker['###VORNAME2###'] = htmlspecialchars($this->piVars['vorname2']);
		$this->marker['###NACHNAME2###'] = htmlspecialchars($this->piVars['nachname2']);
		$this->marker['###STRASSENR2###'] = htmlspecialchars($this->piVars['strassenr2']);
		$this->marker['###ZUSATZ2###'] = htmlspecialchars($this->piVars['zusatz2']);
		$this->marker['###PLZ2###'] = htmlspecialchars($this->piVars['plz2']);		
		$this->marker['###ORT2###'] = htmlspecialchars($this->piVars['ort2']);		
		$this->marker['###LAND2###'] = htmlspecialchars($this->piVars['land2']);
		$this->marker['###VORWAHL2###'] = htmlspecialchars($this->piVars['vorwahl2']);
		$this->marker['###RUFNUMMER2###'] = htmlspecialchars($this->piVars['rufnummer2']);
		$this->marker['###FAXVORWAHL2###'] = htmlspecialchars($this->piVars['faxvorwahl2']);
		$this->marker['###FAXNUMMER2###'] = htmlspecialchars($this->piVars['faxnummer2']);
		$this->marker['###EMAIL2###'] = htmlspecialchars($this->piVars['email2']);
		
		$this->marker['###MSG_ERROR###'] = 'none';
		$this->marker['###NAMEN_ERROR###'] = '';
		$this->marker['###STRASSENR_ERROR###'] = '';
		$this->marker['###PLZORT_ERROR###'] = '';
		$this->marker['###EMAIL_ERROR###'] = '';
		$this->marker['###NAMEN2_ERROR###'] = '';
		$this->marker['###STRASSENR2_ERROR###'] = '';
		$this->marker['###PLZORT2_ERROR###'] = '';
		
		if($this->piVars['sendmail'] == 'true') {
			if($this->mailSubject != 'Einzelheftbestellung DIE ZEIT') {
				if($this->piVars['vorname'] == '' || $this->piVars['nachname'] == '') {
					$this->marker['###NAMEN_ERROR###'] = 'error';
					$this->formError = true;
				}
				if($this->piVars['strassenr'] == '') {
					$this->marker['###STRASSENR_ERROR###'] = 'error';
					$this->formError = true;
				}
				if($this->piVars['plz'] == '' || $this->piVars['ort'] == '') {
					$this->marker['###PLZORT_ERROR###'] = 'error';
					$this->formError = true;
				}
			}
			if(!$this->checkEmail($this->piVars['email'])) {
				$this->marker['###EMAIL_ERROR###'] = 'error';
				$this->formError = true;
			}
			if($this->checkAddress2) {
				if($this->piVars['vorname2'] == '' || $this->piVars['nachname2'] == '') {
					$this->marker['###NAMEN2_ERROR###'] = 'error';
					$this->formError = true;
				}
				if($this->piVars['strassenr2'] == '') {
					$this->marker['###STRASSENR2_ERROR###'] = 'error';
					$this->formError = true;
				}
				if($this->piVars['plz2'] == '' || $this->piVars['ort2'] == '') {
					$this->marker['###PLZORT2_ERROR###'] = 'error';
					$this->formError = true;
				}
			}
			if($this->formError) {
				$this->marker['###MSG_ERROR###'] = 'block';
			}
			else {
				return $this->sendHolidayMail($content, $conf);
			}
		}
		
		# select template subpart
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE###');
		
		# replace markers with marker values
		$content .= $this->cObj->substituteMarkerArray($subpart, $this->marker);
		
		return $content;
	}
		
	
	
	function displayNachsendung($content, $conf) {
				
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_HEAD_1###');
		$this->marker['###SERVICE_HEAD###'] = $this->cObj->substituteMarkerArray($subpart, array());
		
		$this->marker['###VON###'] = htmlspecialchars($this->piVars['von']);
		$this->marker['###BIS###'] = htmlspecialchars($this->piVars['bis']);
		
		$this->marker['###VON_ERROR###'] = '';
		$this->marker['###BIS_ERROR###'] = '';
		
		if($this->piVars['sendmail'] == 'true') {
			if($this->piVars['von'] == '') {
				$this->marker['###VON_ERROR###'] = 'error';
				$this->formError = true;
			}
			if($this->piVars['bis'] == '') {
				$this->marker['###BIS_ERROR###'] = 'error';
				$this->formError = true;
			}
		}
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_DATE_1###');
		$this->marker['###SERVICE_DATE###'] = $this->cObj->substituteMarkerArray($subpart, $this->marker);
	}
	
	
	
	function displayPatenschaft($content, $conf) {
		
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_HEAD_2###');
		$this->marker['###SERVICE_HEAD###'] = $this->cObj->substituteMarkerArray($subpart, array());
		
		$this->marker['###AUSWAHL_1###'] = ($this->piVars['auswahl'] == 'Freunde/Verwandte') ? 'checked' : '';
		$this->marker['###AUSWAHL_2###'] = ($this->piVars['auswahl'] == 'Organisationen') ? 'checked' : '';
		
		if($this->piVars['auswahl'] == 'Organisationen') {
			$this->checkAddress2 = false;
		}
		
		$this->marker['###VON###'] = htmlspecialchars($this->piVars['von']);
		$this->marker['###BIS###'] = htmlspecialchars($this->piVars['bis']);
		
		$this->marker['###VON_ERROR###'] = '';
		$this->marker['###BIS_ERROR###'] = '';
		
		if($this->piVars['sendmail'] == 'true') {
			if($this->piVars['auswahl'] == '') {
				$this->marker['###AUSWAHL_ERROR###'] = 'error';
				$this->formError = true;
			}
			if($this->piVars['von'] == '') {
				$this->marker['###VON_ERROR###'] = 'error';
				$this->formError = true;
			}
			if($this->piVars['bis'] == '') {
				$this->marker['###BIS_ERROR###'] = 'error';
				$this->formError = true;
			}
		}
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_DATE_2###');
		$this->marker['###SERVICE_DATE###'] = $this->cObj->substituteMarkerArray($subpart, $this->marker);
	}
	
	
	
	function displayLieferunterbrechung($content, $conf) {
				
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_HEAD_3###');
		$this->marker['###SERVICE_HEAD###'] = $this->cObj->substituteMarkerArray($subpart, array());
		
		$this->marker['###VON###'] = htmlspecialchars($this->piVars['von']);
		$this->marker['###BIS###'] = htmlspecialchars($this->piVars['bis']);
		
		$this->marker['###VON_ERROR###'] = '';
		$this->marker['###BIS_ERROR###'] = '';
		
		if($this->piVars['sendmail'] == 'true') {
			if($this->piVars['von'] == '') {
				$this->marker['###VON_ERROR###'] = 'error';
				$this->formError = true;
			}
			if($this->piVars['bis'] == '') {
				$this->marker['###BIS_ERROR###'] = 'error';
				$this->formError = true;
			}
		}
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_DATE_3###');
		$this->marker['###SERVICE_DATE###'] = $this->cObj->substituteMarkerArray($subpart, $this->marker);
	}
	
	
	
	function displayZustellreklamation($content, $conf) {
		
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_HEAD_4###');
		$this->marker['###SERVICE_HEAD###'] = $this->cObj->substituteMarkerArray($subpart, array());
		
		$this->marker['###TEXT###'] = htmlspecialchars($this->piVars['text']);
				
		$this->marker['###TEXT_ERROR###'] = 'none';
				
		if($this->piVars['sendmail'] == 'true') {
			if($this->piVars['text'] == '') {
				$this->marker['###TEXT_ERROR###'] = 'error';
				$this->formError = true;
			}			
		}
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_DATE_4###');
		$this->marker['###SERVICE_DATE###'] = $this->cObj->substituteMarkerArray($subpart, $this->marker);
	}
	
	
	
	function displayUmzugsservice($content, $conf) {
				
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_HEAD_5###');
		$this->marker['###SERVICE_HEAD###'] = $this->cObj->substituteMarkerArray($subpart, array());
		
		$this->marker['###VON###'] = htmlspecialchars($this->piVars['von']);
				
		$this->marker['###VON_ERROR###'] = '';
				
		if($this->piVars['sendmail'] == 'true') {
			if($this->piVars['von'] == '') {
				$this->marker['###VON_ERROR###'] = 'error';
				$this->formError = true;
			}			
		}
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_DATE_5###');
		$this->marker['###SERVICE_DATE###'] = $this->cObj->substituteMarkerArray($subpart, $this->marker);
	}
	
	
	
	function displaySingleZeit($content, $conf) {
		
		$arrMarker = array();		
		$arrMarker['###SINGLE_IMAGE###'] = $this->getAboImage($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_img', 'singlezeit'));
		$arrMarker['###SINGLE_HEADLINE###'] = $this->getAboHeadline($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_headline', 'singlezeit'));
		$arrMarker['###SINGLE_TEXT###'] = $this->pi_RTEcssText($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'abo_txt', 'singlezeit'));
				
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_HEAD_6###');
		$this->marker['###SERVICE_HEAD###'] = $this->cObj->substituteMarkerArray($subpart, $arrMarker);
		
		$this->marker['###TEXT###'] = htmlspecialchars($this->piVars['text']);
				
		$this->marker['###TEXT_ERROR###'] = 'none';
				
		if($this->piVars['sendmail'] == 'true') {
			if($this->piVars['text'] == '') {
				$this->marker['###TEXT_ERROR###'] = 'error';
				$this->formError = true;
			}			
		}
		$subpart = $this->cObj->getSubpart($this->template, '###SERVICE_DATE_6###');
		$this->marker['###SERVICE_DATE###'] = $this->cObj->substituteMarkerArray($subpart, $this->marker);
	}
	
	
	
	function sendHolidayMail($content, $conf) {
		# init mail
		$holidayMail = t3lib_div::makeInstance(t3lib_htmlmail); 
		
		$mailData = array();
		foreach($this->piVars as $key => $value) {
			$this->piVars[$key] = utf8_decode($value);
		}
		#t3lib_div::debug($this->piVars);
		
		$holidayMail->add_header('From: DIE ZEIT-Leser-Service <abo@zeit.de>');
		
		$holidayMail->recipient = htmlspecialchars($this->piVars['email']);
		
		$holidayMail->recipient_copy = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'email', 'service');
		#$holidayMail->recipient_copy = 'patrick.bisplinghoff@clicktivities.net';
		
		$holidayMail->subject = 'DIE ZEIT Leser-Service: '.$this->mailSubject;
		
		$holidayMail->add_message('Sehr geehrte(r) '.$this->piVars['anrede'].' '.$this->piVars['nachname'].',');
		$holidayMail->add_message('');
		$holidayMail->add_message('Ihr Auftrag ist bei unserem Kunden-Service eingegangen und wird nach Ihren W�nschen bearbeitet.');
		$holidayMail->add_message('');
		$holidayMail->add_message('Ihre Daten wurden von uns wie folgt aufgenommen:');
		$holidayMail->add_message('');
		
		$holidayMail->add_message('');
		$holidayMail->add_message($this->mailSubject);
		if($this->piVars['auswahl'] != '') {
			$holidayMail->add_message('Auswahl: '.$this->piVars['auswahl']);
		}
		
		if($this->mailSubject == 'ZUSTELLREKLAMATION' || $this->mailSubject == 'Einzelheftbestellung DIE ZEIT') {
			$holidayMail->add_message('');
			$holidayMail->add_message(htmlspecialchars($this->piVars['text']));
		}
		else {
			$holidayMail->add_message('Von: '.htmlspecialchars($this->piVars['von']));
			if($this->mailSubject != 'UMZUGSSERVICE') {
				$holidayMail->add_message('bis: '.htmlspecialchars($this->piVars['bis']));
			}
		}
		$holidayMail->add_message('');
		$holidayMail->add_message($this->mailHL1);
		$holidayMail->add_message('');
		$holidayMail->add_message('Kunden-/Abo-Nr.: '.htmlspecialchars($this->piVars['kundennr']));
		$holidayMail->add_message('Anrede: '.$this->piVars['anrede']);
		$holidayMail->add_message('Vorname: '.htmlspecialchars($this->piVars['vorname']));
		$holidayMail->add_message('Nachname: '.htmlspecialchars($this->piVars['nachname']));
		$holidayMail->add_message('Firma: '.htmlspecialchars($this->piVars['firma']));
		$holidayMail->add_message('Strasse: '.htmlspecialchars($this->piVars['strassenr']));
		$holidayMail->add_message('Adresszusatz: '.htmlspecialchars($this->piVars['zusatz']));
		$holidayMail->add_message('PLZ: '.htmlspecialchars($this->piVars['plz']));
		$holidayMail->add_message('Ort: '.htmlspecialchars($this->piVars['ort']));
		$holidayMail->add_message('Land: '.htmlspecialchars($this->piVars['land']));
		$holidayMail->add_message('Telefon: '.htmlspecialchars($this->piVars['vorwahl']).' - '.htmlspecialchars($this->piVars['rufnummer']));
		$holidayMail->add_message('Fax: '.htmlspecialchars($this->piVars['faxvorwahl']).' - '.htmlspecialchars($this->piVars['faxnummer']));
		$holidayMail->add_message('E-Mail: '.htmlspecialchars($this->piVars['email']));
		$holidayMail->add_message('');
		
		if($this->checkAddress2) {
			$holidayMail->add_message($this->mailHL2);
			$holidayMail->add_message('');
			$holidayMail->add_message('Anrede: '.$this->piVars['anrede2']);
			$holidayMail->add_message('Vorname: '.htmlspecialchars($this->piVars['vorname2']));
			$holidayMail->add_message('Nachname: '.htmlspecialchars($this->piVars['nachname2']));
			$holidayMail->add_message('Firma: '.htmlspecialchars($this->piVars['firma2']));
			$holidayMail->add_message('Strasse: '.htmlspecialchars($this->piVars['strassenr2']));
			$holidayMail->add_message('Adresszusatz: '.htmlspecialchars($this->piVars['zusatz2']));
			$holidayMail->add_message('PLZ: '.htmlspecialchars($this->piVars['plz2']));
			$holidayMail->add_message('Ort: '.htmlspecialchars($this->piVars['ort2']));
			$holidayMail->add_message('Land: '.htmlspecialchars($this->piVars['land2']));
			$holidayMail->add_message('Telefon: '.htmlspecialchars($this->piVars['vorwahl2']).' - '.htmlspecialchars($this->piVars['rufnummer2']));
			$holidayMail->add_message('Fax: '.htmlspecialchars($this->piVars['faxvorwahl2']).' - '.htmlspecialchars($this->piVars['faxnummer2']));
			$holidayMail->add_message('E-Mail: '.htmlspecialchars($this->piVars['email2']));
			$holidayMail->add_message('');
		}
		$holidayMail->add_message('');
		$holidayMail->add_message('Sollten Sie weitere Fragen zu Ihrer Bestellung haben, steht Ihnen unser Leser-Service jederzeit zur Verf�gung.');
		$holidayMail->add_message('DIE ZEIT, Leser-Service, 20080 Hamburg.');
		$holidayMail->add_message('Fax: 0180 - 52 52 908*');
		$holidayMail->add_message('Tel: 0180 - 52 52 909*');
		$holidayMail->add_message('Email: abo@zeit.de');
		$holidayMail->add_message('');
		$holidayMail->add_message('* 14 Cent/Min. aus dem deutschen Festnetz');
		$holidayMail->add_message('');
		$holidayMail->add_message('Genie�en Sie DIE ZEIT');
		$holidayMail->add_message('Ihr Leserservice');
		$holidayMail->add_message('');
		$holidayMail->add_message('');
				
		if($holidayMail->sendTheMail()) {
			$link = tslib_pibase::pi_getPageLink($this->thanksPage);
		}
		else {
			$link = tslib_pibase::pi_getPageLink($this->errorPage);
		}
		return '<script>self.location.href="/'.$link.'";</script>';
		
	}
	
	
	
	function checkEmail($email) {
	  // RegEx begin
	  $nonascii      = "\x80-\xff"; # Non-ASCII-Chars are not allowed
	
	  $nqtext        = "[^\\\\$nonascii\015\012\"]";
	  $qchar         = "\\\\[^$nonascii]";
	
	  $protocol      = '(?:mailto:)';
	
	  $normuser      = '[a-zA-Z0-9][a-zA-Z0-9_.-]*';
	  $quotedstring  = "\"(?:$nqtext|$qchar)+\"";
	  $user_part     = "(?:$normuser|$quotedstring)";
	
	  $dom_mainpart  = '[a-zA-Z0-9][a-zA-Z0-9._-]*\\.';
	  $dom_subpart   = '(?:[a-zA-Z0-9][a-zA-Z0-9._-]*\\.)*';
	  $dom_tldpart   = '[a-zA-Z]{2,5}';
	  $domain_part   = "$dom_subpart$dom_mainpart$dom_tldpart";
	
	  $regex         = "$protocol?$user_part\@$domain_part";
	  // RegEx end
	
	  return preg_match("/^$regex$/",$email);
	}
	
	
	
	function getAboImage($strImage) {
		$conf = array();
		$conf['file'] = 'GIFBUILDER';
		$conf['file.']['XY'] = '269,[10.h]';
		$conf['file.']['backColor'] = '#d6ebff';
		$conf['file.']['10'] = 'IMAGE';
		$conf['file.']['10.']['file'] = 'uploads/tx_zeitaboservice/'.$strImage;
		$conf['file.']['10.']['file.']['width'] = '269m';
		$conf['file.']['10.']['file.']['height'] = '269m';
		$conf['file.']['10.']['file.']['offset'] = 'offset = 269-[10.w]/2,0';
		
		return $this->cObj->IMAGE($conf);
	}
	
	
	function getAboHeadline($strText) {
		$conf = array();
		$conf['altText'] = $strText;
		$conf['file'] = 'GIFBUILDER';
		$conf['file.']['XY'] = '[10.w]+5,[10.h]+4';
		$conf['file.']['backColor'] = '#d6ebff';
		$conf['file.']['10'] = 'TEXT';
		$conf['file.']['10.']['text'] = $strText;
		$conf['file.']['10.']['offset'] = '0,20';
		$conf['file.']['10.']['fontSize'] = '24';
		$conf['file.']['10.']['fontColor'] = '#000033';
		$conf['file.']['10.']['align'] = 'left';
		$conf['file.']['10.']['niceText'] = '1';
		
		return $this->cObj->IMAGE($conf);
	}
	
	
	
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitaboservice/pi1/class.tx_zeitaboservice_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/zeitaboservice/pi1/class.tx_zeitaboservice_pi1.php']);
}

?>