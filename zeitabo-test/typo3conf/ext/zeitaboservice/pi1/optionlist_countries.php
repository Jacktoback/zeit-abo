<?php

$content = '';

$resResult = $GLOBALS['TYPO3_DB']->exec_SELECTquery('cn_iso_3, cn_short_de', 'static_countries', '', '', 'cn_short_de asc');

while($arrRow = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resResult)) {
	
	$selected = ($arrRow['cn_short_de'] == 'Deutschland') ? 'selected' : '';
	$content .= '<option value="'.$arrRow['cn_short_de'].'" '.$selected.'>'.$arrRow['cn_short_de'].'</option>'.chr(10);

}

?>