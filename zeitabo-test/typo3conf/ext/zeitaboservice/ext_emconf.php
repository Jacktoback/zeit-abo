<?php

########################################################################
# Extension Manager/Repository config file for ext: "zeitaboservice"
#
# Auto generated 29-08-2007 18:58
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'DIE ZEIT Abo-Shop Kundenservice',
	'description' => 'Verschiedene Kundenservices',
	'category' => 'plugin',
	'author' => 'Patrick Bisplinghoff',
	'author_email' => 'patrick.bisplinghoff@clicktivities.net',
	'shy' => '',
	'dependencies' => 'cms',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'clicktivities ag',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'cms' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'suggests' => array(
	),
	'_md5_values_when_last_written' => 'a:39:{s:9:"ChangeLog";s:4:"fb7b";s:10:"README.txt";s:4:"c5c6";s:12:"ext_icon.gif";s:4:"2980";s:17:"ext_localconf.php";s:4:"fe51";s:14:"ext_tables.php";s:4:"6657";s:15:"flexform_ds.xml";s:4:"09a5";s:13:"locallang.xml";s:4:"ca22";s:16:"locallang_db.xml";s:4:"f1ad";s:19:"doc/wizard_form.dat";s:4:"01d1";s:20:"doc/wizard_form.html";s:4:"1bd8";s:14:"pi1/ce_wiz.gif";s:4:"8bb7";s:35:"pi1/class.tx_zeitaboservice_pi1.php";s:4:"1627";s:43:"pi1/class.tx_zeitaboservice_pi1_wizicon.php";s:4:"f732";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"e281";s:28:"pi1/optionlist_countries.php";s:4:"f592";s:17:"pi1/template.html";s:4:"a66b";s:24:"pi1/static/editorcfg.txt";s:4:"8bdf";s:20:"res/b_abschicken.gif";s:4:"2ac7";s:24:"res/h_alte_anschrift.gif";s:4:"8526";s:25:"res/h_alte_anschrift2.gif";s:4:"8dbf";s:19:"res/h_anschrift.gif";s:4:"6436";s:27:"res/h_anschrift_freunde.gif";s:4:"3f9e";s:16:"res/h_beginn.gif";s:4:"4c3c";s:17:"res/h_beginn2.gif";s:4:"9729";s:29:"res/h_die_zeit_zieht_mit2.gif";s:4:"72ce";s:25:"res/h_heimatanschrift.gif";s:4:"fcde";s:25:"res/h_meine_anschrift.gif";s:4:"fe82";s:20:"res/h_mitteilung.gif";s:4:"88f4";s:24:"res/h_neue_anschrift.gif";s:4:"c934";s:25:"res/h_neue_anschrift2.gif";s:4:"6fe3";s:24:"res/h_nicht_erhalten.gif";s:4:"acf3";s:24:"res/h_patenanschrift.gif";s:4:"f2b8";s:20:"res/h_singlezeit.gif";s:4:"14c4";s:16:"res/h_urlaub.gif";s:4:"6104";s:26:"res/h_urlaubsanschrift.gif";s:4:"0549";s:21:"res/h_urlaubszeit.gif";s:4:"5ca1";s:15:"res/service.css";s:4:"6220";s:13:"res/trans.gif";s:4:"93e6";}',
);

?>