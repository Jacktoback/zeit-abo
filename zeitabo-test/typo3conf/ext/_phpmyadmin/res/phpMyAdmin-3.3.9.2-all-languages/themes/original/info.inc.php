<?php
/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 * Theme information
 *
 * @version $Id: info.inc.php 44315 2011-02-27 10:47:46Z mehrwert $
 * @package phpMyAdmin-theme
 * @subpackage Original
 */

/**
 *
 */
$theme_name = 'Original';
$theme_full_version = '2.9';
?>
