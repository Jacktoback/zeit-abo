<?php
/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 *
 * @version $Id: innobase.lib.php 44315 2011-02-27 10:47:46Z mehrwert $
 * @package phpMyAdmin-Engines
 */

/**
 *
 */
include_once './libraries/engines/innodb.lib.php';

/**
 *
 * @package phpMyAdmin-Engines
 */
class PMA_StorageEngine_innobase extends PMA_StorageEngine_innodb {}

?>
