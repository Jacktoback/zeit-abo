<?php
if (!defined ("TYPO3_MODE")) {
	die ("Access denied.");
}

$tempColumns = Array (
	"tx_httpsenforcer_force_secure" => Array (		
		"exclude" => 1,		
		"label" => "LLL:EXT:https_enforcer/Resources/Private/Language/locallang_db.xlf:pages.tx_httpsenforcer_force_secure",		
		"config" => Array (
			"type" => "check",
		)
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns("pages",$tempColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("pages","tx_httpsenforcer_force_secure;;;;1-1-1");
?>