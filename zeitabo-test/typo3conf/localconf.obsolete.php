<?php
include(__DIR__ . '/../fileadmin/templates_new/mobile/user_scripts/user_isSmartphone.php');

$TYPO3_CONF_VARS['SYS']['sitename'] = 'www.zeitabo.de';

	// Default password is "joh316" :
	// this is: v1gauex99whb1f4plhqqvapgdg5lnmxk
$TYPO3_CONF_VARS['BE']['installToolPassword'] = 'e3b36f75d03dc1553579a014b7161164';
$TYPO3_CONF_VARS['BE']['installToolPassword'] = '64363dacb1fa614fb6e2820cc302e093';
$TYPO3_CONF_VARS['EXT']['noEdit'] = '0';

$TYPO3_CONF_VARS['BE']['forceCharset'] = 'utf-8';
$TYPO3_CONF_VARS['BE']['fileCreateMask'] = '0664';
$TYPO3_CONF_VARS['BE']['folderCreateMask'] = '0775';
$TYPO3_CONF_VARS['BE']['maxFileSize'] = '20000';

#$TYPO3_CONF_VARS['FE']['addRootLineFields'] = ',tx_add2tca_colorscheme';

$TYPO3_CONF_VARS['EXT']['extList'] = 'tsconfig_help,context_help,extra_page_cm_options,impexp,sys_note,tstemplate,tstemplate_ceditor,tstemplate_info,tstemplate_objbrowser,tstemplate_analyzer,func_wizards,wizard_crpages,wizard_sortpages,lowlevel,install,belog,beuser,aboutmodules,setup,taskcenter,info_pagetsconfig,viewpage,rtehtmlarea,css_styled_content,t3skin';

$typo_db_extTableDef_script = 'extTables.php';


$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT'] = array(
    'init' => array(
        'enableCHashCache' => 1,
        'enableUrlDecodeCache' => 1,
        'enableUrlEncodeHash' => 1,
		'postVarSet_failureMode'=>'redirect_goodUpperDir',
    ),
    'preVars' => array(
        array(
            'GETvar' => 'no_cache',
            'valueMap' => array(
                'no_cache' => '1',
            ),
          'noMatch' => 'bypass',
        ),
        array(
            'GETvar' => 'L',
            'valueMap' => array(
                'de' => '0',
                'at' => '1',
                'ch' => '2',
                'valueDefault' => 'de',
            ),
            'noMatch' => 'bypass',
        ),
    ),
    'pagePath' => array(
        'type' => 'user',
        'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'expireDays' => 1,
        'rootpage_id' => 3
    ),
    'fileName' => array(
        'defaultToHTMLsuffixOnPrev' => 1,
        'index' => array(
            'index.html' => array(
                'keyValues' => array(
                    'type' => 1,
                ),
            ),
            'print.html' => array(
                'keyValues' => array(
                    'print' => 1,
                ),
            ),
            '_DEFAULT' => array(
                'keyValues' => array(
                ),
            ),
        ),
    ),
);


## INSTALL SCRIPT EDIT POINT TOKEN - all lines after this points may be changed by the install script!

$typo_db_username = 'zeitabo3';  //  Modified or inserted by TYPO3 Install Tool.
$typo_db_password = 'UsichVurdEv7';  //  Modified or inserted by TYPO3 Install Tool.
$typo_db_host = 'localhost';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['SYS']['encryptionKey'] = 'f200a20ebe4515e482ba12cc8ede5c43';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['SYS']['compat_version'] = '4.5';	// Modified or inserted by TYPO3 Install Tool. 
$typo_db = 'zeitabo3';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['TTFdpi'] = '96';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['im_path'] = '/usr/bin/';	// Modified or inserted by TYPO3 Install Tool. 
$TYPO3_CONF_VARS['GFX']['im_combine_filename'] = 'combine';	// Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS["GFX"]["im_version_5"] = 'im5';
// Updated by TYPO3 Install Tool 13-04-07 10:15:59
$TYPO3_CONF_VARS['EXT']['extList'] = 'css_styled_content,tsconfig_help,context_help,extra_page_cm_options,impexp,sys_note,tstemplate,tstemplate_ceditor,tstemplate_info,tstemplate_objbrowser,tstemplate_analyzer,func_wizards,wizard_crpages,wizard_sortpages,lowlevel,install,belog,beuser,aboutmodules,setup,taskcenter,info_pagetsconfig,viewpage,rtehtmlarea,t3skin,static_info_tables,static_info_tables_de,templavoila,add2tca,realurl,yejj_swfobject,about,cshmanual,feedit,opendocs,recycler,t3editor,reports,scheduler,zeitabo,zeitaboservice,formhandler,kickstarter,https_enforcer,sm_httpscm,ipswitch,info,perm,func,filelist,t3quixplorer,zeitaboipchecker,zeitabo_richsnipptes,zeit_leserservice,phpmyadmin,zeitabo_crossselling,seo_basics,rsaauth,saltedpasswords,zeitabotopteaser,cwmobileredirect,mc_googlesitemap';	// Modified or inserted by TYPO3 Extension Manager. Modified or inserted by TYPO3 Core Update Manager. 
$TYPO3_CONF_VARS['EXT']['extConf']['static_info_tables_de'] = 'a:1:{s:5:"dummy";s:1:"1";}';	// Modified or inserted by TYPO3 Extension Manager. 
$TYPO3_CONF_VARS['EXT']['extConf']['templavoila'] = 'a:1:{s:7:"enable.";a:3:{s:13:"oldPageModule";s:1:"1";s:19:"selectDataStructure";s:1:"0";s:15:"renderFCEHeader";s:1:"0";}}';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 26-06-07 12:33:11
$TYPO3_CONF_VARS['GFX']['jpg_quality'] = '100';	// Modified or inserted by TYPO3 Install Tool. 
// Updated by TYPO3 Install Tool 05-06-08 10:30:35
// Updated by TYPO3 Extension Manager 30-01-09 20:46:58
$TYPO3_CONF_VARS['GFX']['im_version_5'] = 'gm';	// Modified or inserted by TYPO3 Install Tool. 
$TYPO3_CONF_VARS['GFX']['gdlib_2'] = '1';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 09-08-10 20:23:08
$TYPO3_CONF_VARS['EXT']['extConf']['static_info_tables'] = 'a:2:{s:7:"charset";s:5:"utf-8";s:12:"usePatch1822";s:1:"0";}';	// Modified or inserted by TYPO3 Extension Manager. 
#$TYPO3_CONF_VARS['EXT']['extConf']['phpmyadmin'] = 'a:4:{s:12:"hideOtherDBs";s:1:"1";s:9:"uploadDir";s:21:"uploads/tx_phpmyadmin";s:10:"allowedIps";s:0:"";s:12:"useDevIpMask";s:1:"0";}';	// Modified or inserted by TYPO3 Extension Manager. 
$TYPO3_CONF_VARS['EXT']['extConf']['realurl'] = 'a:5:{s:10:"configFile";s:26:"typo3conf/realurl_conf.php";s:14:"enableAutoConf";s:1:"1";s:14:"autoConfFormat";s:1:"0";s:12:"enableDevLog";s:1:"0";s:19:"enableChashUrlDebug";s:1:"0";}';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 09-08-10 20:50:23
// Updated by TYPO3 Install Tool 12-12-10 12:42:33
// Updated by TYPO3 Core Update Manager 12-12-10 12:43:20
$TYPO3_CONF_VARS['SYS']['displayErrors'] = '1';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 12-12-10 16:20:18
$TYPO3_CONF_VARS['EXT']['extList_FE'] = 'css_styled_content,install,rtehtmlarea,t3skin,static_info_tables,static_info_tables_de,templavoila,add2tca,realurl,yejj_swfobject,feedit,zeitabo,zeitaboservice,formhandler,kickstarter,https_enforcer,sm_httpscm,ipswitch,t3quixplorer,zeitaboipchecker,zeitabo_richsnipptes,zeit_leserservice,phpmyadmin,zeitabo_crossselling,seo_basics,rsaauth,saltedpasswords,zeitabotopteaser,cwmobileredirect,mc_googlesitemap';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 12-12-10 16:25:46
// Updated by TYPO3 Core Update Manager 12-12-10 16:28:15
// Updated by TYPO3 Extension Manager 14-12-10 10:46:46
$TYPO3_CONF_VARS['SYS']['devIPmask'] = '*';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 20-12-10 18:51:17
// Updated by TYPO3 Extension Manager 28-12-10 11:45:12
$TYPO3_CONF_VARS['BE']['disable_exec_function'] = '0';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['gdlib_png'] = '1';	// Modified or inserted by TYPO3 Install Tool. 
$TYPO3_CONF_VARS['GFX']['im_path_lzw'] = '/usr/bin/';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 29-12-10 16:34:50

// Updated by TYPO3 Extension Manager 18-01-11 22:52:40
$TYPO3_CONF_VARS['GFX']['im_noScaleUp'] = '1';	//  Modified or inserted by TYPO3 Install Tool.
$TYPO3_CONF_VARS['GFX']['png_truecolor'] = '1';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 14-02-11 12:10:38
// Updated by TYPO3 Extension Manager 19-03-11 14:37:33
// Updated by TYPO3 Install Tool 31-03-11 13:41:36
$TYPO3_CONF_VARS['FE']['pageOverlayFields'] = 'uid,title,subtitle,nav_title,keywords,description,abstract,author,author_email,url,urltype,shortcut,shortcut_mode,tx_realurl_pathsegment';
// Updated by TYPO3 Extension Manager 27-10-11 09:58:10
$TYPO3_CONF_VARS['BE']['lockSSL'] = '3';

// Updated by TYPO3 Extension Manager 28-12-11 19:44:10
$TYPO3_CONF_VARS['SYS']['setDBinit'] = 'SET NAMES utf8';	// Modified or inserted by TYPO3 Install Tool. 
$TYPO3_CONF_VARS['BE']['versionNumberInFilename'] = '0';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 21-06-12 17:19:29
// Updated by TYPO3 Extension Manager 21-06-12 17:19:52
$TYPO3_CONF_VARS['INSTALL']['wizardDone']['tx_coreupdates_installsysexts'] = '1';	//  Modified or inserted by TYPO3 Upgrade Wizard.
// Updated by TYPO3 Upgrade Wizard 21-06-12 17:19:52
$TYPO3_CONF_VARS['EXT']['extConf']['t3quixplorer'] = 'a:10:{s:9:"no_access";s:5:"^\\.ht";s:11:"show_hidden";s:1:"1";s:8:"home_url";s:0:"";s:8:"home_dir";s:0:"";s:11:"show_thumbs";s:1:"1";s:15:"textarea_height";s:2:"30";s:14:"auto_highlight";s:1:"1";s:12:"disable_text";s:1:"1";s:12:"editable_ext";s:215:"\\.phpcron$|\\.ts$|\\.tmpl$|\\.txt$|\\.php$|\\.php3$|\\.phtml$|\\.inc$|\\.sql$|\\.pl$|\\.htm$|\\.html$|\\.shtml$|\\.dhtml$|\\.xml$|\\.js$|\\.css$|\\.cgi$|\\.cpp$\\.c$|\\.cc$|\\.cxx$|\\.hpp$|\\.h$|\\.pas$|\\.p$|\\.java$|\\.py$|\\.sh$\\.tcl$|\\.tk$";s:8:"php_path";s:3:"php";}';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 29-05-13 15:33:19
$TYPO3_CONF_VARS['MAIL']['defaultMailFromAddress'] = 'noreply@zeit.de';

$TYPO3_CONF_VARS['MAIL']['defaultMailFromName'] = 'DIE ZEIT';	//  Modified or inserted by TYPO3 Install Tool.
// Updated by TYPO3 Install Tool 24-06-13 12:17:30
$TYPO3_CONF_VARS['EXT']['extConf']['phpmyadmin'] = 'a:5:{s:12:"hideOtherDBs";s:1:"1";s:9:"uploadDir";s:21:"uploads/tx_phpmyadmin";s:10:"allowedIps";s:0:"";s:12:"useDevIpMask";s:1:"0";s:10:"ajaxEnable";s:1:"1";}';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 11-09-13 08:35:45
$TYPO3_CONF_VARS['SYS']['enableDeprecationLog']= '';
$TYPO3_CONF_VARS['EXT']['extConf']['seo_basics'] = 'a:2:{s:10:"xmlSitemap";s:1:"0";s:16:"sourceFormatting";s:1:"1";}';	// Modified or inserted by TYPO3 Extension Manager. 
$TYPO3_CONF_VARS['EXT']['extConf']['rsaauth'] = 'a:1:{s:18:"temporaryDirectory";s:8:"/var/opt";}';	//  Modified or inserted by TYPO3 Extension Manager.
$TYPO3_CONF_VARS['EXT']['extConf']['saltedpasswords'] = 'a:2:{s:3:"FE.";a:2:{s:7:"enabled";s:1:"0";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}s:3:"BE.";a:2:{s:7:"enabled";s:1:"1";s:21:"saltedPWHashingMethod";s:28:"tx_saltedpasswords_salts_md5";}}';	//  Modified or inserted by TYPO3 Extension Manager.
// Updated by TYPO3 Extension Manager 09-12-13 12:48:16
$TYPO3_CONF_VARS['BE']['loginSecurityLevel'] = 'rsa';
$TYPO3_CONF_VARS['EXT']['extConf']['cwmobileredirect'] = 'a:18:{s:12:"standard_url";s:14:"www.zeitabo.de";s:10:"mobile_url";s:14:"www.zeitabo.de";s:12:"maintain_url";s:1:"0";s:10:"use_cookie";s:1:"0";s:11:"cookie_name";s:19:"tx_cwmobileredirect";s:15:"cookie_lifetime";s:4:"3600";s:14:"is_mobile_name";s:8:"isMobile";s:14:"no_mobile_name";s:8:"noMobile";s:14:"use_typoscript";s:1:"1";s:5:"debug";s:1:"0";s:9:"error_log";s:0:"";s:19:"add_value_to_params";s:1:"1";s:17:"detection_enabled";s:1:"1";s:19:"redirection_enabled";s:1:"1";s:11:"http_status";s:15:"HTTP_STATUS_303";s:7:"regexp1";s:319:"(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino";s:7:"regexp2";s:1606:"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-";s:19:"redirect_exceptions";s:9:"typo3conf";}';	// Modified or inserted by TYPO3 Extension Manager. 
// Updated by TYPO3 Extension Manager 02-02-15 16:57:00

/**
 * Add local development settings
 */
if (file_exists(__DIR__ . '/LocalDevelopment.php')) {

    require_once __DIR__ . '/LocalDevelopment.php';

}
?>