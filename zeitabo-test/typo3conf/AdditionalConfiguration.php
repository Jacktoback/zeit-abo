<?php
if (file_exists(__DIR__ . '/../fileadmin/templates_new/mobile/user_scripts/user_isSmartphone.php')) {
    include(__DIR__ . '/../fileadmin/templates_new/mobile/user_scripts/user_isSmartphone.php');
}
$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT'] = array(
'init' => array(
'enableCHashCache' => 1,
'enableUrlDecodeCache' => 1,
'enableUrlEncodeHash' => 1,
'postVarSet_failureMode'=>'redirect_goodUpperDir',
),
'preVars' => array(
array(
'GETvar' => 'no_cache',
'valueMap' => array(
'no_cache' => '1',
),
'noMatch' => 'bypass',
),
array(
'GETvar' => 'L',
'valueMap' => array(
'de' => '0',
'at' => '1',
'ch' => '2',
'valueDefault' => 'de',
),
'noMatch' => 'bypass',
),
),
'pagePath' => array(
'type' => 'user',
'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
'spaceCharacter' => '-',
'languageGetVar' => 'L',
'expireDays' => 1,
'rootpage_id' => 3
),
'fileName' => array(
'defaultToHTMLsuffixOnPrev' => 1,
'index' => array(
'index.html' => array(
'keyValues' => array(
'type' => 1,
),
),
'print.html' => array(
'keyValues' => array(
'print' => 1,
),
),
'_DEFAULT' => array(
'keyValues' => array(
),
),
),
),
);
/**
* Add local development settings
*/
if (file_exists(__DIR__ . '/LocalDevelopment.php')) {
require_once __DIR__ . '/LocalDevelopment.php';
}
?>